jQuery(document).on('submit','#vphone',function(){
   jQuery('.outputmsg').text('Loading...');
   var phone=jQuery('#phone').val(); 
   var otp=jQuery('#otp').val();
   var url=jQuery('#url').val();
    $.ajax({
        url: url+"/wp-admin/admin-ajax.php",
        type: 'POST',
        data: "action=verifyphonenum&phone="+phone+"&otp="+otp,
        success: function(result){
            var obj = JSON.parse(result);
            var success=obj.success;
            var msg=obj.message;
            jQuery('.outputmsg').removeClass('error');
            jQuery('.outputmsg').removeClass('success');
            if(success == 0){
                jQuery('.outputmsg').addClass('error');
                jQuery('.outputmsg').text(msg);
            }
            else{
                
                jQuery('.outputmsg').addClass('success');
                jQuery('.outputmsg').text(msg);
            }
        }
    });
   return false;
});

jQuery(document).on('change','#wp_pkpo_form_select',function(){
   
   checkpickuppoint();
});

function checkpickuppoint(){
     var value=jQuery('#wp_pkpo_form_select').val();
   if(value == "No Pickup"){
      jQuery('.pickup_point_date_time').hide();
      jQuery('.pickup_point_time').hide();
   }else{
       jQuery('.pickup_point_date_time').show();
      jQuery('.pickup_point_time').show();
   }  
}

jQuery(document).ready(function(){
  checkpickuppoint();
  setTimeout(function(){
      checkpickuppoint();
  },1000);
  setTimeout(function(){
      checkpickuppoint();
  },2000);
   setTimeout(function(){
      checkpickuppoint();
  },3000);
  
});

