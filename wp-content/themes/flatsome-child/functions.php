<?php



// Add custom Theme Functions here
add_filter("user_register", "get_unique");
function get_unique($user_id){
    $prefix = "C";
   //update_user_meta($user_id,'testmeta',json_encode($_POST));
   $lat;$lng;
   
  if(get_user_meta($user_id,'latadr',true) == "" || get_user_meta($user_id,'lngadr',true) == ""){
    $address='';
    if(isset($_POST['wcfmvm_static_infos']['address']['addr_1']) &&  $_POST['wcfmvm_static_infos']['address']['addr_1'] != ""){  $address .= $_POST['wcfmvm_static_infos']['address']['addr_1']; }
    if(isset($_POST['wcfmvm_static_infos']['address']['addr_2']) &&  $_POST['wcfmvm_static_infos']['address']['addr_2'] != ""){  $address .= ' ' .$_POST['wcfmvm_static_infos']['address']['addr_2']; }
    if(isset($_POST['wcfmvm_static_infos']['address']['city']) &&  $_POST['wcfmvm_static_infos']['address']['city'] != ""){  $address .= ' ' .$_POST['wcfmvm_static_infos']['address']['city']; }
    if(isset($_POST['wcfmvm_static_infos']['address']['state']) &&  $_POST['wcfmvm_static_infos']['address']['state'] != ""){  $address .= ' ' .$_POST['wcfmvm_static_infos']['address']['state']; }
    if(isset($_POST['wcfmvm_static_infos']['address']['country']) &&  $_POST['wcfmvm_static_infos']['address']['country'] != ""){  $address .= ' ' .$_POST['wcfmvm_static_infos']['address']['country']; }
    if(isset($_POST['wcfmvm_static_infos']['address']['zip']) &&  $_POST['wcfmvm_static_infos']['address']['zip'] != ""){  $address .= ' ' .$_POST['wcfmvm_static_infos']['address']['zip']; }
    
    $prepAddr = str_replace(' ','+',$address);
    $purl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c&libraries=places&address='.$prepAddr.'&sensor=false&language=en';
        $geocode=file_get_contents($purl);
        $output= json_decode($geocode);   
    $latitude = $output->results[0]->geometry->location->lat; 
        $longitude = $output->results[0]->geometry->location->lng;
    //update_user_meta($user_id,'addurl',$purl);
    update_user_meta($user_id,'latadr',$latitude);
    update_user_meta($user_id,'lngadr',$longitude);
  }
  
   
    do {
        $unique = mt_rand();
        $unique = substr($unique, 0, 4);
        $unique = $prefix . $unique .$user_id;
    } while (!check_unique($unique));
  
  
    return update_usermeta( $user_id, 'uniquememberid', $unique );//$unique;
}
function check_unique($unique) {
    global $wpdb;
    $result = $wpdb->get_var("SELECT meta_value from $wpdb->usermeta where meta_key='uniquememberid' AND meta_value = '$unique'");
    if(empty($result))
        return true;
    return false;
}

add_action('manage_users_columns', 'add_custom_users_columns', 10, 1 );
function add_custom_users_columns( $columns ) {
   

    $columns['uniquememberid'] = __('Unique Id');
    

    return $columns;
}

add_filter('manage_users_custom_column',  'add_data_to_custom_users_columns', 10, 3);

function add_data_to_custom_users_columns( $value, $column_name, $user_id ) {
    $value = get_user_meta( $user_id, 'uniquememberid', true );
    return $value;
}

add_action('pre_user_query','yoursite_pre_user_search');
function yoursite_pre_user_search($user_search) {
    global $wpdb;
    if (!isset($_GET['s'])) return;

    //Enter Your Meta Fields To Query
    $search_array = array("uniquememberid", "first_name", "last_name");

    $user_search->query_from .= " INNER JOIN {$wpdb->usermeta} ON {$wpdb->users}.ID={$wpdb->usermeta}.user_id AND (";
    for($i=0;$i<count($search_array);$i++) {
        if ($i > 0) $user_search->query_from .= " OR ";
            $user_search->query_from .= "{$wpdb->usermeta}.meta_key='" . $search_array[$i] . "'";
        }
    $user_search->query_from .= ")";        
    $custom_where = $wpdb->prepare("{$wpdb->usermeta}.meta_value LIKE '%s'", "%" . $_GET['s'] . "%");
    $user_search->query_where = str_replace('WHERE 1=1 AND (', "WHERE 1=1 AND ({$custom_where} OR ",$user_search->query_where);

}

  function send_welcome_email_to_new_user($user_id) {
    $user = get_userdata($user_id);
    $user_email = $user->user_email;
    // for simplicity, lets assume that user has typed their first and last name when they sign up
    $user_full_name = $user->user_firstname . $user->user_lastname;
    

    // Now we are ready to build our welcome email
    $to = $user_email;
    $subject = "Hi " . $user_full_name . ", welcome to our site!";
    $body = '
              <h1>Dear ' . $user_email . ',</h1></br>
              <p>Unique ID: '.print_r($user).'</p>
              <p>Thank you for joining snap company. Your account is now active.</p>
              <p>Please go ahead and navigate around your account.</p>
              <p>Let me know if you have further questions, I am here to help.</p>
              <p>Enjoy the rest of your day!</p>
              <p>Kind Regards,</p>
              <p>poanchen&lt;/p>
    ';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    if (wp_mail($to, $subject, $body, $headers)) {
      error_log("email has been successfully sent to user whose email is " . $user_email);
    }else{
      error_log("email failed to sent to user whose email is " . $user_email);
    }
  }

add_action('init','flush_rules');
function flush_rules(){
  flush_rewrite_rules();
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
      return $miles;
  }
}

function so_20990199_product_query( $q ){ 
  
  $dbuser=array();
  
  $args = array(
    'role'    => 'wcfm_vendor',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
  );
  $users = get_users( $args );
  
  foreach ( $users as $user ) {
    /* print '<pre>';
    print_r($_COOKIE);   */
    $id=$user->ID;
    $lat1=get_user_meta($id,'latadr',true);
    $lng1=get_user_meta($id,'lngadr',true);
    if($lat1 == ""){
      $lat1=get_user_meta($id,'_wcfm_store_lat',true);
    }
    if($lng1 == ""){
      $lng1=get_user_meta($id,'_wcfm_store_lng',true);
    }
    $lat2;$lng2;
    if(isset($_COOKIE['preesah_lat'])){
      $lat2=$_COOKIE['preesah_lat'];
      $lng2=$_COOKIE['preesah_lng'];
    }
    //echo " $lat1 , $lng1, $lat2, $lng2 ";
    if($lat1 != "" && $lng1 != "" && $lat2 != "" && $lng2 != ""){
      $distance= distance($lat1 , $lng1, $lat2, $lng2, "M");
      if($distance <=50){
        array_push($dbuser,$id);
      }
    }
      //echo '<li>' . esc_html( $user->display_name ) . '[' . esc_html( $user->user_email ) . ']</li>';
  }
  /*  print '<pre>';
  print_r($dbuser);  */
  if(isset($_COOKIE['preesah_location'])){
    $q->set( 'author__in', $dbuser ); 
  }
  
  
}


add_action( 'woocommerce_product_query', 'so_20990199_product_query' );
/* 

add_filter( 'woocommerce_product_query_tax_query', 'filter_product_topic', 10, 2 );
function filter_product_topic( $tax_query, $query ) {
  
   if(  ! is_product_category()  ) return $tax_query;

    $taxonomy = 'product_cat';

    
        $tax_query[] = array(
            'taxonomy'       => $taxonomy,
            'field'   => 'slug',
            'terms'     => 234,
            'operator'   => 'IN'
        );
  print '<pre>';
  print_r($query);
  print_r($tax_query);
   

    return $tax_query;
}
 */
 
 function modify_logo() {
    $logo_style = '<style type="text/css">';
    $logo_style .= 'body.login{ background: #aa4040; } .login #backtoblog a, .login #nav a{  color: #ffffff;}';
    $logo_style .= 'body.login h1 a {background-image: url(' . get_option("siteurl") . '/wp-content/uploads/2020/11/Logo.png) !important;width: 300px !important;    height: 70px !important;    background-size: 200px 70px !important;}';
    $logo_style .= '</style>';
    echo $logo_style;
}
add_action('login_head', 'modify_logo');

function custom_login_url() {
    return get_option("siteurl");
}
add_filter('login_headerurl', 'custom_login_url');

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a:hover, #adminmenu li.menu-top>a:focus, #adminmenu li.menu-top:hover, #adminmenu li.opensub>a.menu-top, #adminmenu li>a.menu-top:focus, #adminmenu li a:focus div.wp-menu-image:before, #adminmenu li.opensub div.wp-menu-image:before, #adminmenu .wp-submenu li.current a, #adminmenu .wp-submenu a:hover, #adminmenu li div.wp-menu-image:before, #adminmenu .wp-menu-image img, #collapse-menu span, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, #wpadminbar #wp-admin-bar-user-info .display-name, #wpadminbar .menupop:hover .display-name {
    color: #ffffff !important; }  body{     background: #f5f5f5 !important; }
  .wrap .add-new-h2, .wrap .add-new-h2:active, .wrap .page-title-action, .wrap .page-title-action:active{
        border: 1px solid #aa4040;    color: #aa4040 !important;  }
  .wrap .add-new-h2:hover, .wrap .page-title-action:hover , .wp-core-ui .button-secondary{      border-color: #aa4040 !important;    color: #aa4040 !important;
}
  </style>';
}

# Ajeet code start here
add_action( 'woocommerce_register_form_end', 'become_a_driver_link' );
function become_a_driver_link() {
    if ( get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' ) {
  ?>
    <div class="woocommerce-info">
      <a href="<?php echo site_url('/driver-membership') ?>">Become a Driver</a><br />
    </div>
  <?php
  }
}

# create new role driver
add_role(
    'driver',
    __( 'Driver' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
    )
);

# Create Zipcode custom post type
function my_custom_post_zip_code() {
  $labels = array(
    'name'               => _x( 'Zip Code', 'post type general name' ),
    'singular_name'      => _x( 'zip_code', 'post type singular name' ),
    'add_new_item'       => __( 'Add New Zip Code' ),
    'edit_item'          => __( 'Edit Zip Code' ),
    'new_item'           => __( 'New Zip Code' ),
    'all_items'          => __( 'All Zip Code' ),
    'search_items'       => __( 'Search Zip Code' ),
    'not_found'          => __( 'No zip code found' ),
    'not_found_in_trash' => __( 'No zip code found in the trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Zip Code'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Add zip code for searvice availability',
    'public'  => false,
  'show_ui' => true,
    'menu_position' => 5,
    'supports'      => array( 'title' ),
    'has_archive'   => true,
  );
  register_post_type( 'zip_code', $args ); 
}
add_action( 'init', 'my_custom_post_zip_code' );

# get all zipcode of not diliverbale area
$post_type_query  = new WP_Query(  
    array (  
        'post_type'      => 'zip_code',  
        'posts_per_page' => -1  
    )  
); 
$zipDataAll  = $post_type_query->posts;   
$zipCodeArray = wp_list_pluck( $zipDataAll, 'post_title', 'ID' );
$zipcode = 0;

if(isset($_COOKIE['user_zipcode']))
{
  $zipcode = $_COOKIE['user_zipcode'];
}
else
{
 $zipcode = getUserCurrentZipCode($_COOKIE['pin_lat'],$_COOKIE['pin_lng']);
}


$checkServiceAvailabe = array_search($zipcode,$zipCodeArray,true); 
if($checkServiceAvailabe != '')
{
  # Remove add to cart button if service is not available 
  remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
  # Show message service is not available
  add_action( 'woocommerce_single_product_summary', 'QL_add_text_above_add_to_cart', 20 );
  function QL_add_text_above_add_to_cart() {
    echo '<p style="color:red;">Currently service is not available at this location</p>';
  }
}
# Get current user zipcode 
function getUserCurrentZipCode($lat, $lng)
{
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false&key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c';
    $json = @file_get_contents($url);
    $data= json_decode($json);
    $status = $data->status;
    # Return zipcode
    if($status == "OK"){
      $cookieZip = $data->results[0]->address_components[6]->long_name;
      setcookie('user_zipcode', $cookieZip, time() + (86400 * 30), "/");
      return $data->results[0]->address_components[6]->long_name;
    }
    else
      return 0;
}



/* Add phone field to registration form */

function wooc_extra_register_fields() {?>
       <p class="form-row form-row-wide">
       <label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>
       <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" required />
       </p>      
       <div class="clear"></div>
       <?php
 }
 add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
 
 
 /**

* register fields Validating.

*/

function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {

      if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {

             $validation_errors->add( 'billing_phone_error', 'Phone is required!');

      }
         return $validation_errors;
}


add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );

/**
* Below code save extra fields.
*/
function wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
                 // Phone input filed which is used in WooCommerce
                 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
     }    

}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

// Adding Meta container admin shop_order pages
add_action( 'add_meta_boxes', 'mv_add_meta_boxes' );
if ( ! function_exists( 'mv_add_meta_boxes' ) )
{
    function mv_add_meta_boxes()
    {
        add_meta_box( 'mv_other_fields', __('Delivery Date','woocommerce'), 'mv_add_other_fields_for_packaging', 'shop_order', 'side', 'core' );
    }
}

// Adding Meta field in the meta container admin shop_order pages
if ( ! function_exists( 'mv_add_other_fields_for_packaging' ) )
{
    function mv_add_other_fields_for_packaging()
    {
        global $post;

        $meta_field_data = get_post_meta( $post->ID, 'delivery_date', true ) ? get_post_meta( $post->ID, 'delivery_date', true ) : '';

        echo '<input type="hidden" name="mv_other_meta_field_nonce" value="' . wp_create_nonce() . '">
        <p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
            <input type="date" style="width:250px;";" name="my_field_name" placeholder="' . $meta_field_data . '" value="' . $meta_field_data . '"></p>';

    }
}

// Save the data of the Meta field
add_action( 'save_post', 'mv_save_wc_order_other_fields', 10, 1 );
if ( ! function_exists( 'mv_save_wc_order_other_fields' ) )
{

    function mv_save_wc_order_other_fields( $post_id ) {

        // We need to verify this with the proper authorization (security stuff).

        // Check if our nonce is set.
        if ( ! isset( $_POST[ 'mv_other_meta_field_nonce' ] ) ) {
            return $post_id;
        }
        $nonce = $_REQUEST[ 'mv_other_meta_field_nonce' ];

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce ) ) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        // Check the user's permissions.
        if ( 'page' == $_POST[ 'post_type' ] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
        // --- Its safe for us to save the data ! --- //

        // Sanitize user input  and update the meta field in the database.
        update_post_meta( $post_id, 'delivery_date', $_POST[ 'my_field_name' ] );
    }
}

function custom_registration_redirect() {
    wp_logout();
    return get_the_permalink(18);
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);

add_action( 'user_register', 'myplugin_registration_save', 10, 1 );

function myplugin_registration_save( $user_id ) {

    if ( isset( $_POST['billing_phone'] ) )
    {
        $phone=$_POST['billing_phone'];
        $otp=rand(1000,9999);
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://2factor.in/API/V1/b88ec033-3dc6-11eb-83d4-0200cd936042/SMS/".$phone."/".$otp,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "{}",
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
         update_user_meta($user_id, 'phoneotp', $otp);
         update_user_meta($user_id, 'verified', 0);
        }
        
    }

}



function check_checkbox($user, $password)
{
    if($user->ID)
    {
        if(!in_array( 'administrator', (array) $user->roles)  && !in_array( 'wcfm_vendor', (array) $user->roles) ){
        $id=$user->ID;
        $v=get_user_meta($id,'verified',true);
        if(!in_array( 'driver', (array) $user->roles ) ){
         if($v == 0 ){
          $link="<a href='".get_the_permalink(1586)."' style='margin:0;'>verify</a>";
            $user = new WP_Error( 'denied', __("Please $link your Phone Number First.") );
         }else if(get_user_meta($id,'customer_email_verified',true) != true){
            $user = new WP_Error( 'denied', __("Please verify your Email ID First.") );
         }
        }
        else{
         if(get_user_meta($id,'customer_email_verified',true) != true){
            $user = new WP_Error( 'denied', __("Please verify your Email ID First.") );
         }
        }
       }         
        
    }
    

    return $user;
}
add_filter( 'wp_authenticate_user', 'check_checkbox', 10, 3 );



add_shortcode('verifyphoneform','verifyphoneform');
            
function verifyphoneform(){
 
  $form='<form id="vphone" class="woocommerce-form" method="post">
  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="username">Phone Number<span class="required">*</span></label>
						<input type="tel" id="phone" name="phone" class="woocommerce-Input woocommerce-Input--text input-text" required />					</p>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="username">OTP<span class="required">*</span></label>
						<input type="number" id="otp" name="otp" class="woocommerce-Input woocommerce-Input--text input-text" required />					</p>
  <input type="hidden" id="url" value="'.get_option('siteurl').'" />
   <input type="submit" name="checkphone" id="checkphone" />
   <p class="outputmsg"></p>
  </form>';
   
  return $form;
 
}
     
add_action('wp_ajax_verifyphonenum','verifyphonenum');
add_action('wp_ajax_nopriv_verifyphonenum','verifyphonenum');

function verifyphonenum(){
 
 
  $phone=$_POST['phone'];
  $otp=$_POST['otp'];
  
  $result=array();
  $users = get_users(array(
      'meta_key'     => 'billing_phone',
      'meta_value'   => $phone,
      'meta_compare' => '=',
  ));
  
  
  
  if(count($users) == 0){
    $result['success']=0;
    $result['message']='Phone number does not found!';   
  }else{
    $uid=$users[0]->ID;
    $uotp=get_user_meta($uid,'phoneotp',true);
    $verif=get_user_meta($uid,'verified',true);
    if($verif == 1){     
      $result['success']=1;
      $result['message']='Phone number already verified!';   
    }else if($uotp == $otp){
     update_user_meta($uid,'verified',1);
      $result['success']=1;
      $result['message']='Phone number verified Successfully!';   
    }else{
      $result['success']=0;
      $result['message']='OTP is incorrect!';  
    }
  }
  
  print json_encode($result);
  exit;
              
}

/* Registration form validation */

add_filter( 'woocommerce_registration_errors', 'myplugin_registration_errors', 10, 3 );

function myplugin_registration_errors( $errors, $sanitized_user_login, $user_email ) {
        
      
        
         if ( isset( $_POST['billing_phone'] ) ) {
            $hasPhoneNumber= get_users('meta_value='.$_POST['billing_phone']);
            if ( !empty($hasPhoneNumber)) {
                $errors->add( 'billing_phone_error', __( 'Error: Mobile number is already used!.', 'woocommerce' ) );
            }
         }
         
        return $errors;
    }
    
    
      
function my_filter_plugin_updates( $value ) {
   if( isset( $value->response['woocommerce-ajax-filters/woocommerce-filters.php'] ) ) {        
      unset( $value->response['woocommerce-ajax-filters/woocommerce-filters.php'] );
    }
    unset( $value->response['wc-frontend-manager/wc_frontend_manager.php'] );
    return $value;
 }
 add_filter( 'site_transient_update_plugins', 'my_filter_plugin_updates' );
 
 
add_filter('woocommerce_get_price', 'woocommerce_change_price_by_addition', 10, 2);
 
function woocommerce_change_price_by_addition($price, $product) {
	
	//global post object & post id
     global $post;
     
   if ( is_admin() && ! defined( 'DOING_AJAX' )   ) return $price;
    if ( !is_user_logged_in()  ) return $price;   
	 $post_id = $product->get_id();
  
   $user = wp_get_current_user();
    $uid=$user->ID;
    $membership=get_user_meta($uid,'wcfm_membership',true);
    
    if ( $membership == 462 || $membership == 463 || $membership == 26) {
        
      
        $memprice=get_post_meta($membership,'product_per_price',true);
        $products=get_post_meta($membership,'exclude_products',true);
      
        if($memprice !="" && $memprice != 0){
          if(!in_array($post_id,$products)){
            $oprice=($price * $memprice)/ 100;
            $price=$price-$oprice;
            return $price;
          }          
        }        
    }
   
    return  $price;
}
 add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
  show_admin_bar(false);
}


function my_login_redirect( $redirect_to, $user ) {
    //is there a user to check?
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'driver', $user->roles ) ) {
            $id= $user->ID;
            $approved=get_user_meta($id,'approve_driver',true);
            if($approved != "yes"){
              $redirect_to= get_the_permalink(1770);
            }            
            // redirect them to the default place
            return $redirect_to;
        } else {
            return home_url();
        }
    } else {
        return $redirect_to;
    }
}
 
add_filter( 'woocommerce_login_redirect', 'my_login_redirect', 10, 3 );


add_action( 'profile_update', 'my_profile_update', 10, 2 );
 
function my_profile_update( $user_id, $old_user_data ) {
    
    global $wpdb;
    $uid=$old_user_data->ID;
    $approved=get_transient('approve_driver');
    $newval=$_POST['acf']['field_5ff6ac94c4cc6'];
    if($approved != $newval){
   
     
    }else{
    // echo "$approved != $newval";
    
      
    }
    
}
    
    
 add_action('init','checkproductlimitofcuser');
 
 function checkproductlimitofcuser()
 {
   $uid=get_current_user_id();
   $count_products  = wcfm_get_user_posts_count( $uid, 'product', apply_filters( 'wcfm_limit_check_status', 'any' ), array( 'suppress_filters' => 1 ) );
			$membership=get_user_meta($uid,'wcfm_membership',true);
		 $mem=get_post_meta($membership,'wcfm_product_limit',true);
   
   if($mem <= $count_products && $membership != 463){    
     add_filter( 'wcfm_add_new_product_sub_menu','__return_false');
   }
 }
 
 


/* Add class to body for driver user role */

add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    if( is_user_logged_in() ) {
      $user = wp_get_current_user();
      $roles = ( array ) $user->roles;
      if ( ! empty( $user->roles ) && is_array( $user->roles ) && in_array( 'driver', $user->roles ) ) {
         $classes[] = 'driveruser';       
      }  
    }       
    return $classes;
}
 
/* Show driver fields to wordpress user module */

function fb_add_custom_user_profile_fields( $user ) {
 
      $roles = ( array ) $user->roles;
      if ( ! empty( $user->roles ) && is_array( $user->roles ) && in_array( 'driver', $user->roles ) ) {
       $id=$user->ID;
?>
	<h3><?php _e('Driver Licence Information', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="address"><?php _e('Driver Licence Front', 'your_textdomain'); ?><br>
    <small>*Click image to see full Image</small>
			</label></th>
			<td>
    <?php
    $licencefront=get_user_meta($id,'dl_front',true);
    if($licencefront){
     $upload_dir   = wp_upload_dir();     
     $user_dirname = $upload_dir['basedir'].'/driver_images/'.$licencefront;
      if (  file_exists( $user_dirname ) ) {
       $img=$upload_dir['baseurl'].'/driver_images/'.$licencefront;
       echo "<a href='$img' target='_blank'><img alt='view Image ' src='$img' style='width:300px;' /></a>";
     }
    }else{
      print 'No File Uploaded!';    
    } ?>
			</td>
		</tr>
  <tr>
			<th>
				<label for="address"><?php _e('Driver Licence Back', 'your_textdomain'); ?><br>
    <small>*Click image to see full Image</small>
			</label></th>
			<td>
    <?php
    $licencefront=get_user_meta($id,'dl_back',true);
    if($licencefront){
     $upload_dir   = wp_upload_dir();     
     $user_dirname = $upload_dir['basedir'].'/driver_images/'.$licencefront;
      if (  file_exists( $user_dirname ) ) {
       $img=$upload_dir['baseurl'].'/driver_images/'.$licencefront;
       echo "<a href='$img' target='_blank'><img alt='view Image ' src='$img' style='width:300px;' /></a>";
     }
    }else{
      print 'No File Uploaded!';    
    } ?>
			</td>
		</tr>
  
	</table>
<?php
      }

}

add_action( 'show_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'fb_add_custom_user_profile_fields' );


/* Fire email when T-shirt order has been made */

add_action('woocommerce_thankyou', 'enroll_student', 10, 1);
function enroll_student( $order_id ) {
    if ( ! $order_id )
        return;
    // Allow code execution only once 
    if( ! get_post_meta( $order_id, '_thankyou_action_done', true ) ) {

        // Get an instance of the WC_Order object
        $order = wc_get_order( $order_id );

        // Get the order key
        $order_key = $order->get_order_key();
 
        // Get the order number
        $order_key = $order->get_order_number();
        $order_date = $order->order_date;
        $uid=$order->get_user_id();
        $user_info = get_userdata($uid);
        $user_email = get_user_meta($uid,'_billing_email',true);
        
        $address='';
        if(get_post_meta($order_key,'_shipping_address_1',true) != ""){
          $address = get_post_meta($order_key,'_shipping_address_1',true);
        }
        if(get_post_meta($order_key,'_shipping_address_2',true) != ""){
          $address .= ','.get_post_meta($order_key,'_shipping_address_2',true);
        }
        if(get_post_meta($order_key,'_shipping_city',true) != ""){
          $address .= ','.get_post_meta($order_key,'_shipping_city',true);
        }
        if(get_post_meta($order_key,'_shipping_state',true) != ""){
          $address .= ','.get_post_meta($order_key,'_shipping_state',true);
        }
        if(get_post_meta($order_key,'_shipping_country',true) != ""){
          $address .= ','.get_post_meta($order_key,'_shipping_country',true);
        }       
        $address = str_replace(' ', '%20', $address);
        
         $purl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c&libraries=places&address='.$address.'&sensor=false&language=en';
        $geocode=file_get_contents($purl);
        $output= json_decode($geocode);
      
        $latitude = $output->results[0]->geometry->location->lat; 
        $longitude = $output->results[0]->geometry->location->lng;
        update_post_meta($order_key,'addresslat',$latitude);
        update_post_meta($order_key,'addresslng',$longitude);
      
        // Loop through order items
        foreach ( $order->get_items() as $item_id => $item ) {
          
            $product = $item->get_product();

            $product_id = $product->get_id();
            $subtotal = $item->get_subtotal();            
            
            if($product_id == 1770){
             $uid=$order->get_user_id();
            $user_email = get_post_meta($order_key,'_billing_email',true);     
            $headers = array('Content-Type: text/html; charset=UTF-8');
             $desc='<p>'.date_format(date_create($order_date),'F j,Y').' at '.date_format(date_create($order_date),'g:i A').'</p>
              <h2>Oxford Shirt Order:</h2>
              <p>Company Contracted: Full Source <br>
              Company Website: <a href="http://www.fullsource.com" target="_blank">www.fullsource.com</a><br><br>
              Required Order Details: <br>
              Account Order Number: FS1920155<br>
              Quote Number: FS7196140-PQ<br>
              Cost per embroidered shirt: $'.$subtotal.' </p>              
              <p>If you need an more assistance, Customer Service Department & Hours: <b> Monday- Thursday, 8am-7pm and Friday, 8am-6pm EST at <a href="tel:18009750986">1.800.975.0986</a></b></p>
              <p>Points of contact: <br>
              Dustin Williams <br>
              Sales Representative <br>
              <a href="mailto:snapllc2021@gmail.com">dustin.williams@fullsource.com</a> <br>
              Phone: <a href="tel:9044160715">904-416-0715</a><br>
              Cell: <a href="tel:904403">904-403-2427</a></p>
              <p>Dan H. <br>
              Customer Service Supervisor<br>
              <a href="mailto:snapllc2021@gmail.com">dan.h@fullsource.com</a> <br>
              Phone: <a href="tel:9042908614">904.290.8614</a></p>'; 
             
              wp_mail($user_email,"Your Tshirt Order details",$desc,$headers);             
            }

            // Get the product name
            $product_id = $item->get_name();
        }

        // Output some data
       // echo '<p>Order ID: '. $order_id . ' — Order Status: ' . $order->get_status() . ' — Order is paid: ' . $paid . '</p>';

        // Flag the action as done (to avoid repetitions on reload for example)
        $order->update_meta_data( '_thankyou_action_done', true );
        $order->save();
    }
}

/* Add new order status for delivery */

// Register new status
function register_delivered_shipment_order_status() {
    register_post_status( 'wc-delivered', array(
        'label'                     => 'Delivered',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Delivered (%s)', 'Delivered (%s)' )
    ) );
     register_post_status( 'wc-accepted', array(
        'label'                     => 'Order Accepted',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Order Accepted (%s)', 'Order Accepted (%s)' )
    ) );
     register_post_status( 'wc-rejected', array(
        'label'                     => 'Order Rejected',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Order Rejected (%s)', 'Order Rejected (%s)' )
    ) );
     register_post_status( 'wc-pickup', array(
        'label'                     => 'Pick up',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Picked up (%s)', 'Pick up (%s)' )
    ) );
    
}
add_action( 'init', 'register_delivered_shipment_order_status' );

// Register in wc_order_statuses.
function my_new_wc_order_statuses( $order_statuses ) {
    $order_statuses['wc-delivered'] = _x( 'Order Delivered', 'Order status', 'woocommerce' );
    $order_statuses['wc-accepted'] = _x( 'Order Accepted', 'Order status', 'woocommerce' );
    $order_statuses['wc-rejected'] = _x( 'Order Rejected', 'Order status', 'woocommerce' );
    $order_statuses['wc-pickup'] = _x( 'Order Picked up', 'Order status', 'woocommerce' );
    return $order_statuses;
}

add_filter( 'wc_order_statuses', 'my_new_wc_order_statuses' );

//add_filter( 'woocommerce_cart_item_name', 'shipping_class_in_item_name', 20, 3);
function shipping_class_in_item_name( $item_name, $cart_item, $cart_item_key ) {

global $woocommerce;
	// If the page is NOT the Shopping Cart or the Checkout, then return the product title (otherwise continue...)
	if( ! ( is_cart() || is_checkout() ) ) {
		return $item_name;
	}

	$product = $cart_item['data']; // Get the WC_Product object instance

 $pid=$product->get_id();
 $uid=$product->post->post_author;
 
 $lat=get_user_meta($uid,'latadr',true);
 $lng=get_user_meta($uid,'lngadr',true);
 
 if($lat == ""){ $lat=get_user_meta($uid,'_wcfm_store_lat',true);   }
 if($lng == ""){ $lng=get_user_meta($uid,'_wcfm_store_lng',true);   }
 
 $shippingaddr=WC()->cart->get_customer()->get_shipping_address_1().','.WC()->cart->get_customer()->get_shipping_address_2().','.WC()->cart->get_customer()->get_shipping_city().','.WC()->cart->get_customer()->get_shipping_state().','.WC()->cart->get_customer()->get_shipping_country().','.WC()->cart->get_customer()->get_shipping_postcode();
 
 $prepAddr = str_replace(' ','+',$shippingaddr);
 $purl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c&libraries=places&address='.$prepAddr.'&sensor=false&language=en';
 $geocode=file_get_contents($purl);
 $output= json_decode($geocode);   
 $latitude = $output->results[0]->geometry->location->lat; 
 $longitude = $output->results[0]->geometry->location->lng;
 $distance=0;
 if($lat != "" && $lng != "" && $latitude != "" && $longitude != ""){
 $distance= distance($lat , $lng, $latitude, $longitude, "M");
     
 }
 $rate=round($distance * 1.25);
 
	$label = __( 'Delivery Charge', 'woocommerce' );
 
	// Output the Product Title and the new code which wraps the Shipping Class name
	return $item_name . '<br>
		<p class="item-shipping_class" style="margin:0.25em 0 0; font-size: 0.875em;">
		<em>' .$label . ': </em>$'  . $rate . ' '  . '</p>';

}
 
  /* Add service fee to cart */
 
add_action( 'woocommerce_cart_calculate_fees', 'custom_fee_based_on_cart_total', 10, 1 );
function custom_fee_based_on_cart_total( $cart ) {

    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
    
    // The conditional Calculation
    $store=array();
    $fee = 10;    
   
    
    foreach ( WC()->cart->get_cart() as $cart_item ) {
        $product = $cart_item['data'];
        if(!empty($product)){
           $pid=$product->get_id();
           $uid=$product->post->post_author;
           if(!in_array($uid,$store)){
            array_push($store, $uid);
           
           $lat=get_user_meta($uid,'latadr',true);
           $lng=get_user_meta($uid,'lngadr',true);
           
           if($lat == ""){ $lat=get_user_meta($uid,'_wcfm_store_lat',true);   }
           if($lng == ""){ $lng=get_user_meta($uid,'_wcfm_store_lng',true);   }
           
           $shippingaddr=WC()->cart->get_customer()->get_shipping_address_1().','.WC()->cart->get_customer()->get_shipping_address_2().','.WC()->cart->get_customer()->get_shipping_city().','.WC()->cart->get_customer()->get_shipping_state().','.WC()->cart->get_customer()->get_shipping_country().','.WC()->cart->get_customer()->get_shipping_postcode();
           
           $prepAddr = str_replace(' ','+',$shippingaddr);
           $purl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c&libraries=places&address='.$prepAddr.'&sensor=false&language=en';
           $geocode=file_get_contents($purl);
           $output= json_decode($geocode);   
           $latitude = $output->results[0]->geometry->location->lat; 
           $longitude = $output->results[0]->geometry->location->lng;
           $distance=0;
           if($lat != "" && $lng != "" && $latitude != "" && $longitude != ""){
           $distance= distance($lat , $lng, $latitude, $longitude, "M");
               
           }
           $rate=round($distance * 1.25);
           if($rate > 0){
              $snme=ucfirst(get_user_meta($uid,'store_name',true)); 
             $cart->add_fee( __( "Delivery Fee for $snme Store", "woocommerce" ), $rate, false );
           }
          
          }
        }    
    }
    $fee=$fee * count($store);
     $cart->add_fee( __( "Service Fee", "woocommerce" ), $fee, false );
    
}

function custom_pre_get_posts_query( $q ) {

  
    $q->set( 'post__not_in', array(1770) );

}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' ); 
