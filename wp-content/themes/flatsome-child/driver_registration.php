<?php
/* Template Name: Driver Registration */
get_header();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$uploadPatch = wp_get_upload_dir();
	$email = $_REQUEST['email'];
	$username = $_REQUEST['dri_name'];
	$password = $_REQUEST['password'];
	$user_data = array(
		'ID' => '',
		'user_pass' => $password,
		'user_login' => $username,
		'user_email' => $email,
		'role' => 'driver',
		'user_status' => '0'
	);
	$new_userid = wp_insert_user($user_data);
	$dl_front_filename = "dlf".time();
	$dl_back_filename = "dlb".time();
	$upload_dir = wp_get_upload_dir();
	$filePath = $upload_dir['basedir']."/driver_images/";
	$dl_front_name = rand(10,100).$_FILES["dl_front"]["name"];
	$dl_back_name = rand(10,100).$_FILES["dl_back"]["name"];
	$dl_front_url = $filePath.$dl_front_name;
	$dl_back_url = $filePath.$dl_back_name;
	$address=trim($_REQUEST['billing_address_1']);
	update_user_meta($new_userid,'billing_address_1',$address);
	update_user_meta($new_userid,'billing_state',trim($_REQUEST['billing_city']));
	update_user_meta($new_userid,'billing_city',trim($_REQUEST['billing_state']));
	update_user_meta($new_userid,'billing_postcode',trim($_REQUEST['billing_zipcode']));
	$address = str_replace(' ', '%20', $address);
	 $purl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c&libraries=places&address='.$address.'&sensor=false&language=en';
        $geocode=file_get_contents($purl);
        $output= json_decode($geocode);      
        $latitude = $output->results[0]->geometry->location->lat; 
        $longitude = $output->results[0]->geometry->location->lng;
        update_user_meta($new_userid,'addlat',$latitude);
				 update_user_meta($new_userid,'addlat',$latitude);
       
	
	if(move_uploaded_file($_FILES["dl_front"]["tmp_name"], $dl_front_url ))
	{
		update_user_meta( $new_userid, 'dl_front', $dl_front_name );
	}
	if(move_uploaded_file($_FILES["dl_back"]["tmp_name"], $dl_back_url ))
	{
		update_user_meta( $new_userid, 'dl_back', $dl_back_name );
	}
	if($new_userid){
		print '<br><br><p class="text-center">Success! Registration Successfull. Please verify your email to accesss your account</p>';
	
		$cev_skip_verification_for_selected_roles = get_option('cev_skip_verification_for_selected_roles');
		
		$user_role = get_userdata( $new_userid );
		
		$verified = get_user_meta( $new_userid, 'customer_email_verified', true );
		
		$cev_enable_email_verification = get_option('cev_enable_email_verification',1);		
		
		if ( 'administrator' !== $user_role->roles[0] && $cev_skip_verification_for_selected_roles[$user_role->roles[0]] == 0 && $cev_enable_email_verification == 1 && $verified != 'true') {
			
			$current_user = get_user_by( 'id', $new_userid );
			$user_id                         = $current_user->ID;
			$email_id                        = $current_user->user_email;
			$user_login                      = $current_user->user_login;
			$user_email                      = $current_user->user_email;
				$my_account = woo_customer_email_verification()->my_account;
			WC_customer_email_verification_email_Common()->wuev_user_id  = $current_user->ID;
			WC_customer_email_verification_email_Common()->wuev_myaccount_page_id = $my_account;
			$is_user_created                 = true;		
			$is_secret_code_present                = get_user_meta( $user_id, 'customer_email_verification_code', true );
	
			if ( '' === $is_secret_code_present ) {
				$secret_code = md5( $user_id . time() );
				update_user_meta( $new_userid, 'customer_email_verification_code', $secret_code );
			} 
			
			$cev_email_for_verification = get_option('cev_email_for_verification',0);
			//echo $secret_code;exit;
			if($cev_email_for_verification == 0){
				WC_customer_email_verification_email_Common()->code_mail_sender( $current_user->user_email );
			}
			$is_new_user_email_sent = true;
		}
	
	}
}
?>
<div class="col2-set row row-divided row-large" id="customer_login">
<div class="col-1 large-6 col pb-0">
<div class="account-login-inner">
<h3 class="uppercase">Driver Registration</h3>
<form class="woocommerce-form woocommerce-form-register register" enctype="multipart/form-data" method="post" name="dri_registration">				
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="dri_name">Name&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="dri_name" id="dri_name" >					
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="email">Email&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="email" >					
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="dl_front">Driver License Front&nbsp;<span class="required">*</span></label>
		<input type="file" accept="image/*,application/pdf" class="" name="dl_front" id="dl_front" >	
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="dl_back">Driver License Back&nbsp;<span class="required">*</span></label>
		<input type="file" accept="image/*,application/pdf" class="" name="dl_back" id="dl_back" >		
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="dl_addr">Address&nbsp;<span class="required">*</span></label>
		<input type="text" name="billing_address_1" id="billing_address_1" required="required" placeholder="Street Address*">
		<input type="text" name="billing_city" id="billing_city" required="required" placeholder="City*">
		<select name="billing_state" id="billing_state" required="required" style="margin: 0 0 5px 0;">
				<option value="" selected="selected">Select a State</option>
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="DC">District Of Columbia</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="GU">Guam</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="PR">Puerto Rico</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="VI">Virgin Islands </option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
			</select>
		<input type="text" name="billing_zipcode" id="billing_zipcode" required="required" placeholder="Zip Code*">
	
	
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="password">Password&nbsp;<span class="required">*</span></label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" >
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="password">Confirm  Password&nbsp;<span class="required">*</span></label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="repassword" id="repassword" >
	</p>
	<p class="woocommerce-form-row form-row">
		<input type="submit" class="woocommerce-Button" name="register" value="Register">
	</p>
</form>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script type="text/javascript">
$(function() {
/*$.validator.addMethod('filesize', function (value, element, arg) {
    var minsize=1000000; // min 1kb
    console.log(element+"=="+arg);
    if((value>minsize)&&(value<=arg)){
        return true;
    }else{
        return false;
    }
});*/

jQuery.validator.addMethod('mypassword', function(value, element) {
        return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
    },
    'Password must contain at least one numeric and one alphabetic character.');

jQuery.validator.addMethod("filesize", function(value, element, param) {
    var isOptional = this.optional(element),
        file;
    
    if(isOptional) {
        return isOptional;
    }
    
    if ($(element).attr("type") === "file") {
        
        if (element.files && element.files.length) {
            
            file = element.files[0];            
            return ( file.size && file.size <= param ); 
        }
    }
    return false;
}, "File size is too large.");
var aurl = "<?php echo get_template_directory_uri() ?>-child";

  $("form[name='dri_registration']").validate({

    rules: {
      dri_name: "required",
			billing_address_1: "required",
			billing_city: "required",
			billing_state: "required",
			billing_zipcode: "required",
      email: {
        required: true,
        email: true,
        remote:{
        	url: aurl+"/ajax.php",
        	method:"POST",
        	data: {
        		"action":"check_user",
    		        email: function() {
        			return $( "#email" ).val();
     			 }

        	},
        }
      },
      dl_front: {
        required: true,
        accept: "image/*,application/pdf",
        filesize: 1000000, // 1 MB
      },
      dl_back: {
        required: true,
        accept: "image/*,application/pdf",
        filesize: 1000000, // 1 MB
      },
      password: {
        required: true,
        minlength: 8,
				mypassword: true
      },
			repassword : {
				 required: true,
                    minlength : 8,
                    equalTo : "#password"
                }
    },
    // Specify validation error messages
    messages: {
      name: "Please enter your name",
			billing_address_1: "Please enter your Complete Address",
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 8 characters long"
      },
       email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email already in use!"
        },
      
      dl_front:{
	        filesize:" file size must be less than 1 MB.",
	        accept:"Please upload images or pdf file.",
	        required:"Please selct DL Front."
	    },
	    dl_back:{
	        filesize:" file size must be less than 1 MB.",
	        accept:"Please upload images or pdf file.",
	        required:"Please selct DL Back."
	    }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>
<?php get_footer(); ?>