<div class="row category-page-row">
		<?php
			$category = get_queried_object();
			$slug=$category->slug;
		?>

		<div class="col col-12">
		<div class="products row row-small large-columns-3 medium-columns-2 small-columns-2 has-equal-box-heights">
		
		
		<?php
		/**
		 * Hook: woocommerce_before_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20 (FL removed)
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );

		?>

		<?php
		/**
		 * Hook: woocommerce_archive_description.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		do_action( 'woocommerce_archive_description' );
		
		$dbuser=array();
		$args = array(
			'role'    => 'wcfm_vendor',
			'orderby' => 'user_nicename',
			'order'   => 'ASC'
		);
		$users = get_users( $args );
		
		
		foreach ( $users as $user ) {
			
			  
		$id=$user->ID;
			$lat1=get_user_meta($id,'latadr',true);
			$lng1=get_user_meta($id,'lngadr',true);
			if($lat1 == ""){
				$lat1=get_user_meta($id,'_wcfm_store_lat',true);
			}
			if($lng1 == ""){
				$lng1=get_user_meta($id,'_wcfm_store_lng',true);
			}
			 $lat2;$lng2;
		
			if($_COOKIE['preesah_lat']){
				$lat2=$_COOKIE['preesah_lat'];
				$lng2=$_COOKIE['preesah_lng'];
			}
			else if($_COOKIE['pin_lat']){
				$lat2=$_COOKIE['pin_lat'];
				$lng2=$_COOKIE['pin_lng'];
			}
			//echo " $lat1 , $lng1, $lat2, $lng2 ";
			if($lat1 != "" && $lng1 != "" && $lat2 != "" && $lng2 != ""){
				$distance= distance($lat1 , $lng1, $lat2, $lng2, "K");
				if($distance <=50){
					array_push($dbuser,$id);
					$author = get_user_by('id', $id);
					//print_r($author);
					$displayname=$author->data->user_nicename;
					$store=get_user_meta($id,'store_name',true);
					$imgid=get_user_meta($id,'sn_user_avatar',true);
					$imgurl='';
					if($imgid != ""){
						$imgurl=wp_get_attachment_thumb_url($imgid);
					}else{
						$imgurl = get_avatar_url($id, array('size' => 450));
					}
					
					//print_r($author);
					?>
					
					<div class="product-small col has-hover product type-product post-190 status-publish first instock product_cat-alcohol-and-beverage-services has-post-thumbnail shipping-taxable purchasable product-type-simple"  style="width:33%">
						<div class="col-inner">
						<?php //do_action( 'woocommerce_before_shop_loop_item' ); ?>
						<div class="product-small box <?php echo flatsome_product_box_class(); ?>">
							<div class="box-image">
								<div class="<?php echo flatsome_product_box_image_class(); ?>">
									<a href="<?php echo "/store/".$displayname."/category/".$slug; ?>">
									    <?php  echo '<img src="'.$imgurl.'" alt="'.$displayname.'" />'; ?></a>
								</div>
							</div>

							<div class="box-text <?php echo flatsome_product_box_text_class(); ?>">
								<?php
									do_action( 'woocommerce_before_shop_loop_item_title' );
									
									echo '<div class="title-wrapper"><p class="name product-title woocommerce-loop-product__title"><a href="/store/'.$store.'/category/'.$slug.'">'.ucfirst($displayname).'</a></p>';	
									echo '</div>';


								?>
							</div>
						</div>
						<?php //do_action( 'woocommerce_after_shop_loop_item' ); ?>
						</div>
					</div>
					<?php
					
				}
			}
				
		}

		if(count($dbuser) == 0) {
			
			echo '<div class="shop-container">
					<p class="woocommerce-info">No Store was found matching your location.</p>
				</div>';
		}
		?>

		<?php
			/**
			 * Hook: flatsome_products_after.
			 *
			 * @hooked flatsome_products_footer_content - 10
			 */
			do_action( 'flatsome_products_after' );
			/**
			 * Hook: woocommerce_after_main_content.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
		?>
		</div>
</div>

