<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_display_name"><?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo esc_attr( $user->display_name ); ?>" /> <span><em><?php esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'woocommerce' ); ?></em></span>
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</p>

	<fieldset>
		<legend><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
	
	
	
	<?php
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
    if ( ! empty( $user->roles ) && is_array( $user->roles ) && in_array( 'driver', $user->roles ) ) {
		
       $id=$user->ID;
		?>
			<h4><?php _e('Driver Licence Information', 'your_textdomain'); ?></h4>
			
			<table class="form-table">
				<tr>
					<th>
						<label for="address"><?php _e('Driver Licence Front', 'your_textdomain'); ?><br>
			<small>*Click image to see full Image</small>
					</label></th>
					<td>
			<?php
			$licencefront=get_user_meta($id,'dl_front',true);
			if($licencefront){
			 $upload_dir   = wp_upload_dir();     
			 $user_dirname = $upload_dir['basedir'].'/driver_images/'.$licencefront;
			  if (  file_exists( $user_dirname ) ) {
			   $img=$upload_dir['baseurl'].'/driver_images/'.$licencefront;
			   echo "<a href='$img' target='_blank'><img alt='view Image ' src='$img' style='width:300px;' /></a>";
			 }
			}else{
			  print 'No File Uploaded!';    
			} ?>
					</td>
				</tr>
		  <tr>
					<th>
						<label for="address"><?php _e('Driver Licence Back', 'your_textdomain'); ?><br>
			<small>*Click image to see full Image</small>
					</label></th>
					<td>
			<?php
			$licencefront=get_user_meta($id,'dl_back',true);
			if($licencefront){
			 $upload_dir   = wp_upload_dir();     
			 $user_dirname = $upload_dir['basedir'].'/driver_images/'.$licencefront;
			  if (  file_exists( $user_dirname ) ) {
			   $img=$upload_dir['baseurl'].'/driver_images/'.$licencefront;
			   echo "<a href='$img' target='_blank'><img alt='view Image ' src='$img' style='width:300px;' /></a>";
			 }
			}else{
			  print 'No File Uploaded!';    
			} ?>
					</td>
				</tr>
		  
			</table>
		<?php
      }	
	?>	
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
