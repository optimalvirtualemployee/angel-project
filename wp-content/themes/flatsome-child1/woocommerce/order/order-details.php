<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

	<h2 class="woocommerce-order-details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="woocommerce-table__product-table product-total"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );

			foreach ( $order_items as $item_id => $item ) {
				$product = $item->get_product();

				wc_get_template(
					'order/order-details-item.php',
					array(
						'order'              => $order,
						'item_id'            => $item_id,
						'item'               => $item,
						'show_purchase_note' => $show_purchase_note,
						'purchase_note'      => $product ? $product->get_purchase_note() : '',
						'product'            => $product,
					)
				);
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
        

    function get_order_status_history($order_id){
    
       global $wpdb;
    
        $table_perfixed = $wpdb->prefix . 'comments';
        $results = $wpdb->get_results("
            SELECT *
            FROM $table_perfixed
            WHERE  `comment_post_ID` = $order_id
            AND  `comment_type` LIKE  'order_note'
        ");
    
        foreach($results as $note){
            $order_note[]  = array(
                'note_id'      => $note->comment_ID,
                'note_date'    => $note->comment_date,
                'note_author'  => $note->comment_author,
                'note_content' => $note->comment_content,
            );
        }
        return $order_note;
    }    
    $order_notes = get_order_status_history( $order_id );
       
			?>
		</tbody>

		<tfoot>
			<?php
			foreach ( $order->get_order_item_totals() as $key => $total ) {
				?>
					<tr>
						<th scope="row"><?php echo esc_html( $total['label'] ); ?></th>
						<td><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>
					</tr>
					<?php
			}
			?>
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>
	</table>

	<?php
    
      do_action( 'woocommerce_order_details_after_order_table', $order );
      $i=1;
     foreach($order_notes as $note){
            
            $date=$note['note_date'];
            $note_id = $note['note_id'];
            $note_date = $note['note_date'];
            $note_author = $note['note_author'];
            $note_content = $note['note_content'];
        
            if($i == 1){
                echo '<h3> Tracking Details </h3>';
            }
												$driver=get_post_meta($order_id,'driveruser',true);
												if($driver != ""){
												?>
	<h5>Driver's current location</h5>	
 <div id="map-layer" style="margin: 20px 0px;max-width: 100%;min-height:400px;"></div>
	<?php } else{
			echo '';
	}
        
            // Outputting each note content for the order
			//echo '<p>'.$i.'. '.$note_content.'  '.date_format(date_create($date),'d M, Y h:ia').'</p>';
			echo '<div class="d-flex mb-20"> <div class="datetime-c p-10">'.date_format(date_create($date),'d M, Y h:ia').'</div><div class="comments p-10">'.$i.'. '.$note_content.'</div></div>';
            $i++;
        }
        
        $ddate= get_post_meta( $order_id, 'delivery_date', true );
        if($ddate != ""){   
		 echo '<div class="d-flex mb-20"> <div class="datetime-c p-10">'.date_format(date_create($ddate),'d M, Y').'</div><div class="comments p-10"> Expected Delivery Date</div></div>';	        
         //echo '<p>'.$i.'. Expected Delivery Date is '.date_format(date_create($ddate),'d M, Y').'</p>';
        }		
		
    
    ?>
</section>

<script>
      
    var map;
		var waypoints;
    	 
    
      // (A) PROPERTIES
     
     var delay = 10000; // Delay between location refresh
	 setTimeout(function(){
		 initMap();
	 },200);
    
      // (B) INIT
     	function initMap() {
        var mapLayer = document.getElementById("map-layer"); 
        var centerCoordinates = new google.maps.LatLng(37.6, -95.665);
        var defaultOptions = { center: centerCoordinates, zoom: 4 }
        map = new google.maps.Map(mapLayer, defaultOptions);
        show();
      }

						 setInterval(show(), 1000);
      // (C) GET DATA FROM SERVER AND UPDATE MAP
      function show() {
        // (C1) DATA
        var data = new FormData();
        data.append('orderid', <?php print $order_id; ?>);
		data.append('action', 'getdriverlocations');
        
        
	
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            directionsDisplay.setMap(map);

        // (C2) AJAX
        var xhr = new XMLHttpRequest();
        xhr.open('POST',  "<?php print  get_option('siteurl'); ?>/wp-admin/admin-ajax.php");
        xhr.onload = function () {
          //track.map.innerHTML = "<div>LOADED "+Date.now()+"</div>";
          var res = JSON.parse(this.response);
		
		  
          if (res.status==1) {
            waypoints = Array();
            for (let rider of res.latlng) {              
                	   console.log(rider[0]+', '+rider[1]);
                    waypoints.push({
                        location: rider[0]+', '+rider[1],
                        stopover: false  
                    });
                   // console.log(waypoints);
                
                var locationCount = waypoints.length;
                if(locationCount > 0) {
                    var start = waypoints[0].location;
                    var end = waypoints[locationCount-1].location;
                    //console.log(start+'...'+end);
                    drawPath(directionsService, directionsDisplay,start,end);
                }
            }
          } 
        };
        xhr.send(data);
      
      }
    
    
    function drawPath(directionsService, directionsDisplay,start,end) {
            directionsService.route({
              origin: start,
              destination: end,
              waypoints: waypoints,
              optimizeWaypoints: true,
              travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                directionsDisplay.setDirections(response);
                } else {
                window.alert('Problem in showing direction due to ' + status);
                }
            });
      }
    
    </script>

 
<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}