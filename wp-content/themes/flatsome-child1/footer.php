<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main>

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer>

</div>


<?php wp_footer(); 



?>

<?php
$user = wp_get_current_user();
if ( in_array( 'driver', (array) $user->roles ) ) {
  
	?>
	<style>
		.dashboard-links .woocommerce-MyAccount-navigation-link--payment-methods,.dashboard-links .wishlist-account-element ,.dashboard-links .woocommerce-MyAccount-navigation-link--downloads,.dashboard-links .woocommerce-MyAccount-navigation-link--inquiry, #my-account-nav .woocommerce-MyAccount-navigation-link--downloads, #my-account-nav .woocommerce-MyAccount-navigation-link--inquiry,#my-account-nav .woocommerce-MyAccount-navigation-link--payment-methods,#my-account-nav .wishlist-account-element{
			display: none !important;
		}
	</style>
	<?php
}
	

?>

<script>

function GoogleGeocode() {
  geocoder = new google.maps.Geocoder();
  this.geocode = function(address, callbackFunction) {
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var result = {};
          result.latitude = results[0].geometry.location.lat();
          result.longitude = results[0].geometry.location.lng();
          callbackFunction(result);
        } else {
          alert("Geocode was not successful for the following reason: " + status);
          callbackFunction(null);
        }
      });
  };
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

jQuery(document).ready(function(){
	
	var address=  getCookie("preesah_location");
	if(address){
		jQuery('input#placeapi').val(address);
	}
	else{
	     var d = new Date();
			  d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
			  var expires = "expires="+d.toUTCString();
			  //document.cookie =  "preesah_location=Delhi, India;" + expires + ";path=/";
			  //document.cookie =  "preesah_lat=28.7040592;" + expires + ";path=/";
			  //document.cookie = "preesah_lng=77.10249019999999;" + expires + ";path=/";
			//  jQuery('input#placeapi').val('Delhi, India');
	    
	}
});

jQuery(document).on('change','input#placeapi',function(){
	setTimeout(function()
	{
		var location=jQuery('input#placeapi').val();
		
		var g = new GoogleGeocode();
		var address = location;
		g.geocode(address, function(data) {
			if(data != null) {
			  olat = data.latitude;
			  olng = data.longitude;
			  var d = new Date();
			  d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
			  var expires = "expires="+d.toUTCString();
			  document.cookie =  "preesah_location=" + address + ";" + expires + ";path=/";
			  document.cookie =  "preesah_lat=" + olat + ";" + expires + ";path=/";
			  document.cookie = "preesah_lng=" + olng + ";" + expires + ";path=/";
				window.location.reload();
			  //console.log("<p><strong>"+location+" -> </strong> Latitude: " + olat + " , " + "Longitude: " + olng + "</p>");
			} else {
			  //Unable to geocode
			  //alert('ERROR! Unable to geocode address. Please re-check added address');
			}
		});
	},1000);
	
});
</script>
<?php //if(!current_user_can('administrator') ) { ?>
<p id="demo"></p>
<script>
	var geocoder;

  geocoder = new google.maps.Geocoder();
var pin_lat_available = document.cookie.match(new RegExp('(^| )' + 'pin_lat' + '=([^;]+)'));
if(!pin_lat_available)
{}
    var x = document.getElementById("demo");
    getLocation();
    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      }
    }
    
    function showPosition(position) {
       var d = new Date();
      d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
      var expires = "expires="+d.toUTCString();
			
      document.cookie =  "pin_lat=" + position.coords.latitude + ";" + expires + ";path=/";
      document.cookie = "pin_lng=" + position.coords.longitude + ";" + expires + ";path=/";
			
			var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      //console.log(results)
        if (results[1]) {
						//formatted address
					 country='';
					 jk=0;
					 //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
								for (var b=0;b<results[0].address_components[i].types.length;b++) {
										console.log(results[0].address_components[i].types[b]);
								//there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
										if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
												city= results[0].address_components[i];
											jk=1;
										}
										if (results[0].address_components[i].types[b] == "country") {
												country= results[0].address_components[i];
											 jk=2;
										}
										if(jk == 2){ break; }
								}
						}
					 //city data
					 var add=city.long_name+ ", "+country.long_name;
					 jQuery('input#placeapi').val(add);
						
					}
        }
      });
			
      }
  
  
			
    

</script>
<?php // } ?>


<script type='text/javascript' src='<?php bloginfo('stylesheet_directory'); ?>/js/verifyphone.js' ></script>

</body>
</html>