��    N      �  k   �      �  +   �  8   �            	   (     2  
   >     I     ^     o  =   �     �  
   �  	   �     �     �  1   �          $     8     Q     V  E   u  W   �  =   	  
   Q	  
   \	  
   g	     r	     �	     �	     �	     �	     �	     �	  
   �	  	   �	  
   �	     
  %   
     7
     <
     R
  a   Y
     �
     �
     �
     �
     �
     �
  F        Z     ^     u     �     �     �  \   �  q     &   �  0   �  $   �        #         D  =   `     �     �     �     �  U   �  .   *     Y     k     �     �     �    �  6   �  5        >     E     Y     b     n     �     �     �  =   �     �     �                 5   *     `     m  )   �     �     �  B   �  e   �  A   b     �  
   �     �     �  
   �     �     �     �     �                     &     5     N     l     r     �  O   �     �     �     �     	          '  \   8  
   �     �     �  	   �     �     �  n     �   r       	   &     0     6     =  	   K     U  %   X     ~     �     �  G   �      �               6     ;     H     /                 @   G      N   I   L      )               <             -   A                       >          H   =       9                  7          J   2          &   ;   ,               5   F      8   :       4          ?               M   !      	      "   #          %   3   +   K   '   0       .   
      D   B   1              *   C                (   6   $       E                    Add widgets here to appear in your sidebar. Appears at the bottom of the content on posts and pages. Author: Base Color Scheme Blue Gray Bright Blue Bright Red Comments are closed. Content Bottom 1 Content Bottom 2 Continue reading<span class="screen-reader-text"> "%s"</span> Dark Dark Brown Dark Gray Dark Red Default Edit<span class="screen-reader-text"> "%s"</span> Featured Footer Primary Menu Footer Social Links Menu Gray Inconsolata font: on or offon It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment<span class="screen-reader-text"> on %s</span> Light Blue Light Gray Link Color Main Text Color Medium Brown Medium Gray Menu Merriweather font: on or offon Montserrat font: on or offon Next Next Image Next page Next post: Nothing Found Oops! That page can&rsquo;t be found. Page Page Background Color Pages: Parent post link<span class="meta-nav">Published in</span><span class="post-title">%title</span> Previous Previous Image Previous page Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Red Search Results for: %s Secondary Text Color Sidebar Skip to content Social Links Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Twenty Sixteen requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again. Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  View all posts by %s White Yellow collapse child menu comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; expand child menu https://wordpress.org/ labelSearch for: placeholderSearch &hellip; submit buttonSearch PO-Revision-Date: 2019-03-26 12:02:34+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: tr
Project-Id-Version: Themes - Twenty Sixteen
 Yan sütunda gözükecek bileşenleri buradan ekleyin. Yazı ve sayfalarda içeriğin en altında görünür Yazar: Taban renk şeması Mavi gri Parlak mavi Parlak kırmızı Yorumlar kapatıldı. Alt içerik 1 Alt içerik 2 Okumaya devam et<span class="screen-reader-text"> "%s"</span> Koyu Koyu kahverengi Koyu gri Koyu kırmızı Varsayılan Düzenle<span class="screen-reader-text"> "%s"</span> Öne Çıkan Alt kısım birincil menü Alt kısım sosyal bağlantılar menüsü Gri on Burada bir şey bulamadık. Bir de aramayı denemek ister misiniz? Görünen o ki bakındığınız her ne ise bulamıyoruz. Belki arama özelliği yardımcı olabilir. <span class="screen-reader-text">%s için</span> bir yorum yapın Açık mavi Açık gri Bağlantı rengi Ana yazı rengi Kahverengi Orta gri Menü açık on Sonraki Sonraki görsel Sonraki sayfa Sonraki yazı: Hiçbir şey bulunamadı Amanın! O sayfa bulunamadı. Sayfa Sayfa arka planı rengi Sayfalar <span class="meta-nav">Yayın yeri</span><span class="post-title">%title</span> Önceki Önceki görsel Önceki sayfa Önceki yazı: Birincil menü %s gururla sunar İlk yazınızı yayımlamak için hazır mısınız? <a href="%1$s">Buradan başlayın</a>. Kırmızı Arama sonuçları: %s İkincil yazı rengi Yan Menü İçeriğe geç Sosyal bağlantılar menüsü Üzgünüz, arama terimlerinizle hiçbir şey eşleşmedi. Lütfen farklı anahtar kelimelerle tekrar deneyin. Yirmi Onaltı'yı kullanabilmeniz için en az WordPress 4.4 sürümüne sahip olmalısınız. %s sürümünü kullanıyorsunuz. Lütfen güncelleyin ve tekrar deneyin. Kategoriler Tam boyut Yazar Biçim Yayın tarihi Etiketler ,  %s tarafından yazılan tüm yazılar Beyaz Sarı Alt menüyü toparla &ldquo;%2$s&rdquo; için %1$s yorum &ldquo;%2$s&rdquo; için %1$s yorum &ldquo;%s&rdquo; için bir yorum Alt menüyü genişlet https://wordpress.org/ Ara: Ara &hellip; Ara 