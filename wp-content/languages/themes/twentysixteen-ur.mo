��    N      �  k   �      �  +   �  8   �            	   (     2  
   >     I     ^     o  =   �     �  
   �  	   �     �     �  1   �          $     8     Q     V  E   u  W   �  =   	  
   Q	  
   \	  
   g	     r	     �	     �	     �	     �	     �	     �	  
   �	  	   �	  
   �	     
  %   
     7
     <
     R
  a   Y
     �
     �
     �
     �
     �
     �
  F        Z     ^     u     �     �     �  \   �  q     &   �  0   �  $   �        #         D  =   `     �     �     �     �  U   �  .   *     Y     k     �     �     �    �  P   �  M   &  	   t     ~     �     �     �     �     �     
  L   *     w     �     �     �  
   �  H   �            #   4     X     _  x   h  �   �  D   s     �     �     �     �          +     A     J     N     R     Y     m          �  5   �     �     �       X     
   g     r     �     �     �  $   �     �     l  '   s     �     �  %   �     �  �     �   �     �     �     �     �     �     �     �  *   �     $     -  !   =  O   _  '   �     �     �                ;     /                 @   G      N   I   L      )               <             -   A                       >          H   =       9                  7          J   2          &   ;   ,               5   F      8   :       4          ?               M   !      	      "   #          %   3   +   K   '   0       .   
      D   B   1              *   C                (   6   $       E                    Add widgets here to appear in your sidebar. Appears at the bottom of the content on posts and pages. Author: Base Color Scheme Blue Gray Bright Blue Bright Red Comments are closed. Content Bottom 1 Content Bottom 2 Continue reading<span class="screen-reader-text"> "%s"</span> Dark Dark Brown Dark Gray Dark Red Default Edit<span class="screen-reader-text"> "%s"</span> Featured Footer Primary Menu Footer Social Links Menu Gray Inconsolata font: on or offon It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment<span class="screen-reader-text"> on %s</span> Light Blue Light Gray Link Color Main Text Color Medium Brown Medium Gray Menu Merriweather font: on or offon Montserrat font: on or offon Next Next Image Next page Next post: Nothing Found Oops! That page can&rsquo;t be found. Page Page Background Color Pages: Parent post link<span class="meta-nav">Published in</span><span class="post-title">%title</span> Previous Previous Image Previous page Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Red Search Results for: %s Secondary Text Color Sidebar Skip to content Social Links Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Twenty Sixteen requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again. Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  View all posts by %s White Yellow collapse child menu comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; expand child menu https://wordpress.org/ labelSearch for: placeholderSearch &hellip; submit buttonSearch PO-Revision-Date: 2019-03-25 11:49:24+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: ur_PK
Project-Id-Version: Themes - Twenty Sixteen
 ویجٹ سائیڈ بار میں دکھانے کے لیے یہاں ڈالیں۔ پوسٹیں اور صفحات کے متن کے نیچے نظر آتا ہے۔ مصنف: بنیادی کلر اسکیم نیلا گرے روشن نیلا روشن سرخ تبصرے بند ہیں۔ مواد کا نچلا حصہ 1 مواد کا نچلا حصہ 2 <span class="screen-reader-text">"%s "</span> پڑھنا جاری رکھیں گہرا گہرا براون گہرا گرے گہرا سرخ طےشدہ <span class="screen-reader-text">"%s "</span> میں ترمیم کریں نمایاں فوٹر بنیادی مینو فوٹر شوشل لنکس مینو گرے جاری ایسا لگتا ہے کہ اس مقام پر کچھ نہیں ملا۔ شاید تلاش آپ کی مدد کر سکے؟ ایسا لگتا ہے کہ ہم وہ تلاش نہیں کر سکے جسے آپ ڈھونڈ رہے ہیں۔ شاید تلاش مدد کر سکے۔ <span class="screen-reader-text"> %s</span> پر تبصرہ کریں ہلکا نیلا ہلکا گرے روابط کا رنگ بنیادی متن کا رنگ درمیانہ براؤن درمیانہ گرے مینو off off آگے اگلی تصویر اگلا صفحہ اگلی پوسٹ: کچھ نہیں ملا اوہ، مطلوبہ صفحہ نہیں مل سکا۔ صفحہ پس منظر کا رنگ صفحات: <span class="meta-nav">شائع کیا از</span><span class="post-title">%title</span> پیچھے پچھلی تصویر پچھلا صفحہ سابقہ پوسٹ: بنیادی مینو فخریہ تقویت شدہ از %s آپ اپنی پہلی پوسٹ شائع کرنے لیے تیار ہیں؟ <a href="%1$s">یہاں سے شروع کریں</a>۔ سرخ تلاش کے نتائج برائے: %s ثانوی متن کا رنگ سائیڈ بار چھوڑیں مواد پر جائیں شوشل لنکس مینو معاف کیجیے گا، لیکن آپ کی تلاش کردہ اصطلاح سے کچھ نہیں ملا سکا۔ براہ مہربانی کسی اور کی ورڈز سے دوبارہ تلاش کرنے کی کوشش کریں۔ Twenty Sixteen کم از کم WordPress 4.4 پر کام کرتی ہے۔ آپ ورژن %s استعمال کر رہے ہیں۔ اپگریڈ کرنے کے بعد کوشش کریں۔ زمرہ جات بڑا سائز مصنف فارمیٹ درج کیا گیا ٹیگز ، %s کی تمام پوسٹیں دیکھیں سفید پیلے رنگ ذیلی مینو بند کریں &ldquo;%2$s&rdquo; پر %1$s تبصرہ &ldquo;%2$s&rdquo; پر %1$s تبصرے &ldquo;%s&rdquo; پر ایک تبصرہ ذیلی مینو کھولیں https://wordpress.org/ تلاش کریں: تلاش کریں &hellip; تلاش 