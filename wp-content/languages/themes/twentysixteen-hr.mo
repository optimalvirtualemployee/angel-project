��    N      �  k   �      �  +   �  8   �            	   (     2  
   >     I     ^     o  =   �     �  
   �  	   �     �     �  1   �          $     8     Q     V  E   u  W   �  =   	  
   Q	  
   \	  
   g	     r	     �	     �	     �	     �	     �	     �	  
   �	  	   �	  
   �	     
  %   
     7
     <
     R
  a   Y
     �
     �
     �
     �
     �
     �
  F        Z     ^     u     �     �     �  \   �  q     &   �  0   �  $   �        #         D  =   `     �     �     �     �  U   �  .   *     Y     k     �     �     �  s  �  ;   9  1   u     �     �  
   �     �     �     �     �       <   !     ^     d  
   q     |     �  2   �  	   �     �  (   �            L     [   k  ?   �               "     1     D     T     b     k     n  	   r     |     �     �     �  !   �     �     �  	     P     	   `     j     z     �     �     �  O   �               6     M     Z  (   p  Z   �  �   �  
   x     �     �     �     �     �     �     �     �     �     �  _   �     S     r     �  
   �     �  	   �     /                 @   G      N   I   L      )               <             -   A                       >          H   =       9                  7          J   2          &   ;   ,               5   F      8   :       4          ?               M   !      	      "   #          %   3   +   K   '   0       .   
      D   B   1              *   C                (   6   $       E                    Add widgets here to appear in your sidebar. Appears at the bottom of the content on posts and pages. Author: Base Color Scheme Blue Gray Bright Blue Bright Red Comments are closed. Content Bottom 1 Content Bottom 2 Continue reading<span class="screen-reader-text"> "%s"</span> Dark Dark Brown Dark Gray Dark Red Default Edit<span class="screen-reader-text"> "%s"</span> Featured Footer Primary Menu Footer Social Links Menu Gray Inconsolata font: on or offon It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment<span class="screen-reader-text"> on %s</span> Light Blue Light Gray Link Color Main Text Color Medium Brown Medium Gray Menu Merriweather font: on or offon Montserrat font: on or offon Next Next Image Next page Next post: Nothing Found Oops! That page can&rsquo;t be found. Page Page Background Color Pages: Parent post link<span class="meta-nav">Published in</span><span class="post-title">%title</span> Previous Previous Image Previous page Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Red Search Results for: %s Secondary Text Color Sidebar Skip to content Social Links Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Twenty Sixteen requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again. Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  View all posts by %s White Yellow collapse child menu comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; expand child menu https://wordpress.org/ labelSearch for: placeholderSearch &hellip; submit buttonSearch PO-Revision-Date: 2018-12-06 20:46:28+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: hr
Project-Id-Version: Themes - Twenty Sixteen
 Ovdje dodajte widgete koji će se pojaviti u bočnoj traci. Prikazuje se pri dnu sadržaja objava i stranica. Autor: Osnovna shema boja Plavo siva Jarko plava Jarko crvena Komentari su isključeni. Donji sadržaj 1 Donji sadržaj 2 Nastavi čitati<span class="screen-reader-text"> "%s"</span> Tamno Tamno smeđa Tamno siva Tamno crvena Izvorno Uredi<span class="screen-reader-text"> "%s"</span> Istaknuto Primarni izbornik podnožja Izbornik društvenih poveznica podnožja Siva uklj. Izgleda da se ništa ne nalazi na ovoj lokaciji. Možda da probate pretragu? Izgleda da ne možemo prikazati ono što ste zahtijevali. Možda da pokušate sa pretragom? Ostavite komentar<span class="screen-reader-text"> na %s</span> Svjetlo plava Svjetlo siva Boja poveznice Glavna boja teksta Umjereno smeđa Umjereno siva Izbornik on off Sljedeće Sljedeća slika Sljedeća stranica Sljedeća objava: Nije pronađeno Ups! Ta stranica nije pronađena. Stranica Pozadinska boja stranice Stranice: <span class="meta-nav">Objavljeno u</span><span class="post-title">%title</span> Prethodno Prethodna slika Prethodna stranica Prethodna objava: Primarni izbornik Ponosno pokreće %s Spremni ste da objavite vašu prvu objavu? <a href="%1$s">Započnite ovdje</a>. Crvena Rezultati pretrage za: %s Sekundarna boja teksta Bočna traka Preskoči na sadržaj Izbornik za poveznice društvenih mreža Nema rezultata za vaš traženi pojam. Pokušajte ponovo koristeći drugu ključnu riječ. Za korištenje Twenty Sixteen potrebna je najmanje inačica WordPress 4.4. Koristite inačicu %s. Nadogradite i pokušajte ponovno. Kategorije Pun veličina Autor Format Objavljeno dana Oznake ,  Pregledaj sve objave od %s Bjela Žuto zatvori podizbornik %1$s misao o &ldquo;%2$s&rdquo; %1$s misli o &ldquo;%2$s&rdquo; %1$s misli o &ldquo;%2$s&rdquo; Jedna misao o &ldquo;%s&rdquo; proširi podizbornik https://wordpress.org/ Pretraži: Pretraži &hellip; Pretraži 