��    N      �  k   �      �  +   �  8   �            	   (     2  
   >     I     ^     o  =   �     �  
   �  	   �     �     �  1   �          $     8     Q     V  E   u  W   �  =   	  
   Q	  
   \	  
   g	     r	     �	     �	     �	     �	     �	     �	  
   �	  	   �	  
   �	     
  %   
     7
     <
     R
  a   Y
     �
     �
     �
     �
     �
     �
  F        Z     ^     u     �     �     �  \   �  q     &   �  0   �  $   �        #         D  =   `     �     �     �     �  U   �  .   *     Y     k     �     �     �    �  :   �  =        N     U     n     {     �     �     �     �  6   �               '     3     C  3   O  	   �  !   �  -   �     �     �  @   �  X   )  A   �  
   �  
   �     �     �       
          
   "  
   -     8     A     P     a     r  %   �     �     �  	   �  P   �     &     /     >     O     `     o  Q   �     �     �               +     8  x   U  �   �  
   R     ]     n     t     |  	   �     �     �     �     �     �  L   �  "   "     E     Z     q     y     �     /                 @   G      N   I   L      )               <             -   A                       >          H   =       9                  7          J   2          &   ;   ,               5   F      8   :       4          ?               M   !      	      "   #          %   3   +   K   '   0       .   
      D   B   1              *   C                (   6   $       E                    Add widgets here to appear in your sidebar. Appears at the bottom of the content on posts and pages. Author: Base Color Scheme Blue Gray Bright Blue Bright Red Comments are closed. Content Bottom 1 Content Bottom 2 Continue reading<span class="screen-reader-text"> "%s"</span> Dark Dark Brown Dark Gray Dark Red Default Edit<span class="screen-reader-text"> "%s"</span> Featured Footer Primary Menu Footer Social Links Menu Gray Inconsolata font: on or offon It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment<span class="screen-reader-text"> on %s</span> Light Blue Light Gray Link Color Main Text Color Medium Brown Medium Gray Menu Merriweather font: on or offon Montserrat font: on or offon Next Next Image Next page Next post: Nothing Found Oops! That page can&rsquo;t be found. Page Page Background Color Pages: Parent post link<span class="meta-nav">Published in</span><span class="post-title">%title</span> Previous Previous Image Previous page Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Red Search Results for: %s Secondary Text Color Sidebar Skip to content Social Links Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Twenty Sixteen requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again. Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  View all posts by %s White Yellow collapse child menu comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; expand child menu https://wordpress.org/ labelSearch for: placeholderSearch &hellip; submit buttonSearch PO-Revision-Date: 2018-12-08 13:22:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: gl_ES
Project-Id-Version: Themes - Twenty Sixteen
 Engadir widgets aquí para aparecer na súa barra lateral. Aparece na parte inferior do contido, en entradas e páxinas. Autor: Esquema de cores de base Gris azulado Azul brillante Vermello brillante Comentarios pechados. Contido inferior 1 Contido inferior 2	 Ler máis<span class="screen-reader-text"> "%s"</span> Escuro Marrón escuro Gris escuro Vermello escuro Por Defecto Editar<span class="screen-reader-text"> "%s"</span> Destacado Menú principal do pé de páxina Menú de ligazóns sociais do  pé de páxina Gris aceso Parece que non hai nada nesa ubicacion ¿queres probar a buscar? Asemella que non podemos atopar o que estás buscando. Tal vez a busca póidalle axudar. Deixa un comentario<span class="screen-reader-text"> en %s</span> Azul claro Gris claro Cor das ligazóns Cor principal do texto Marrón medio Gris medio Menú activado	  activado	  Seguinte Imaxe Seguinte Páxina seguinte Artigo Seguinte: Non se atopou nada Ups! Esta páxina non se pode atopar. Páxina Cor do fondo da páxina Páxinas: <span class="meta-nav">Publicado en</span><span class="post-title">%title</span> Anterior Imaxe Anterior Páxina anterior Artigo anterior: Menú primario Basado en %s traducido por %s ¿Preparado para publica-lo teu primeiro artigo? <a href="%1$s">Empeza aquí</a>. Vermello Resultados da procura de: %s Cor de texto secundario Barra lateral Ir o contido Menú de Conexións sociais  Síntoo, pero non hai nada que corresponda aos teus criterios de busca. Por favor, proba de novo con palabras distintas. Twenty  Sixteen require polo menos a versión 4.4 de  WordPress. Estás a usar a versión %s. Por favor, actualiza e proba de novo. Categorias Tamaño completo Autor Formato Publicado o Etiquetas ,  Ver todalas entradas por %s Branco Amarelo colapsar menú fillo %1$s idea sobre &ldquo;%2$s&rdquo; %1$s pensamentos sobre &ldquo;%2$s&rdquo; 	Un comentario en &ldquo;%s&rdquo; expandir menú fillo https://wordpress.org/ Buscar: Buscar &hellip; Buscar 