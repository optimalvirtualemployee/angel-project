��    N      �  k   �      �  +   �  8   �            	   (     2  
   >     I     ^     o  =   �     �  
   �  	   �     �     �  1   �          $     8     Q     V  E   u  W   �  =   	  
   Q	  
   \	  
   g	     r	     �	     �	     �	     �	     �	     �	  
   �	  	   �	  
   �	     
  %   
     7
     <
     R
  a   Y
     �
     �
     �
     �
     �
     �
  F        Z     ^     u     �     �     �  \   �  q     &   �  0   �  $   �        #         D  =   `     �     �     �     �  U   �  .   *     Y     k     �     �     �  v  �  C   <  8   �     �     �  
   �     �     �     �          '  <   9     v     |  
   �     �     �  2   �  	   �     �  "   �     "     '  B   *  Z   m  9   �            
        *     =     L     Y     ^     a  	   d     n     ~     �     �  !   �     �     �  	   �  R     	   U     _     o     �     �     �  O   �     �               5     B  !   X  g   z  |   �  
   _     j     y       
   �     �     �     �     �     �     �  m   �  "   B     e     v  	   �     �  	   �     /                 @   G      N   I   L      )               <             -   A                       >          H   =       9                  7          J   2          &   ;   ,               5   F      8   :       4          ?               M   !      	      "   #          %   3   +   K   '   0       .   
      D   B   1              *   C                (   6   $       E                    Add widgets here to appear in your sidebar. Appears at the bottom of the content on posts and pages. Author: Base Color Scheme Blue Gray Bright Blue Bright Red Comments are closed. Content Bottom 1 Content Bottom 2 Continue reading<span class="screen-reader-text"> "%s"</span> Dark Dark Brown Dark Gray Dark Red Default Edit<span class="screen-reader-text"> "%s"</span> Featured Footer Primary Menu Footer Social Links Menu Gray Inconsolata font: on or offon It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment<span class="screen-reader-text"> on %s</span> Light Blue Light Gray Link Color Main Text Color Medium Brown Medium Gray Menu Merriweather font: on or offon Montserrat font: on or offon Next Next Image Next page Next post: Nothing Found Oops! That page can&rsquo;t be found. Page Page Background Color Pages: Parent post link<span class="meta-nav">Published in</span><span class="post-title">%title</span> Previous Previous Image Previous page Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Red Search Results for: %s Secondary Text Color Sidebar Skip to content Social Links Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Twenty Sixteen requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again. Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  View all posts by %s White Yellow collapse child menu comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; expand child menu https://wordpress.org/ labelSearch for: placeholderSearch &hellip; submit buttonSearch PO-Revision-Date: 2019-06-09 15:05:11+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: bs_BA
Project-Id-Version: Themes - Twenty Sixteen
 Dodajte dodatke ovdje i oni će se pojaviti u vašoj bočnoj traci. Prikazuje se na dnu sadržaja na člancima i stranicama. Autor: Osnovna šema boja Plavo sivo Jarko plavo Svijetlo crveno Komentari su onemogućeni. Sadržaj na dnu 1 Sadržaj na dnu 2 Nastavi čitati<span class="screen-reader-text"> "%s"</span> Tamna Tamno smeđe Tamno sivo Tamno crveno Početno Uredi<span class="screen-reader-text"> "%s"</span> Istaknuto Primarni meni podnožje Meni društvenih linkova podnožje Sivo on Nema sadržaja na ovoj lokaciji. Možda da pokušate sa pretragom? Izgleda da ne možemo prikazati ono što ste zahtjevali. Možda da pokušate sa pretragom? Komentarišite<span class="screen-reader-text"> %s</span> Svijetlo plavo Svijetlo sivo Boja linka Glavna boja teksta Srednje smeđe Srednje sivo Meni on on Sljedeća Sljedeća slika Sljedeća stranica Sljedeći članak: Nije pronađeno Ups! Ta stranica nije pronađena. Stranica Pozadinska boja stranice Stranice: <span class="meta-nav">Objavljeno kao</span><span class="post-title">%title</span> Prethodno Prethodna slika Prethodna stranica Prethodni članak: Glavni meni Pokreće %s Spremni ste da objavite vaš prvi članak? <a href="%1$s">Započnite ovdje</a>. Crveno Rezultati pretrage za: %s Sekundarna boja teksta Bočna traka Preskoči na sadržaj Meni linkova za društvene mreže Žao nam je, nema rezultata za vaš traženi pojam. Pokušajte ponovo koristeći drugu ključnu riječ. Twenty Sixteen zahtjeva najmanje WordPress 4.4. Vi imate verziju %s. Molimo vas da izvršite ažuriranje i pokušate ponovo. Kategorije Puna veličina Autor Format Objavljeno Oznake ,  Pregledaj sve članke od %s Bijelo Žuto zatvori podmeni %1$s komentar na &ldquo;%2$s&rdquo; %1$s komentara na &ldquo;%2$s&rdquo; %1$s komentara na &ldquo;%2$s&rdquo; Jedan komentar na &ldquo;%s&rdquo; proširi podmeni https://bs.wordpress.org/ Pretraži Pretraži &hellip; Pretraži 