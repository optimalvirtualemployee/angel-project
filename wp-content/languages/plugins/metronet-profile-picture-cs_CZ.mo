��          �      �       0     1     @     Q     _     z     �     �      �  5   �          "  6   2  U  i     �     �     �  "   �          1     O  *   l  E   �     �     �  =                            	         
                        Crop Thumbnail Override Avatar? Profile Image Profile picture not found. Remove Profile Image Remove profile image Set Profile Image Upload or Change Profile Picture Use the native WP uploader on your user profile page. User Profile Picture User not found. http://wordpress.org/plugins/metronet-profile-picture/ PO-Revision-Date: 2020-03-31 19:56:26+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n >= 2 && n <= 4) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: cs_CZ
Project-Id-Version: Plugins - User Profile Picture - Stable (latest release)
 Oříznout miniaturu Přepsat avatar? Profilový obrázek Profilový obrázek nebyl nalezen. Remove Profile Image Odstranit profilový obrázek Nastavit profilový obrázek Nahrajte nebo změňte profilový obrázek Použijte nativní WP funkce pro nahrávání profilových obrázků. User Profile Picture Uživatel nebyl nalezen. http://wordpress.org/extend/plugins/metronet-profile-picture/ 