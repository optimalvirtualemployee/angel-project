��          �      l      �  
   �     �  �         �  
   �     �     �     �     �  
   �     �     �     �     �     
          "  	   %     /     L  .  k  
   �     �  �   �     ^     f  
   x     �     �     �     �     �     �  
   �     �     �  
          	        (     E        	                                   
                                                       AGILELOGIX Agile Store Locator Agile Store Locator is a WordPress Plugin that renders stores list with google maps V3 API. Paste ShortCode [ASL_STORELOCATOR] in your page/post. Details Directions Distance From GET DIRECTIONS Get Your Directions Loading... None Phone Search Location Select Option Show Distance In Stores To Zoom Here http://agilestorelocator.com https://agilestorelocator.com/ PO-Revision-Date: 2018-02-18 14:52:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: de
Project-Id-Version: Plugins - Store Locator WordPress - Stable (latest release)
 AGILELOGIX Agile Store Locator Agile Store Locator ist ein WordPress-Plugin, das Läden mit Google Maps V3 API rendert. Füge den ShortCode [ASL_STORELOCATOR] in deine Seite / deinen Beitrag ein. Details Wegbeschreibungen Entfernung Von WEGBESCHREIBUNGEN ERHALTEN Erhalte deine Wegbeschreibung Wird geladen... Kein Telefon Ort suchen Option wählen Entfernung anzeigen in Geschäfte Nach Hier Zoom http://agilestorelocator.com https://agilestorelocator.com/ 