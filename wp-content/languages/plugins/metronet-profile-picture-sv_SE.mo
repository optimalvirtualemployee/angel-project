��    #      4  /   L           	               2  
   @     K     Z     s     �     �     �     �     �     �     �     	       P   +     |     �      �     �     �     �  
   �     �                  !   @  C   b     �  6   �     �  0       G     S     f     |  
   �     �     �     �     �     �          !  
   8     C     ]     p     �  c   �     �     	  !   	     >	     S	     n	     �	     �	     �	  	   �	  %   �	      �	  T   
     b
  6   
     �
                                #                           "                                                   !             	      
                            Author Author Details Author Information Click to Edit Cozmoslabs Crop Thumbnail Disable Gutenberg Blocks Disable Image Sizes Disable Image Sizes? Gutenberg Blocks Latest Posts Override Avatar? Profile Image Profile picture not found. Remove Profile Image Remove profile image Save Options Select this option to disable the four image sizes User Profile Picture Creates. Set Profile Image Settings Upload or Change Profile Picture User Profile Picture User not found. User not owner. View Posts View Website View all posts by Website Welcome to User Profile Picture! You must be able to upload files. You must have a role of editor or above to set a new profile image. Your options have been saved. http://wordpress.org/plugins/metronet-profile-picture/ https://www.cozmoslabs.com PO-Revision-Date: 2020-03-31 14:54:49+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: sv_SE
Project-Id-Version: Plugins - User Profile Picture - Stable (latest release)
 Författare Författardetaljer Författarinformation Klicka för att redigera Cozmoslabs Beskär miniatyr Inaktivera Gutenberg-block Inaktivera bildstorlekar Inaktivera bildstorlekar? Gutenberg-block Senaste inläggen Åsidosätt profilbild Profilbild Profilbild hittades inte. Ta bort profilbild Ta bort profilbild Spara alternativ Välj detta alternativ för att inaktivera de fyra bildstorlekarna som User Profile Picture skapar. Ställ in profilbild Inställningar Ladda upp eller ändra profilbild User Profile Picture Användaren hittades inte. Användaren är inte ägare. Visa inlägg Visa webbplats Visa alla inlägg av Webbplats Välkommen till User Profile Picture! Du måste kunna ladda upp filer. Du måste ha en roll som redaktör eller över för att ställa in en ny profilbild. Dina alternativ har sparats. http://wordpress.org/plugins/metronet-profile-picture/ https://www.cozmoslabs.com 