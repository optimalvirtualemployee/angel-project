��          \      �       �      �   	   �      �      �   6     `   8     �  g  �          $     1     J  <   ]  �   �     N                                       CedCommerce Live Demo One Click Order Re-Order Re-Order The items are added to cart from your previous order . This extension is used to place the previous order again while order status is completed or not. http://cedcommerce.com PO-Revision-Date: 2016-06-08 12:57:43+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: pl
Project-Id-Version: Plugins - One Click Order Re-Order - Development (trunk)
 CedCommerce Demonstracja One Click Order Re-Order Zamów jeszcze raz Produkty z wybranego zamówienia zostały dodane do koszyka. Ta wtyczka dodaje przycisk umożliwiający dodanie do koszyka produktów z wcześniejszego zamówienia, niezależnie od tego, czy zostało ono zrealizowane, czy też ma inny stan. http://cedcommerce.com 