��    %      D  5   l      @     A     H     W     j  
   x     �     �     �     �     �     �     �               ,     A     V  v   c  P   �     +     =      F  5   g     �     �     �  
   �     �     �     �        !   %  C   G     �  6   �     �  0  �     ,     2     B     T  
   o     z     �     �  $   �     �     �     	     +	     ;	     Y	     o	     �	  �   �	  v   3
     �
  
   �
  '   �
  W   �
      N     o      �     �     �     �     �  *   �  0     ^   >     �  6   �     �                         #                 "          	       !                     $                              %       
                                                              Author Author Details Author Information Click to Edit Cozmoslabs Crop Thumbnail Disable Gutenberg Blocks Disable Image Sizes Disable Image Sizes? Gutenberg Blocks Latest Posts Override Avatar? Profile Image Profile picture not found. Remove Profile Image Remove profile image Save Options Select this option if you do not want User Profile Picture to show up in Gutenberg or do not plan on using the blocks. Select this option to disable the four image sizes User Profile Picture Creates. Set Profile Image Settings Upload or Change Profile Picture Use the native WP uploader on your user profile page. User Profile Picture User not found. User not owner. View Posts View Website View all posts by Website Welcome to User Profile Picture! You must be able to upload files. You must have a role of editor or above to set a new profile image. Your options have been saved. http://wordpress.org/plugins/metronet-profile-picture/ https://www.cozmoslabs.com PO-Revision-Date: 2020-12-02 19:59:56+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: sq_AL
Project-Id-Version: Plugins - User Profile Picture - Stable (latest release)
 Autor Hollësi Autori Të dhëna Autori Klikoni për ta Përpunuar Cozmoslabs Qethe Miniaturën Çaktivizo Blloqe Gutenberg Çaktivizo Madhësi Figurash Të çaktivizohen Madhësi Figurash? Blloqe Gutenberg Postimet Më të Reja Të anashkalohet Avatari? Figurë Profili Fotoja e profilit s’u gjet. Hiqni Figurë Profili Hiqe figurën e profilit Ruaji Mundësitë Përzgjidheni këtë mundësi, nëse s’doni që Foto Profili Përdoruesi të shfaqet në Gutenberg, ose nëse s’keni në plan të përdorni blloqet. Përzgjidheni këtë mundësi që të çaktivizohen katër madhësitë e figurave që krijon Foto Profili Përdoruesi. Vendosni Figurë Profili Rregullime Ngarkoni ose Ndryshoni Foton e Profilit Përdorni ngarkuesin origjinal të WP-së në faqen tuaj të profilit të përdoruesit. Foto e Profilit të Përdoruesit S’u gjet përdorues. Përdoruesi s’është pronari. Shihni Postime Shihni Sajt Shihini krejt postimet nga Sajt Mirë se vini te Foto Profili Përdoruesi! Duhet të jeni në gjendje të ngarkoni kartela. Që të caktoni një figurë të re profili, duhet të keni rolin e redaktorit ose më sipër. Mundësitë tuaja u ruajtën. http://wordpress.org/plugins/metronet-profile-picture/ https://www.cozmoslabs.com 