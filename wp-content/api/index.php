<?php
ini_set('display_errors',0);

date_default_timezone_set("Asia/Kolkata");
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With, TOKEN');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require_once 'vendor/autoload.php';
require_once "config.inc.php";
require_once "models/Database.php";
require_once "controllers/common.php";
require_once "controllers/user.php";
require_once "controllers/products.php";
require_once "controllers/orders.php";

$app = new \Slim\App;

$app->get('/', function ($request, $response, $args) {
  sendResponse(array(), STATUS_OK, 'Welcome');
});

$app->post('/login', function ($request, $response, $args) {
  openRequest($request, $response, $args, 'login');
});

$app->post('/usersignup', function ($request, $response, $args) {
  openRequest($request, $response, $args, 'usersignup');
});

$app->get('/getuserinfo',function($request, $response, $args) {
  tokenRequest($request, $response, $args, 'getuserinfo');
});

$app->post('/profileupdate', function ($request, $response, $args) {
    tokenRequest($request, $response, $args, 'profileUpdate');
});

$app->post('/uploadprofileimage', function ($request, $response, $args) {
    tokenRequest($request, $response, $args, 'uploadprofileimage');
});

$app->get('/categories',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getCategories');
});

$app->get('/getourproducts',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getOurProducts');
});

$app->get('/getproductbycategory/{catid}',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getProductByCategory');
});

$app->get('/getproduct/{pid}',function($request, $response, $args) {
    openRequest($request, $response, $args, 'getProductsByID');
});

$app->post('/getmyorders', function ($request, $response, $args) {
    tokenRequest($request, $response, $args, 'getCustomerOrders');
});

$app->post('/offerlist', function ($request, $response, $args) {
    openRequest($request, $response, $args, 'getOfferLists');
});

$app->post('/driversignup', function ($request, $response, $args) {
  openRequest($request, $response, $args, 'driversignup');
});

$app->post('/driverlogin', function ($request, $response, $args) {
  openRequest($request, $response, $args, 'driverlogin');
});

$app->post('/getvendorsbycategory',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getvendorsbycategory');
});

$app->post('/getproductsbyvendors',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getproductsbyvendors');
});

$app->post('/getrecommendedproduct',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getrecommendedproduct');
});

$app->post('/addproducttocart',function($request, $response, $args) {
  openRequest($request, $response, $args, 'addproducttocart');
});

$app->post('/removeproductfromcart',function($request, $response, $args) {
  openRequest($request, $response, $args, 'removeproductfromcart');
});

$app->post('/removeallproductfromcart',function($request, $response, $args) {
  openRequest($request, $response, $args, 'removeallproductfromcart');
});

$app->post('/getallcartitems',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getallcartitems');
});

$app->post('/addnewaddress',function($request, $response, $args) {
  openRequest($request, $response, $args, 'addnewaddress');
});

$app->post('/getalladdress',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getalladdress');
});

$app->post('/placeOrderRequest',function($request, $response, $args) {
  openRequest($request, $response, $args, 'placeOrderRequest');
});

$app->get('/getslides',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getslides');
});

$app->post('/getcustomerprofile',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getcustomerprofile');
});

$app->post('/editcustomerprofile',function($request, $response, $args) {
  openRequest($request, $response, $args, 'editcustomerprofile');
});
$app->get('/popularproducts',function($request, $response, $args) {
  openRequest($request, $response, $args, 'popularproducts');
});

$app->post('/generatepin',function($request, $response, $args) {
  openRequest($request, $response, $args, 'generatepin');
});

$app->post('/loginwithpin',function($request, $response, $args) {
  openRequest($request, $response, $args, 'loginwithpin');
});

$app->post('/editdriverprofile',function($request, $response, $args) {
  openRequest($request, $response, $args, 'editdriverprofile');
});

$app->post('/getdriverprofile',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getdriverprofile');
});

$app->post('/getcustomerorders',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getcustomerorderss');
});

$app->post('/makereorder',function($request, $response, $args) {
  openRequest($request, $response, $args, 'makereorder');
});

$app->post('/forgotpassword',function($request, $response, $args) {
  openRequest($request, $response, $args, 'forgotpassword');
});

$app->post('/sendotponphone',function($request, $response, $args) {
  openRequest($request, $response, $args, 'sendotponphone');
});

$app->post('/verifyotponphone',function($request, $response, $args) {
  openRequest($request, $response, $args, 'verifyotponphone');
});

$app->post('/deactivateaccount',function($request, $response, $args) {
  openRequest($request, $response, $args, 'deactivateaccount');
});

$app->post('/updateprofilestatus',function($request, $response, $args) {
  openRequest($request, $response, $args, 'updateprofilestatus');
});

$app->post('/updatepassword',function($request, $response, $args) {
  openRequest($request, $response, $args, 'updatepassword');
});

$app->post('/updatedriverpin',function($request, $response, $args) {
  openRequest($request, $response, $args, 'updatedriverpin');
});

$app->post('/updatecardetails',function($request, $response, $args) {
  openRequest($request, $response, $args, 'updatecardetails');
});

$app->post('/usersocialogin', function($request, $response, $args){
  openRequest($request, $response, $args,'usersocialogin');
});

$app->post('/phoneverify',function($request, $response, $args) {
  openRequest($request, $response, $args, 'phoneverify');
});

$app->post('/resendverificationcode',function($request, $response, $args) {
  openRequest($request, $response, $args, 'sendotponphone');
});

$app->post('/getallopenorders',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getallopenorders');
});

$app->post('/changeorderstatus',function($request, $response, $args) {
  openRequest($request, $response, $args, 'changeorderstatus');
});

$app->post('/getdriverorders',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getalldriverorders');
});

$app->post('/getorderdetails',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getorderdetails');
});

$app->post('/getorderinvoice',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getorderinvoice');
});

$app->post('/getcardetails',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getcardetails');
});

$app->post('/gettodayorders',function($request, $response, $args) {
  openRequest($request, $response, $args, 'gettodayorders');
});

$app->post('/submitorderreviews',function($request, $response, $args) {
  openRequest($request, $response, $args, 'submitorderreviews');
});

$app->post('/getorderreview',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getorderreview');
});

$app->post('/calculatedeliverycharges',function($request, $response, $args) {
  openRequest($request, $response, $args, 'calculatedeliverycharges');
});

$app->post('/getordercount',function($request, $response, $args) {
  openRequest($request, $response, $args, 'getordercount');
});

$app->post('/sendotponphonebynumber',function($request, $response, $args) {
  openRequest($request, $response, $args, 'sendotponphonebynumber');
});

$app->post('/verifyotpbyphone',function($request, $response, $args) {
  openRequest($request, $response, $args, 'verifyotpbyphone');
});

$app->post('/changepassword',function($request, $response, $args) {
  openRequest($request, $response, $args, 'changepassword');
});

$app->post('/checkcoupon',function($request, $response, $args) {
  openRequest($request, $response, $args, 'checkcoupon');
});

$app->post('/updateprofilepic',function($request, $response, $args) {
  openRequest($request, $response, $args, 'updateprofilepic');
});

$app->post('/ordersbydate',function($request, $response, $args) {
  openRequest($request, $response, $args, 'ordersbydate');
});
$app->run();
