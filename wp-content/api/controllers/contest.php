<?php
/* 
Author - Optimal
Title - Contest library 
Version - 1.1
Date -  20-01-2019
*/
require_once "models/Contest.php";

//Get list of active contests
function contests($request, $response, $args){
  $code = STATUS_OK;
  $message = 'Success';
  $req = $request->getParams();
  $contest = new Contest();
  $rows = $contest->getActiveContests();
  
  sendResponse($rows, $code, $message);
}

//Get contest details by id
function contestdetails($request, $response, $args){
  $code = STATUS_OK;
  $message = 'Success';
  $req = $request->getParams();
  $contest = new Contest();
  $id = $req['id'];
  $row = $contest->getContest($id);
  
  sendResponse($row, $code, $message);
}

//Get contest entries by contest id
function contestentries($request, $response, $args){
  $code = STATUS_OK;
  $message = 'Success';
  $req = $request->getParams();
  $contest = new Contest();
  $id = $req['id'];
  $rows = $contest->getParticipants($id);
  
  sendResponse($rows, $code, $message);
}

//Post contest entry to participate 
function participate($request, $response, $args){
  $code = STATUS_OK;
  $message = 'Success';
  $req = $request->getParams();
  $contest = new Contest();
  $message = $contest->postEntry($req, $args['user_id']);
  
  sendResponse(array(), $code, $message);
}