<?php
/* 
Author - Vipin Singh
Title - User library 
Version - 1.1
Date -  28-10-2020
*/
require_once "models/UserDAO.php";
require_once('../wp-load.php');
require_once('braintree/Braintree.php');

  function login($request, $response, $args) {
    require_once('../wp-load.php');
    global $wpdb;

    $code       = '';
    $message    = '';
    $req        = $request->getParams();
    $uemail     = $req['user_email'];
    $upass      = $req['user_pass'];

    $creds['user_login'] = $uemail;
    $creds['user_password'] = $upass; 
    $creds['remember'] = true;
    $user = wp_signon ($creds, true);

    $userDAO = new UserDAO();
    //$arrRowDatas = $userDAO->getLoginByCreds($uemail,$upass);
    //pre($arrRowDatas);
   
    if(isset($user->errors)){
       $err=strip_tags($user->get_error_message());
      $res=array();
      if($err == "Please verify your Phone Number First."){
        $user = get_user_by( 'email', $uemail );
        $res['id']=$user->ID;
      }
      else if($err == "Please verify your Email ID First."){
        $user = get_user_by( 'email', $uemail );
        $res['id']=$user->ID;
        $cev_skip_verification_for_selected_roles = get_option('cev_skip_verification_for_selected_roles');
          $new_userid=$user->ID;
          $user_role = get_userdata( $new_userid );
          
          $verified = get_user_meta( $new_userid, 'customer_email_verified', true );
          
          $cev_enable_email_verification = get_option('cev_enable_email_verification',1);		
          
          if ( 'administrator' !== $user_role->roles[0] && $cev_skip_verification_for_selected_roles[$user_role->roles[0]] == 0 && $cev_enable_email_verification == 1 && $verified != 'true') {
            
            $current_user = get_user_by( 'id', $new_userid );
            $user_id                         = $current_user->ID;
            $email_id                        = $current_user->user_email;
            $user_login                      = $current_user->user_login;
            $user_email                      = $current_user->user_email;
              $my_account = woo_customer_email_verification()->my_account;
            WC_customer_email_verification_email_Common()->wuev_user_id  = $current_user->ID;
            WC_customer_email_verification_email_Common()->wuev_myaccount_page_id = $my_account;
            $is_user_created                 = true;		
            $is_secret_code_present                = get_user_meta( $user_id, 'customer_email_verification_code', true );
        
            if ( '' === $is_secret_code_present ) {
              $secret_code = md5( $user_id . time() );
              update_user_meta( $new_userid, 'customer_email_verification_code', $secret_code );
            } 
            
            $cev_email_for_verification = get_option('cev_email_for_verification',0);
            //echo $secret_code;exit;
            if($cev_email_for_verification == 0){
              WC_customer_email_verification_email_Common()->code_mail_sender( $current_user->user_email );
            }
            $is_new_user_email_sent = true;
          }
      }
      $code       = STATUS_UNAUTHORIZED;
      $message    = $err;
      $arrRow[]   = $res;
    } else {
      $user_id = $user->ID;
      $user_email = $user->user_email;
      $arrRowData['user_token'] = generateToken($user_id, $user_email, $req['device_id']);
      $userDAO->updateToken($user_id, $arrRowData['user_token'], $req['device_id']);

      $code       = STATUS_OK;
      $message    = 'Success';
      $arrRow['id'] = $user_id;
      $arrRow['name'] = $user->display_name;
      $arrRow['email'] = $user_email;
      $getUserToken = $userDAO->getUserTokenByID($user_id);
      $arrRow['token'] = $getUserToken['token'];
    }  

    sendResponse($arrRow, $code, $message);
  }

  function usersignup($request, $response, $args) {
  
    global $wpdb;
    $code = '';
    $message = '';
    $req = $request->getParams();
    $uname = $req['user_name'];
    $uemail  = $req['user_email'];
    $upass  = $req['user_pass'];
    $uphone  = $req['user_phone'];
    $device_id  = $req['device_id'];
   

    $user_id = username_exists( $uemail );
      $arrRow=array();
      
       $hasPhoneNumber= get_users('meta_value='.$uphone);
       if ( !empty($hasPhoneNumber)) {
          $code = STATUS_BAD_REQUEST;
           $message = 'Phone number already exists.';
        }
     else if ( ! $user_id && false == email_exists( $uemail ) ) {
       
         $uid = wp_create_user( $uemail, $upass, $uemail );
         $user = get_user_by( 'id', $uid );
         $user->set_role( 'customer' );
         update_user_meta($uid,'nickname',$uname);
          update_user_meta($uid,'billing_phone',$uphone);
           update_user_meta($uid,'device_id',$device_id);
         update_user_meta($uid,'first_name',$uname);
         //add_user_meta($uid,'verified',1);
         $arrRow['id']=$uid;
         $code = STATUS_OK;
         $message = 'User created successfully!';
         
         $cev_skip_verification_for_selected_roles = get_option('cev_skip_verification_for_selected_roles');
          $new_userid=$uid;
          $user_role = get_userdata( $new_userid );
          
          $verified = get_user_meta( $new_userid, 'customer_email_verified', true );
          
          $cev_enable_email_verification = get_option('cev_enable_email_verification',1);		
          
          if ( 'administrator' !== $user_role->roles[0] && $cev_skip_verification_for_selected_roles[$user_role->roles[0]] == 0 && $cev_enable_email_verification == 1 && $verified != 'true') {
            
            $current_user = get_user_by( 'id', $new_userid );
            $user_id                         = $current_user->ID;
            $email_id                        = $current_user->user_email;
            $user_login                      = $current_user->user_login;
            $user_email                      = $current_user->user_email;
              $my_account = woo_customer_email_verification()->my_account;
            WC_customer_email_verification_email_Common()->wuev_user_id  = $current_user->ID;
            WC_customer_email_verification_email_Common()->wuev_myaccount_page_id = $my_account;
            $is_user_created                 = true;		
            $is_secret_code_present                = get_user_meta( $user_id, 'customer_email_verification_code', true );
        
            if ( '' === $is_secret_code_present ) {
              $secret_code = md5( $user_id . time() );
              update_user_meta( $new_userid, 'customer_email_verification_code', $secret_code );
            } 
            
            $cev_email_for_verification = get_option('cev_email_for_verification',0);
            //echo $secret_code;exit;
            if($cev_email_for_verification == 0){
              WC_customer_email_verification_email_Common()->code_mail_sender( $current_user->user_email );
            }
            $is_new_user_email_sent = true;
          }
          
          
          if ( $uphone )
          {
              $phone=$uphone;
              $otp=rand(1000,9999);
              $curl = curl_init();
              
              curl_setopt_array($curl, array(
                CURLOPT_URL => "http://2factor.in/API/V1/b88ec033-3dc6-11eb-83d4-0200cd936042/SMS/".$phone."/".$otp,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
              ));
              
              $response = curl_exec($curl);
              $err = curl_error($curl);
              
              curl_close($curl);
              
              if ($err) {
                echo "cURL Error #:" . $err;
              } else {
               update_user_meta($user_id, 'phoneotp', $otp);
               update_user_meta($user_id, 'verified', 0);
              }
              
          }          
          
         
     } else {
         $code = STATUS_BAD_REQUEST;
         $message = 'User already exists.';
     }
     sendResponse($arrRow, $code, $message);
  }

  function getuserinfo($request, $response, $args){    
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();
    //$req = $request->getParams();  
    /*$user_id = $args['user_id'];

    
    $userMetaDetails = $userDAO->getUserMeta($user_id);
    //pre($userMetaDetails);
    $json = array();*/
    /*foreach ($userMetaDetails as $keys => $vals) { 
        
          $kindex = $vals['meta_key'];
          $vindex = $vals['meta_value']; 
          preg_match_all('~\Child~', $kindex, $match);
          if(count($match[0])>0){
             $newExp = explode("_",$kindex);
             $json['childData'][$newExp[1]][$newExp[0]] = $vindex;
          }else{
            $json[$kindex] = $vindex;
          }         
      }
      ksort($json['childData']);*/
    //$json['userInfo'] = $userMetaDetails;
    //$json['wallet'] = $userDAO->getWalletBalance($user_id); 
    /*$httpObj = getWoocommerceObject();
    pre($httpObj->customers->get($user_id)->customer);
    $json['avatar'] = $httpObj->customers->get($user_id)->customer->avatar_url;
    sendResponse($json,$code, $message);*/
    $json = array();
    if (isset($user_id)) {
      $code = STATUS_OK;
      $message = 'Success';
      $httpObj = getWoocommerceObject();
      $userMetaDetails = $userDAO->getUserMetaProfilePic($user_id);
      $userDetail = $userDAO->getUserDetailsByID($user_id);
      $json['profileImg'] = $userMetaDetails['meta_value'];
      $json['userdetail']['id'] = $userDetail['ID'];
      $json['userdetail']['name'] = $userDetail['display_name'];
      $json['userdetail']['email'] = $userDetail['user_email'];
      $json['usershippingdetails'] = $httpObj->customers->get($user_id)->customer;

    } else {
      $code = STATUS_BAD_REQUEST;
      $message = 'Please enter user id.';
      $json = '';
    }
    sendResponse($json, $code, $message);

  }

  function profileUpdate($request, $response, $args) {
    /*
    $req = $request->getParams();
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();    
    
    $userDAO->userDeleteUserMetaTblByID($req, $user_id);
    $arrRow = $userDAO->userRegisterMetaTblForProfile($req, $user_id);
      
    sendResponse($arrRow, $code, $message);*/
    $code = STATUS_OK;
    $message = 'Profile has been updated';

    $req = $request->getParams();
    $httpObj = getWoocommerceObject();
    $customer = $httpObj->customers->get($args['user_id']);
    //pre($customer);
    $customer->customer->shipping_address->first_name     = $req['firstname'];
    $customer->customer->shipping_address->last_name      = $req['lastname'];
    $customer->customer->shipping_address->company        = $req['company'];
    $customer->customer->shipping_address->address_1      = $req['address_1'];
    $customer->customer->shipping_address->address_2      = $req['address_2'];
    $customer->customer->shipping_address->city           = $req['city'];
    $customer->customer->shipping_address->state          = $req['state'];
    $customer->customer->shipping_address->postcode       = $req['postcode'];
    $customer->customer->shipping_address->country        = $req['country'];
    $customer->customer->shipping_address->email          = $req['email'];
    $customer->customer->shipping_address->phone          = $req['phone'];

    $arrRow = $httpObj->customers->update($customer->customer->id,(array)$customer);

    sendResponse($arrRow->customer, $code, $message);

  }

  function uploadprofileimage($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Profile image has been updated';
    $req = $request->getParams();
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();

    $file_name = $_FILES['profileImg']['name'];
    $uploadfile = UPLOADS_PATH . basename($_FILES['profileImg']['name']);

    if (move_uploaded_file($_FILES['profileImg']['tmp_name'], $uploadfile)) {
      $code = STATUS_OK;
      $message = 'Profile image has been updated';
    } else {
      $code = STATUS_BAD_REQUEST;
      $message = 'Upload error';
    }

    $data = array();
    $data['profileImg'] = UPLOADS_URL . basename($_FILES['profileImg']['name']);

    $userDAO->uploadImgDelete($user_id);
    $arrRow = $userDAO->uploadImg($user_id,$data);

    sendResponse($arrRow, $code, $message);
  }
  
  
  
  function driversignup($request,$response,$args){
    global $wpdb;
    
    $arg=$request->getParams();
    $name=$arg['name'];
    $email=$arg['email'];
    $pwd=$arg['password'];
    $uphone=$arg['phone'];
    
    $user_id = username_exists( $email );
      $arrRow=array();
     $hasPhoneNumber= get_users('meta_value='.$uphone);
      if ( !empty($hasPhoneNumber)) {
         $code = STATUS_BAD_REQUEST;
        $message = 'Phone number already exists.';
      } 
      else if ( ! $user_id && false == email_exists( $email ) ) {
       
         $uid = wp_create_user( $email, $pwd, $email );
         $user = get_user_by( 'id', $uid );
         $user->set_role( 'driver' );
         $new_userid=$uid;
         $cev_skip_verification_for_selected_roles = get_option('cev_skip_verification_for_selected_roles');
          $new_userid=$uid;
          $user_role = get_userdata( $new_userid );
          
          $verified = get_user_meta( $new_userid, 'customer_email_verified', true );
          
          $cev_enable_email_verification = get_option('cev_enable_email_verification',1);		
          
          if ( 'administrator' !== $user_role->roles[0] && $cev_skip_verification_for_selected_roles[$user_role->roles[0]] == 0 && $cev_enable_email_verification == 1 && $verified != 'true')
          {
            
            $current_user = get_user_by( 'id', $new_userid );
            $user_id                         = $current_user->ID;
            $email_id                        = $current_user->user_email;
            $user_login                      = $current_user->user_login;
            $user_email                      = $current_user->user_email;
              $my_account = woo_customer_email_verification()->my_account;
            WC_customer_email_verification_email_Common()->wuev_user_id  = $current_user->ID;
            WC_customer_email_verification_email_Common()->wuev_myaccount_page_id = $my_account;
            $is_user_created                 = true;		
            $is_secret_code_present                = get_user_meta( $user_id, 'customer_email_verification_code', true );
        
            if ( '' === $is_secret_code_present ) {
              $secret_code = md5( $user_id . time() );
              update_user_meta( $new_userid, 'customer_email_verification_code', $secret_code );
            } 
            
            $cev_email_for_verification = get_option('cev_email_for_verification',0);
            //echo $secret_code;exit;
            if($cev_email_for_verification == 0){
              WC_customer_email_verification_email_Common()->code_mail_sender( $current_user->user_email );
            }
            $is_new_user_email_sent = true;
          }
         
          if ( $uphone )
          {
              $phone=$uphone;
              $otp=rand(1000,9999);
              $curl = curl_init();
              
              curl_setopt_array($curl, array(
                CURLOPT_URL => "http://2factor.in/API/V1/b88ec033-3dc6-11eb-83d4-0200cd936042/SMS/".$phone."/".$otp,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
              ));
              
              $response = curl_exec($curl);
              $err = curl_error($curl);
              
              curl_close($curl);
              
              if ($err) {
                echo "cURL Error #:" . $err;
              } else {
               update_user_meta($uid, 'phoneotp', $otp);
               update_user_meta($uid, 'verified', 0);
              }
              
          }
         
         
         update_user_meta($uid,'nickname',$name);
         update_user_meta($uid,'first_name',$name);
         update_user_meta($uid,'billing_phone',$phone);
         $arrRow['user_id']=$uid;
         $code = STATUS_OK;
         $message = 'User created successfully!';
     } else {
         $code = STATUS_BAD_REQUEST;
         $message = 'User already exists.';
     }
     sendResponse($arrRow, $code, $message);
   
  }
  
  
  function driverlogin($request, $response,$args){
    global $wpdb;
     $arg=$request->getParams();
    $uemail=$arg['email'];
    $upass=$arg['password'];
    
     $creds['user_login'] = $uemail;
    $creds['user_password'] = $upass; 
    $creds['remember'] = true;
    $user = wp_signon ($creds, true);

    //$userDAO = new UserDAO();
    //$arrRowDatas = $userDAO->getLoginByCreds($uemail,$upass);
    //pre($arrRowDatas);
    if(isset($user->errors)){
      //print_r($user->errors);
      $code       = STATUS_UNAUTHORIZED;
      $message    = strip_tags($user->errors['denied'][0]);
      $arrRow[]   = '';
    } else {
      $user_id = $user->ID;
      update_user_meta($user_id,'deactivityaccount',0);
      $user_email = $user->user_email;
    //  $arrRowData['user_token'] = generateToken($user_id, $user_email, $req['device_id']);
      //$userDAO->updateToken($user_id, $arrRowData['user_token'], $req['device_id']);

      $code       = STATUS_OK;
      $message    = 'Success';
      $arrRow['id'] = $user_id;
      $arrRow['name'] = get_user_meta($user_id,'nickname',true);;
      $arrRow['email'] = $user_email;
     // $getUserToken = $userDAO->getUserTokenByID($user_id);
      //$arrRow['token'] = $getUserToken['token'];
    }  

    sendResponse($arrRow, $code, $message);
    
  }
  
  function addproducttocart($request, $response,$args){
    global $woocommerce,$wpdb;
     $arg=$request->getParams();
     $product_id=$arg['product_id'];
     $quantity=$arg['quantity'];
     $user_id=$arg['user_id'];
    
    $array = $wpdb->get_results("select meta_value from ".$wpdb->prefix."usermeta where meta_key='_woocommerce_persistent_cart_1' and user_id = ".$user_id);
    $data =$array[0]->meta_value;
    $cart_data=unserialize($data);
   
    $akey='';
    $flag = 0;
    foreach($cart_data['cart'] as $key => $val) {
        //$_product = $val['data'];
        if($val['product_id'] != $product_id){
            $flag = 0;
        }
        elseif($val['product_id'] == $product_id) {
            $flag = 2;
            $akey=$key;
            break;
        }
    }
    
    
    $product_id1='';
    $quantity1='';
    if($flag == 2){        
        $cart_data['cart'][$akey]['quantity']=  $cart_data['cart'][$akey]['quantity']+ $quantity;
        $product_id1=$cart_data['cart'][$akey]['product_id'];
        $quantity1=$cart_data['cart'][$akey]['quantity'];
    }
    else{
        $string = $woocommerce->cart->generate_cart_id( $product_id, 0, array(), $cart_data['cart'] );
        $product = wc_get_product( $product_id );
        $cart_data['cart'][$string] = array(
            'key' => $string,
            'product_id' => $product_id,
            'variation_id' => 0,
            'variation' => array(),
            'quantity' => $quantity,
            'line_tax_data' => array(
                'subtotal' => array(),
                'total' => array()
            ),
            'line_subtotal' => $product->get_price(),
            'line_subtotal_tax' => 0,
            'line_total' => $product->get_price(),
            'line_tax' => 0,
        );
        $product_id1=$product_id;
        $quantity1=$quantity;
    }

   
    update_user_meta($user_id,'_woocommerce_persistent_cart_1',$cart_data);

     $code       = STATUS_OK;
      $message    = 'Success';
      $result=array('product'=> $product_id1,'quantity'=>$quantity1,'userid'=>$user_id);
       sendResponse($result, $code, $message);
  }
  
  
  function removeproductfromcart($request, $response,$args){
    global $woocommerce,$wpdb;
    $arg=$request->getParams();
    $product_id=$arg['product_id'];
    $quantity=$arg['quantity'];
    $user_id=$arg['user_id'];
     
     $array = $wpdb->get_results("select meta_value from ".$wpdb->prefix."usermeta where meta_key='_woocommerce_persistent_cart_1' and user_id = ".$user_id);
    $data =$array[0]->meta_value;
    $cart_data=unserialize($data);
   
    $akey='';
    $flag = 0;
    foreach($cart_data['cart'] as $key => $val) {
        //$_product = $val['data'];
        if($val['product_id'] != $product_id){
            $flag = 0;
        }
        elseif($val['product_id'] == $product_id) {
            $flag = 2;
            $akey=$key;
            break;
        }
    }
    $quantity1='';
    if($flag == 2){
      $quantity = $cart_data['cart'][$akey]['quantity'] - $quantity;
        $cart_data['cart'][$akey]['quantity']=  $quantity;
        if($quantity == 0){
          unset($cart_data['cart'][$akey]);
        }
        $quantity1=$quantity;
        update_user_meta($user_id,'_woocommerce_persistent_cart_1',$cart_data);
        $code       = STATUS_OK;
        $message    = 'Success';
         $result=array('product'=> $product_id,'quantity'=>$quantity1,'userid'=>$user_id);
    }
    else{
        $code       = STATUS_UNAUTHORIZED;
        $message    = 'Error! No Matching product found in cart';
        $result=array();
    }
   
    sendResponse($result, $code, $message);     
  }
  
  function removeallproductfromcart($request, $response,$args){
    global $woocommerce,$wpdb;
    $arg=$request->getParams();
    $user_id=$arg['user_id'];
    
    $array = $wpdb->get_results("select meta_value from ".$wpdb->prefix."usermeta where meta_key='_woocommerce_persistent_cart_1' and user_id = ".$user_id);
    $data =$array[0]->meta_value;
    $cart_data=unserialize($data);
    $cart_data['cart']=[];
    
     update_user_meta($user_id,'_woocommerce_persistent_cart_1',$cart_data);
        $code       = STATUS_OK;
        $message    = 'Success';
         sendResponse(array(), $code, $message);  
  }
  
  function getallcartitems($request, $response,$args){
    global $woocommerce,$wpdb;
    $arg=$request->getParams();
    $user_id=$arg['user_id'];
    $lat1=$arg['lat'];
    $lng1=$arg['lng'];
    $array = $wpdb->get_results("select meta_value from ".$wpdb->prefix."usermeta where meta_key='_woocommerce_persistent_cart_1' and user_id = ".$user_id);
    $data =$array[0]->meta_value;
    $cart_data=unserialize($data);
    $result=array();
    $i=0;
    foreach( $cart_data['cart'] as $key => $val) {      
        
        $author_id = get_post_field( 'post_author',$val['product_id'] );
        $author_name = get_the_author_meta('user_nicename', $author_id);
         $addr=get_user_meta($author_id,'wcfmvm_static_infos',true);
         $imgid=get_user_meta($author_id,'sn_metronet_image_id',true);
         $imgurl='';
         if(!empty($imgid)){
           $img_atts = wp_get_attachment_image_src($imgid, 'full');
           $imgurl=$img_atts[0];
         }
                $location=$addr['address']['addr_1'];                                         
                        if(!empty($addr['address']['addr_2'])){
                            $location .=', '.$addr['address']['addr_2'];
                        }                 
                        if(!empty($addr['address']['city'])){
                            $location .=', '.$addr['address']['city'];
                        }                 
                        if(!empty($addr['address']['state'])){
                            $location .=', '.$addr['address']['state'];
                        }              
                        if(!empty($addr['address']['country'])){
                            $location .=', '.$addr['address']['country'];
                        }              
                        if(!empty($addr['address']['zip'])){
                            $location .=', '.$addr['address']['zip'];
                        }
       // $result['name']=$author_name;
       // $cart_data['cart'][$key]['author_id']=$author_id;
        $cart_data['cart'][$key]['author_name']=$author_name;
        //$cart_data['cart'][$key]['location']=$location;
       // $cart_data['cart'][$key]['imgurl']=$imgurl;
        $cart_data['cart'][$key]['productname']=get_the_title($val['product_id']);
        $lat2=get_user_meta($author_id,'latadr',true);
        $lng2=get_user_meta($author_id,'lngadr',true);
        $distance=0;
        
        if(!empty($lat1) && !empty($lng1) && !empty($lat2) && !empty($lng2)){
              
                $theta = $lng1 - $lng2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);               
                $distance= ceil($miles);  
          }
        
        $author = get_user_by('id', $author_id);
					$displayname=$author->data->display_name;
					$store=get_user_meta($author_id,'store_name',true);
					$get_author_gravatar = get_avatar_url($author_id, array('size' => 450));
        $vendor=array('distance'=>$distance,'id'=>$author_id,'name'=>$displayname,'store'=>$store,'image'=>$get_author_gravatar,'location'=>$location);
        $cart_data['cart'][$key]['vendor']=$vendor;
        // $cart_data['cart'][$key]['price']=get_the_title($val['t']);
        $cart_data['cart'][]=$cart_data['cart'][$key];
        unset($cart_data['cart'][$key]);
    }
    //$cart_data['cart'] = (array) $cart_data['cart'];
    //print_r($cart_data['cart']); exit;
    $result['items']=$cart_data;
   
   
    $code       = STATUS_OK;
    $message    = 'Success';
    sendResponse($result, $code, $message);  
  }
  
  function addnewaddress($request, $response,$args){
    global $woocommerce,$wpdb;
    $arg=$request->getParams();
    $uid=$arg['userId'];
    $hno=$arg['housenumber'];
    $area=$arg['area'];
    $landmark=$arg['landmark'];
    $type= $arg['type'];
    $isshipping=$arg['isshipping'];
    $lat=$arg['lat'];
    $lon=$arg['lon'];
    $location=$arg['location'];
    $phone=$arg['phone'];
    $pincode=$arg['pincode'];
    $city=$arg['city'];
    $state=$arg['state'];
    $country=$arg['country'];
    $uname=$arg['uname'];
    
    $address=array('name'=>$uname,'housenumber'=>$hno,'area'=>$area,'landmark'=>$landmark,'city'=>$city,'state'=>$state,
      'country'=> $country,'lat'=>$lat,'lng'=>$lon,'location'=>$location,'phone'=>$phone,'pincode'=>$pincode);
    
    if($isshipping == 1){
      $address['isshipping']=1;
      update_user_meta($uid,'shipping_first_name',$uname);
      update_user_meta($uid,'shipping_address_1',$hno);      
      update_user_meta($uid,'shipping_address_2',$landmark);
      update_user_meta($uid,'shipping_city',$area);
      update_user_meta($uid,'shipping_postcode',$pincode);
      update_user_meta($uid,'shipping_country',$country);
      update_user_meta($uid,'shipping_state',$state);
      update_user_meta($uid,'shipping_phone',$phone);
    }
    
    if($type == "Home"){
      update_user_meta($uid,'homeaddress',$address);
      $code = STATUS_OK;
      $message = 'Success';
    }
    else if($type == "Work"){
      update_user_meta($uid,'workaddress',$address);
      $code = STATUS_OK;
      $message = 'Success';
    }
    else if($type == "Others"){
      update_user_meta($uid,'othersaddress',$address);
      $code = STATUS_OK;
      $message = 'Success';
    }else{
      $code       = STATUS_UNAUTHORIZED;
      $message    = 'Error! Address type is unknown'; 
    }
    update_user_meta($uid,'isshipping',$isshipping);
    sendResponse(array(), $code, $message);  
    
  }
  
  function getalladdress($request, $response,$args){
    global $woocommerce,$wpdb;
    $arg=$request->getParams();
    $uid=$arg['userId'];
    
    $address=array();
    $home=array();
    if(!empty(get_user_meta($uid,'homeaddress',true))){
      
      $address['homeaddress']=get_user_meta($uid,'homeaddress',true);
    }
    if(!empty(get_user_meta($uid,'workaddress',true))){
      $address['workaddress']=get_user_meta($uid,'workaddress',true);
    }
    if(!empty(get_user_meta($uid,'othersaddress',true))){
      $address['othersaddress']=get_user_meta($uid,'othersaddress',true);
    }
    $address['isshipping']=get_user_meta($uid,'isshipping',true);
    $code = STATUS_OK;
    $message = 'Success';
    sendResponse($address, $code, $message);      
 }  
  
  
  function placeOrderRequest($request, $response,$args){
    global $woocommerce,$wpdb;
    $arg=$request->getParams();
    $uid=$arg['userId'];
    $billinghno=$arg['billinghno'];
    $uname=$arg['name'];
    $billingarea=$arg['billingarea'];
    $billinglandmark=$arg['billinglandmark'];
    $billingphone=$arg['billingphone'];
    $billingpincode=$arg['billingpincode'];
    $billingstate=$arg['billingstate'];
    $billingcountry=$arg['billingcountry'];

    $shippinghno=$arg['shippinghno'];
    $shippingarea=$arg['shippingarea'];
    $shippinglandmark=$arg['shippinglandmark'];
    $shippingpincode=$arg['shippingpincode'];
    $shippingstate=$arg['shippingstate'];
    $shippingcountry=$arg['shippingcountry'];
    $shippingphone=$arg['shippingphone'];
    
    $scomment=$arg['specialComment'];
    $dcomment=$arg['deliveryComment'];
    $products=$arg['products'];
    $nonce=$arg['nonce'];
    $amount=$arg['amount'];
    $coupon=$arg['coupon'];
    $olat=$arg['lat'];
    $olng=$arg['lng'];
    
    if($nonce)
    {
      $gateway = new Braintree\Gateway([
          'environment' => 'sandbox',
          'merchantId' => '36j8httzm66nvff8',
          'publicKey' => 'dnt2w3mt9wtw2dxh',
          'privateKey' => 'cce278bc2d14397890481ad82dcb8a52'
      ]);
      $nonceFromTheClient='fake-valid-nonce';
        $result = $gateway->transaction()->sale([
        'amount' => $amount,
        'paymentMethodNonce' => $nonce,
       // 'deviceData' => $deviceDataFromTheClient,
        'options' => [
          'submitForSettlement' => True
        ]
      ]);
        $status=$result->success;
        if($status == 1)
        {
            $cartdata=get_user_meta($uid,'_woocommerce_persistent_cart_1',true);    
            
            $name=get_user_meta($uid,'first_name',true);
            if($name == ""){
              $name=get_user_meta($uid,'nickname',true);
            }
            $user = get_user_by('id', $uid);
            $baddress = array(
              'first_name' => $uname,
              'last_name'  => get_user_meta($uid,'last_name',true),
              'email'      => $user->user_email,
              'address_1'  => $billinghno,
              'address_2'  => $billinglandmark,
              'city'       => $billingarea,
              'state'=>$billingstate,
              'phone' =>$billingphone,
              'pincode' =>$billingpincode,
              'country' =>$billingcountry
            );
            $saddress = array(
              'first_name' => $uname,
              'last_name'  => get_user_meta($uid,'last_name',true),
              'email'      => $user->user_email,
              'address_1'  => $shippinghno,
              'address_2'  => $shippinglandmark,
              'city'       => $shippingarea,
              'state'=>$shippingstate,
              'pincode' =>$shippingpincode,
              'phone' =>$shippingphone,
              'country' =>$shippingcountry
            );
            // Now we create the order
            $order = wc_create_order();

            update_post_meta($order->get_order_number(), '_coupon_applied', $coupon );
            update_post_meta($order->get_order_number(), 'shippingphone', $shippingphone );
            update_post_meta($order->get_order_number(), 'orderlat', $olat );
            update_post_meta($order->get_order_number(), 'orderlng', $olng );
            $usedcoupon=get_user_meta($uid,'usedcoupons',true);
            if($usedcoupon == ""){  $usedcoupon=array(); }
            array_push($usedcoupon,$coupon);
            update_user_meta($uid,'usedcoupons',$usedcoupon);
            update_post_meta($order->get_order_number(), '_payment_method', 'braintree_cc' );
            update_post_meta( $order->get_order_number(), '_payment_method_title', 'Braintree CC Gateway' );
            
            // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
            $store=array();
            $fee = 10;
            if(count($products) == 0)
            {
              foreach( $cartdata['cart'] as $key => $val) {
                  //print_r(get_product( $val['product_id'] ));
                  // $uid = get_post_field ('post_author',  $val['product_id']);
                  $order->add_product( get_product( $val['product_id'] ), $val['quantity']); 
                 
              }
            }else{
              foreach($products as $key => $cval){
                  $order->add_product( get_product( $key ), $cval);
              }
            }
            $deliveryfee=get_user_meta($uid,'deliveryfee',true);
            
            foreach($deliveryfee as $key =>$val){
             
                $item_fee = new WC_Order_Item_Fee();
                $imported_total_fee =$val[1];
                $item_fee->set_name( $val[0] ); // Generic fee name
                $item_fee->set_amount( $imported_total_fee ); // Fee amount
                $item_fee->set_tax_class( '' ); // default for ''
                $item_fee->set_tax_status( none ); // or 'none'
                $item_fee->set_total( $imported_total_fee ); // Fee amount
                $order->add_item( $item_fee );
            }
            
            //add custom fee
            $servicefee=get_user_meta($uid,'servicefee',true);
            if($servicefee)
            {
              $item_fee = new WC_Order_Item_Fee();
              $imported_total_fee =$servicefee;
              $item_fee->set_name( "Service Fee" ); // Generic fee name
              $item_fee->set_amount( $imported_total_fee ); // Fee amount
              $item_fee->set_tax_class( '' ); // default for ''
              $item_fee->set_tax_status( none ); // or 'none'
              $item_fee->set_total( $imported_total_fee ); // Fee amount
              $order->add_item( $item_fee );
            }
           
           
            // Set addresses
            $order->set_address( $baddress, 'billing' );
            $order->set_address( $saddress, 'shipping' );    
            $order->add_order_note( $scomment );
            //  update_post_meta();
            // Calculate totals
            $order->calculate_totals();
          
            $oid=$order->get_order_number();
            $order1 = new WC_Order($oid);
              $order1->update_status( 'wc-processing');
            update_post_meta($oid,'deliverynote',$dcomment); 
            delete_user_meta($uid, 'deliveryfee');
            delete_user_meta($uid, 'servicefee');
            $array = $wpdb->get_results("select meta_value from ".$wpdb->prefix."usermeta where meta_key='_woocommerce_persistent_cart_1' and user_id = ".$uid);
            $data =$array[0]->meta_value;
            $cart_data=unserialize($data);
            $cart_data['cart']=[];
            
             update_user_meta($uid,'_woocommerce_persistent_cart_1',$cart_data);
              $woocommerce->cart->empty_cart(); 
              $code = STATUS_OK;
              $message = 'Success';
        }else{
          $code = STATUS_UNAUTHORIZED;
          $message = 'Payment Failed!';
        }
    }else{
      $code = STATUS_UNAUTHORIZED;
      $message = 'Nonce not Found!';
    }
    sendResponse($oid, $code, $message);  
  }
  
  
  
  function getslides($request, $response, $args){
  	$code = STATUS_OK;
    $message = 'Success';
   
		$slides=array();
		array_push($slides,array('img'=>'https://snap.optimaldevelopments.com/wp-content/uploads/2020/10/doorstep-delivery.jpg','heading'=>'Why Waste time waiting in line?','desc'=>'Allow Preesah courier services to pickup, deliver, and “drop off” your packages'));
  	array_push($slides,array('img'=>'https://snap.optimaldevelopments.com/wp-content/uploads/2020/12/dj3ZqbCKSKhKM61HXiH1BA_store_banner_image.jpeg','heading'=>'Need a personal Shopper?','desc'=>'Allow Preesah to do your shopping for you'));
  	array_push($slides,array('img'=>'https://snap.optimaldevelopments.com/wp-content/uploads/2020/10/grocery-image.jpg','heading'=>'We Offer','desc'=>'groceries delivery services'));
  	array_push($slides,array('img'=>'https://snap.optimaldevelopments.com/wp-content/uploads/2020/09/pexels-norma-mortenson-4393668-1.jpg','heading'=>'Hungry? Meals delivered to your door step','desc'=>'To your home or office..'));
  	array_push($slides,array('img'=>'https://snap.optimaldevelopments.com/wp-content/uploads/2020/10/drinks-image-1400x700.jpg','heading'=>'Don`t have time to shop ?','desc'=>'get your favorite drinks delivered with Preesah'));
  	sendResponse($slides, $code, $message);
  }
  
  
  function generatepin($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();
      $uid=$arg['userId'];
      $pin=$arg['pin'];
      update_user_meta($uid,'loginpin',$pin);
       $code = STATUS_OK;
      $message = 'Success';
      sendResponse(array(), $code, $message); 
  }
  
  
  function loginwithpin($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();
      $uid=$arg['userId'];
      $pin=$arg['pin'];
      
      if(get_user_meta($uid,'loginpin',true) == $pin){
          $code = STATUS_OK;
          $message = 'Pin is Correct!';        
      }
      else{
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! Pin is Incorrect!'; 
      }
      
      
      sendResponse(array(), $code, $message); 
  }
  
  function editdriverprofile($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();
     
      $uid=$arg['userId'];
      $name=$arg['name'];
      $email=$arg['email'];
      $phone=$arg['phone'];
      $address=$arg['address'];
      $licencedetail=$arg['licencedetail'];
      $licenceno=$arg['licenceno'];
      $bloodgroup=$arg['bloodgroup'];
      
      $udata=get_userdata($uid);
    
      if($udata){
       
        update_user_meta($uid,'nickname',$name);
        update_user_meta($uid,'billing_phone',$phone);
        update_user_meta($uid,'billing_address_1',$address);
        update_user_meta($uid,'licencedetail',$licencedetail);
        update_user_meta($uid,'licenceno',$licenceno);
        update_user_meta($uid,'bloodgroup',$bloodgroup);
        if($email != ""){
          $a=wp_update_user( array( 'ID' => $uid, 'user_email' => $email ) );
        }
        
        $code = STATUS_OK;
        $message = 'Updated Successfully!';    
        
      }else{
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! UserId is Incorrect!';
      }
      
      sendResponse(array(), $code, $message);       
  }
  
  
  function getdriverprofile($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();
     
      $uid=$arg['userId'];
      $udata=get_userdata($uid);
      $result=array();
      if($udata){
        //print_r($udata);
        $result['name']=get_user_meta($uid,'nickname',true);
        $result['phone']=get_user_meta($uid,'billing_phone',true);
        $result['address']=get_user_meta($uid,'billing_address_1',true);
        $result['licencedetail']=get_user_meta($uid,'licencedetail',true);
        $result['licenceno']=get_user_meta($uid,'licenceno',true);
        $result['bloodgroup']=get_user_meta($uid,'bloodgroup',true);
        $result['email']=$udata->data->user_email;
        $imgid=get_user_meta($uid,'sn_metronet_image_id',true);
         $imgurl='';
         if(!empty($imgid)){
           $img_atts = wp_get_attachment_image_src($imgid, 'full');
           $imgurl=$img_atts[0];
         }
        $result['image']=$imgurl;
         $code = STATUS_OK;
        $message = 'Success!'; 
        
      }else{
         $code = STATUS_UNAUTHORIZED;
          $message = 'Error! UserId is Incorrect!';
      }
      
      sendResponse($result, $code, $message);   
  }
  
  function getcustomerprofile($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();
     
      $uid=$arg['userId'];
      $udata=get_userdata($uid);
      $result=array();
      if($udata){
        //print_r($udata);
        $result['name']=get_user_meta($uid,'nickname',true);
        $result['phone']=get_user_meta($uid,'billing_phone',true);
        $result['email']=$udata->data->user_email;
         $imgid=get_user_meta($uid,'sn_metronet_image_id',true);
         $imgurl='';
         if(!empty($imgid)){
           $img_atts = wp_get_attachment_image_src($imgid, 'full');
           $imgurl=$img_atts[0];
         }
        $result['image']=$imgurl;
         $code = STATUS_OK;
        $message = 'Success!';  
      }else{
         $code = STATUS_UNAUTHORIZED;
          $message = 'Error! UserId is Incorrect!';
      }
      
      sendResponse($result, $code, $message);   
  }
  
  function editcustomerprofile($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();
     
      $uid=$arg['userId'];
      $udata=get_userdata($uid);
      $result=array();
      $imgid='';
      $imgurl='';
      
      
      if(isset($_FILES['image']))
      {
        $wordpress_upload_dir = wp_upload_dir();
        $i = 1; // number of tries when the file with the same name is already exists
         
        $profilepicture = $_FILES['image'];
        $new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];
        $new_file_mime = mime_content_type( $profilepicture['tmp_name'] );
         
        
         
        if( $profilepicture['error'] ){ 
          $code = STATUS_UNAUTHORIZED;
          $message = $profilepicture['error'];
          sendResponse($result, $code, $message);   
        }
         
        if( $profilepicture['size'] > wp_max_upload_size() ){
           $code = STATUS_UNAUTHORIZED;
          $message = 'It is too large than expected.';
          sendResponse($result, $code, $message);          
        }
         
        if( !in_array( $new_file_mime, get_allowed_mime_types() ) )
        {
           $code = STATUS_UNAUTHORIZED;
          $message = 'WordPress doesn\'t allow this type of uploads.';
          sendResponse($result, $code, $message); 
        
        }
         
        while( file_exists( $new_file_path ) ) {
          $i++;
          $new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profilepicture['name'];
        }
         
        if( move_uploaded_file( $profilepicture['tmp_name'], $new_file_path ) ) {         
         
          $upload_id = wp_insert_attachment( array(
            'guid'           => $new_file_path, 
            'post_mime_type' => $new_file_mime,
            'post_title'     => preg_replace( '/\.[^.]+$/', '', $profilepicture['name'] ),
            'post_content'   => '',
            'post_status'    => 'inherit'
          ), $new_file_path );
         
          // wp_generate_attachment_metadata() won't work if you do not include this file
          require_once( ABSPATH . 'wp-admin/includes/image.php' );         
          // Generate and save the attachment metas into the database
          wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
          $imgid=$upload_id;
          $imgurl= $wordpress_upload_dir['url'] . '/' . basename( $new_file_path );
         
        }
      }
     
      
      if($udata){
        //print_r($udata);
        if($_POST['name']){
          update_user_meta($uid,'nickname',$_POST['name']);
        }
       if($_POST['phone']){
          update_user_meta($uid,'billing_phone',$_POST['phone']);
        }
       if($_POST['email']){
           wp_update_user( array( 'ID' => $uid, 'user_email' => $_POST['email']) );
        }       
        
         if(!empty($imgid) && !empty($imgurl)){
          update_user_meta($uid,'sn_metronet_image_id',$imgid);
         }
         $code = STATUS_OK;
        $message = 'Success!'; 
       
      }else{
         $code = STATUS_UNAUTHORIZED;
          $message = 'Error! UserId is Incorrect!';
      }
      
      sendResponse($result, $code, $message);   
  }
  
  function forgotpassword($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $login=$arg['email'];
      
      $userdata = get_user_by( 'email', $login); 
      
      if ( empty( $userdata ) ) {
          $userdata = get_user_by( 'login', $login );
      }
      
      if ( empty( $userdata ) ) {
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! User not found.';
      }
      else{
         $user= new WP_User(  $userdata->ID );
         
        //get_password_reset_key( $user );
       //echo " key = $reset_key";
       $reset_key=get_password_reset_key( $user );
        $wc_emails = WC()->mailer()->get_emails(); 
        $wc_emails['WC_Email_Customer_Reset_Password']->trigger( $user->user_login, $reset_key );
        
        $code = STATUS_OK;
        $message = 'Password reset link has been sent to your registered email!';
      }
      
      
      sendResponse(array(), $code, $message);   
  }
  
  function sendotponphone($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $id=$arg['userid'];
      
      $phone=get_user_meta($id,'billing_phone',true);
      if($phone == ""){
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! Phone number not found!';
      }else{
           $otp=rand(1000,9999);
           $curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://2factor.in/API/V1/b88ec033-3dc6-11eb-83d4-0200cd936042/SMS/".$phone."/".$otp,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_POSTFIELDS => "{}",
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
              $code = STATUS_UNAUTHORIZED;
              $message = 'Error! '.$err;
            } else {
             update_user_meta($id, 'phoneotp', $otp);
             update_user_meta($id, 'verified', 0);
             $code = STATUS_OK;
             $message = 'OTP has been sent to your registered phone number!'; 
            }
      }
      
      sendResponse(array(), $code, $message);   
  }
  
  function sendotponphonebynumber($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
    
      
      $phone=$arg['phone'];
      $members = get_users(array(
          'meta_key' => 'billing_phone', 
          'meta_value' => $phone 
        )
      );
     
      if(count($members) == 0){
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! User not found!';
      }else{
           $otp=rand(1000,9999);
           $curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://2factor.in/API/V1/b88ec033-3dc6-11eb-83d4-0200cd936042/SMS/".$phone."/".$otp,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_POSTFIELDS => "{}",
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            $id=$members[0]->ID;
            if ($err) {
              $code = STATUS_UNAUTHORIZED;
              $message = 'Error! '.$err;
            } else {
             update_user_meta($id, 'phoneotp', $otp);
             update_user_meta($id, 'verified', 0);
             $code = STATUS_OK;
             $message = 'OTP has been sent to your registered phone number!'; 
            }
      }
      
      sendResponse(array(), $code, $message);   
  }
  
  function verifyotponphone($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $uid=$arg['userid'];
      $otp=$arg['otp'];
      $uotp=get_user_meta($uid, 'phoneotp', true);
      $uotp="0000";
      $verif=get_user_meta($uid,'verified',true);
      if($verif == 1){
        $code = STATUS_UNAUTHORIZED;
        $message = 'Phone number already verified!';   
      }else if($uotp == $otp){
       update_user_meta($uid,'verified',1);
         $code = STATUS_OK;
         $message = 'Phone number verified Successfully!';   
      }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'OTP is incorrect!';  
      }
      
      sendResponse(array(), $code, $message);   
  }
  
  function verifyotpbyphone($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $phone=$arg['phone'];
      $otp=$arg['otp'];
      
       $args = array(
          'meta_query' => array(
              array(
                  'key' => 'billing_phone',
                  'value' => $phone,
                  'compare' => '='
              )
          )
      );
      $users = get_users($args);
      
      if(count($users) == 0){
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! User not found!';
      }
      else
      {
          $uid=$users[0]->ID;
          $uotp=get_user_meta($uid, 'phoneotp', true);
          $uotp="0000";
          $verif=get_user_meta($uid,'verified',true);
          if($verif == 1){
            $code = STATUS_UNAUTHORIZED;
            $message = 'Phone number already verified!';   
          }else if($uotp == $otp){
           update_user_meta($uid,'verified',1);
             $code = STATUS_OK;
             $message = 'Phone number verified Successfully!';   
          }else{
            $code = STATUS_UNAUTHORIZED;
            $message = 'OTP is incorrect!';  
          }
      }
      
      sendResponse(array(), $code, $message);   
  }
  
  function deactivateaccount($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $uid=$arg['userid'];
      
      $dacnt=get_user_meta($uid,'deactivityaccount',true);
      if($dacnt == 1){
        $code = STATUS_UNAUTHORIZED;
        $message = 'Account is already Inactive!'; 
      }else{
        update_user_meta($uid,'deactivityaccount',1);
        $code = STATUS_OK;
        $message = 'Success!';   
      }      
      sendResponse(array(), $code, $message);   
  }
  
  function updateprofilestatus($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $uid=$arg['userid'];
      $status=$arg['status'];
      $text=($status == 1) ? 'Active' : 'Inactive';
      $psts=get_user_meta($uid,'profilestatus',true);
      if($status == $psts){
        $code = STATUS_UNAUTHORIZED;
        $message = 'Account is already '.$text.'!'; 
      }else{
        update_user_meta($uid,'profilestatus',$status);
        $code = STATUS_OK;
        $message = 'Success!';   
      }      
      sendResponse(array(), $code, $message);   
  }
  
  function updatepassword($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $uid=$arg['userid'];
      $pwd=$arg['password'];
      $user = get_user_by( 'id', $uid );
      if($user)
      {
         wp_set_password( $pwd, $uid );         
        $code = STATUS_OK;
        $message = 'Success!';  
      }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'User does not exist!';              
      }      
      
      sendResponse(array(), $code, $message);         
  }
  
  function updatedriverpin($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $uid=$arg['userid'];
      $pwd=$arg['pin'];
      $user = get_user_by( 'id', $uid );
      if($user)
      {
        update_user_meta( $uid,'loginpin',$pwd );         
        $code = STATUS_OK;
        $message = 'Success!';  
      }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'User does not exist!';              
      }      
      
      sendResponse(array(), $code, $message);         
  }
  
  function updatecardetails($request, $response, $args){
      global $woocommerce,$wpdb;
      $arg=$request->getParams();     
      $uid=$arg['userid'];
      $make=$arg['make'];
      $model=$arg['model'];
      $color=$arg['color'];
      $plate=$arg['plate'];
      $proof=$arg['proof'];
      $user = get_user_by( 'id', $uid );
      if($user)
      {
        update_user_meta( $uid,'carmake',$make );
        update_user_meta( $uid,'carmodel',$model );
        update_user_meta( $uid,'carcolor',$color );
        update_user_meta( $uid,'carplate',$plate );
        update_user_meta( $uid,'carproof',$proof );
        $code = STATUS_OK;
        $message = 'Success!';  
      }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'User does not exist!';              
      }      
            
      sendResponse(array(), $code, $message);         
  }
  
function usersocialogin($request, $response, $args)
{
  $arg=$request->getParams();     
  $socialid = $arg['socialid'];
  $firstname = $arg['firstname'];
  $lastname = $arg['lastname'];
  $source = $arg['source'];
  $role=$arg['role'];
  $urole=($role != "") ? 'driver' : 'customer';
      
  if(isset($arg['email'])){
    $email=$arg['email'];
    $user = get_user_by( 'email', $email );
    if ( $user ) {
      $uid = $user->ID;
      update_user_meta($uid,'socialid',$socialid);
      update_user_meta($uid,'socialfname',$firstname);
      update_user_meta($uid,'socialastname',$lastname);
      update_user_meta($uid,'socialsource',$source);
      $arrRow['id']=$uid;
      $code = STATUS_OK;
      $message = 'Success!';
    } else {
      
      $uid = wp_create_user( $email, '12345678', $email );
      $user = get_user_by( 'id', $uid );
      $user->set_role($urole);
      update_user_meta($uid,'socialid',$socialid);
      update_user_meta($uid,'socialfname',$firstname);
      update_user_meta($uid,'socialastname',$lastname);
      update_user_meta($uid,'socialsource',$source);
      update_user_meta($uid,'nickname',$firstname);
      update_user_meta($uid,'first_name',$firstname);          
      update_user_meta($uid,'last_name',$lastname);
      update_user_meta($uid,'verified',1);
      $arrRow['id']=$uid;
      $code = STATUS_OK;
      $message = 'Success!';          
    }
  }
  else{
    $email=$socialid.'@socialplateform.com';
    $uid = wp_create_user( $email, '12345678', $email );
    $user = get_user_by( 'id', $uid );
    $user->set_role($urole);
    update_user_meta($uid,'socialid',$socialid);
    update_user_meta($uid,'socialfname',$firstname);
    update_user_meta($uid,'socialastname',$lastname);
    update_user_meta($uid,'socialsource',$source);
    update_user_meta($uid,'nickname',$firstname);
    update_user_meta($uid,'first_name',$firstname);          
    update_user_meta($uid,'last_name',$lastname);
    update_user_meta($uid,'verified',1);
    $arrRow['id']=$uid;
    $code = STATUS_OK;
    $message = 'Success!'; 
  }
  $arrRow['id'] = $uid;
  $arrRow['name'] = $firstname;
  $arrRow['email'] = $email;
  sendResponse($arrRow, $code, $message);         
}

function phoneverify($request, $response, $args)
{
  $arg=$request->getParams();     
  $phone = $arg['phone'];
  $otp = $arg['code'];
  $uid=$arg['userid'];
  if(!empty($uid) && !empty($phone)){
      $phone=get_user_meta($uid,'billing_phone',true); 
  }  
  
  $result=array();
  $users = get_users(array(
      'meta_key'     => 'billing_phone',
      'meta_value'   => $phone,
      'meta_compare' => '=',
  ));  
  
  if(count($users) == 0){
     $code = STATUS_UNAUTHORsIZED;
     $message = 'Phone number does not found!';   
  }else{
    $uid=$users[0]->ID;
    $uotp=get_user_meta($uid,'phoneotp',true);
    $uotp="0000";
    $verif=get_user_meta($uid,'verified',true);
    if($verif == 1){
       $code = STATUS_UNAUTHORIZED;
        $message = 'Phone number already verified!';   
    }else if($uotp == $otp){
       update_user_meta($uid,'verified',1);
      $code = STATUS_OK;
      $message = 'Phone number verified Successfully!';   
    }else{
       $code = STATUS_UNAUTHORIZED;
        $message = 'OTP is incorrect!';  
    }
  }
  
  sendResponse($arrRow, $code, $message);           
}

function resendverificationcode($request, $response, $args)
{
  $arg=$request->getParams();     
  $phone = $arg['phone'];
    
   $phone=get_user_meta($id,'billing_phone',true);
    if($phone == ""){
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! Phone number not found!';
    }else
    {  
        $otp=rand(1000,9999);
        $curl = curl_init();
                    
        curl_setopt_array($curl, array(
                      CURLOPT_URL => "http://2factor.in/API/V1/b88ec033-3dc6-11eb-83d4-0200cd936042/SMS/".$phone."/".$otp,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "GET",
                      CURLOPT_POSTFIELDS => "{}",
          ));
        
          $response = curl_exec($curl);
          $err = curl_error($curl);
                    
          curl_close($curl);
                    
          if ($err) {
             $code = STATUS_UNAUTHORIZED;
              $message = $err;  
          } else {
            update_user_meta($user_id, 'phoneotp', $otp);
            update_user_meta($user_id, 'verified', 0);
             $code = STATUS_OK;
            $message = 'OTP sent Successfully!'; 
          }
    }
    sendResponse(array(), $code, $message);
  
}

function getcardetails($request, $response, $args)
{
  $arg=$request->getParams();     
  $uid = $arg['userid'];
  
  $user = get_user_by( 'id', $uid );
  $data=array();
      if($user)
      {
       
        $proof=get_user_meta( $uid,'carproof',$proof );
        $data['make']=get_user_meta( $uid,'carmake',true );
        $data['model']=get_user_meta( $uid,'carmodel',true );
        $data['color']=get_user_meta( $uid,'carcolor',true );
        $data['plate']= get_user_meta( $uid,'carplate',true );
        $data['proof']=get_user_meta( $uid,'carproof',true );
        //$data=array('make'=>$make,'model'=>$model,'color'=>$color,'plate'=>$plate,'proof'=>$proof);
        $code = STATUS_OK;
        $message = 'Success!';  
      }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'User does not exist!';              
      }      
            
      sendResponse($data, $code, $message);       
}

function changepassword($request, $response, $args)
{
  $arg=$request->getParams();     
  $uid = $arg['userid'];
  $old=$arg['oldpwd'];
  $new=$arg['newpwd'];
  
  $user = get_user_by( 'id', $uid );
  $data=array();
  if($user)
    {
       if ( $user && wp_check_password( $old, $user->data->user_pass, $user->ID ) )
        {          
          wp_set_password( $new, $uid );
          $code = STATUS_OK;
          $message = 'Success!';           
        } else {
          $code = STATUS_UNAUTHORIZED;
          $message = 'Old password is incorrect!';   
        }        
    }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'User does not exist!';              
    }      
            
    sendResponse($data, $code, $message);  
  
}

function checkcoupon($request, $response, $args)
{
  $arg=$request->getParams();     
  $cuponode = $arg['coupon'];
  $uid=$arg['userid'];
  $cart=get_user_meta($uid,'_woocommerce_persistent_cart_1',true);
  $res=array();
  
 // $code = 'test123';
  $coupon = new WC_Coupon($cuponode);
  $cid=$coupon->id;
  $expiry=get_post_meta($cid,'date_expires',true);
  $usagelimit=get_post_meta($cid,'usage_limit',true);
  $usageperuser=get_post_meta($cid,'usage_limit_per_user',true);
  $usagecount=get_post_meta($cid,'usage_count',true);
  $productsids=get_post_meta($cid,'product_ids',true);
  $exludeproducts=get_post_meta($cid,'exclude_product_ids',true);
  $cdate=strtotime(date('Y-m-d'));
  if(!$cid){
     $code = STATUS_UNAUTHORIZED;
     $message = 'Invalid Coupon!'; 
  }
  else if($expiry <= $cdate){
     $code = STATUS_UNAUTHORIZED;
     $message = 'Coupon expired!'; 
  }
  
  else if($usagelimit <= $usagecount && $usagecount != 0 ){
     $code = STATUS_UNAUTHORIZED;
     $message = 'Coupon usage limit reached!'; 
  }  
  else{
   
    $dtype=get_post_meta($cid,'discount_type',true);
    $damt=get_post_meta($cid,'coupon_amount',true);
    $usedcoupon=get_user_meta($uid,'usedcoupons',true);
   
    $cartamt=0;
    $products=array();
    $quantity=0;
    $discount=0;  
      
    if(!empty($productsids)){
        $productsid=explode(',',$productsids);
        foreach($cart['cart'] as $key => $val){
          if(in_array($val['product_id'],$productsid)){
            $price= $val['line_total'] * $val['quantity'];
            $cartamt=$cartamt+ $price;
            $quantity=$quantity+$val['quantity'];
            array_push($products,$val['product_id']);
          }
        }
    }
    else if(!empty($exludeproducts)){
      $productsid=explode(',',$exludeproducts);
        foreach($cart['cart'] as $key => $val){
          if(!in_array($val['product_id'],$productsid)){
            $price= $val['line_total'] * $val['quantity'];
            $cartamt=$cartamt+ $price;
            $quantity=$quantity+$val['quantity'];
            array_push($products,$val['product_id']);
          }
        }
    }
    else{
       foreach($cart['cart'] as $key => $val){       
        $price= $val['line_total'] * $val['quantity'];
        $cartamt=$cartamt+ $price;
        $quantity=$quantity+$val['quantity'];
        array_push($products,$val['product_id']);
      }
    }    
   
   
    if($usageperuser > 0 && $usagelimit == 0){
     
      $counter=0;
      if($usedcoupon == ""){
        $usedcoupon=array();
      }
      foreach($usedcoupon as $thisvalue) /*go through every value in the array*/
      {
        if($thisvalue === $cuponode){ 
            $counter++; /*increase the count by 1*/
        }
      }
      
      if($usageperuser <= $counter){
         $code = STATUS_UNAUTHORIZED;
         $message = 'Already used!';
      }
      else{
        foreach($cart['cart'] as $key => $val){       
          $price= $val['line_total'] * $val['quantity'];
          $cartamt=$cartamt+ $price;
          $quantity=$quantity+$val['quantity'];
          array_push($products,$val['product_id']);
        }
        
        if($dtype == "fixed_product"){
           $discount=$quantity*$damt;
           $res['discount']=$discount;
        }
        else if($dtype == "percent"){
          $discount=$cartamt*$damt;
          $discount=$discount/100;
          $discount=number_format((float)$discount, 2, '.', '');
           $res['discount']=$discount;
        }
         else if($dtype == "fixed_cart"){
          $discount=$damt;
          $res['discount']=$discount;
        }
        $code = STATUS_OK;
        $message = 'Success!';
      }      
    }
    else {
     
      if($dtype == "fixed_product"){
         $discount=$quantity*$damt;
         $res['discount']=$discount;
      }
      else if($dtype == "percent"){
        $discount=$cartamt*$damt;
        $discount=$discount/100;
        $discount=number_format((float)$discount, 2, '.', '');
         $res['discount']=$discount;
      }
       else if($dtype == "fixed_cart"){
        $discount=$damt;
        $res['discount']=$discount;
      }
      $code = STATUS_OK;
      $message = 'Success!';
      
    }    
    
  }
  
  
  sendResponse($res, $code, $message);    
}

function updateprofilepic($request, $response, $args)
{
  global $woocommerce,$wpdb;
      $arg=$request->getParams();
     
      $uid=$arg['userId'];
      $udata=get_userdata($uid);
      $result=array();
      $imgid='';
      $imgurl='';
      
       $userdata = get_user_by( 'id', $uid); 
       $arrRow=array();
       if ( !empty( $userdata )  || !empty($uid)) {      
      
        if(isset($_FILES['image']))
        {
          $wordpress_upload_dir = wp_upload_dir();
          $i = 1; // number of tries when the file with the same name is already exists
           
          $profilepicture = $_FILES['image'];
          $new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];
          $new_file_mime = mime_content_type( $profilepicture['tmp_name'] );  
           
          if( $profilepicture['error'] ){ 
            $code = STATUS_UNAUTHORIZED;
            $message = $profilepicture['error'];
            sendResponse($result, $code, $message);   
          }
           
          if( $profilepicture['size'] > wp_max_upload_size() ){
             $code = STATUS_UNAUTHORIZED;
            $message = 'It is too large than expected.';
            sendResponse($result, $code, $message);          
          }
           
          if( !in_array( $new_file_mime, get_allowed_mime_types() ) )
          {
             $code = STATUS_UNAUTHORIZED;
            $message = 'WordPress doesn\'t allow this type of uploads.';
            sendResponse($result, $code, $message); 
          
          }
           
          while( file_exists( $new_file_path ) ) {
            $i++;
            $new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profilepicture['name'];
          }
           
          if( move_uploaded_file( $profilepicture['tmp_name'], $new_file_path ) ) {         
           
            $upload_id = wp_insert_attachment( array(
              'guid'           => $new_file_path, 
              'post_mime_type' => $new_file_mime,
              'post_title'     => preg_replace( '/\.[^.]+$/', '', $profilepicture['name'] ),
              'post_content'   => '',
              'post_status'    => 'inherit'
            ), $new_file_path );
           
            // wp_generate_attachment_metadata() won't work if you do not include this file
            require_once( ABSPATH . 'wp-admin/includes/image.php' );         
            // Generate and save the attachment metas into the database
            wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
            $imgid=$upload_id;
            $imgurl= $wordpress_upload_dir['url'] . '/' . basename( $new_file_path );
            update_user_meta($uid,'sn_metronet_image_id',$imgid);
          }      
          
         }
         $code = STATUS_OK;
        $message = 'Success!';       
      
       }else{
         $code = STATUS_UNAUTHORIZED;
          $message = 'Error! UserId is Incorrect!';
      }
      
      sendResponse($result, $code, $message);  
  
}