<?php 
class Firebase {

    public function send($notification_message,$firebase_registration_key,$title='Himalaya',$click_action='http://localhost:3000/',$icon_url='https://test.lazymonkey.in/web-push-demo/images/icon.png') {
        $param = array('data'=>array(
                        'title' => $title,
                        'body'  => $notification_message,
                        'icon'  => $icon_url,
                    ),
            'click_action'  => $click_action,
            'to'            => $firebase_registration_key
        );
        return $this->sendPushNotification($param);
    }    
    /*
    * This function will make the actuall curl request to firebase server
    * and then the message is sent 
    */
    private function sendPushNotification($fields) {        
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        //building headers for the request
        $headers = array(
            'Authorization: key=AAAAc-qxR8c:APA91bHu-klgnQypcRqos0edEgdOYc3zjtqOrk15dJk-3HsJSruvS7xEuWhTzTfTsVmeouYkNygD7xtgRrlv0AjOdolta9ekTQMH0MS90HKpu644FBNjCgUZO1e8Z9Zpm6gKX7DELW3C',
            'Content-Type: application/json'
        );

        //Initializing curl to open a connection
        $ch = curl_init(); 
        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);        
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);
        //adding headers 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        //adding the fields in json format 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        //finally executing the curl request 
        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        //Now close the connection
        curl_close($ch);
 
        //and return the result 
        return $result;
    }

}