<?php
/* 
Author - Vipin Singh
Title - woocommerce library 
Version - 1.1
Date -  02-11-2020
*/
require_once('../wp-load.php');
  function getCustomerOrders($request, $response, $args) {
    $code       = STATUS_OK;
    $message    = 'Success';
  	$httpObj   = getWoocommerceObject();
    $req       = $request->getParams();
  	$orderid   = $req['orderid'];
  	if((int)$orderid > 0) {		
  		$arrRow = $httpObj->orders->get($orderid)->order;	
  	}else{
  		$arrRow = $httpObj->customers->get_orders($args['user_id'])->orders;
  	}	
  	sendResponse($arrRow, $code, $message);
  }

  /*function getWooCustomerInfo($request, $response, $args){	
  	$httpObj = getWoocommerceObject();
  	$arrRow = $httpObj->customers->get($args['user_id']);	
  	sendResponse($arrRow->customer);
  }

  function updateWooCustomerAddress($request, $response, $args){	
  	
   $req = $request->getParams();
  	$httpObj = getWoocommerceObject();
  	$customer = $httpObj->customers->get($args['user_id']);
  	//pre($customer);
      $customer->customer->shipping_address->first_name = $req['firstname'];
      $customer->customer->shipping_address->last_name  = $req['lastname'];
      $customer->customer->shipping_address->company 	  = $req['company'];
      $customer->customer->shipping_address->address_1  = $req['address_1'];
      $customer->customer->shipping_address->address_2  = $req['address_2'];
      $customer->customer->shipping_address->city 	  = $req['city'];
      $customer->customer->shipping_address->state 	  = $req['state'];
      $customer->customer->shipping_address->postcode   = $req['postcode'];
      $customer->customer->shipping_address->country 	  = 'IN';
      $customer->customer->shipping_address->email 	  = $req['email'];
      $customer->customer->shipping_address->phone	  = $req['phone'];

      $arrRow = $httpObj->customers->update($customer->customer->id,(array)$customer);
  	sendResponse($arrRow);
  }*/



  function getProductCategory($request, $response, $args){
    $req = $request->getParams();
    $httpObj = getWoocommerceObject();
    $arrRow = $httpObj->products->get_categories()->product_categories;
    sendResponse($arrRow); 
  }



  function placeOrderCustomer($request, $response, $args){   
    $req = $request->getParams();
    $userDAO = new UserDAO(); 

    $response = $userDAO->getWalletBalance($args['user_id']);

    $httpObj 	= getWoocommerceObject();
    $customer = $httpObj->customers->get($args['user_id'])->customer;  
    

    $lines = array();
    $i=0;
    $line = $req['line_items'];
    foreach($line as $key => $val){
    	$lines[$i]['product_id'] 	= $val['product_id'];
    	$lines[$i]['name'] 			= $val['name'];
    	$lines[$i]['quantity'] 		= $val['quantity'];
    	$i++;
    }

    $data = array(
        		'payment_method' 		=> 'Wallet payment',
        		'payment_method_title' 	=> 'Wallet payment',
        		'set_paid' 				=> true,
        		'status'				=> 'processing',
        		'customer_id' 			=> $args['user_id'],
        		'billing_address' 	=> array(
            		'firs_tname' 	=> $customer->billing_address->first_name,
            		'last_name' 	    => $customer->billing_address->last_name,
            		'address_1' 	=> $customer->billing_address->address_1,
            		'address_2' 	=> $customer->billing_address->address_2,
            		'city' 			=> $customer->billing_address->city,
            		'state' 		=> $customer->billing_address->state,
            		'postcode' 		=> $customer->billing_address->postcode,
            		'country' 		=> $customer->billing_address->country,
            		'email' 		=> $customer->billing_address->email,
            		'phone' 		=> $customer->billing_address->phone
        		),
        		'shipping_address' 	=> array(
            		'first_name' 	=> $customer->shipping_address->first_name,
            		'last_name' 	=> $customer->shipping_address->last_name,
            		'address_1' 	=> $customer->shipping_address->address_1,
            		'address_2' 	=> $customer->shipping_address->address_2,
            		'city' 			=> $customer->shipping_address->city,
            		'state' 		=> $customer->shipping_address->state,
            		'postcode' 		=> $customer->shipping_address->postcode,
            		'country' 		=> $customer->shipping_address->country
        		),
        		'line_items' 	=> $lines,        		
    		);
   	
    $arrRow = $httpObj->orders->create($data);
    if((int)$arrRow->order->order_number > 0 && $arrRow->order->status == "completed"){
    	$WooInfo = $userDAO->getWalletBalance($args['user_id']);
    	$TotalDebit = $arrRow->order->total;
    	$Balance = $WooInfo->balance;
    	$remaining = $Balance - $TotalDebit;
    	$details = 'For order payment from mobile app #'.$arrRow->order->order_number;
    	$userDAO->updateUserBalance($args['user_id'],$TotalDebit,$remaining,'debit',$details);
    }
    sendResponse($arrRow->order); 
  }

  function cancelOrder($request, $response, $args){
    $req = $request->getParams();
    $userDAO = new UserDAO();
    $orderid = $req['orderid'];
    $httpObj = getWoocommerceObject();
    $arrRow = $httpObj->orders->update_status( $orderid, 'cancelled' );
    if ($arrRow->order->status == 'cancelled') {
      //$WooInfo = $userDAO->getWalletBalance($args['user_id']);
      $WooInfo = $userDAO->getUserBalance($args['user_id']);
      $remainBal = $arrRow->order->subtotal;
      $Balance = $WooInfo['balance'];
      $totalBal = floor($Balance) + floor($remainBal);

      $details = 'For Cancel Order from mobile app #'.$arrRow->order->order_number;
      $userDAO->updateUserBalance($args['user_id'],$remainBal,$totalBal,'credit',$details);
    }

    sendResponse($arrRow);
  }
  
  function getcustomerorderss($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $userid = $req['user_id'];
     $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_value' => $userid,
        'post_type' => wc_get_order_types(),
        'post_status' => array_keys(wc_get_order_statuses()), 'post_status' => array('wc-processing','wc-completed'),
    ));

    $Order_Array = []; 
    foreach ($customer_orders as $customer_order) {
        $orderq = wc_get_order($customer_order);  
        $Order_Array[] = [
            "id" => $orderq->get_id(),
            "price" => $orderq->get_total(),
            "status" => $orderq->get_status(),
            "date" => $orderq->get_date_created()->date_i18n('Y-m-d')
        ];
    }
      $code       = STATUS_OK;
      $message    = 'Success';
      $arrRow=$Order_Array;
    
   sendResponse($arrRow, $code, $message);       
  }
  
  function makereorder($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $orderid = $req['orderid'];
    $uid = $req['userid'];
    
    $userdata = get_user_by( 'id', $uid); 
      $arrRow=array();
    if ( !empty( $userdata )  || $uid != "") {
   
        // print_r($customer_orders);  
           
            $order = wc_get_order($orderid);
            $cart_data=array();
            // Iterating through each WC_Order_Item_Product objects
            foreach ($order->get_items() as $item_key => $item ):
            
                // Item ID is directly accessible from the $item_key in the foreach loop or
                $item_id = $item->get_id();
            
              
                //print_r($product);
                $product_id   = $item->get_product_id(); 
                $quantity     = $item->get_quantity();  
                
                $item_data    = $item->get_data();    
                $product_name = $item_data['name'];
                $product_id   = $item_data['product_id'];
                $variation_id = $item_data['variation_id'];
                $quantity     = $item_data['quantity'];
                $tax_class    = $item_data['tax_class'];
                $line_subtotal     = $item_data['subtotal'];
                $line_subtotal_tax = $item_data['subtotal_tax'];
                $line_total        = $item_data['total'];
                $line_total_tax    = $item_data['total_tax'];
            
                 
                $product        = $item->get_product();     
                $product_type   = $product->get_type();
                $product_sku    = $product->get_sku();
                $product_price  = $product->get_price();
                $stock_quantity = $product->get_stock_quantity();
                $string = $woocommerce->cart->generate_cart_id( $product_id, 0, array(), $cart_data['cart'] );
                $cart_data['cart'][$string] = array(
                    'key' => $string,
                    'product_id' => $product_id,
                    'variation_id' => $variation_id,
                    'variation' => array(),
                    'quantity' => $quantity,
                    'line_tax_data' => array(
                        'subtotal' => array(),
                        'total' => array()
                    ),
                    'line_subtotal' => $product->get_price(),
                    'line_subtotal_tax' => $item_data['subtotal_tax'],
                    'line_total' => $product->get_price(),
                    'line_tax' => $item_data['total_tax']
                );
            
            endforeach;
            update_user_meta($uid,'_woocommerce_persistent_cart_1',$cart_data);
             $code       = STATUS_OK;
            $message    = 'Success';
             $arrRow=$cart_data;
      }else{
          $code = STATUS_UNAUTHORIZED;
          $message = 'Error! User not found!';
      }   
     
    
   sendResponse($arrRow, $code, $message);       
  }
  
  function getallopenorders($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $uid = $req['userid'];
    $lat1 = $req['lat'];
    $lon1 = $req['lng'];
    if(!$lat1 || $lat1 == ""){
       $lat1=get_user_meta($uid,'addlat',true);
    }
    if(!$lon1 || $lon1 == ""){
       $lon1=get_user_meta($uid,'addlng',true);
    }   
    $rejorders= get_user_meta( $uid, '_rejected_orders', true );
    $rids=array();
    for($i=0;$i<count($rejorders);$i++){
      array_push($rids,$rejorders[$i]['orderid']);
    }
    if($lat1 != "" && $lon1 != ""){
      
      $customer_orders = get_posts(array(
        'numberposts' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'post_type' => wc_get_order_types(),
        'exclude'=>$rids,
        'post_status' => array_keys(wc_get_order_statuses()), 'post_status' => array('wc-processing'),
      ));
      
      $Order_Array = []; 
      foreach ($customer_orders as $customer_order) {
          $orderq = wc_get_order($customer_order);
          $oid= $orderq->get_id();
          $lat2=get_post_meta($oid,'addresslat',true);
          $lon2=get_post_meta($oid,'addresslng',true);
         
          $unit='M';
          
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);
        
            
              $km=$miles;
              
              if($km <= 50){
                $Order_Array[] = [
                    "id" => $orderq->get_id(),
                    "price" => $orderq->get_total(),
                     "status" => $orderq->get_status(),
                    "date" => $orderq->get_date_created()->date_i18n('Y-m-d')
                ];
              }              
            
            /* else if ($unit == "N") {
              print ($miles * 0.8684);
            } else {
              print $miles;
            } */
          
  
      }
    
      $code       = STATUS_OK;
      $message    = 'Success';
      $arrRow=$Order_Array;
      
    }else{
      $code = STATUS_UNAUTHORIZED;
      $message = 'Please Enter your Address First';
    }
   sendResponse($arrRow, $code, $message);   
  }
  
  function changeorderstatus($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $uid = $req['userid'];
    $oid = $req['orderid'];
    $status = $req['status'];
    $statusdesc = $req['statusdesc'];
    
    $user= get_post_meta( $oid, '_customer_user', true );
    $rejorders= get_user_meta( $uid, '_rejected_orders', true );
    if(empty($rejorders)){  $rejorders =array(); }
    
    $udata=get_userdata($uid);
    if(!$udata){
       $code = STATUS_UNAUTHORIZED;
       $message = 'Error! UserId is Incorrect!';
    }
    else if(get_post_type($oid) != "shop_order")
    {
       $code = STATUS_UNAUTHORIZED;
       $message = 'Order does not exist!';
    }
    else{      
      $order = new WC_Order($oid);
      $order_status  = $order->get_status();
    
      if($order_status == $status){
        $code = STATUS_UNAUTHORIZED;
        $message = 'Order status is already Changed!';
      }
      else if (!empty($order)) {
          
          
          
          if($status == "rejected"){
            $dt=array('orderid'=>$oid,'date'=>date('d-m-Y h:i:s'),'desc'=>$statusdesc);
            array_push($rejorders,$dt);
            update_user_meta($uid,'_rejected_orders',$rejorders);
          }else{
            $order->update_status($status );
            update_post_meta($oid,'driveruser',$uid);
            update_post_meta($oid,'modifiedstatus',date('d-m-Y'));
            update_post_meta($oid,'modifiedstatus1',date('d-m-Y h:i:s'));
            update_post_meta($oid,'statusdesc',$statusdesc);
          }
          $code = STATUS_OK;
          $message = 'Updated Successfully!';        
        
      }else{
        $code = STATUS_UNAUTHORIZED;
        $message = 'Order does not exist!';
      }
      
    }      
      sendResponse(array(), $code, $message);  
  }
  
  
  function getalldriverorders($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $uid = $req['userid'];
    $status=$req['status'];
      $Order_Array = []; 
    $udata=get_userdata($uid);
    if(!$udata){
       $code = STATUS_UNAUTHORIZED;
       $message = 'Error! UserId is Incorrect!';
    }
    else{
          $rejorders= get_user_meta( $uid, '_rejected_orders', true );
          $rids=array();
          for($i=0;$i<count($rejorders);$i++){
            array_push($rids,$rejorders[$i]['orderid']);
          }
          $rids=array_unique($rids);
         
      if($status == "wc-rejected"){
                  
           $customer_orders = get_posts(array(
            //'numberposts' => -1,
            'include' =>array($rids),
             'numberposts' => -1,
            'orderby' => 'date',
            'order' => 'DESC',        
            'post_type' => wc_get_order_types(),
             'post_status' => 'any', 
          ));
             
      }else{
          $customer_orders = get_posts(array(
            'numberposts' => -1,
            'orderby' => 'date', 'exclude' =>$rids,
            'order' => 'DESC','meta_key'=>'driveruser','meta_value'=>$uid,        
            'post_type' => wc_get_order_types(),
           'post_status' => array($status), 
          ));
      }
    
      foreach ($customer_orders as $customer_order) {
          $orderq = wc_get_order($customer_order);
          $oid= $orderq->get_id();
          $shipping = array(
                'first_name' => $orderq->get_shipping_first_name(),
                'last_name' => $orderq->get_shipping_last_name(),
                'company' => $orderq->get_shipping_company(),
                'address_1' => $orderq->get_shipping_address_1(),
                'address_2' => $orderq->get_shipping_address_2(),
                'city' => $orderq->get_shipping_city(),
                'state' => $orderq->get_shipping_state(),
                'formated_state' => WC()->countries->states[$orderq->get_shipping_country()][$orderq->get_shipping_state()], //human readable formated state name
                'postcode' => $orderq->get_shipping_postcode(),
                'country' => $orderq->get_shipping_country(),
                'formated_country' => WC()->countries->countries[$orderq->get_shipping_country()] //human readable formated country name
            );
          $rating=get_post_meta($orderq->get_id(),'driveraing',true);
          $comment=get_post_meta($orderq->get_id(),'drivercomment',true);
          $datetime=get_post_meta($orderq->get_id(),'modifiedstatus1',true);
            $date=date_format(date_create($datetime),"j/M/Y");
            $time=date_format(date_create($datetime),"g:iA");
          $Order_Array[] = [
                    "id" => $orderq->get_id(),
                    "price" => $orderq->get_total(),
                     "status" => $orderq->get_status(),
                     "address" =>$shipping,
                     'rating'=>$rating,
                     'comment' =>$comment,
                     'time' => $time,
                    "date" => $date
          ];                     
         
          $code = STATUS_OK;
          $message = 'Success!';   
  
      }
    }
    
      sendResponse($Order_Array, $code, $message);  
  }
  
  function gettodayorders($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $uid = $req['userid'];
      $Order_Array = []; 
    $udata=get_userdata($uid);
    if(!$udata){
       $code = STATUS_UNAUTHORIZED;
       $message = 'Error! UserId is Incorrect!';
    }
    else{
      
    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'     => 'driveruser',
                'value'   => $uid,
                'compare' => '=',
            ),
            array(
                'key'     => 'modifiedstatus',
                'compare' => '=',
                'value'   => date('d-m-Y')
            ),
        ),       
        'post_type' => wc_get_order_types(),
        'post_status' => 'any'
      ));
  
     
      foreach ($customer_orders as $customer_order) {
          $orderq = wc_get_order($customer_order);
          $oid= $orderq->get_id();
          $shipping = array(
                'first_name' => $orderq->get_shipping_first_name(),
                'last_name' => $orderq->get_shipping_last_name(),
                'company' => $orderq->get_shipping_company(),
                'address_1' => $orderq->get_shipping_address_1(),
                'address_2' => $orderq->get_shipping_address_2(),
                'city' => $orderq->get_shipping_city(),
                'state' => $orderq->get_shipping_state(),
                'formated_state' => WC()->countries->states[$orderq->get_shipping_country()][$orderq->get_shipping_state()], //human readable formated state name
                'postcode' => $orderq->get_shipping_postcode(),
                'country' => $orderq->get_shipping_country(),
                'phone' =>get_post_meta($orderq->get_id(),'_billing_phone',true),
                'lat' =>get_post_meta($orderq->get_id(),'orderlat',true),
                'lng' =>get_post_meta($orderq->get_id(),'orderlng',true),
                'formated_country' => WC()->countries->countries[$orderq->get_shipping_country()] //human readable formated country name
            );
          $rating=get_post_meta($orderq->get_id(),'driveraing',true);
          $comment=get_post_meta($orderq->get_id(),'drivercomment',true);
          $datetime=get_post_meta($orderq->get_id(),'modifiedstatus1',true);
          $date=date_format(date_create($datetime),"j/M/Y");
          $time=date_format(date_create($datetime),"g:iA");
          $Order_Array[] = [
                    "id" => $orderq->get_id(),
                    "price" => $orderq->get_total(),
                     "status" => $orderq->get_status(),
                     "address" =>$shipping,
                     'rating'=>$rating,
                     'comment' =>$comment,
                     'time' => $time,
                    "date" => $date
          ];                     
                         
         
          $code = STATUS_OK;
          $message = 'Success!';     
      }
    }    
      sendResponse($Order_Array, $code, $message);  
  }
  
  function getorderdetails($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $oid = $req['orderid'];
    $orders=array();
    
     $odata = $wpdb->get_results("select * from ".$wpdb->prefix."wcfm_marketplace_orders where order_id = ".$oid);
      for($i=0;$i<count($odata);$i++){
        $vid=$odata[$i]->vendor_id;
        $user = get_userdata($vid);
        $name=$user->data->display_name;
        $orders['products'][$vid]=array('storename'=>$name,'items'=>array());
      }
    
    
    if(get_post_type($oid) != "shop_order")
    {
       $code = STATUS_UNAUTHORIZED;
       $message = 'Order does not exist!';
    }
    else{      
      $order = new WC_Order($oid);
     // print_r($order);
      
      $status=$order->get_status();
      $uid=$order->get_user_id();
      $date=$order->get_date_created()->date('Y-m-d H:i:s');
      $products=array();
     
      foreach ($order->get_items() as $item_key => $item ):
          
          $product      = $item->get_product(); // Get the WC_Product object
      
          $product_id   = $item->get_product_id(); // the Product id
      
          ## Access Order Items data properties (in an array of values) ##
          $item_data    = $item->get_data();
      
          $product_name = $item_data['name'];
          $product_id   = $item_data['product_id'];
          $variation_id = $item_data['variation_id'];
          $quantity     = $item_data['quantity'];
          $tax_class    = $item_data['tax_class'];
          $line_subtotal     = $item_data['subtotal'];
          $line_subtotal_tax = $item_data['subtotal_tax'];
          $line_total        = $item_data['total'];
          $line_total_tax    = $item_data['total_tax'];
          $post_author_id = get_post_field( 'post_author', $product_id );
          $user = get_userdata($post_author_id);
          $sname=$user->data->display_name;
          $da=array('vendor'=>$post_author_id,'storename'=>$sname,'id'=>$product_id,'name'=>$product_name,'quantity'=>$quantity,'total'=>$line_total,'total_tax'=>$line_total_tax);
          $orders['products'][$post_author_id]['items']=$da;
         
      endforeach;
      $oj=0;
      foreach($orders['products'] as $key => $val){
        unset($orders['products'][$key]);
        $orders['products'][$oj]=$val;
        $oj++;
      }
      
      //$orders['products'][$post_author_id]=array_values( $orders['products'][$post_author_id]);
      $driver=get_post_meta($oid,'driveruser',true);
      $driverdetails=array('id'=>$driver,'name'=>get_user_meta($driver,'first_name',true),'phone'=>get_user_meta($driver,'billing_phone',true));
      $billing= array(
                'first_name' => $order->get_billing_first_name(),
                'last_name' => $order->get_billing_last_name(),
                'company' => $order->get_billing_company(),
                'address_1' => $order->get_billing_address_1(),
                'address_2' => $order->get_billing_address_2(),
                'city' => $order->get_billing_city(),
                'state' => $order->get_billing_state(),
                'formated_state' => WC()->countries->states[$order->get_billing_country()][$order->get_billing_state()], //human readable formated state name
                'postcode' => $order->get_billing_postcode(),
                'country' => $order->get_billing_country(),
                'formated_country' => WC()->countries->countries[$order->get_billing_country()], //human readable formated country name
                'email' => $order->get_billing_email(),
                'phone' => $order->get_billing_phone()
            );
      $shipping = array(
                'first_name' => $order->get_shipping_first_name(),
                'last_name' => $order->get_shipping_last_name(),
                'company' => $order->get_shipping_company(),
                'address_1' => $order->get_shipping_address_1(),
                'address_2' => $order->get_shipping_address_2(),
                'city' => $order->get_shipping_city(),
                'state' => $order->get_shipping_state(),
                'phone'=>get_post_meta($oid,'shipping_phone',true),
                'formated_state' => WC()->countries->states[$order->get_shipping_country()][$order->get_shipping_state()], //human readable formated state name
                'postcode' => $order->get_shipping_postcode(),
                'country' => $order->get_shipping_country(),
                'formated_country' => WC()->countries->countries[$order->get_shipping_country()] //human readable formated country name
            ); 
      $total = wc_format_decimal($order->get_total());
      $tax = wc_format_decimal($order->get_total_tax());
      $payment_method = $order->get_payment_method();
      $orders['deliveryfee']=array();
      foreach( $order->get_items('fee') as $item_id => $item_fee ){
        
            // The fee name
             $fee_name = $item_fee->get_name();
        
            // The fee total amount
             $fee_total = $item_fee->get_total();
        
            // The fee total tax amount
           // print $fee_total_tax = $item_fee->get_total_tax();
            
            if (strpos($fee_name, 'Delivery') !== false){
              array_push( $orders['deliveryfee'],array($fee_name=>$fee_total));
            }
            else if (strpos($fee_name, 'Service') !== false){
              $orders['servicefee']=$fee_total;
            }
            else{
               $orders[$fee_name]=$fee_total;
            }
        }
    //  print_r($order->get_fees());
      
      $orders['id']=$oid;
      $orders['total']=$total;
      $orders['driver']=$driverdetails;
      $orders['userid']=$uid;
      $orders['status']=$status;
      $orders['date']=$date;
   //   $orders['products']=$products;
      $orders['shipping_address']=$shipping;
      $orders['billing_address']=$billing;
      $orders['paymentmethod']=$payment_method;
      $orders['distance']='';
      $orders['estimatedtime']='';
      $orders['rating']='';
      $orders['comment']='';
    
       $code = STATUS_OK;
       $message = 'Success!';
    }
      sendResponse($orders, $code, $message);  
    
  }
  
  function getorderinvoice($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $oid = $req['orderid'];
    $upload_basedir = WP_CONTENT_DIR . '/uploads';
    $filename = $upload_basedir.'/pdf-invoices/'.$oid.'.pdf';
    $url='';
     if(get_post_type($oid) != "shop_order")
    {
       $code = STATUS_UNAUTHORIZED;
       $message = 'Order does not exist!';
    }
    else{   
      if (file_exists($filename)) {
        $code = STATUS_OK;
        $message = 'Success!';
        $url=get_option('siteurl').'/wp-content/uploads/pdf-invoices/'.$oid.'.pdf';;
      } else {
        $code = STATUS_UNAUTHORIZED;
        $message = 'Invoice does not exist!';
      }
    }
     sendResponse($url, $code, $message);  
    
  }
  
  
  function submitorderreviews($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $oid = $req['orderid'];
    $type= $req['type'];
    $id=$req['id'];
    $uid=$req['userid'];
    $rating= $req['rating'];
    $comment= $req['comment'];
    $vid= $req['vendorid'];    
    $author_obj = get_user_by('id', $uid);
    $uemail=$author_obj->user_email;
    $uname=get_user_meta($uid,'nickname',true);
    
    if ( FALSE === get_post_status( $oid ) ) {
      
     $code = STATUS_UNAUTHORIZED;
     $message = 'Order does not exist!';
      
    } else {
      if($type == "product"){
        
        $comment_id = wp_insert_comment( array(
            'comment_post_ID'      => $id, // <=== The product ID where the review will show up
            'comment_author'       => $uname,
            'comment_author_email' => $uemail,
            'comment_content'      => $comment,
            'comment_parent'       => 0,
            'user_id'              => $uid, 
            'comment_date'         => date('Y-m-d H:i:s'),
            'comment_approved'     => 0,
        ) );
        
        // HERE inserting the rating (an integer from 1 to 5)
        update_comment_meta( $comment_id, 'rating', $rating );
        update_comment_meta( $comment_id, 'commentype', 'product' );
        update_comment_meta( $comment_id, 'orderid', $oid);
      }
      else if($type == "vendor"){
        $table=$wpdb->prefix."wcfm_marketplace_reviews";
         $wpdb->insert($table, array('vendor_id' => $vid, 'author_id' => $uid, 'author_name' => $uname,
            'author_email' => $uemail, 'review_description' => $comment, 'review_rating' => $rating, 'approved' => 1)); 
        $rid = $wpdb->insert_id;
        $wpdb->insert($wpdb->prefix."wcfm_marketplace_review_rating_meta", array('review_id' => $rid, 'key' => 'orderid', 'value' => $oid,'type' => 'order')); 
        
      }
      else{
        update_post_meta($oid,'drivercomment',$comment);
        update_post_meta($oid,'driveraing',$rating);
      }
      $code = STATUS_OK;
      $message = 'Submitted';
    }
     sendResponse(array(), $code, $message);  
  }

  function getorderreview($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $oid = $req['orderid'];
    $type= $req['type'];
    $data=array();
    if ( FALSE === get_post_status( $oid ) ) {
      
     $code = STATUS_UNAUTHORIZED;
     $message = 'Order does not exist!';
      
    } else {
      
      if($type == "product"){
        
        $comments = get_comments (array ('meta_key'=> 'orderid', 'meta_value'=> $oid));
        for($i=0;$i<count($comments);$i++){
          $con=array('product'=>$comments[$i]->comment_post_ID,'content'=>$comments[$i]->comment_content,'rating'=>get_comment_meta($comments[$i]->comment_ID,'rating',true));
          array_push($data,$con);
        }        
      }
      else if($type == "vendor"){
        $table=$wpdb->prefix."wcfm_marketplace_reviews";
         $table1=$wpdb->prefix."wcfm_marketplace_review_rating_meta";
        $q="SELECT * FROM $table a,$table1 b WHERE  b.value = '$oid' and b.review_id=a.ID";
        $data1 = $wpdb->get_row( $q);
       $data=array('comment'=>$data1->review_description,'rating'=>$data1->review_rating);
      
        
      }
      else{
        $data=array('comment'=>get_post_meta($oid,'drivercomment',true),'rating'=>get_post_meta($oid,'driveraing',true));
      
      }
        $code = STATUS_OK;
      $message = 'Submitted';
    
    }
    sendResponse($data, $code, $message);     
    
  }
  
  
  function calculatedeliverycharges($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $user_id = $req['userid'];
    $lat1 = $req['lat'];
    $lng1 = $req['lng'];
    
     $store=array();
     $fee = 10;
     $feearr=array('delivery'=>array());
    
    $array = $wpdb->get_results("select meta_value from ".$wpdb->prefix."usermeta where meta_key='_woocommerce_persistent_cart_1' and user_id = ".$user_id);
    $data =$array[0]->meta_value;
    $cart_data=unserialize($data);
    $result=array('delivery'=>array());
    $i=0;
    $stores=array();
    foreach( $cart_data['cart'] as $key => $val) {      
        $pid=$val['product_id'];
        $uid = get_post_field( 'post_author',$val['product_id'] );
         if(!in_array($uid,$store)){
            array_push($store, $uid);
           
           $lat=get_user_meta($uid,'latadr',true);
           $lng=get_user_meta($uid,'lngadr',true);
           
           if($lat == ""){ $lat=get_user_meta($uid,'_wcfm_store_lat',true);   }
           if($lng == ""){ $lng=get_user_meta($uid,'_wcfm_store_lng',true);   }
           if($lat1 !="" && $lng1 != ""){
            $latitude=$lat1;
            $longitude=$lng1;
           }
           else{
              $shippingaddr=get_user_meta($user_id,'shipping_address_1',true).','.get_user_meta($user_id,'shipping_address_2',true).','.get_user_meta($user_id,'shipping_city',true).','.get_user_meta($user_id,'shipping_state',true).','.get_user_meta($user_id,'shipping_country',true).','.get_user_meta($user_id,'shipping_postcode',true);
              
              $prepAddr = str_replace(' ','+',$shippingaddr);
              $purl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDaTbMlsuLMz_TVworPJoVf8VvAS2G8Z2c&libraries=places&address='.$prepAddr.'&sensor=false&language=en';
              $geocode=file_get_contents($purl);
              $output= json_decode($geocode);   
              $latitude = $output->results[0]->geometry->location->lat; 
              $longitude = $output->results[0]->geometry->location->lng;
              $distance=0;
            }
           if(!$latitude  || !$longitude){
              $code = STATUS_UNAUTHORIZED;
              $message = 'Address does not exist!';
           }else{
            
           
           if($lat != "" && $lng != "" && $latitude != "" && $longitude != ""){
                $unit='M';
                $theta = $lng - $longitude;
                $dist = sin(deg2rad($lat)) * sin(deg2rad($latitude)) +  cos(deg2rad($lat)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);
              
                if ($unit == "K") {
                     $distance= ($miles * 1.609344);
                } else if ($unit == "N") {
                     $distance= ($miles * 0.8684);
                } else {
                     $distance= $miles;
                }
               $distance= distance($lat , $lng, $latitude, $longitude, "M");
               
           }
           $rate=round($distance * 1.25);
           if($rate > 0){
            $snme=ucfirst(get_user_meta($uid,'store_name',true));
            array_push($feearr['delivery'],array("Delivery Fee for $snme Store", $rate));
           }
           
           }
           
        }
    }
    update_user_meta($user_id,'deliveryfee',$feearr['delivery']);
    $fee=$fee * count($store);
    update_user_meta($user_id,'servicefee',$fee);
    $feearr['servicefee']= $fee;
    $code = STATUS_OK;
    $message = 'Submitted';
     sendResponse($feearr, $code, $message);     
  }
  
  function getordercount($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
   
    $driver = $req['driver'];
    $result=array();
    $orders=wc_get_orders(array(
        'limit'=>-1,
        'type'=> 'shop_order',
        'status'=> array( 'accepted' ),
        'meta_key'     => 'driveruser',
        'meta_value' => $driver,
        'meta_compare' => '=',
        )
    );
    $count=count($orders);
    $result['acceptedorders']=$count;
    
    $orders=wc_get_orders(array(
        'limit'=>-1,
        'type'=> 'shop_order',
        'status'=> array( 'delivered' ),
        'meta_key'     => 'driveruser',
        'meta_value' => $driver,
        'meta_compare' => '=',
        )
    );
    $count=count($orders);
    $result['deliveredorders']=$count;
    
    $orders=wc_get_orders(array(
        'limit'=>-1,
        'type'=> 'shop_order',
        'status'=> array( 'rejected' ),
        'meta_key'     => 'driveruser',
        'meta_value' => $driver,
        'meta_compare' => '=',
        )
    );
    $count=count($orders);
    $result['rejectedorders']=$count;
    $code = STATUS_OK;
    $message = 'Submitted';
    sendResponse(array('orders'=>$result), $code, $message);  
  }

  
  
  function ordersbydate($request, $response, $args){
    global $wpdb, $woocommerce;
    $req = $request->getParams();
    $from = $req['from'];
    $to=$req['to'];
    $status=$req['status'];
    $uid=$req['userid'];
    $page=$req['page'];
      $Order_Array = []; 
    $udata=get_userdata($uid);
    $posts=10;
    if(!$udata){
       $code = STATUS_UNAUTHORIZED;
       $message = 'Error! UserId is Incorrect!';
    }
    else{
      
      if($status == "rejected" || $status == "all"){
        
        $rejorders= get_user_meta( $uid, '_rejected_orders', true );
        $rids=array();
        for($i=0;$i<count($rejorders);$i++){
          $odate=date_format(date_create($rejorders[$i]['date']),'Y-m-d');
          $contractDateBegin = date('Y-m-d', strtotime($from));
          $contractDateEnd = date('Y-m-d', strtotime($to));
         // echo "$contractDateBegin <= $odate && $odate <= $contractDateEnd";
          if($contractDateBegin <= $odate && $odate <= $contractDateEnd)
          {
            array_push($rids,$rejorders[$i]['orderid']);
          }          
        }
        $rids=array_unique($rids);
        $end=$page * $posts;
        $start=$end-$posts;
       // echo ".... $start ... $posts";
       // print_r(array_slice($rids,$start, $posts));
        //print_r($rids);
        $orders=array_slice($rids,$start, $posts);
        
        $customer_orders = get_posts(array(
            'include' =>$rids,
             'numberposts' => -1,
            'orderby' => 'date',        
            'order' => 'DESC',
            'post_type' => wc_get_order_types(),
             'post_status' => 'any',
          ));
    
       if($status == "all"){
          $customer_orders1 = get_posts(array(           
            'numberposts' => -1,
            'orderby' => 'date',
            'order' => 'DESC','meta_key'=>'driveruser','meta_value'=>$uid,        
            'post_type' => wc_get_order_types(),'offset'=>$start,
            'post_status' => array('wc-delivered'),'numberposts' => $posts
          ));            
          $customer_orders = array_merge( $customer_orders, $customer_orders1);
        }
        
      }else if($status == "delivered"){
          $end=$page * $posts;
           $start=$end-$posts;
          $customer_orders = get_posts(array(
            'numberposts' => -1,
            'orderby' => 'date',
            'order' => 'DESC','meta_key'=>'driveruser','meta_value'=>$uid,        
            'post_type' => wc_get_order_types(),'offset'=>$start,
            'post_status' => array('wc-delivered'),'numberposts' => $posts
          ));
      }
     
     
         
          foreach ($customer_orders as $customer_order) {
              $orderq = wc_get_order($customer_order);
              $oid= $orderq->get_id();
              $shipping = array(
                    'first_name' => $orderq->get_shipping_first_name(),
                    'last_name' => $orderq->get_shipping_last_name(),
                    'company' => $orderq->get_shipping_company(),
                    'address_1' => $orderq->get_shipping_address_1(),
                    'address_2' => $orderq->get_shipping_address_2(),
                    'city' => $orderq->get_shipping_city(),
                    'state' => $orderq->get_shipping_state(),
                    'formated_state' => WC()->countries->states[$orderq->get_shipping_country()][$orderq->get_shipping_state()], //human readable formated state name
                    'postcode' => $orderq->get_shipping_postcode(),
                    'country' => $orderq->get_shipping_country(),
                    'formated_country' => WC()->countries->countries[$orderq->get_shipping_country()] //human readable formated country name
                );
              $rating=get_post_meta($orderq->get_id(),'driveraing',true);
              $comment=get_post_meta($orderq->get_id(),'drivercomment',true);
              $datetime=get_post_meta($orderq->get_id(),'modifiedstatus1',true);
                $date=date_format(date_create($datetime),"j/M/Y");
                $time=date_format(date_create($datetime),"g:iA");
              $Order_Array[] = [
                        "id" => $orderq->get_id(),
                        "price" => $orderq->get_total(),
                         "status" => $orderq->get_status(),
                         "address" =>$shipping,
                         'rating'=>$rating,
                         'comment' =>$comment,
                         'time' => $time,
                        "date" => $date
              ];                     
             
              $code = STATUS_OK;
              $message = 'Success!';   
      
          
        }
    }
    
      sendResponse($Order_Array, $code, $message);  
  }