<?php
/* 
Author - Vipin Singh
Title - woocommerce library 
Version - 1.1
Date -  02-11-2020
*/
class UserDAO extends Database {

    private $tblUser = 'sn_users';   
    private $tblUserToken = 'sn_users_token';
    private $tblUserMeta = 'sn_usermeta';
    //private $tblOtp = 'hl_user_otp';
    //private $tblWooWallet = 'hl_woo_wallet_transactions';
    //private $tblState = 'hl_states_cities'; 
    //private $tblFirebase = 'hl_firebase_token';


    function updateToken($user_id, $token, $device_id) {
      $sql = "INSERT INTO $this->tblUserToken (user_id, token, device_id) VALUES(:user_id, :token, :device_id) ";
      $sql .= "ON DUPLICATE KEY UPDATE token = :token";
      $this->query($sql);
      $this->bind(':user_id', $user_id);
      $this->bind(':token', $token);
      $this->bind(':device_id', $device_id);
      $this->execute();
      return $this->lastInsertId();
    }

    function getLoginByCreds($uemail,$upass) {
      $sql = "SELECT * FROM $this->tblUser WHERE user_email=:user_email AND user_pass =:user_pass";
      $this->query($sql);
      $this->bind(':user_email', $uemail);
      $this->bind(':user_pass', $upass);
      $result = $this->single();
      return $result;
    }

    function registerUser($user_email,$user_pass,$display_name) {
      $uname = explode('@', $user_email);
      $sql = "INSERT INTO $this->tblUser (user_login,user_email,user_pass,user_nicename,display_name) VALUES('".$uname[0]."', '".$user_email."','".$user_pass."','".$display_name."','".$display_name."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }

    function userEmailExists($user_email){
      $sql = "SELECT * FROM $this->tblUser WHERE user_email=:user_email ";
      $this->query($sql);
      $this->bind(':user_email', $user_email);
      $result = $this->single();
      return $result;
    }

    function getUserDetailsByID($uid) {
      $sql = "SELECT * FROM $this->tblUser WHERE ID=:ID ";
      $this->query($sql);
      $this->bind(':ID', $uid);
      $result = $this->single();
      return $result;
    }

    function getUserMetaProfilePic($user_id) {
      $sql = "SELECT meta_key,meta_value FROM $this->tblUserMeta WHERE user_id = '".$user_id."' AND meta_key='profileImg'  ORDER BY user_id ASC ";
      $this->query($sql);
      $result = $this->single();
      return $result; 
    }

    function getUserTokenByID($user_id){
      $sql = "SELECT * FROM $this->tblUserToken WHERE user_id = '".$user_id."' ";
      $this->query($sql);
      $result = $this->single();
      return $result;
    }

    function uploadImg($uid,$data){
      foreach ($data as $key => $val) { 
        $sql = "INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$uid."', '".$key."', '".$val."')";
        $this->query($sql);
        $result = $this->execute();
      }
      return $result;
    }

    function uploadImgDelete($user_id) {       
      $sql = "DELETE FROM $this->tblUserMeta WHERE user_id='".$user_id."' AND meta_key='profileImg' ";
      $this->query($sql);
      $result = $this->execute();
      return $result;
    }

}

//end of class
