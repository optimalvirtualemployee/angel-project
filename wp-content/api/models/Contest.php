<?php
/* 
Author - Optimal
Title - Contest library 
Version - 1.1
Date -  20-01-2019
*/
class Contest extends Database {
	//Set Global Database
	private $tblContest = 'hl_contests';
	private $tblContestParticipant = 'hl_contest_participants';
	private $tblUserMeta = 'hl_usermeta';
	
	// Get active contests
	function getActiveContests(){
		$sql = " SELECT id, title, categories AS category FROM $this->tblContest WHERE status = '1' ";
		$this->query($sql);      
		$result = $this->resultset();
		return $result;     
	}
	
	// Get contest details by id
	function getContest( $id ){
		$sql = " SELECT * FROM $this->tblContest WHERE id = '$id' AND status = '1' ";
		$this->query($sql);      
		$result = $this->single();
		if(!empty($result)){
			$result['category'] = $result['categories'];
			unset($result['categories']);
		}
		$arr_quesions = array();
		if(!empty($result['quiz_questions'])){
			$questions = unserialize($result['quiz_questions']);
			foreach($questions['que'] as $key=>$question){
				$answer = $questions['ans'][$key];
				$arr_quesions[$key] = array('question'=>stripcslashes($question), 'options'=>$questions['opt'][$key], 'answer'=>$questions['opt'][$key][$answer]);
			}
			$result['quiz_questions'] = $arr_quesions;
		}
		$winnerusers = '';
		if(!empty($result['winners'])){
			$winners = unserialize($result['winners']);
			foreach($winners as $winner){
				$sqlu = " SELECT meta_key, meta_value FROM $this->tblUserMeta WHERE meta_key IN('first_name', 'last_name', 'billing_city') AND user_id = '".(int)$winner."' ";
				$this->query($sqlu);
				$user = $this->resultset();
				$first_name = '';
				$last_name = '';
				$city = '';
				foreach($user as $u){
					if($u['meta_key'] == 'first_name')
						$first_name = $u['meta_value'];
					else if($u['meta_key'] == 'last_name')
						$last_name = $u['meta_value'];
					else if($u['meta_key'] == 'billing_city')
						$city = $u['meta_value'];
				}
				$winnerusers .= $first_name.' '.$last_name;
				if(!empty($city))
					$winnerusers .= ', '.$city;
				$winnerusers .= '<br />';
			}
			$result['winners'] = $winnerusers;
		}
		return $result;     
	}
	
	// Get contest entries by contest id
	function getParticipants( $id ){
		$sqlc = " SELECT title, categories, quiz_questions FROM $this->tblContest WHERE id = '$id' AND status = '1' ";
		$this->query($sqlc);
		$contest = $this->single();
		$result = array();
		if(!empty($contest)){
			$questions = unserialize($contest['quiz_questions']);
			$sql = " SELECT id, user_id, title, photos, score, essay, quiz_answers, created FROM $this->tblContestParticipant WHERE contest_id = '$id' AND status = 'Approved' ";
			$this->query($sql);      
			$result = $this->resultset();
			if(!empty($result)){
				foreach($result as $key=>$row){
					$sqlu = " SELECT meta_key, meta_value FROM $this->tblUserMeta WHERE meta_key IN('first_name', 'last_name', 'billing_city') AND user_id = '".(int)$row['user_id']."' ";
					$this->query($sqlu);
					$this->bind(':ID', $row['user_id']);
					$user = $this->resultset();
					$first_name = '';
					$last_name = '';
					$city = '';
					foreach($user as $u){
						if($u['meta_key'] == 'first_name')
							$first_name = $u['meta_value'];
						else if($u['meta_key'] == 'last_name')
							$last_name = $u['meta_value'];
						else if($u['meta_key'] == 'billing_city')
							$city = $u['meta_value'];
					}
					$result[$key]['name'] = $first_name.' '.$last_name;
					$result[$key]['city'] = $city;
					$result[$key]['title'] = !empty($row['title']) ? stripcslashes($row['title']) : $contest['title'];
					$result[$key]['essay'] = stripcslashes($row['essay']);
					$score = 0;
					if(!empty($row['quiz_answers'])){
						$qansarr = unserialize($row['quiz_answers']);
						foreach($qansarr as $qkey=>$quiz_answer){
							$ai = array_search($quiz_answer['answer'], $questions['opt'][$qkey]);
							if($questions['ans'][$qkey] == $ai){
								$score += 10;
							}
						}
					}
					$result[$key]['score'] = $score;
					unset($result[$key]['quiz_answers']);
					$result[$key]['created'] = date('d M Y', strtotime($row['created']));
				}
			}
		}
		return $result;     
	}
	
	// Post contest entry to participate
	function postEntry( $data = array(), $user_id = 0 ){		
		$id = $data['id'];
		$data['user_id'] = $user_id;
		$data['contest_id'] = $id;
		$sql = " SELECT id, categories, quiz_questions FROM $this->tblContest WHERE id = '$id' AND status = '1' ";
		$this->query($sql);      
		$result = $this->single();
		$message = 'Some error occured, Try again later!';
		$error = true;
		if(!empty($result)){
			$type = !empty($result['categories']) ? $result['categories'] : '';
			switch($type){
				case 'Photo':					
					$arrex = array('image/jpeg', 'image/jpg', 'image/png');
					if (in_array($_FILES['photo']['type'], $arrex)) {
						$uploadedfile = $_FILES['photo']['name'];
						$uploadfile = UPLOADS_PATH . basename($uploadedfile);
						if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile)) {
						  $code = STATUS_OK;
						  $message = 'Photo/image has been uploaded';
						  $data['photos'] = UPLOADS_URL . basename($_FILES['photo']['name']);
						  $error = false;
						} else {
						  $code = STATUS_BAD_REQUEST;
						  $message = 'Upload error';
						}
					}else{
						$message = 'Please choose a valid image type (jpeg, jpg or png only).';
					}
					unset($data['essay']);
					unset($data['quiz_answers']);
				break;
				case 'Essay':
					unset($data['photo']);
					unset($data['quiz_answers']);
					$error = false;
				break;
				case 'Quiz':
				default:
					$qa_answers = array();
					if(!empty($result['quiz_questions'])){
						$questions = unserialize($result['quiz_questions']);
						//$qanswers = json_decode($data['quiz_answers'], true);
						$qanswers = json_encode($data['quiz_answers'], true);
						foreach($questions['que'] as $key=>$question){
							$answer = isset($qanswers[$key]) ? $qanswers[$key] : '';
							$qa_answers[$key] = array('question'=>stripcslashes($question), 'answer'=>$answer);
						}
					}
					$data['quiz_answers'] = serialize($qa_answers);
					unset($data['photo']);
					unset($data['essay']);
					$error = false;
				break;
			}
			unset($data['id']);

			if($error == false){
				$sqlp = " SELECT id FROM $this->tblContestParticipant WHERE contest_id = '$id' AND user_id = '$user_id' ";
				$this->query($sqlp);      
				$resultp = $this->single();
				if (empty($resultp['id'])) {
					$data['created'] = date('Y-m-d H:i:s');
					$sqli = sprintf(
						" INSERT INTO $this->tblContestParticipant (%s) VALUES ('%s') ",
						implode(",",array_keys($data)),
						implode("','",array_values($data))
					);
					//echo $sqli;die;
					$this->query($sqli);
					$this->execute();
					$message = 'Data posted';
				} else {
					/*
					$uid = $resultp['id'];
					$update_str = "";
					foreach($data as $uk => $uv ){
						$update_str .= $uk."= '$uv', ";
					}
					$update_str .= " status = 'Pending' ";
					$sqlu = " UPDATE $this->tblContestParticipant SET $update_str WHERE id = '$uid' ";
					//echo $sqlu;die;
					$this->query($sqlu);
					$this->execute();
					$message = 'Data posted';
					*/
					$message = 'You have already participated';
				}
			}
		}
		return $message;     
	}
}