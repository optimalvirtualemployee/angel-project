<?php woo_customer_email_verification() ?>

<?php
/**
 * html code for tools tab
 */
$more_plugins = array(
	0 => array(
		'title' => 'SMS for WooCommerce',
		'description' => 'Keep your customers informed by sending them automated SMS text messages with order & delivery updates. You can send SMS notifications to customers when the order status is updated or when the shipment is out for delivery and more…',
		'image' => 'smswoo-addons-icon.jpg',
		'url' => 'https://www.zorem.com/product/sms-for-woocommerce/',
		'file' => 'sms-for-woocommerce/sms-for-woocommerce.php',
		'price' => 79
	),
	1 => array(
		'title' => 'Country Based Restrictions Pro',
		'description' => 'The country-based restrictions plugin by zorem works by the WooCommerce Geolocation or the shipping country added by the customer and allows you to restrict products on your store to sell or not to sell to specific countries.',
		'image' => 'cbr-icon.png',
		'url' => 'https://www.zorem.com/product/country-based-restriction-pro/',
		'file' => 'country-base-restrictions-pro-addon/country-base-restrictions-pro-addon.php',
		'price' => 79
	),
	2 => array(
		'title' => 'Advanced Order Status Manager',
		'description' => 'The Advanced Order Status Manager allows store owners to manage the WooCommerce orders statuses, create, edit, and delete custom Custom Order Statuses and integrate them into the WooCommerce orders flow.',
		'image' => 'AOSM-addons-icon.jpg',
		'url' => 'https://www.zorem.com/product/advanced-order-status-manager/',
		'file' => 'advanced-order-status-manager/advanced-order-status-manager.php',
		'price' => 49
	),
	3 => array(
		'title' => 'Sales Report Email Pro',
		'description' => 'The Sales Report Email Pro will help know how well your store is performing and how your products are selling by sending you a daily, weekly, or monthly sales report by email, directly from your WooCommerce store.',
		'image' => 'sre-icon.png',
		'url' => 'https://www.zorem.com/product/sales-report-email-for-woocommerce/',
		'file' => 'sales-report-email-pro-addon/sales-report-email-pro-addon.php',
		'price' => 79
	),
); 

//$status = install_plugin_install_status( $plugin );
$pro_plugins = array(
	0 => array(
		'title' => 'Tracking Per Item Add-on',
		'description' => 'The Tracking per item is add-on for the Advanced Shipment Tracking for WooCommerce plugin that lets you attach tracking numbers to line items and to line item quantities.',
		'url' => 'https://www.zorem.com/product/tracking-per-item-ast-add-on/',
		'image' => 'tpi-addon-icon.jpg',
		'file' => 'ast-tracking-per-order-items/ast-tracking-per-order-items.php',
		'price' => 79
	),
	1 => array(
		'title' => 'SMS for WooCommerce',
		'description' => 'Keep your customers informed by sending them automated SMS text messages with order & delivery updates. You can send SMS notifications to customers when the order status is updated or when the shipment is out for delivery and more…',
		'url' => 'https://www.zorem.com/product/sms-for-woocommerce/',
		'image' => 'smswoo-addons-icon.jpg',
		'file' => 'sms-for-woocommerce/sms-for-woocommerce.php',
		'price' => 79
	),
	2 => array(
		'title' => 'Advanced Order Status Manager',
		'description' => 'The Advanced Order Status Manager allows store owners to manage the WooCommerce orders statuses, create, edit, and delete custom Custom Order Statuses and integrate them into the WooCommerce orders flow.',
		'url' => 'https://www.zorem.com/product/advanced-order-status-manager/',
		'image' => 'AOSM-addons-icon.jpg',
		'file' => 'advanced-order-status-manager/advanced-order-status-manager.php',
		'price' => 79
	),
	3 => array(
		'title' => 'Sales Report Email Pro',
		'description' => 'The Sales Report Email Pro will help know how well your store is performing and how your products are selling by sending you a daily, weekly, or monthly sales report by email, directly from your WooCommerce store.',
		'url' => 'https://www.zorem.com/product/sales-report-email-for-woocommerce/',
		'image' => 'sre-icon.jpg',
		'file' => 'sales-report-email-pro-addon/sales-report-email-pro-addon.php',
		'price' => 79
	),		
);

$free_plugins = array(
	0 => array(
		'title' => 'Advanced Shipment Tracking',
		'description' => 'Advanced Shipment Tracking (AST) for WooCommerce lets you add tracking information to orders and provides your customers an easy way to track their orders. AST provides powerful features that let WooCommerce shop owners to better manage and automate their post-shipping orders flow, reduce time spent on customer service and increase customer satisfaction..',
		'image' => 'ast-icon.png',
		'url' => 'https://www.zorem.com/product/woocommerce-advanced-shipment-tracking/',
		'file' => 'woo-advanced-shipment-tracking/woocommerce-advanced-shipment-tracking.php'
	),
	1 => array(
		'title' => 'Advanced Local Pickup',
		'description' => 'Advanced Local Pickup lets you mark your WooCommerce order status as “Ready for Pickup” and adds the pickup instructions to the email sent to your customers.',
		'image' => 'alp-icon.png',
		'url' => 'https://wordpress.org/plugins/advanced-local-pickup-for-woocommerce/',
		'file' => 'advanced-local-pickup-for-woocommerce/woo-advanced-local-pickup.php'
	),
	2 => array(
		'title' => 'Salse Report Email',
		'description' => 'The Sales Report Email will help know how well your store is performing and how your products are selling by sending you a daily, weekly, or monthly sales report by email, directly from your WooCommerce store.',
		'image' => 'sre-icon.jpg',
		'url' => 'https://wordpress.org/plugins/woo-advanced-sales-report-email/',
		'file' => 'woo-advanced-sales-report-email/woocommerce-advanced-sales-report-email.php',

	),
	3 => array(
		'title' => 'Shop Manager Admin',
		'description' => 'Access WooCommerce orders, products, coupons, reports and shop settings directly from the WordPress admin bar at the frontend and backend of the shop.',
		'image' => 'sma-icon.png',
		'url' => 'https://wordpress.org/plugins/woo-shop-manager-admin-bar/',
		'file' => 'woo-shop-manager-admin-bar/woo-shop-manager-admin.php'
	),
	4 => array(
		'title' => 'Customer verification',
		'description' => 'The Customer verification plugin will reduce WooCommerce registration spam in your store and will require them to verify their email address by sending a verification link to the email address which they registered to their account.',
		'image' => 'cev-icon.png',
		'url' => 'https://wordpress.org/plugins/customer-email-verification-for-woocommerce/',
		'file' => 'customer-email-verification-for-woocommerce/customer-email-verification-for-woocommerce.php'
	),
	5 => array(
		'title' => 'Ajax Login/Register',
		'description' => 'Improve your WooCommerce shop UI/UX with the AJAX-powered login & registration process with no page refresh!',
		'image' => 'ajax-login-register-icon.png',
		'url' => 'https://wordpress.org/plugins/woo-ajax-loginregister/',
		'file' => 'woo-ajax-loginregister/woocommerce-ajax-login-register.php'
	),
	6 => array(
		'title' => 'Sales Report By Country',
		'description' => 'This plugin simply adds a report tab to display sales report by country WooCommerce Reports.',
		'image' => 'src-icon.png',
		'url' => 'https://wordpress.org/plugins/woo-sales-by-country-reports/',
		'file' => 'woo-sales-by-country-reports/woocommerce-sales-by-country-report.php',
		
	),	
); 

?>
<section id="cev_content_addons" class="cev_tab_section">
	<div class="d_table addons_page_dtable" style="">		
		<?php if ( class_exists( 'customer_email_verification_pro' ) ) { ?>	
		<input id="tab_addons" type="radio" name="inner_tabs" class="cev_tab_input" data-tab="add-ons" checked="">
		<label for="tab_addons" class="inner_tab_label"><?php _e( 'Add-ons', 'customer-email-verification-for-woocommerce' ); ?></label>
		
		<input id="tab_license" type="radio" name="inner_tabs" class="cev_tab_input" data-tab="add-ons">
		<label for="tab_license" class="inner_tab_label"><?php _e( 'License', 'customer-email-verification-for-woocommerce' ); ?></label>
		<hr class="inner_tabs_hr">
		<?php } ?>				
		<section id="content_tab_addons" class="<?php if ( class_exists( 'customer_email_verification_pro' ) ) { ?>inner_tab_section<?php } ?>"> 
        <div class="section-content">
            
				<div class="cev_row">
                <?php if ( !class_exists( 'customer_email_verification_pro' ) ) { ?>
               	<div class="plugins_section free_plugin_section">
									<div class="single_plugin">
						<div class="free_plugin_inner">
							<div class="paid_plugin_image">
		
							 <img src="<?php echo  woo_customer_email_verification()->plugin_dir_url() . 'assets/css/images/email-verification-icon.svg'; ?>">
							</div>
							<div class="paid_plugin_description">
								<h3 class="plugin_title">Customer Email Verification Pro</h3>
								<p>The Customer email verification Pro allows you to require customers (and guest customers) to verify their email address before they can checkout on your store, delay the New account emails to after successful verification, resend limits, verification code expiration, advanced email verification management and more..</p><a href="https://www.zorem.com/product/customer-email-verification-for-woocommerce/" class="button button-primary btn_cev2" target="blank">From $79</a>
									
							</div>
						</div>
					</div>	
									<div class="single_plugin">
						<div class="free_plugin_inner">
							<div class="paid_plugin_image">
								
							 <img src="<?php echo  woo_customer_email_verification()->plugin_dir_url() . 'assets/css/images/email-verification-icon.svg'; ?>">
							</div>
							<div class="paid_plugin_description">
								<h3 class="plugin_title">Customer Approval for WooCommerce</h3>
								<p>The Customer Approval for WooCommerce allows you to require customers admin approval mode allows you to completely block your store for selective customers and you can create a membership site by restricting access to your content and selling members only products delay the New account emails to after successful  verification more..  </p><a href="https://www.zorem.com/product/paypal-tracking-add-on/" class="button button-primary btn_cev2" target="blank">Coming Soon!</a>
									
							</div>
						</div>
					</div>	
						
			</div>
                  <?php } ?>
				</div>
			</div>		
			
			<div class="plugins_section zorem_plugin_section">
				<div class="addons_page_header">
					<h2 class="addons_page_title">WooCommerce plugins by zorem</h2>
					<p>Improve your store, automate your workflow and save time managing your store</p>
				</div>
				<div class="zorem_plugin_container">
					<?php foreach($more_plugins as $plugin){ ?>
						<div class="zorem_single_plugin">
							<div class="free_plugin_inner">
								<div class="plugin_image">
									<img src="<?php echo woo_customer_email_verification()->plugin_dir_url()?>assets/images/<?php echo $plugin['image']; ?>">
								</div>
								<div class="plugin_description">
									<h3 class="plugin_title"><?php echo $plugin['title']; ?></h3>
									<p><?php echo $plugin['description']; ?></p>
									<?php 
								if ( is_plugin_active( $plugin['file'] ) ) { ?>
									<button type="button" class="button button button-primary btn_blue2">Active</button>
								<?php } else{ ?>
									<a href="<?php echo $plugin['url']; ?>" class="button button-primary btn_cev2" target="blank"><?php if($plugin['price'] == ''){ echo 'Free'; } else{ _e('From', 'customer-email-verification-for-woocommerce'); ?> $<?php echo $plugin['price']; } ?></a>
								<?php } ?>								
								</div>
							</div>	
						</div>	
					<?php } ?>						
				</div>
			</div>						
		</section>
		
		<section id="content_tab_license" class="inner_tab_section">
			<?php do_action('cev_license_tab_content_data_array'); ?>
		</section>						
	</div>
</section>
