<?php
/**
 * html code for settings tab
 */
?>
<section id="cev_content_settings" class="cev_tab_section">
	<div class="cev_tab_inner_container">
		<form method="post" id="cev_settings_form" action="" enctype="multipart/form-data"><?php #nonce?>						
        	<div class="cev_outer_form_table">
            	<div class="cev_outer_form_table_section">
					<table class="form-table header-table">
						<tbody>
							<tr valign="top" class="heading-box border_1">
								<td>
									<h3 style=""><?php _e( 'Customer email verification', 'customer-email-verification-for-woocommerce' ); ?></h3>
                                </td>
                                <td>
									<div class="submit cev-btn">
										<div class="spinner  " style="float:none"></div>
											<button name="save" class="cev_settings_save button-primary woocommerce-save-button" type="submit" value="Save changes"><?php _e( 'Save changes', 'customer-email-verification-for-woocommerce' ); ?></button>
											<?php wp_nonce_field( 'cev_settings_form_nonce', 'cev_settings_form_nonce' ); ?>
											<input type="hidden" name="action" value="cev_settings_form_update">
									</div>
								</td>
							</tr>
                            	<?php  $this->get_html( $this->get_cev_settings_data() ); ?>
                                
                        		<?php do_action('cev_email_settings_add_new_html'); 
                                 if ( !class_exists( 'customer_email_verification_pro' ) ) { ?>
                                 <?php  $this->get_html_new( $this->get_cev_settings_data_new() ); }?>	
                                 </tbody>
                          </table>
                      	</div>
					</div>              					
		
				<?php 					 if ( class_exists( 'customer_email_verification_pro' ) ) { ?>
                                     					
        							 	<div class="cev_outer_form_table">
            								<div class="cev_outer_form_table_section">
												<table class="form-table header-table">
													<tbody>
														<tr valign="top" class="heading-box border_1">
															<td>
																<h3 style=""><?php _e( 'Email verification settings', 'customer-email-verification-for-woocommerce' ); ?></h3>
                               								 </td>
                              							     <td>
																<div class="submit cev-btn">
																	<div class="spinner  " style="float:none"></div>
																		<button name="save" class="cev_settings_save button-primary woocommerce-save-button" type="submit" value="Save changes"><?php _e( 'Save changes', 'customer-email-verification-for-woocommerce' ); ?></button>
																			<?php wp_nonce_field( 'cev_settings_form_nonce', 'cev_settings_form_nonce' ); ?>
																				<input type="hidden" name="action" value="cev_settings_form_update">
																	</div>
															</td>
														</tr> 
														<?php } ?> <?php if ( class_exists( 'customer_email_verification_pro' ) ) { ?>
                           								<tr valign="top">
															<td class="border_1" colspan="2">	
																<?php  $this->get_html_new( $this->get_cev_settings_data_new() ); ?>	
															</td>
														</tr>
                                                        <?php } ?>
                           		   						 <?php if ( class_exists( 'customer_email_verification_pro' ) ) { ?> </tbody>
                            			</table> </div> </div>  <?php  } ?>
         </form>	
                           
        <div class="cev_outer_form_table_section">
        	<table class="form-table header-table">
            	<tbody>
                
					<tr valign="top"  class="heading-box border_1">
                		<td colspan="2">
							<h3 style="cev-verification-widget"><?php _e( 'Customer view', 'customer-email-verification-for-woocommerce' ); ?>
                             </h3>     
                        </td>
					</tr>
                </tbody>
            </table>  
            <ul class="size_customize submit cev-btn">
				<li>
					<label style=""><?php _e( 'Customize the verification widget popup and the email notifications sent to customers to verify their email address', 'customer-email-verification-for-woocommerce' ); ?></label>
						<a href="<?php echo cev_initialise_customizer_settings::get_customizer_url('cev_main_controls_section'); ?>" class="button-primary cev-btn-large"><?php _e( 'Customize', 'customer-email-verification-for-woocommerce' ); ?></a>
				</li>
			</ul>
         </div>
      </div>
  </section>