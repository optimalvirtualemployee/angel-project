<?php
/**
 * Customizer Setup and Custom Controls
 *
 */

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
class cev_verification_widget_style {
	// Get our default values	
	private static $order_ids  = null;
	
	public function __construct() {
		// Get our Customizer defaults
		$this->defaults = $this->cev_generate_defaults();		
		
		// Register our sample default controls
		add_action( 'customize_register', array( $this, 'cev_my_verification_widget_style' ) );
		
		// Only proceed if this is own request.				
		if ( ! cev_verification_widget_style::is_own_customizer_request() && ! cev_verification_widget_style::is_own_preview_request()) {
			return;
		}			
		
		// Register our sections
		add_action( 'customize_register', array( wc_cev_customizer(), 'cev_add_customizer_sections' ) );	
		
		// Remove unrelated components.
		add_filter( 'customize_loaded_components', array( wc_cev_customizer(), 'remove_unrelated_components' ), 99, 2 );
		
		// Remove unrelated sections.
		add_filter( 'customize_section_active', array( wc_cev_customizer(), 'remove_unrelated_sections' ), 10, 2 );	
		
		// Unhook divi front end.
		add_action( 'woomail_footer', array( wc_cev_customizer(), 'unhook_divi' ), 10 );
		
		// Unhook Flatsome js
		add_action( 'customize_preview_init', array( wc_cev_customizer(), 'unhook_flatsome' ), 50  );	
		

		add_filter( 'customize_controls_enqueue_scripts', array( wc_cev_customizer(), 'enqueue_customizer_scripts' ) );	
		

		add_action( 'customize_preview_init', array( $this, 'enqueue_preview_scripts' ) );		
	}			
		
	/**
	 * add css and js for preview
	*/	
	public function enqueue_preview_scripts() {		 
		 wp_enqueue_style('cev-pro-preview-styles', woo_customer_email_verification()->plugin_dir_url() . 'assets/css/preview-styles.css', array(), woo_customer_email_verification()->version  );		 
	}	
	
	/**
	 * Checks to see if we are opening our custom customizer preview
	 *
	 * @access public
	 * @return bool
	 */
	public static function is_own_preview_request() {
		return isset( $_REQUEST['action'] ) && 'preview_cev_verification_lightbox' === $_REQUEST['action'];
	}
	
	/**
	 * Checks to see if we are opening our custom customizer controls
	 *
	 * @access public
	 * @return bool
	 */
	public static function is_own_customizer_request() {
		return isset( $_REQUEST['section'] ) && $_REQUEST['section'] === 'cev_verification_widget_style';
	}
	
	/**
	 * Get Customizer URL
	 *
	 */
	public static function get_customizer_url( $section, $return_tab ) {	
			$customizer_url = add_query_arg( array(
				'cev-customizer' => '1',
				'section' => $section,						
				'autofocus[section]' => 'cev_verification_widget_style',
				'url'                  => urlencode( add_query_arg( array( 'action' => 'preview_cev_verification_lightbox' ), home_url( '/' ) ) ),
				'return'               => urlencode( cev_verification_widget_style::get_cev_widget_style_page_url( $return_tab ) ),								
			), admin_url( 'customize.php' ) );		

		return $customizer_url;
	}
	
	/**
	 * Get WooCommerce email settings page URL
	 *
	 * @access public
	 * @return string
	 */
	public static function get_cev_widget_style_page_url( $return_tab ) {
		return admin_url( 'admin.php?page=customer-email-verification-for-woocommerce&tab='.$return_tab );
	}
	
	/**
	 * code for initialize default value for customizer
	*/	
	public function cev_generate_defaults() {
		$customizer_defaults = array(	
			'cev_verification_popup_background_color'	=> '#f5f5f5',
			'cev_verification_popup_overlay_background_color' => '#ffffff',
		);
		return $customizer_defaults;
	}						
	
	/**
	 * Register our sample default controls
	 */
	public function cev_my_verification_widget_style( $wp_customize ) {	
	
		/**
		* Load all our Customizer Custom Controls
		*/
		require_once trailingslashit( dirname(__FILE__) ) . 'custom-controls.php';		
													
		
		
		// Table Background color
		$wp_customize->add_setting( 'cev_verification_popup_overlay_background_color',
			array(
				'default' => $this->defaults['cev_verification_popup_overlay_background_color'],
				'transport' => 'refresh',
				'sanitize_callback' => 'sanitize_hex_color',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_popup_overlay_background_color',
			array(
				'label' => __( 'Overlay background Color', 'customer-email-verification-for-woocommerce' ),
				'section' => 'cev_verification_widget_style',
				'priority' => 1, // Optional. Order priority to load the control. Default: 10
				'type' => 'color',
			)
		);
		
		// email button color overly
		$wp_customize->add_setting( 'cev_verification_popup_background_color',
			array(
				'default' => $this->defaults['cev_verification_popup_background_color'],
				'transport' => 'refresh',
				'sanitize_callback' => '',
				'type' => 'option',
			)
		);
		$wp_customize->add_control( 'cev_verification_popup_background_color',
			array(
     			'label' => __( 'Widget background Color', 'customer-email-verification-for-woocommerce' ),
      			'description' => '',
      			'section' => 'cev_verification_widget_style',
     			'priority' => 2, // Optional. Order priority to load the control. Default: 10
      			'type' => 'color',
 		   )
		
		);	
									
	}		
	
}

/**
 * Initialise our Customizer settings
 */

$cev_verification_widget_style = new cev_verification_widget_style();