<?php
/**
 * @wordpress-plugin
 * Plugin Name: Customer Email Verification for WooCommerce 
 * Plugin URI: https://www.zorem.com/products/customer-email-verification-for-woocommerce/ 
 * Description: The Customer verification helps WooCommerce store owners to reduce registration spam by requiring customers to verify their email address when they register an account on your store, before they can access their account area.
 * Version: 1.3.3
 * Author: zorem
 * Author URI: https://www.zorem.com 
 * License: GPL-2.0+
 * License URI: 
 * Text Domain: customer-email-verification-for-woocommerce
 * Domain Path: /lang/
 * WC tested up to: 4.9.2
*/


class zorem_woo_customer_email_verification {
	/**
	 * Customer verification for WooCommerce version.
	 *
	 * @var string
	 */
	public $version = '1.3.3';
	
	/**
	 * Initialize the main plugin function
	*/
    public function __construct() {
		
		$this->plugin_file = __FILE__;
		// Add your templates to this array.
		
		if(!defined('CUSTOMER_EMAIL_VERIFICATION_PATH')) define( 'CUSTOMER_EMAIL_VERIFICATION_PATH', $this->get_plugin_path());
			
		$this->my_account = get_option( 'woocommerce_myaccount_page_id' );

		if ( '' === $this->my_account ) {
			$this->my_account = get_option( 'page_on_front' );
		}
		
		if ( $this->is_wc_active() ) {
			
			// Include required files.
			$this->includes();			
			
			//start adding hooks
			$this->init();						

			$this->admin->init();	
			
			$this->email->init();
			
			$this->preview->init();
			
			//$this->csv_permalink->init();
			
			add_action( 'plugins_loaded', array( $this, 'on_plugins_loaded' ) );
		}	
	}
	
	/**
	 * Check if WooCommerce is active
	 *
	 * @access private
	 * @since  1.0.0
	 * @return bool
	*/
	private function is_wc_active() {
		
		if ( ! function_exists( 'is_plugin_active' ) ) {
			require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
		}
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			$is_active = true;
		} else {
			$is_active = false;
		}
		

		// Do the WC active check
		if ( false === $is_active ) {
			add_action( 'admin_notices', array( $this, 'notice_activate_wc' ) );
		}		
		return $is_active;
	}
	
	/**
	 * Display WC active notice
	 *
	 * @access public
	 * @since  1.0.0
	*/
	public function notice_activate_wc() {
		?>
		<div class="error">
			<p><?php printf( __( 'Please install and activate WooCommerce!', 'customer-email-verification-for-woocommerce' ), '<a href="' . admin_url( 'plugin-install.php?tab=search&s=WooCommerce&plugin-search-input=Search+Plugins' ) . '">', '</a>' ); ?></p>
		</div>
		<?php
	}
	
	/**
	 * Gets the absolute plugin path without a trailing slash, e.g.
	 * /path/to/wp-content/plugins/plugin-directory.
	 *
	 * @return string plugin path
	 */
	public function get_plugin_path() {
		if ( isset( $this->plugin_path ) ) {
			return $this->plugin_path;
		}

		$this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );

		return $this->plugin_path;
	}
	
	/**
	 * Gets the absolute plugin url.
	 */	
	public function plugin_dir_url(){
		return plugin_dir_url( __FILE__ );
	}
	
	/*
	* init when class loaded
	*/
	public function init(){
		add_action( 'plugins_loaded', array( $this, 'customer_email_verification_load_textdomain'));
		
		add_action( 'admin_notices', array( $this, 'admin_notice_pro_update' ) );		
		add_action('admin_init', array( $this, 'cev_pro_notice_ignore' ) );

		//Custom Woocomerce menu
		add_action('admin_menu', array( $this->admin, 'register_woocommerce_menu' ), 99 );
		
		//load css js 
		add_action( 'admin_enqueue_scripts', array( $this->admin, 'admin_styles' ), 4);	
		add_filter( 'woocommerce_account_menu_items', array( $this, 'cev_account_menu_items' ), 10, 1 );	
		add_filter( 'woocommerce_account_menu_items', array( $this, 'hide_cev_menu_my_account' ), 999 );
		add_action( 'init', array( $this, 'cev_add_my_account_endpoint' ) );		
		add_action( 'woocommerce_account_email-verification_endpoint', array( $this, 'cev_email_verification_endpoint_content' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'front_styles' ));
		
		//callback for add action link for plugin page	
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ),  array( $this , 'my_plugin_action_links' ));

	}
	
	
	
	/*** Method load Language file ***/
	function customer_email_verification_load_textdomain() {
		load_plugin_textdomain( 'customer-email-verification-for-woocommerce', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
	}
	
	/*
	* include files
	*/
	private function includes(){
		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-admin.php';
		$this->admin = WC_customer_email_verification_admin::get_instance();

		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-email.php';
		$this->email = WC_customer_email_verification_email::get_instance();	
		
		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-preview-front.php';
		$this->preview = WC_customer_email_verification_preview::get_instance();		
		
		require_once $this->get_plugin_path() . '/includes/class-wc-customer-email-verification-email-common.php';
	}

	/*
	* include file on plugin load
	*/
	public function on_plugins_loaded() {	
		require_once $this->get_plugin_path() . '/includes/customizer/class-customer-verification-new-customizer.php';
		require_once $this->get_plugin_path() . '/includes/customizer/class-cev-customizer.php';
		require_once $this->get_plugin_path() . '/includes/customizer/class-cev-new-account-email-customizer.php';
		require_once $this->get_plugin_path() . '/includes/customizer/verification-widget-style.php';
		require_once $this->get_plugin_path() . '/includes/customizer/verification-widget-message.php';	
		require_once $this->get_plugin_path() . '/includes/cev-wc-admin-notices.php';		
										
	}
	
	/**
	 * Include front js and css
	*/
	public function front_styles(){				
		wp_register_script( 'cev-front-js', woo_customer_email_verification()->plugin_dir_url().'assets/js/front.js', array( 'jquery' ), woo_customer_email_verification()->version );
		wp_localize_script( 'cev-front-js', 'cev_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_register_style( 'cev_front_style',  woo_customer_email_verification()->plugin_dir_url() . 'assets/css/front.css', array(), woo_customer_email_verification()->version );		
		
		global $wp;	
		$current_slug = add_query_arg( array(), $wp->request );
		$email_verification_url = rtrim(wc_get_account_endpoint_url( 'email-verification' ),"/");
		
		if(home_url( $wp->request ) == $email_verification_url){	
			wp_enqueue_style( 'cev_front_style' );			
			wp_enqueue_script( 'cev-front-js' );			
		}	
	
		
		if(is_checkout()){	
			wp_enqueue_style( 'cev_front_style' );			
			wp_enqueue_script( 'cev-front-js' );			
		}
		if(is_cart()){	
			wp_enqueue_style( 'cev_front_style' );			
			wp_enqueue_script( 'cev-front-js' );			
		}	
		
	}
	
	/**
	* Account menu items
	*
	* @param arr $items
	* @return arr
	*/
	public function cev_account_menu_items( $items ) {
		$items['email-verification'] = __( 'Sign up email verification', 'customer-email-verification-for-woocommerce' );
		return $items;
	}
	
	/**
	* @snippet       Hide Edit Address Tab @ My Account
	* @how-to        Get CustomizeWoo.com FREE
	* @sourcecode    https://businessbloomer.com/?p=21253
	* @author        Rodolfo Melogli
	* @testedwith    WooCommerce 3.5.1
	* @donate $9     https://businessbloomer.com/bloomer-armada/
	*/			
	public function hide_cev_menu_my_account( $items ) {
		unset($items['email-verification']);
		return $items;
	}
	
	/**
	* Add endpoint
	*/
	public function cev_add_my_account_endpoint() {
		add_rewrite_endpoint( 'email-verification', EP_PAGES );
		if(version_compare(get_option( 'cev_version' ),'1.5', '<') ){
			global $wp_rewrite;
			$wp_rewrite->set_permalink_structure('/%postname%/');
			$wp_rewrite->flush_rules();
			update_option( 'cev_version', '1.5');				
		}
	}
	
	/**
	* Information content
	*/
	public function cev_email_verification_endpoint_content() {
		$current_user = wp_get_current_user();
		$email = $current_user->user_email;						
		$verified  = get_user_meta( get_current_user_id(), 'customer_email_verified', true );
		
		$cev_skip_verification_for_selected_roles = get_option('cev_skip_verification_for_selected_roles');
		$cev_verification_widget_style = new cev_verification_widget_style();
		$cev_verification_overlay_color = get_option('cev_verification_popup_overlay_background_color',$cev_verification_widget_style->defaults['cev_verification_popup_overlay_background_color']);
		
		if ( is_super_admin() || 'administrator' == $current_user->roles[0] || $cev_skip_verification_for_selected_roles[$current_user->roles[0]] != 0) {			
			return;
		}
		if ( 'true' === $verified ) {
			return;
		}	
		?>
		<style>
		.cev-authorization-grid__visual{
			background: <?php echo $this->hex2rgba($cev_verification_overlay_color,'0.7'); ?>;	
		}		
		</style>
		<?php 	
		$cev_button_color_widget_header =  get_option('cev_button_color_widget_header','#212121');
		$cev_button_text_color_widget_header =  get_option('cev_button_text_color_widget_header','#ffffff');
		$cev_button_text_size_widget_header =  get_option('cev_button_text_size_widget_header','15');
		$cev_widget_header_image_width =  get_option('cev_widget_header_image_width','150');
		$cev_button_text_header_font_size = get_option('cev_button_text_header_font_size','22');
		
	    require_once $this->get_plugin_path() . '/includes/views/cev_admin_endpoint_popup_template.php';
	}	
	
	/* Convert hexdec color string to rgb(a) string */
 
	public function hex2rgba($color, $opacity = false) {
	
		$default = 'rgba(116,194,225,0.7)';
	
		//Return default if no color provided
		if(empty($color))
			return $default; 
	
		//Sanitize $color if "#" is provided 
			if ($color[0] == '#' ) {
				$color = substr( $color, 1 );
			}
	
			//Check if color has 6 or 3 characters and get values
			if (strlen($color) == 6) {
					$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
			} elseif ( strlen( $color ) == 3 ) {
					$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
			} else {
					return $default;
			}
	
			//Convert hexadec to rgb
			$rgb =  array_map('hexdec', $hex);
	
			//Check if opacity is set(rgba or rgb)
			if($opacity){
				if(abs($opacity) > 1)
					$opacity = 1.0;
				$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
			} else {
				$output = 'rgb('.implode(",",$rgb).')';
			}
	
			//Return rgb(a) color string
			return $output;
	}	
	
	/*
	* Display admin notice on plugin install or update
	*/
	public function admin_notice_pro_update(){ 		
		
		if ( get_option('cev_pro_notice_ignore') ) return;
		
		$dismissable_url = esc_url(  add_query_arg( 'cev-pro-ignore-notice', 'true' ) );
		?>		
		<style>		
		.wp-core-ui .notice.cev-dismissable-notice{
			position: relative;
			padding-right: 38px;
		}
		.wp-core-ui .notice.cev-dismissable-notice a.notice-dismiss{
			padding: 9px;
			text-decoration: none;
		} 
		.wp-core-ui .button-primary.btn_pro_notice {
			background: transparent;
			color: #395da4;
			border-color: #395da4;
			text-transform: uppercase;
			padding: 0 11px;
			font-size: 12px;
			height: 30px;
			line-height: 28px;
			margin: 5px 0 15px;
		}
		</style>
         <?php if ( !class_exists( 'customer_email_verification_pro' ) ) { ?>	
		<div class="notice updated notice-success cev-dismissable-notice">
			<a href="<?php echo $dismissable_url; ?>" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></a>
			<h3 style="margin-top: 10px;">Upgrade to Customer Email Verification Pro!</h3>
			<p>We just released a <a href="https://www.zorem.com/product/customer-email-verification-for-woocommerce/">Pro</a> The pro version allows you to require customers to verify their email address before they can checkout on your store, you can delay the new account email notification to after successful verification, require verification when the email address is updated from the account, set validation code expiration and more..</p>
			<a class="button-primary btn_pro_notice" target="blank" href="https://www.zorem.com/product/customer-email-verification-for-woocommerce/">Upgrade to Pro Now ></a>
		</div>
        <?php } ?>
	<?php 		
	}	
	
	/*
	* Hide admin notice on dismiss of ignore-notice
	*/
	public function cev_pro_notice_ignore(){
		if (isset($_GET['cev-pro-ignore-notice'])) {
			update_option( 'cev_pro_notice_ignore', 'true' );
		}
	}
	
	/**
	 * Add plugin action links.
	 *
	 * Add a link to the settings page on the plugins.php page.
	 *
	 * @since 1.0.0
	 *
	 * @param  array  $links List of existing plugin action links.
	 * @return array         List of modified plugin action links.
	 */
	function my_plugin_action_links( $links ) {
		$links = array_merge( array(
			'<a href="' . esc_url( admin_url( '/admin.php?page=customer-email-verification-for-woocommerce' ) ) . '">' . __( 'Settings', 'woocommerce' ) . '</a>'
		), $links );
		
		if(!class_exists('customer_email_verification_pro')) {
			$links = array_merge( $links, array(
				'<a target="_blank" style="color: green; font-weight: bold;" href="' . esc_url( 'https://www.zorem.com/product/customer-verification-for-woocommerce/?utm_source=wp-admin&utm_medium=CEVPRO&utm_campaign=add-ons') . '">' . __( 'Go Pro', 'woocommerce' ) . '</a>'
			) );
		}
		
		return $links;
	}

}

/**
 * Returns an instance of zorem_woo_il_post.
 *
 * @since 1.0
 * @version 1.0
 *
 * @return zorem_woo_il_post
*/
function woo_customer_email_verification() {
	static $instance;

	if ( ! isset( $instance ) ) {		
		$instance = new zorem_woo_customer_email_verification();
	}

	return $instance;
}

/**
 * Register this class globally.
 *
 * Backward compatibility.
*/
woo_customer_email_verification();