=== Ni WooCommerce Sales Report ===
Contributors: anzia
Tags:report, product, print, export, sales report, order report, product report, sales report, daily report
Stable tag: 3.4.9
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/agpl-3.0.html
Requires at least: 4.7
Tested up to: 5.6
WC requires at least: 3.0.0
WC tested up to: 4.9.0
Last Updated Date: 15-January-2021
Requires PHP: 7.0


Enhance WooCommerce sales report beautifully and provide complete solutions for WooCommerce reporting.

== Description ==
<p>
WooCommerce sales report provide sales key indicators, order product sales report, category report and many WooCommerce sales report add-ons.
</p>
<p>
Order product sales report provide order details, customer product details and order details. Order product details include product name, product quantity product total, product discount, product price and many more product information. Customer product details showing customer first name, billing email address and billing country. Order information include order number, order status, order total, order product tax, order line total and etc.
</p>
<p>
<strong>Sales key indicators</strong>
<ul>
	<li>Total Sales order count and value</li>
	<li>This Year Sales order count and value</li>
	<li>This Month Sales order count and value</li>
	<li>This Week Sales order count and value</li>
	<li>Yesterday Sales order count and value</li>
</ul>
<strong>Today Sales Analysis indicators</strong>
<ul>
	<li>Today Order Count</li>
	<li>Today Sales</li>
	<li>Today Product Sold</li>
	<li>Today Discount</li>
	<li>Today Tax</li>
</ul>
<strong>Customer key indicators</strong>
<ul>
	<li>Total Customer Count</li>
	<li>Today Customer Count</li>
	<li>Total Guest Customer Count</li>
	<li>Today Guest Customer Count</li>
</ul>
<strong>Stock Analysis</strong>
<ul>
	<li>Low in stock</li>
	<li>Out of stock</li>
	<li>Most Stocked</li>
</ul>
</p>
<p>
<strong>Top order report sumamry</strong>
<ul>
	<li>Top 10 Recent order list</li>
	<li>Top 5 customer report</li>
	<li>Top 5 billing country report</li>
	<li>Order Status Report in tabular or Pie Chart</li>
	<li>Payemnt gateway report in tabular or Pie Chart</li>
</ul>
</p>
<p>
<strong>Top product summary report</strong>
<ul>
	<li>Today Top Product</li>
	<li>Yesterday Top Product</li>
	<li>Last 7 Days Top Product</li>
</ul>	
</p>


<blockquote>
<h4>Language support</h4>
<ul>
<li>English (Default)</li>
<li>Italiano/Italia</li>
</ul>
<p></p>
</blockquote>


<blockquote>
<h4>for more analytical report buy our pro version</h4>
<ul>
<li>  <a href="http://demo.naziinfotech.com?demo_login=woo_sales_report" target="_blank">View Demo Ni WooCommerce Sales Report Pro</a></li>
<li>  <a href="http://naziinfotech.com/product/ni-woocommerce-sales-report-pro/" target="_blank">Buy Now Ni WooCommerce Sales Report Pro</a></li>
</ul>
<p></p>
</blockquote>


<blockquote>
<h4>Ni WooCommerce Sales Report Pro Version Feature</h4>
<p>we have <a href="http://naziinfotech.com/product/ni-woocommerce-sales-report-pro/" rel="nofollow">Ni WooCommerce Sales Report Pro</a> that have more than 15+ report and 20+ sales analytical count and amount. </p>
<ul>
<li>Dashboard Sales Report Analysis (Order Count,Sales, Product Sold,Discount,Tax )</li>
<li>Sales Order Report, Filter By Date range, Order No, First Name and Order Status, Export,</li>
<li>Bar chart show the Monthly sales amount on dashboard</li>
<li>Line chart show the daily sales amount on dashboard</li>
<li>Sold Product Sales Report</li>
<li>Variation Product Report</li>
<li>Order Status wise sales report</li>
<li>Customer wise sales report</li>
<li>Payment Gateway wise sales report</li>
<li>Country wise sales report</li>
<li>Coupon wise sales report</li>
<li>Product wise sales report</li>
<li>Filter all report with date range, customer name, billing name, emaill address, order status and many more</li>
<li>Ability to filter and CSV/Excel export the sold product report </li>
</ul>
<p></p>
</blockquote>


<blockquote>
<h4>Support/Feedback Email/New Report Requirement</h4>
<ul>
<li>We are open to your suggestions and feedback - Thank you for using or trying out one of our plugins</li>
<li><a href="support@naziinfotech.com">support@naziinfotech.com</a></li>
</ul>
<p></p>
</blockquote>

== Installation ==

1. Use the WordPress plugin installer or upload files to WordPress plugin directory.
2. Activate "Ni WooCommerce Sales Report" plugin from admin panel.
3. Admin Menu Name "Ni Sales Report". 

== Frequently Asked Questions ==

= Where i can get help? =

Click on support tab or email:support@naziinfotech.com

= Do you customize this plugin? =

Yes, as per requirement we can customize this plugin.

== Screenshots ==

1. Dashboard show sales report and key sale indicator
2. Recent order sales report
3. Order status reports on dashboard.
4. Payment method reports on dashboard
5. Sales order list with days filter option 
6. Today Sales analysis on dashboard (Order Count, Sales, Product Sold, Discount, Tax).
7. Category sales report.
8. Top Product report (Today Top Product, Yesterday Top Product, Last 7 Days Top Product).
9. Oreder Summary Report.


== Changelog ==

= version 3.4.9 -15-January-2021 =
* Tested: Compatible With WooCommerce 4.9.0
* Tested: Compatible With WordPress 5.6

= version 3.4.8 -03-December-2020 =
* Tested: Compatible With WooCommerce 4.7.1
* Tested: Compatible With WordPress 5.5.3
* Added: Added New Monthly Sales Report

= version 3.4.7 -18-October-2020 =
* Tested: Compatible With WooCommerce 4.6.0 
* Tested: Compatible With WordPress 5.5.1

= version 3.4.6 -16-September -2020 =
* Tested: Compatible With WooCommerce 4.5.1
* Tested: Compatible With WordPress 5.5.1

= version 3.4.5 -16-August-2020 =
* Tested: Compatible With WooCommerce 4.3.3
* Tested: Compatible With WordPress 5.5


= version 3.4.4 -11-August-2020 =
* Tested: Compatible With WooCommerce 4.3.2 
* Added: New Hook


= version 3.4.2 -17-July-2020 =
* Tested: Compatible With WooCommerce 4.3.0
* Tested: Compatible With WordPress 5.4.2


= version 3.4.1 -08-June-2020 =
* Tested: Compatible With WooCommerce 4.2.0 
* Tested: Compatible With WordPress 5.4.1


= version 3.4.0 -12-April-2020 =
* Tested: Compatible With WooCommerce 4.0.1 
* Tested: Compatible With WordPress 5.4

= version 3.3.9 -24-January-2020 =
* Tested: Compatible With WooCommerce 3.9.2
* Tested: Compatible With WordPress 5.3.2

= version 3.3.8 -31-December-2019 =
* Tested: Compatible With WooCommerce 3.8.1
* Tested: Compatible With WordPress 5.3.1

= version 3.3.7 -15-November-2019 =
* Tested: Compatible With WordPress   5.3

= version 3.3.6 -06-November-2019 =
* Tested: Compatible With WordPress 5.2.4
* Tested: Compatible With WooCommerce 3.8.0 
* Added: Added Yearly Sales

= version 3.3.5 -26-September-2019 =
* Tested: Compatible With WordPress 5.2.3

= version 3.3.4 -17-August-2019 =
* Tested: Compatible With WooCommerce 3.7.0


= version 3.3.3 -29-June-2019 =
* Tested: Compatible With WooCommerce 3.6.4
* Tested: Compatible With WordPress 5.2.2
* Added:  New Summary Report


= version 3.3.2 -11-May-2019 =
* Tested: Compatible With WooCommerce 3.6.2
* Tested: Compatible With WordPress 5.2
* Added:  New layout UI
* BugFix: Internal bugFix 

= version 3.3.1 -16-April-2019 =
* Tested: Compatible With WooCommerce 3.5.8

= version 3.3.0 -31-March-2019 =
* Tested: Compatible With WooCommerce 3.5.7
* Tested: Compatible With WordPress 5.1.1

= version 3.2.9 -24-February-2019 =
* Tested: Compatible With WooCommerce 3.5.5
* Tested: Compatible With WordPress 5.1

= version 3.2.8 -23-January-2019 =
* Tested: Compatible With WooCommerce 3.5.4
* Tested: Compatible With WordPress 5.0.3

= version 3.2.7 -17-December-2018 =
* Tested: Compatible With WooCommerce 3.5.2
* Tested: Compatible With WordPress 5.0.1

= version 3.2.6 -11-November-2018 =
* Tested: Compatible With WooCommerce 3.5.1

= version 3.2.5 -12-October-2018 =
* Tested: Compatible With WooCommerce 3.4.6

= version 3.2.4 -07-October-2018 =
* Tested: Updated hook

= version 3.2.3 -08-September-2018 =
* Tested: Compatible With WooCommerce 3.4.5
* Tested: Compatible With WordPress 4.9.8
* Tested: Added New Top product report

= version 3.2.2 -26-July-2018 =
* Tested: Compatible With WooCommerce 3.4.3
* Modify: add alternate row color

= version 3.2.1 -15-July-2018 =
* Tested: Compatible With WooCommerce 3.4.3
* Tested: Compatible With WordPress 4.9.7

= version 3.2 -15-June-2018 =
* BugFix: Internal bugFix 

= version 3.1.9 -10-June-2018 =
* Tested: Compatible With WooCommerce 3.4.2
* Tested: Compatible With WordPress 4.9.6
* Tested: Added hook for order product report

= version 3.1.8 -22-May-2018 =
* Added: Code improve

= version 3.1.7 -29-April-2018 =
* Added: UI improve
* Change: ajax object 

= version 3.1.6 -15-April-2018 =
* Tested: Compatible With WooCommerce 3.3.5
* Added: UI improve

= version 3.1.5 -04-April-2018 =
* Tested: Compatible With WooCommerce 3.3.4

= version 3.1.4 -19-March-2018 =
* Added: UI improve
* Added: Code improve
* BugFix: Internal bugFix 


= version 3.1.3 -06-March-2018 =
* Added: Added Italiano/Italia Language PO(nisalesreport-it_IT.po) and MO(nisalesreport-it_IT.mo) files

= version 3.1.2 -25-February-2018 =
* Tested: Compatible With WooCommerce 3.3.3

= version 3.1.1 -14-February-2018 =
* Tested: Compatible With WooCommerce 3.3.1
* Tested: Compatible With WordPress 4.9.4

= version 3.1 -26-January-2018 =
* Added: Stock count(Low in stock | Out of stock | Most Stocked) on dashboard


= version 3.0 -26-January-2018 =
* Added: Added new category report
* Tested: Compatible With WordPress 4.9.2

= version 2.11 -8-January-2018 =
* Added: last 60 days search option 

= version 2.10 -14-December-2017 =
* Tested: Compatible With WooCommerce 3.2.6
* Tested: Compatible With WordPress 4.9.1


= version 2.9 -19-November-2017 =
* Tested: Compatible With WooCommerce 3.2.4
* Tested: Compatible With WordPress 4.9


= version 2.8.10 - 12-November-2017 =
* Tested: Compatible With WooCommerce 3.2.3
* Tested: Compatible With WordPress 4.8.3
* Added:  Added do_action for menu and dashboard summary count

= version 2.8.9 - 28-October-2017 =
* Tested: Compatible With WooCommerce 3.2.1
* Added:  Dashboard UI and functionality improvement 

= version 2.8.8 - 12-September-2017 =
* Tested: Compatible With WordPress 4.8.2

= version 2.8.7 - 12-August-2017 =
* Add: Text Domain
* Tested: Compatible With WordPress 4.8.1

= version 2.8.6 - 23-July-2017 =
* Tested: Compatible With WooCommerce 3.1.1
* Added : Billing email address clickable


= version 2.8.5 - 19-June-2017 =
* Tested: Compatible With WooCommerce 3.0.8
* Tested: Compatible With WordPress 4.8

= version 2.8.4 - 05-June-2017 =
* Added : Customer count analysis on dashboard
* BugFix: Internal bugFix 


= version 2.8.3 - 22-May-2017 =
* Tested: Compatible With WooCommerce 3.0.7
* Tested: Compatible With WordPress 4.7.5

= version 2.8.2 - 05-April-2017 =
* Tested: Compatible With WooCommerce 3.0.0

= version 2.8.1 - 29-March-2017 =
* Added : TOP 5 country report
* Added : TOP 5 customer report

= version 2.8 - 15-March-2017 =
* Tested: Compatible With WordPress 4.7.3
* Added : Add Search button
* Added : Add Search by order no
* Added : Added Hyperlink for order edit

= version 2.7 - 24-February-2017 =
* BugFix: Internal bugFix 
* Added : Addon Cost of Goods 

= version 2.6 - 21-February-2017 =
* Added :  Today Sales analysis on dashboard (Order Count,Sales, Product Sold,Discount,Tax )

= version 2.5 - 07-February-2017 =
* Tested: Compatible With WooCommerce 2.6.14 


= version 2.4 - 29-January-2017 =
* Tested: Compatible With WordPress 4.7.2
* Added : New report addon 

= version 2.3 - 23-January-2017 =
* Tested: Compatible With WordPress 4.7.1
* Tested: Compatible With Woocommerce 2.6.13


= version 2.2 - 08-January-2017 =
* BugFix: Improvement and Internal bugFix 

= version 2.1 - 26-December-2016 =
* Tested: Compatible With WordPress 4.7
* Tested: Compatible With Woocommerce 2.6.11

= version 2.0 - 18-December-2016 =
* Added:  Dashboard UI and functionality improvement 
* Added:  payment gateway sales report
* Added:  order status sales report
* BugFix: Internal bugFix 
* Tested: Compatible With Woocommerce 2.6.9


= version 1.8.9 - 29-November-2016 =
* BugFix: Internal bugFix 

= version 1.8.8 - 20-November-2016 =
* Added: Recent order list on dashboard  

= version 1.8.7 - 13-November-2016 =
* Tested: Compatible With Woocommerce 2.6.8 
* Added: Order status reports on dashboard  

= version 1.8.6 - 30-October-2016 =

* Tested: Compatible With Woocommerce 2.6.7 


= version 1.8.5 - 18-September-2016 =
* Added: Payment method summary report on dashboard  
* Tested: Compatible With WordPress 4.6.1

= version 1.8.4 - 23-August-2016 =
* BugFix: Internal bugFix
* Tested: Compatible With WordPress 4.6
* Tested: Compatible With Woocommerce 2.6.4 

= version 1.8.3 - 30-June-2016 =
* BugFix: Internal bugFix
* Tested: Compatible With WordPress 4.5.3

= version 1.8.2 - 20-June-2016 =
* Tested: Compatible With WordPress 4.5.2
* Tested: Compatible With Woocommerce 2.6.1 

= version 1.8.1 - 21-April-2016 =
* Added: Launch Ni WooCommerce Sales Report Pro

= version 1.8 - 16-April-2016 =
* Tested: Compatible With WordPress 4.5
* Tested: Compatible With Woocommerce 2.5.5 

= version 1.7 - 06-February-2016 =
* Tested: Compatible With WordPress 4.4.2
* Tested: Compatible With Woocommerce 2.5.2
* Added: Today completed order count in order summary

= version 1.6 - 21-December-2015 =
* Change: UI improvements
* Tested: Compatible With WordPress 4.4
* Tested: Compatible With Woocommerce 2.4.12

= version 1.5 - 20-October-2015 =
BugFix: Internal bugFix

= version 1.4 - 18-October-2015 =

* Added:  Print sales order product
* Added:  Sales summary (Qty, Tax and Product Total) at the bottom of the sales list
* Change: Plugin menu icon 
* Tested: Compatible With WordPress 4.3.1
* Tested: Compatible With Woocommerce 2.4.7

= version 1.3 - 22-Aug-2015 =

* Added: Last 10 days order filter  
* Tested: Compatible With WordPress 4.3
* Tested: Compatible With Woocommerce 2.4.5

= version 1.2 =
* Added: Order Status

= version 1.1 =
* Fixed: Woocommerce Price Format
* Added: Total Quantity
* Added: Total Tax 
* Added: Total Product 
* Tested: Compatible With WordPress 4.2.2

= version 1.0 =
* Initial release

== Upgrade Notice ==

New Features add upgrade please.

== Disclaimer ==
Users are fully responsible for their own use.