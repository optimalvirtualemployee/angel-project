<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "woocommerce_pdf_invoices_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $attribute_taxonomy_names = wc_get_attribute_taxonomy_names();
    $attribute_taxonomy_names = array_combine($attribute_taxonomy_names, $attribute_taxonomy_names);

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'woocommerce_pdf_invoices_options',
        'use_cdn' => TRUE,
        'dev_mode' => FALSE,
        'display_name' => 'WooCommerce PDF Invoices',
        'display_version' => '1.3.16',
        'page_title' => 'WooCommerce PDF Invoices',
        'update_notice' => TRUE,
        'intro_text' => '',
        'footer_text' => '&copy; ' . date('Y') . ' weLaunch',
        'admin_bar' => TRUE,
        'menu_type' => 'submenu',
        'menu_title' => 'PDF Invoices',
        'allow_sub_menu' => TRUE,
        'page_parent' => 'woocommerce',
        'page_parent_post_type' => 'your_post_type',
        'customizer' => FALSE,
        'default_mark' => '*',
        'hints' => array(
            'icon_position' => 'right',
            'icon_color' => 'lightgray',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     *
     * ---> START SECTIONS
     *
     */

    // if(isset($_POST['post_ID']) || (isset($_GET['page']) && $_GET['page'] == "wc-settings")) { 
    //     return;
    // }
    if(isset($_GET['tab']) && $_GET['tab'] == "email") {
        return;
    }
    
    global $woocommerce;
    $mailer = $woocommerce->mailer();
    $wc_emails = $mailer->get_emails();

    $non_order_emails = array(
        'customer_note',
        'customer_reset_password',
        'customer_new_account'
    );

    $emails = array();
    foreach ($wc_emails as $class => $email) {
        if ( !in_array( $email->id, $non_order_emails ) ) {
            switch ($email->id) {
                case 'new_order':
                    $emails[$email->id] = sprintf('%s (%s)', $email->title, __( 'Admin email', 'woocommerce-pdf-invoices-packing-slips' ) );
                    break;
                case 'customer_invoice':
                    $emails[$email->id] = sprintf('%s (%s)', $email->title, __( 'Manual email', 'woocommerce-pdf-invoices-packing-slips' ) );
                    break;
                default:
                    $emails[$email->id] = $email->title;
                    break;
            }
        }
    }

    Redux::setSection( $opt_name, array(
        'title'  => __( 'PDF Invoices', 'woocommerce-pdf-invoices' ),
        'id'     => 'general',
        'desc'   => __( 'Need support? Please use the comment function on codecanyon.', 'woocommerce-pdf-invoices' ),
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'General', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'general-settings',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'enable',
                'type'     => 'checkbox',
                'title'    => __( 'Enable', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Enable PDF Invoices to use the options below', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'generalAutomatic',
                'type'     => 'checkbox',
                'title'    => __( 'Create Invoices Automatically', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'PDF invoices will be created for each order automatically.', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'generalAttachToMail',
                'type'     => 'checkbox',
                'title'    => __( 'Attach Invoice to Email', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Attach invoices automatically to orders.', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'     =>'generalAttachToMailStatus',
                'type'  => 'select',
                'title' => __('Attach to Email order Statuses', 'woocommerce-pdf-invoices'), 
                'multi' => true,
                'options' => $emails,
                'default' => array(
                    'new_order',
                    'customer_invoice',
                    'customer_processing_order',
                    'customer_completed_order',
                    'customer_refunded_order',
                    'customer_partially_refunded_order',
                ),
                'required' => array('generalAttachToMail','equals','1'),
            ),
            array(
                'id'       => 'generalShowTaxRate',
                'type'     => 'checkbox',
                'title'    => __( 'Show Tax Rates', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Instead of showing the tax amount also show the tax rate.', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'generalShowInvoice',
                'type'     => 'checkbox',
                'title'    => __( 'Show Invoice to Customers', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Show Invoice in Thank you and Order detail pages.', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'generalShowTaxRate',
                'type'     => 'checkbox',
                'title'    => __( 'Show Tax Rates', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Instead of showing the tax amount also show the tax rate.', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'generalShowTaxesBeforeTotal',
                'type'     => 'checkbox',
                'title'    => __( 'Show Taxes before Total', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Show tax rate and total taxes before total.', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
            array(
                'id'       => 'generalShowTaxesBeforeTotalText',
                'type'     => 'text',
                'title'    => __('Tax rate text', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'You can use 3 variables here: first is the tax rate, then tax name and tax label. Use %s for this.', 'woocommerce-pdf-invoices' ),
                'default'  => __( 'including %s Taxes', 'woocommerce-pdf-invoices'),
                'required' => array('generalShowTaxesBeforeTotal','equals','1'),
            ),

            array(
                'id'       => 'generalInvoiceDueDateDays',
                'type'     => 'spinner', 
                'title'    => __('Invoice Due Date Days', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'Use {{invoice_due_date}} as a variable.', 'woocommerce-pdf-invoices' ),
                'default'  => '30',
                'min'      => '1',
                'step'     => '1',
                'max'      => '300',
            ),
            array(
                'id'       => 'generalAttachToMailAdditionalPDF1',
                'title'    => __( 'Additional Email PDF Attachment 1', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'This attachment will be added to the mail and also shows in customers thank you and order details if enabled.', 'woocommerce-pdf-invoices' ),
                'type' => 'media',
                'mode' => false,
                'url'      => true,
            ),
            array(
                'id'       => 'generalAttachToMailAdditionalPDF2',
                'title'    => __( 'Additional Email PDF Attachment 2', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'This attachment will be added to the mail and also shows in customers thank you and order details if enabled.', 'woocommerce-pdf-invoices' ),
                'type' => 'media',
                'mode' => false,
                'url'      => true,
            ),
            array(
                'id'       => 'generalAttachToMailAdditionalPDF3',
                'title'    => __( 'Additional Email PDF Attachment 3', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'This attachment will be added to the mail and also shows in customers thank you and order details if enabled.', 'woocommerce-pdf-invoices' ),
                'type' => 'media',
                'mode' => false,
                'url'      => true,
            ),
            array(
                'id'       => 'generalRenderInvoice',
                'type'     => 'checkbox',
                'title'    => __( 'Render Invoice instead of Download', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Instead of downloading the invoice it will show the PDF directly in browser.', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Invoice Number', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'invoice-number',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'invoiceNumberEnable',
                'type'     => 'checkbox',
                'title'    => __( 'Enable Invoice Numbering', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'If enabled you can use {{invoice_number}} in the invoice document. Click on regenerate if you enable this for the first time.', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
            array(
                'id'       => 'invoiceNumberPrefix',
                'type'     => 'text',
                'title'    => __('Invoice Number Prefix', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'The Prefix can contain custom text or the following vars: {{date}}, {{order_id}}, {{customer_id}}.', 'woocommerce-pdf-invoices' ),
                'default'  => __( 'I-{{date}}-', 'woocommerce-pdf-invoices'),
                'required' => array('invoiceNumberEnable','equals','1'),
            ),
            array(
                'id'       => 'invoiceNumberSuffix',
                'type'     => 'text',
                'title'    => __('Invoice Number Suffix', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'The Suffix can contain custom text or the following vars: {{date}}, {{order_id}}, {{customer_id}}.', 'woocommerce-pdf-invoices' ),
                'default'  => __( '', 'woocommerce-pdf-invoices'),
                'required' => array('invoiceNumberEnable','equals','1'),
            ),
            array(
                'id'       => 'invoiceNumberDateFormat',
                'type'     => 'text',
                'title'    => __('Invoice Number Date Format', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'Use any date format constants from here: <a target="_blank" href="https://www.php.net/manual/en/function.date.php#refsect1-function.date-parameters">PHP Date Constants</a>', 'woocommerce-pdf-invoices' ),
                'default'  => __( 'Y-m', 'woocommerce-pdf-invoices'),
                'required' => array('invoiceNumberEnable','equals','1'),
            ),
            array(
                'id'     =>'invoiceNumberPadLength',
                'type'     => 'spinner', 
                'title'    => __('Number pad length', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'E.g. 7 = 0000005', 'woocommerce-pdf-invoices' ),
                'default'  => '3',
                'min'      => '1',
                'step'     => '1',
                'max'      => '40',
                'required' => array('invoiceNumberEnable','equals','1'),
            ),
            array(
                'id'   => 'invoiceNumberRegenerate',
                'type' => 'info',
                'desc' => '<div style="text-align:center;">' . __('This will (re)generate all invoice numbers.', 'wordpress-gdpr') . '<br>
                    <a href="' . get_admin_url() . 'admin.php?page=woocommerce_pdf_invoices_options_options&regenerate-invoice-numbers=true" class="button button-success">' . __('Regenerate Invoice Numbers', 'woocommerce-bought-together') . '</a>
                    </div>',
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Layout', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'layout',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'format',
                'type'     => 'select',
                'title'    => __( 'Format', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Choose a pre-defined page size. A4 is recommended!', 'woocommerce-pdf-invoices' ),
                'options'  => array(
                    'A4' => __('A4', 'woocommerce-pdf-invoices'),
                    'A4-L' => __('A4 Landscape', 'woocommerce-pdf-invoices'),
                    'A0' => __('A0', 'woocommerce-pdf-invoices'),
                    'A0-L' => __('A0 Landscape', 'woocommerce-pdf-invoices'),
                    'A1' => __('A1', 'woocommerce-pdf-invoices'),
                    'A1-L' => __('A1 Landscape', 'woocommerce-pdf-invoices'),
                    'A3' => __('A3', 'woocommerce-pdf-invoices'),
                    'A3-L' => __('A3 Landscape', 'woocommerce-pdf-invoices'),
                    'A5' => __('A5', 'woocommerce-pdf-invoices'),
                    'A5-L' => __('A5 Landscape', 'woocommerce-pdf-invoices'),
                    'A6' => __('A6', 'woocommerce-pdf-invoices'),
                    'A6-L' => __('A6 Landscape', 'woocommerce-pdf-invoices'),
                    'A7' => __('A7', 'woocommerce-pdf-invoices'),
                    'A7-L' => __('A7 Landscape', 'woocommerce-pdf-invoices'),
                    'A8' => __('A8', 'woocommerce-pdf-invoices'),
                    'A8-L' => __('A8 Landscape', 'woocommerce-pdf-invoices'),
                    'A9' => __('A9', 'woocommerce-pdf-invoices'),
                    'A9-L' => __('A9 Landscape', 'woocommerce-pdf-invoices'),
                    'A10' => __('A10', 'woocommerce-pdf-invoices'),
                    'A10-L' => __('A10 Landscape', 'woocommerce-pdf-invoices'),
                    'Letter' => __('Letter', 'woocommerce-pdf-invoices'),
                    'Legal' => __('Legal', 'woocommerce-pdf-invoices'),
                    'Executive' => __('Executive', 'woocommerce-pdf-invoices'),
                    'Folio' => __('Folio', 'woocommerce-pdf-invoices'),
                ),
                'default' => 'A4',
            ),
            array(
                'id'             => 'layoutPadding',
                'type'           => 'spacing',
                // 'output'         => array('.site-header'),
                'mode'           => 'padding',
                'units'          => array('px'),
                'units_extended' => 'false',
                'title'          => __('Padding', 'woocommerce-pdf-invoices'),
                'default'            => array(
                    'padding-top'     => '50px', 
                    'padding-right'   => '60px', 
                    'padding-bottom'  => '10px', 
                    'padding-left'    => '60px',
                    'units'          => 'px', 
                ),
            ),
            array(
                'id'     =>'layoutTextColor',
                'type'  => 'color',
                'title' => __('Text Color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default'   => '#333333',
            ),
            array(
                'id'     =>'layoutFontFamily',
                'type'  => 'select',
                'title' => __('Default Font', 'woocommerce-pdf-invoices'), 
                'options'  => array(
                    'dejavusans' => __('Sans', 'woocommerce-pdf-invoices' ),
                    'dejavuserif' => __('Serif', 'woocommerce-pdf-invoices' ),
                    'dejavusansmono' => __('Mono', 'woocommerce-pdf-invoices' ),
                    'droidsans' => __('Droid Sans', 'woocommerce-pdf-invoices'),
                    'droidserif' => __('Droid Serif', 'woocommerce-pdf-invoices'),
                    'lato' => __('Lato', 'woocommerce-pdf-invoices'),
                    'lora' => __('Lora', 'woocommerce-pdf-invoices'),
                    'merriweather' => __('Merriweather', 'woocommerce-pdf-invoices'),
                    'montserrat' => __('Montserrat', 'woocommerce-pdf-invoices'),
                    'opensans' => __('Open sans', 'woocommerce-pdf-invoices'),
                    'opensanscondensed' => __('Open Sans Condensed', 'woocommerce-pdf-invoices'),
                    'oswald' => __('Oswald', 'woocommerce-pdf-invoices'),
                    'ptsans' => __('PT Sans', 'woocommerce-pdf-invoices'),
                    'sourcesanspro' => __('Source Sans Pro', 'woocommerce-pdf-invoices'),
                    'slabo' => __('Slabo', 'woocommerce-pdf-invoices'),
                    'raleway' => __('Raleway', 'woocommerce-pdf-invoices'),
                ),
                'default'   => 'dejavusans',
            ),
            array(
                'id'     =>'layoutFontSize',
                'type'     => 'spinner', 
                'title'    => __('Default font size', 'woocommerce-pdf-invoices'),
                'default'  => '9',
                'min'      => '1',
                'step'     => '1',
                'max'      => '40',
            ),
            array(
                'id'     =>'layoutFontLineHeight',
                'type'     => 'spinner', 
                'title'    => __('Default line height', 'woocommerce-pdf-invoices'),
                'default'  => '12',
                'min'      => '1',
                'step'     => '1',
                'max'      => '40',
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'header',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'enableHeader',
                'type'     => 'checkbox',
                'title'    => __( 'Enable', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Enable header', 'woocommerce-pdf-invoices' ),
                'default' => '1',
            ),

            array(
                'id'     =>'headerBackgroundColor',
                'type' => 'color',
                'title' => __('Header background color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#333333',
                'required' => array('enableHeader','equals','1'),
            ),
            array(
                'id'     =>'headerTextColor',
                'type'  => 'color',
                'title' => __('Header text color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#FFFFFF',
                'required' => array('enableHeader','equals','1'),
            ),
            array(
                'id'     =>'headerFontSize',
                'type'     => 'spinner', 
                'title'    => __('Header font size', 'woocommerce-pdf-invoices'),
                'default'  => '8',
                'min'      => '1',
                'step'     => '1',
                'max'      => '40',
            ),
            array(
                'id'     =>'headerLayout',
                'type'  => 'select',
                'title' => __('Header Layout', 'woocommerce-pdf-invoices'), 
                'required' => array('enableHeader','equals','1'),
                'options'  => array(
                    'oneCol' => __('1/1', 'woocommerce-pdf-invoices' ),
                    'twoCols' => __('1/2 + 1/2', 'woocommerce-pdf-invoices' ),
                    'threeCols' => __('1/3 + 1/3 + 1/3', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'twoCols',
            ),
            array(
                'id'     =>'headerMargin',
                'type'     => 'spinner', 
                'title'    => __('Header Margin', 'woocommerce-pdf-invoices'),
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '200',
                'required' => array('enableHeader','equals','1'),
            ),
            array(
                'id'     =>'headerHeight',
                'type'     => 'spinner', 
                'title'    => __('Header Height', 'woocommerce-pdf-invoices'),
                'default'  => '30',
                'min'      => '1',
                'step'     => '1',
                'max'      => '200',
                'required' => array('enableHeader','equals','1'),
            ),
            array(
                'id'     =>'headerVAlign',
                'type'  => 'select',
                'title' => __('Vertical Align', 'woocommerce-pdf-invoices'), 
                'required' => array('enableHeader','equals','1'),
                'options'  => array(
                    'top' => __('Top', 'woocommerce-pdf-invoices' ),
                    'middle' => __('Middle', 'woocommerce-pdf-invoices' ),
                    'bottom' => __('Bottom', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'middle',
            ),
            array(
                'id'     =>'headerTopLeft',
                'type'  => 'select',
                'title' => __('Top Left Header', 'woocommerce-pdf-invoices'), 
                'required' => array('enableHeader','equals','1'),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'bloginfo',
            ),
            array(
                'id'     =>'headerTopLeftText',
                'type'  => 'editor',
                'title' => __('Top Left Header Text', 'woocommerce-pdf-invoices'), 
                'required' => array('headerTopLeft','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'headerTopLeftImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Left Header Image', 'woocommerce-pdf-invoices'), 
                'required' => array('headerTopLeft','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'headerTopMiddle',
                'type'  => 'select',
                'title' => __('Top Middle Header', 'woocommerce-pdf-invoices'), 
                'required' => array('headerLayout','equals','threeCols'),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
            ),
            array(
                'id'     =>'headerTopMiddleText',
                'type'  => 'editor',
                'title' => __('Top Middle Header Text', 'woocommerce-pdf-invoices'), 
                'required' => array('headerTopMiddle','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'headerTopMiddleImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Middle Header Image', 'woocommerce-pdf-invoices'), 
                'required' => array('headerTopMiddle','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'headerTopRight',
                'type'  => 'select',
                'title' => __('Top Right Header', 'woocommerce-pdf-invoices'), 
                'required' => array('headerLayout','equals',array('threeCols','twoCols')),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'pagenumber',
            ),
            array(
                'id'     =>'headerTopRightText',
                'type'  => 'editor',
                'title' => __('Top Right Header Text', 'woocommerce-pdf-invoices'), 
                'required' => array('headerTopRight','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'headerTopRightImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Right Header Image', 'woocommerce-pdf-invoices'), 
                'required' => array('headerTopRight','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Address', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'address',
        'subsection' => true,
        'fields'     => array(
            // array(
            //     'id'       => 'addressLayout',
            //     'type'     => 'image_select',
            //     'title'    => __( 'Select Layout', 'woocommerce-pdf-invoices' ),
            //     'options'  => array(
            //         '1'      => array('img'   => plugin_dir_url( __FILE__ ) . 'img/1.png'),
            //         '2'      => array('img'   => plugin_dir_url( __FILE__ ). 'img/2.png'),
            //         '3'      => array('img'   => plugin_dir_url( __FILE__ ). 'img/3.png'),
            //     ),
            //     'default' => '1'
            // ),
            array(
                'id'     =>'addressTextLeft',
                'type'  => 'editor',
                'title' => __('Address Text Left', 'woocommerce-pdf-invoices'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => '
                <span style="font-size: 9px;">WeLaunch - In den Sandbergen - 49808 Lingen (Ems)</span><br>
                <br>
                {{billing_company}}<br>
                {{billing_first_name}} {{billing_last_name}}<br>
                {{billing_address_1}} {{billing_address_2}}<br>
                {{billing_postcode}} {{billing_city}}<br>
                {{billing_state}} {{billing_country}}'
            ),
            array(
                'id'     =>'addressTextRight',
                'type'  => 'editor',
                'title' => __('Address Text Right', 'woocommerce-pdf-invoices'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    'Invoice No. {{id}}<br>
                    Invoice Date {{order_created}}<br>
                    <br>
                    Your Customer No. {{customer_id}}'
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Content', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'content',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'     =>'contentTextIntro',
                'type'  => 'editor',
                'title' => __('Content Intro Text', 'woocommerce-pdf-invoices'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    '<h4>Invoice No {{id}}</h4>
                    Dear {{billing_first_name}} {{billing_last_name}},<br>
                    <br>
                    thank you very much for your order and the trust you have placed in!<br>
                    I hereby invoice you for the following:'
            ),
            array(
                'id'       => 'contentItemsShowPos',
                'type'     => 'checkbox',
                'title'    => __( 'Show Position', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'     =>'contentItemsShowPosWidth',
                'type'     => 'spinner', 
                'title'    => __('Pos Column Size (%)', 'woocommerce-pdf-invoices'),
                'default'  => '6',
                'min'      => '1',
                'step'     => '10',
                'max'      => '100',
                'required' => array('contentItemsShowPos','equals','1'),
            ),
                array(
                    'id'       => 'contentItemsShowPosName',
                    'type'     => 'text',
                    'title'    => __( 'Column Pos Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Pos.', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowPos','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowImage',
                'type'     => 'checkbox',
                'title'    => __( 'Show Image', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'     =>'contentItemsShowImageWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Image Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '10',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowImage','equals','1'),
                ),
                array(
                    'id'     =>'contentItemsShowImageSize',
                    'type'     => 'spinner', 
                    'title'    => __('Image size (px)', 'woocommerce-pdf-invoices'),
                    'default'  => '80',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '150',
                    'required' => array('contentItemsShowImage','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowImageName',
                    'type'     => 'text',
                    'title'    => __( 'Column Image Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Image', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowImage','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowProduct',
                'type'     => 'checkbox',
                'title'    => __( 'Show Product Name', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'     =>'contentItemsShowProductWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '17',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowProduct','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowProductName',
                    'type'     => 'text',
                    'title'    => __( 'Column Product Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Product', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowProduct','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowLinks',
                    'type'     => 'checkbox',
                    'title'    => __( 'Show Product Link', 'woocommerce-pdf-invoices' ),
                    'default' => 1,
                    'required' => array('contentItemsShowProduct','equals','1'),
                ),
            array(
                'id'   => 'contentItemsShowAttributes',
                'type' => 'select',
                'options' => $attribute_taxonomy_names,
                'multi' => true,
                'title' => __('Show Attributes', 'woocommerce-single-variations'), 
                'subtitle' => __('Product attributes & values will display below product name. ', 'woocommerce-single-variations'),
            ),
            array(
                'id'       => 'contentItemsShowSKU',
                'type'     => 'checkbox',
                'title'    => __( 'Show SKU', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'     =>'contentItemsShowSKUWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '10',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowSKU','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowSKUName',
                    'type'     => 'text',
                    'title'    => __( 'Column SKU Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('SKU', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowSKU','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowWeight',
                'type'     => 'checkbox',
                'title'    => __( 'Show Weight', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'     =>'contentItemsShowWeightWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '13',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowWeight','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowWeightName',
                    'type'     => 'text',
                    'title'    => __( 'Column Weight Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Weight', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowWeight','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowDimensions',
                'type'     => 'checkbox',
                'title'    => __( 'Show Dimensions', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowDimensionsWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '10',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowDimensions','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowDimensionsName',
                    'type'     => 'text',
                    'title'    => __( 'Column Dimensions Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Dimensions', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowDimensions','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowShortDescription',
                'type'     => 'checkbox',
                'title'    => __( 'Show Short Description', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowShortDescriptionWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '17',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowShortDescription','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowShortDescriptionName',
                    'type'     => 'text',
                    'title'    => __( 'Column Short Description', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Short Description', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowShortDescription','equals','1'),
                ),

            array(
                'id'       => 'contentItemsShowDescription',
                'type'     => 'checkbox',
                'title'    => __( 'Show Description', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowDescriptionWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '17',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowDescription','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowDescriptionName',
                    'type'     => 'text',
                    'title'    => __( 'Column Description', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Description', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowDescription','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowQty',
                'type'     => 'checkbox',
                'title'    => __( 'Show Quantity', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'     =>'contentItemsShowQtyWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '10',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowQty','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowQtyName',
                    'type'     => 'text',
                    'title'    => __( 'Column Qty Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Qty', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowQty','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowPrice',
                'type'     => 'checkbox',
                'title'    => __( 'Show Price', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'       => 'contentItemsShowPriceWithoutTaxes',
                    'type'     => 'checkbox',
                    'title'    => __( 'Show Price without taxes', 'woocommerce-pdf-invoices' ),
                    'default' => 0,
                    'required' => array('contentItemsShowPrice','equals','1'),
                ),
                array(
                    'id'     =>'contentItemsShowPriceWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '9',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowPrice','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowPriceName',
                    'type'     => 'text',
                    'title'    => __( 'Column Price Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Price', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowPrice','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowTotalWithoutVAT',
                'type'     => 'checkbox',
                'title'    => __( 'Show Product Total without VAT', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowTotalWithoutVATWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '9',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowTotalWithoutVAT','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowTotalWithoutVATName',
                    'type'     => 'text',
                    'title'    => __( 'Column Total Without VAT Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Total (without VAT)', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowTotalWithoutVAT','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowVATInPercent',
                'type'     => 'checkbox',
                'title'    => __( 'Show Procut VAT in percent', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowVATInPercentWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '9',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowVATInPercent','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowVATInPercentName',
                    'type'     => 'text',
                    'title'    => __( 'Column Total Without VAT Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('VAT (in %)', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowVATInPercent','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowVAT',
                'type'     => 'checkbox',
                'title'    => __( 'Show Product VAT', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowVATWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '9',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowVAT','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowVATName',
                    'type'     => 'text',
                    'title'    => __( 'Column VAT Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('VAT', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowVAT','equals','1'),
                ),
            array(
                'id'       => 'contentItemsShowCouponTotal',
                'type'     => 'checkbox',
                'title'    => __( 'Show Coupon Total Discount', 'woocommerce-pdf-invoices' ),
                'default' => 0,
            ),
                array(
                    'id'     =>'contentItemsShowCouponTotalWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '10',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowCouponTotal','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowCouponTotalName',
                    'type'     => 'text',
                    'title'    => __( 'Column Coupon Total', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Discount', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowCouponTotal','equals','1'),
                ),

            array(
                'id'       => 'contentItemsShowTotal',
                'type'     => 'checkbox',
                'title'    => __( 'Show Product Total', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
                array(
                    'id'     =>'contentItemsShowTotalWidth',
                    'type'     => 'spinner', 
                    'title'    => __('Column Size (%)', 'woocommerce-pdf-invoices'),
                    'default'  => '9',
                    'min'      => '1',
                    'step'     => '10',
                    'max'      => '100',
                    'required' => array('contentItemsShowTotal','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowTotalName',
                    'type'     => 'text',
                    'title'    => __( 'Column Total Name', 'woocommerce-pdf-invoices' ),
                    'default'  => __('Total', 'woocommerce-pdf-invoices'),
                    'required' => array('contentItemsShowTotal','equals','1'),
                ),
                array(
                    'id'       => 'contentItemsShowTotalSaved',
                    'type'     => 'checkbox',
                    'title'    => __( 'Show Product Total Saved Price', 'woocommerce-pdf-invoices' ),
                    'subtitle'    => __( 'When a product is on sale or a coupon is applied it will show the difference amount that is saved.', 'woocommerce-pdf-invoices' ),
                    'default' => 0,
                ),
                    array(
                        'id'       => 'contentItemsShowTotalSavedText',
                        'type'     => 'text',
                        'title'    => __( 'Product Total Saved Price Text', 'woocommerce-pdf-invoices' ),
                        'default'  => __('You saved: %s', 'woocommerce-pdf-invoices'),
                        'required' => array('contentItemsShowTotalSaved','equals','1'),
                    ),


            array(
                'id'       => 'contentItemsTotalsShowCartSubtotal',
                'type'     => 'checkbox',
                'title'    => __( 'Show Total – Cart Subtotal', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'contentItemsTotalsShowShipping',
                'type'     => 'checkbox',
                'title'    => __( 'Show Total – Shipping', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),
            array(
                'id'       => 'contentItemsTotalsShowPayment',
                'type'     => 'checkbox',
                'title'    => __( 'Show Total – Payment Method', 'woocommerce-pdf-invoices' ),
                'default' => 1,
            ),

            array(
                'id'     =>'contentItemsEvenBackgroundColor',
                'type' => 'color',
                'url'      => true,
                'title' => __('Even Items Background Color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#FFFFFF',
            ),
            array(
                'id'     =>'contentItemsEvenTextColor',
                'type' => 'color',
                'url'      => true,
                'title' => __('Even Items Text Color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#333333',
            ),
            array(
                'id'     =>'contentItemsOddBackgroundColor',
                'type' => 'color',
                'url'      => true,
                'title' => __('Odd Items Background Color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#ebebeb',
            ),
            array(
                'id'     =>'contentItemsOddTextColor',
                'type' => 'color',
                'url'      => true,
                'title' => __('Odd Items Text Color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#333333',
            ),
            array(
                'id'     =>'contentTextOutro',
                'type'  => 'editor',
                'title' => __('Content Outro Text', 'woocommerce-pdf-invoices'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    'Please transfer the invoice amount with invoice number to the account stated below.<br>
                    The invoice amount is due immediately.<br>
                    <br>
                    Payment Method: {{payment_method_title}}<br>
                    Shipping Method: {{shipping_method_title}}<br>
                    Your Note: {{customer_note}}<br>
                    <br>
                    Yours sincerely<br>
                    WeLaunch'
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer', 'woocommerce-pdf-invoices' ),
        // 'desc'       => __( '', 'woocommerce-pdf-invoices' ),
        'id'         => 'footer',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'enableFooter',
                'type'     => 'checkbox',
                'title'    => __( 'Enable', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Enable footer', 'woocommerce-pdf-invoices' ),
                'default' => '1',
            ),
            array(
                'id'     =>'footerBackgroundColor',
                'type' => 'color',
                'url'      => true,
                'title' => __('Footer background color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#F7F7F7',
                'required' => array('enableFooter','equals','1'),
            ),
            array(
                'id'     =>'footerTextColor',
                'type'  => 'color',
                'url'      => true,
                'title' => __('Footer text color', 'woocommerce-pdf-invoices'), 
                'validate' => 'color',
                'default' => '#333333',
                'required' => array('enableFooter','equals','1'),
            ),
            array(
                'id'     =>'footerFontSize',
                'type'     => 'spinner', 
                'title'    => __('Footer font size', 'woocommerce-pdf-invoices'),
                'default'  => '8',
                'min'      => '1',
                'step'     => '1',
                'max'      => '40',
            ),
            array(
                'id'     =>'footerLayout',
                'type'  => 'select',
                'title' => __('Footer Layout', 'woocommerce-pdf-invoices'), 
                'required' => array('enableFooter','equals','1'),
                'options'  => array(
                    'oneCol' => __('1/1', 'woocommerce-pdf-invoices' ),
                    'twoCols' => __('1/2 + 1/2', 'woocommerce-pdf-invoices' ),
                    'threeCols' => __('1/3 + 1/3 + 1/3', 'woocommerce-pdf-invoices' ),
                    'fourCols' => __('1/4 + 1/4 + 1/4 + 1/4', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'fourCols',
                'required' => array('enableFooter','equals','1'),
            ),
            array(
                'id'     =>'footerMargin',
                'type'     => 'spinner', 
                'title'    => __('Footer Margin', 'woocommerce-pdf-invoices'),
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '200',
                'required' => array('enableFooter','equals','1'),
            ),
            array(
                'id'     =>'footerHeight',
                'type'     => 'spinner', 
                'title'    => __('Footer Height', 'woocommerce-pdf-invoices'),
                'default'  => '30',
                'min'      => '1',
                'step'     => '1',
                'max'      => '200',
                'required' => array('enableFooter','equals','1'),
            ),
            array(
                'id'     =>'footerVAlign',
                'type'  => 'select',
                'title' => __('Vertical Align', 'woocommerce-pdf-invoices'), 
                'required' => array('enableFooter','equals','1'),
                'options'  => array(
                    'top' => __('Top', 'woocommerce-pdf-invoices' ),
                    'middle' => __('Middle', 'woocommerce-pdf-invoices' ),
                    'bottom' => __('Bottom', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'top',
            ),
            array(
                'id'     =>'footerTopLeft',
                'type'  => 'select',
                'title' => __('Top Left Footer', 'woocommerce-pdf-invoices'), 
                'required' => array('enableFooter','equals','1'),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'text',
            ),
            array(
                'id'     =>'footerTopLeftText',
                'type'  => 'editor',
                'title' => __('Top Left Footer Text', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopLeft','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    'Company<br>
                    Address 123<br>
                    1234 City<br>
                    Country'
            ),
            array(
                'id'     =>'footerTopLeftImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Left Footer Image', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopLeft','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'footerTopMiddleLeft',
                'type'  => 'select',
                'title' => __('Top Middle Left Footer', 'woocommerce-pdf-invoices'), 
                'required' => array('footerLayout','equals', array('fourCols')),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'text',
            ),
            array(
                'id'     =>'footerTopMiddleLeftText',
                'type'  => 'editor',
                'title' => __('Top Middle Left Footer Text', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopMiddleLeft','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    'Tel.: 0160 123 1534<br>
                    E-Mail: info@yourdomain.com<br>
                    Web: https://yourdomain.com'
            ),
            array(
                'id'     =>'footerTopMiddleLeftImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Middle Left Footer Image', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopMiddleLeft','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'footerTopMiddleRight',
                'type'  => 'select',
                'title' => __('Top Middle Right Footer', 'woocommerce-pdf-invoices'), 
                'required' => array('footerLayout','equals', array('fourCols','threeCols')),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'text',
            ),
            array(
                'id'     =>'footerTopMiddleRightText',
                'type'  => 'editor',
                'title' => __('Top Middle Right Footer Text', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopMiddleRight','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    'VAT-ID: 123 435 456<br>
                    Managing Director: Your Name'
            ),
            array(
                'id'     =>'footerTopMiddleRightImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Middle Right Footer Image', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopMiddleRight','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
            array(
                'id'     =>'footerTopRight',
                'type'  => 'select',
                'title' => __('Top Right Footer', 'woocommerce-pdf-invoices'), 
                'required' => array('footerLayout','equals', array('fourCols','threeCols','twoCols')),
                'options'  => array(
                    'none' => __('None', 'woocommerce-pdf-invoices' ),
                    'bloginfo' => __('Blog information', 'woocommerce-pdf-invoices' ),
                    'text' => __('Custom text', 'woocommerce-pdf-invoices' ),
                    'pagenumber' => __('Pagenumber', 'woocommerce-pdf-invoices' ),
                    'image' => __('Image', 'woocommerce-pdf-invoices' ),
                    'exportinfo' => __('Export Information', 'woocommerce-pdf-invoices' ),
                    'qr' => __('QR-Code', 'woocommerce-pdf-invoices' ),
                ),
                'default' => 'text',
            ),
            array(
                'id'     =>'footerTopRightText',
                'type'  => 'editor',
                'title' => __('Top Right Footer Text', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopRight','equals','text'),
                'args'   => array(
                    'teeny'            => false,
                ),
                'default' => 
                    'Bank: Deutsche Bank<br>
                    IBAN: DE 123345 3 456<br>
                    BIC: GEN0123'
            ),
            array(
                'id'     =>'footerTopRightImage',
                'type' => 'media',
                'url'      => true,
                'title' => __('Top Right Footer Image', 'woocommerce-pdf-invoices'), 
                'required' => array('footerTopRight','equals','image'),
                'args'   => array(
                    'teeny'            => false,
                )
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Advanced settings', 'woocommerce-pdf-invoices' ),
        'desc'       => __( 'Custom stylesheet / javascript.', 'woocommerce-pdf-invoices' ),
        'id'         => 'advanced',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'debugMode',
                'type'     => 'checkbox',
                'title'    => __( 'Enable Debug Mode', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'This stops creating the PDF and shows the plain HTML.', 'woocommerce-pdf-invoices' ),
                'default'   => 0,
            ),
            array(
                'id'       => 'debugMPDF',
                'type'     => 'checkbox',
                'title'    => __( 'Enable MPDF Debug Mode', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Show image , font or other errors in the PDF Rendering engine.', 'woocommerce-pdf-invoices' ),
                'default'   => 0,
            ),
            array(
                'id'       => 'dateFormat',
                'type'     => 'text',
                'title'    => __('Date Format', 'woocommerce-pdf-invoices'),
                'subtitle' => __( 'Specify the date format. Default is ' . get_option('date_format'), 'woocommerce-pdf-invoices' ),
                'default'  => get_option('date_format'),
            ),
            array(
                'id'       => 'customCSS',
                'type'     => 'ace_editor',
                'mode'     => 'css',
                'title'    => __( 'Custom CSS', 'woocommerce-pdf-invoices' ),
                'subtitle' => __( 'Add some stylesheet if you want.', 'woocommerce-pdf-invoices' ),
            ),
        )
    ));


    Redux::setSection( $opt_name, array(
        'title'  => __( 'Preview', 'woocommerce-pdf-invoices' ),
        'id'     => 'preview',
        'desc'   => __( 'Need support? Please use the comment function on codecanyon.', 'woocommerce-pdf-invoices' ),
        'icon'   => 'el el-eye-open',
    ) );

    /*
     * <--- END SECTIONS
     */
