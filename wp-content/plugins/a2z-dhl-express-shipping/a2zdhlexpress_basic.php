<?php
/**
 * Plugin Name: Automated DHL Express Shipping
 * Plugin URI: https://hitshipo.com/
 * Description: Realtime Shipping Rates, Shipping label, Pickup, commercial invoice automation included.
 * Version: 2.10.7
 * Author: HITShipo
 * Author URI: https://hitshipo.com/
 * Developer: hitshipo
 * Developer URI: https://hitshipo.com/
 * Text Domain: a2z_dhlexpress
 * Domain Path: /i18n/languages/
 *
 * WC requires at least: 2.6
 * WC tested up to: 4.7
 *
 *
 * @package WooCommerce
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define WC_PLUGIN_FILE.
if ( ! defined( 'A2Z_DHLEXPRESS_PLUGIN_FILE' ) ) {
	define( 'A2Z_DHLEXPRESS_PLUGIN_FILE', __FILE__ );
}



// Include the main WooCommerce class.
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	
	if( !class_exists('a2z_dhlexpress_parent') ){
		Class a2z_dhlexpress_parent
		{
			private $errror = '';
			public function __construct() {
				add_action( 'woocommerce_shipping_init', array($this,'a2z_dhlexpress_init') );
				add_action( 'init', array($this,'hit_order_status_update') );
				add_filter( 'woocommerce_shipping_methods', array($this,'a2z_dhlexpress_method') );
				add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'a2z_dhlexpress_plugin_action_links' ) );
				add_action( 'add_meta_boxes', array($this, 'create_dhl_shipping_meta_box' ));
				add_action( 'save_post', array($this, 'hit_create_dhl_shipping'), 10, 1 );
				add_action( 'save_post', array($this, 'hit_create_dhl_return_shipping'), 10, 1 );
				add_filter( 'bulk_actions-edit-shop_order', array($this, 'hit_bulk_order_menu'), 10, 1 );
				add_filter( 'handle_bulk_actions-edit-shop_order', array($this, 'hit_bulk_create_order'), 10, 3 );
				add_action( 'admin_notices', array($this, 'shipo_bulk_label_action_admin_notice' ) );
				add_filter( 'woocommerce_product_data_tabs', array($this,'hit_product_data_tab') );
				add_action( 'woocommerce_process_product_meta', array($this,'hit_save_product_options' ));
				add_filter( 'woocommerce_product_data_panels', array($this,'hit_product_option_view') );

				// add_action( 'woocommerce_checkout_order_processed', array( $this, 'hit_wc_checkout_order_processed' ) );
				// add_action( 'woocommerce_thankyou', array( $this, 'hit_wc_checkout_order_processed' ) );
				add_action( 'woocommerce_order_status_processing', array( $this, 'hit_wc_checkout_order_processed' ) );
				add_action('woocommerce_order_details_after_order_table', array( $this, 'dhl_track' ) );

				$general_settings = get_option('a2z_dhl_main_settings');
				$general_settings = empty($general_settings) ? array() : $general_settings;

				if(isset($general_settings['a2z_dhlexpress_v_enable']) && $general_settings['a2z_dhlexpress_v_enable'] == 'yes' ){
					add_action( 'woocommerce_product_options_shipping', array($this,'hit_choose_vendor_address' ));
					add_action( 'woocommerce_process_product_meta', array($this,'hit_save_product_meta' ));

					// Edit User Hooks
					add_action( 'edit_user_profile', array($this,'hit_define_dhl_credentails') );
					add_action( 'edit_user_profile_update', array($this, 'save_user_fields' ));

				}
			
			}

			public function hit_product_data_tab( $tabs) {

				$tabs['hits_product_options'] = array(
					'label'		=> __( 'HITShipo - DHL Options', 'woocommerce' ),
					'target'	=> 'hit_dhl_product_options',
					// 'class'		=> array( 'show_if_simple', 'show_if_variable' ),
				);
			
				return $tabs;
			
			}

			public function hit_save_product_options( $post_id ){
				if( isset($_POST['hits_dhl_cc']) ){
					$cc = $_POST['hits_dhl_cc'];
					update_post_meta( $post_id, 'hits_dhl_cc', (string) esc_html( $cc ) );
					// print_r($post_id);die();
				}
			}

			public function hit_product_option_view(){
				global $woocommerce, $post;
				$hits_dhl_saved_cc = get_post_meta( $post->ID, 'hits_dhl_cc', true);
				?>
				<div id='hit_dhl_product_options' class='panel woocommerce_options_panel'>
					<div class='options_group'>
						<p class="form-field">
							<label for="hits_dhl_cc"><?php _e( 'Enter Commodity code', 'woocommerce' ); ?></label>
							<span class='woocommerce-help-tip' data-tip="<?php _e('Enter commodity code for product (20 charcters max).','hit_ups') ?>"></span>
							<input type='text' id='hits_dhl_cc' name='hits_dhl_cc' maxlength="20" <?php echo (!empty($hits_dhl_saved_cc) ? 'value="'.$hits_dhl_saved_cc.'"' : '');?> style="width: 30%;">
						</p>
					</div>
				</div>
				<?php
			}

			public function hit_bulk_order_menu( $actions ) {
				// echo "<pre>";print_r($actions);die();
				$actions['create_label_shipo'] = __( 'Create Labels - HITShipo', 'woocommerce' );
				return $actions;
			}

			public function hit_bulk_create_order($redirect_to, $action, $order_ids){
				$success = 0;
				$failed = 0;
				$failed_ids = [];
				if($action == "create_label_shipo"){
					
					if(!empty($order_ids)){
						$create_shipment_for = "default";
						$service_code = "N";
						$ship_content = 'Shipment Content';
						$pickup_mode = 'manual';
						
						foreach($order_ids as $key => $order_id){
							$order = wc_get_order( $order_id );
							if($order){

									$order_data = $order->get_data();
									$order_id = $order_data['id'];
									$order_currency = $order_data['currency'];

									$order_shipping_first_name = $order_data['shipping']['first_name'];
									$order_shipping_last_name = $order_data['shipping']['last_name'];
									$order_shipping_company = empty($order_data['shipping']['company']) ? $order_data['shipping']['first_name'] :  $order_data['shipping']['company'];
									$order_shipping_address_1 = $order_data['shipping']['address_1'];
									$order_shipping_address_2 = $order_data['shipping']['address_2'];
									$order_shipping_city = $order_data['shipping']['city'];
									$order_shipping_state = $order_data['shipping']['state'];
									$order_shipping_postcode = $order_data['shipping']['postcode'];
									$order_shipping_country = $order_data['shipping']['country'];
									$order_shipping_phone = $order_data['billing']['phone'];
									$order_shipping_email = $order_data['billing']['email'];
									
									
									$items = $order->get_items();
									$pack_products = array();
									$general_settings = get_option('a2z_dhl_main_settings',array());

									if($general_settings['a2z_dhlexpress_country'] != $order_shipping_country){
										$service_code = "P";
									}

									foreach ( $items as $item ) {
										$product_data = $item->get_data();

										$product = array();
										$product['product_name'] = str_replace('"', '', $product_data['name']);
										$product['product_quantity'] = $product_data['quantity'];
										$product['product_id'] = $product_data['product_id'];

										$product_variation_id = $item->get_variation_id();
										if(empty($product_variation_id)){
											$getproduct = wc_get_product( $product_data['product_id'] );
										}else{
											$getproduct = wc_get_product( $product_variation_id );
										}
										
										$woo_weight_unit = get_option('woocommerce_weight_unit');
										$woo_dimension_unit = get_option('woocommerce_dimension_unit');

										$dhl_mod_weight_unit = $dhl_mod_dim_unit = '';

										if(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'KG_CM')
										{
											$dhl_mod_weight_unit = 'kg';
											$dhl_mod_dim_unit = 'cm';
										}elseif(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'LB_IN')
										{
											$dhl_mod_weight_unit = 'lbs';
											$dhl_mod_dim_unit = 'in';
										}
										else
										{
											$dhl_mod_weight_unit = 'kg';
											$dhl_mod_dim_unit = 'cm';
										}

										$product['price'] = $getproduct->get_price();

										if(!$product['price']){
											$product['price'] = (isset($product_data['total']) && isset($product_data['quantity'])) ? number_format(($product_data['total'] / $product_data['quantity']), 2) : 0;
										}

										if ($woo_dimension_unit != $dhl_mod_dim_unit) {
										$prod_width = $getproduct->get_width();
										$prod_height = $getproduct->get_height();
										$prod_depth = $getproduct->get_length();

										//wc_get_dimension( $dimension, $to_unit, $from_unit );
										$product['width'] = round(wc_get_dimension( $prod_width, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);
										$product['height'] = round(wc_get_dimension( $prod_height, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);
										$product['depth'] = round(wc_get_dimension( $prod_depth, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);

										}else {
											$product['width'] = $getproduct->get_width();
											$product['height'] = $getproduct->get_height();
											$product['depth'] = $getproduct->get_length();
										}
										
										if ($woo_weight_unit != $dhl_mod_weight_unit) {
											$prod_weight = $getproduct->get_weight();
											$product['weight'] = round(wc_get_weight( $prod_weight, $dhl_mod_weight_unit, $woo_weight_unit ), 2);
										}else{
											$product['weight'] = $getproduct->get_weight();
										}

										$pack_products[] = $product;
										
									}
									
									$custom_settings = array();
									$custom_settings['default'] = array(
														'a2z_dhlexpress_site_id' => $general_settings['a2z_dhlexpress_site_id'],
														'a2z_dhlexpress_site_pwd' => $general_settings['a2z_dhlexpress_site_pwd'],
														'a2z_dhlexpress_acc_no' => $general_settings['a2z_dhlexpress_acc_no'],
														'a2z_dhlexpress_import_no' => $general_settings['a2z_dhlexpress_import_no'],
														'a2z_dhlexpress_shipper_name' => $general_settings['a2z_dhlexpress_shipper_name'],
														'a2z_dhlexpress_company' => $general_settings['a2z_dhlexpress_company'],
														'a2z_dhlexpress_mob_num' => $general_settings['a2z_dhlexpress_mob_num'],
														'a2z_dhlexpress_email' => $general_settings['a2z_dhlexpress_email'],
														'a2z_dhlexpress_address1' => $general_settings['a2z_dhlexpress_address1'],
														'a2z_dhlexpress_address2' => $general_settings['a2z_dhlexpress_address2'],
														'a2z_dhlexpress_city' => $general_settings['a2z_dhlexpress_city'],
														'a2z_dhlexpress_state' => $general_settings['a2z_dhlexpress_state'],
														'a2z_dhlexpress_zip' => $general_settings['a2z_dhlexpress_zip'],
														'a2z_dhlexpress_country' => $general_settings['a2z_dhlexpress_country'],
														'a2z_dhlexpress_gstin' => $general_settings['a2z_dhlexpress_gstin'],
														'a2z_dhlexpress_con_rate' => $general_settings['a2z_dhlexpress_con_rate'],
														'service_code' => $service_code,
														'a2z_dhlexpress_label_email' => $general_settings['a2z_dhlexpress_label_email'],
													);
									$vendor_settings = array();
								// 	if(isset($general_settings['a2z_dhlexpress_v_enable']) && $general_settings['a2z_dhlexpress_v_enable'] == 'yes' && isset($general_settings['a2z_dhlexpress_v_labels']) && $general_settings['a2z_dhlexpress_v_labels'] == 'yes'){
								// 	// Multi Vendor Enabled
								// 	foreach ($pack_products as $key => $value) {
								// 		$product_id = $value['product_id'];
								// 		$dhl_account = get_post_meta($product_id,'dhl_express_address', true);
								// 		if(empty($dhl_account) || $dhl_account == 'default'){
								// 			$dhl_account = 'default';
								// 			if (!isset($vendor_settings[$dhl_account])) {
								// 				$vendor_settings[$dhl_account] = $custom_settings['default'];
								// 			}
											
								// 			$vendor_settings[$dhl_account]['products'][] = $value;
								// 		}

								// 		if($dhl_account != 'default'){
								// 			$user_account = get_post_meta($dhl_account,'a2z_dhl_vendor_settings', true);
								// 			$user_account = empty($user_account) ? array() : $user_account;
								// 			if(!empty($user_account)){
								// 				if(!isset($vendor_settings[$dhl_account])){

								// 					$vendor_settings[$dhl_account] = $custom_settings['default'];
													
								// 				if($user_account['a2z_dhlexpress_site_id'] != '' && $user_account['a2z_dhlexpress_site_pwd'] != '' && $user_account['a2z_dhlexpress_acc_no'] != ''){
													
								// 					$vendor_settings[$dhl_account]['a2z_dhlexpress_site_id'] = $user_account['a2z_dhlexpress_site_id'];

								// 					if($user_account['a2z_dhlexpress_site_pwd'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_site_pwd'] = $user_account['a2z_dhlexpress_site_pwd'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_acc_no'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_acc_no'] = $user_account['a2z_dhlexpress_acc_no'];
								// 					}

								// 					$vendor_settings[$dhl_account]['a2z_dhlexpress_import_no'] = !empty($user_account['a2z_dhlexpress_import_no']) ? $user_account['a2z_dhlexpress_import_no'] : '';
													
								// 				}

								// 				if ($user_account['a2z_dhlexpress_address1'] != '' && $user_account['a2z_dhlexpress_city'] != '' && $user_account['a2z_dhlexpress_state'] != '' && $user_account['a2z_dhlexpress_zip'] != '' && $user_account['a2z_dhlexpress_country'] != '' && $user_account['a2z_dhlexpress_shipper_name'] != '') {
													
								// 					if($user_account['a2z_dhlexpress_shipper_name'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_shipper_name'] = $user_account['a2z_dhlexpress_shipper_name'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_company'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_company'] = $user_account['a2z_dhlexpress_company'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_mob_num'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_mob_num'] = $user_account['a2z_dhlexpress_mob_num'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_email'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_email'] = $user_account['a2z_dhlexpress_email'];
								// 					}

								// 					if ($user_account['a2z_dhlexpress_address1'] != '') {
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_address1'] = $user_account['a2z_dhlexpress_address1'];
								// 					}

								// 					$vendor_settings[$dhl_account]['a2z_dhlexpress_address2'] = $user_account['a2z_dhlexpress_address2'];
													
								// 					if($user_account['a2z_dhlexpress_city'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_city'] = $user_account['a2z_dhlexpress_city'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_state'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_state'] = $user_account['a2z_dhlexpress_state'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_zip'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_zip'] = $user_account['a2z_dhlexpress_zip'];
								// 					}

								// 					if($user_account['a2z_dhlexpress_country'] != ''){
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_country'] = $user_account['a2z_dhlexpress_country'];
								// 					}

								// 					$vendor_settings[$dhl_account]['a2z_dhlexpress_gstin'] = $user_account['a2z_dhlexpress_gstin'];
								// 					$vendor_settings[$dhl_account]['a2z_dhlexpress_con_rate'] = $user_account['a2z_dhlexpress_con_rate'];

								// 				}
													
								// 					if(isset($general_settings['a2z_dhlexpress_v_email']) && $general_settings['a2z_dhlexpress_v_email'] == 'yes'){
								// 						$user_dat = get_userdata($dhl_account);
								// 						$vendor_settings[$dhl_account]['a2z_dhlexpress_label_email'] = $user_dat->data->user_email;
								// 					}
													

								// 					if($order_data['shipping']['country'] != $vendor_settings[$dhl_account]['a2z_dhlexpress_country']){
								// 						$vendor_settings[$dhl_account]['service_code'] = empty($service_code) ? $user_account['a2z_dhlexpress_def_inter'] : $service_code;
								// 					}else{
								// 						$vendor_settings[$dhl_account]['service_code'] = empty($service_code) ? $user_account['a2z_dhlexpress_def_dom'] : $service_code;
								// 					}
								// 				}
								// 				unset($value['product_id']);
								// 				$vendor_settings[$dhl_account]['products'][] = $value;
								// 			}
								// 		}

								// 	}

								// }

								if(empty($vendor_settings)){
									$custom_settings['default']['products'] = $pack_products;
								}else{
									$custom_settings = $vendor_settings;
								}

								if(!empty($general_settings) && isset($general_settings['a2z_dhlexpress_integration_key']) && isset($custom_settings[$create_shipment_for])){
									$mode = 'live';
									if(isset($general_settings['a2z_dhlexpress_test']) && $general_settings['a2z_dhlexpress_test']== 'yes'){
										$mode = 'test';
									}

									$execution = 'manual';
									
									$boxes_to_shipo = array();
									if (isset($general_settings['a2z_dhlexpress_packing_type']) && $general_settings['a2z_dhlexpress_packing_type'] == "box") {
										if (isset($general_settings['a2z_dhlexpress_boxes']) && !empty($general_settings['a2z_dhlexpress_boxes'])) {
											foreach ($general_settings['a2z_dhlexpress_boxes'] as $box) {
												if ($box['enabled'] != 1) {
													continue;
												}else {
													$boxes_to_shipo[] = $box;
												}
											}
										}
									}

									$c_codes = [];

									foreach($custom_settings[$create_shipment_for]['products'] as $prod_to_shipo){
										$saved_cc = get_post_meta( $prod_to_shipo['product_id'], 'hits_dhl_cc', true);
										if(!empty($saved_cc)){
											$c_codes[] = $saved_cc;
										}
									}
									
									$data = array();
									$data['integrated_key'] = $general_settings['a2z_dhlexpress_integration_key'];
									$data['order_id'] = $order_id;
									$data['exec_type'] = $execution;
									$data['mode'] = $mode;
									$data['meta'] = array(
										"site_id" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_site_id'],
										"password"  => $custom_settings[$create_shipment_for]['a2z_dhlexpress_site_pwd'],
										"accountnum" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_acc_no'],
										"t_company" => $order_shipping_company,
										"t_address1" => str_replace('"', '', $order_shipping_address_1),
										"t_address2" => str_replace('"', '', $order_shipping_address_2),
										"t_city" => $order_shipping_city,
										"t_state" => $order_shipping_state,
										"t_postal" => $order_shipping_postcode,
										"t_country" => $order_shipping_country,
										"t_name" => $order_shipping_first_name . ' '. $order_shipping_last_name,
										"t_phone" => $order_shipping_phone,
										"t_email" => $order_shipping_email,
										"dutiable" => $general_settings['a2z_dhlexpress_duty_payment'],
										"insurance" => $general_settings['a2z_dhlexpress_insure'],
										"pack_this" => "Y",
										"products" => $custom_settings[$create_shipment_for]['products'],
										"pack_algorithm" => $general_settings['a2z_dhlexpress_packing_type'],
										"boxes" => $boxes_to_shipo,
										"max_weight" => $general_settings['a2z_dhlexpress_max_weight'],
										"plt" => ($general_settings['a2z_dhlexpress_ppt'] == 'yes') ? "Y" : "N",
										"airway_bill" => ($general_settings['a2z_dhlexpress_aabill'] == 'yes') ? "Y" : "N",
										"sd" => ($general_settings['a2z_dhlexpress_sat'] == 'yes') ? "Y" : "N",
										"cod" => ($general_settings['a2z_dhlexpress_cod'] == 'yes') ? "Y" : "N",
										"service_code" => $custom_settings[$create_shipment_for]['service_code'],
										"email_alert" => ( isset($general_settings['a2z_dhlexpress_email_alert']) && ($general_settings['a2z_dhlexpress_email_alert'] == 'yes') ) ? "Y" : "N",
										"shipment_content" => $ship_content,
										"s_company" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_company'],
										"s_address1" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_address1'],
										"s_address2" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_address2'],
										"s_city" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_city'],
										"s_state" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_state'],
										"s_postal" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_zip'],
										"s_country" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_country'],
										"gstin" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_gstin'],
										"s_name" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_shipper_name'],
										"s_phone" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_mob_num'],
										"s_email" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_email'],
										"label_size" => $general_settings['a2z_dhlexpress_print_size'],
										"sent_email_to" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_label_email'],
										"pic_exec_type" => $pickup_mode,
										"pic_loc_type" => (isset($general_settings['a2z_dhlexpress_pickup_loc_type']) ? $general_settings['a2z_dhlexpress_pickup_loc_type'] : ''),
										"pic_pac_loc" => (isset($general_settings['a2z_dhlexpress_pickup_pac_loc']) ? $general_settings['a2z_dhlexpress_pickup_pac_loc'] : ''),
										"pic_contact_per" => (isset($general_settings['a2z_dhlexpress_pickup_per_name']) ? $general_settings['a2z_dhlexpress_pickup_per_name'] : ''),
										"pic_contact_no" => (isset($general_settings['a2z_dhlexpress_pickup_per_contact_no']) ? $general_settings['a2z_dhlexpress_pickup_per_contact_no'] : ''),
										"pic_door_to" => (isset($general_settings['a2z_dhlexpress_pickup_door_to']) ? $general_settings['a2z_dhlexpress_pickup_door_to'] : ''),
										"pic_type" => (isset($general_settings['a2z_dhlexpress_pickup_type']) ? $general_settings['a2z_dhlexpress_pickup_type'] : ''),
										"pic_days_after" => (isset($general_settings['a2z_dhlexpress_pickup_date']) ? $general_settings['a2z_dhlexpress_pickup_date'] : ''),
										"pic_open_time" => (isset($general_settings['a2z_dhlexpress_pickup_open_time']) ? $general_settings['a2z_dhlexpress_pickup_open_time'] : ''),
										"pic_close_time" => (isset($general_settings['a2z_dhlexpress_pickup_close_time']) ? $general_settings['a2z_dhlexpress_pickup_close_time'] : ''),
										"pic_mail_date" => date('c'),
										"pic_date" => date("Y-m-d"),
										"payment_con" => (isset($general_settings['a2z_dhlexpress_pay_con']) ? $general_settings['a2z_dhlexpress_pay_con'] : 'S'),
										"cus_payment_con" => (isset($general_settings['a2z_dhlexpress_cus_pay_con']) ? $general_settings['a2z_dhlexpress_cus_pay_con'] : ''),
										"translation" => ( (isset($general_settings['a2z_dhlexpress_translation']) && $general_settings['a2z_dhlexpress_translation'] == "yes" ) ? 'Y' : 'N'),
										"translation_key" => (isset($general_settings['a2z_dhlexpress_translation_key']) ? $general_settings['a2z_dhlexpress_translation_key'] : ''),
										"commodity_code" => $c_codes,
									);
									 	//echo '<pre>';print_r($data);die();
										$curl = curl_init();
										curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode( $data));
										curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
										curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
										curl_setopt_array($curl, array(
											CURLOPT_URL            => "https://app.hitshipo.com/api/dhl_manual.php",
											// CURLOPT_URL            => "localhost/hitshipo/api/dhl_manual.php",		//For Bulk Ship
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_ENCODING       => "",
											CURLOPT_MAXREDIRS      => 10,
											CURLOPT_HEADER         => false,
											CURLOPT_TIMEOUT        => 60,
											CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
											CURLOPT_CUSTOMREQUEST  => 'POST',
										));

										$output = json_decode(curl_exec($curl),true);
										// echo '<pre>';print_r($output);die();
										if($output){
											if(isset($output['status']) || isset($output['pickup_status'])){

												if(isset($output['status']) && is_array($output['status']) && $output['status'][0] != 'success'){
													// update_option('hit_dhl_status_'.$order_id, $output['status'][0]);
													$failed += 1;
													$failed_ids[] = $order_id;

												}else if(isset($output['status']) && $output['status'] == 'success'){
													$output['user_id'] = $create_shipment_for;
													$result_arr = json_decode(get_option('hit_dhl_values_'.$order_id, array()));
													$result_arr[] = $output;

													update_option('hit_dhl_values_'.$order_id, json_encode($result_arr));

													$success += 1;
													
												}
												if (isset($output['pickup_status']) && $output['pickup_status'] != 'Success') {
													$pic_res['status'] = "failed";
													update_option('hit_dhl_pickup_values_'.$order_id, json_encode($pic_res));
												}elseif (isset($output['pickup_status']) && $output['pickup_status'] == 'Success') {
													$pic_res['confirm_no'] = $output['pickup_confirm_no'];
													$pic_res['ready_time'] = $output['pickup_ready_time'];
													$pic_res['pickup_date'] = $output['pickup_date'];
													$pic_res['status'] = "success";

													update_option('hit_dhl_pickup_values_'.$order_id, json_encode($pic_res));
												}
											}else{
												$failed += 1;
												$failed_ids[] = $order_id;
											}
										}else{
											$failed += 1;
											$failed_ids[] = $order_id;
										}
									}
							}else{
								$failed += 1;
							}
							
						}
						return $redirect_to = add_query_arg( array(
							'success_lbl' => $success,
							'failed_lbl' => $failed,
							// 'failed_lbl_ids' => implode( ',', rtrim($failed_ids, ",") ),
						), $redirect_to );
					}
				}
				
			}

			function shipo_bulk_label_action_admin_notice() {
				if(isset($_GET['success_lbl']) && isset($_GET['failed_lbl'])){
					printf( '<div id="message" class="updated fade"><p>
						Generated labels: '.$_GET['success_lbl'].' Failed Label: '.$_GET['failed_lbl'].' </p></div>');
				}

			}

			public function dhl_track($order){
				$general_settings = get_option('a2z_dhl_main_settings',array());
				$order_id = $order->get_id();
				$json_data = get_option('hit_dhl_values_'.$order_id);

				if (!empty($json_data) && isset($general_settings['a2z_dhlexpress_trk_status_cus']) && $general_settings['a2z_dhlexpress_trk_status_cus'] == "yes") {

					$array_data_to_track = json_decode($json_data, true);
					$track_datas = array();

					if (isset($array_data_to_track[0])) {
						$track_datas = $array_data_to_track;
					}else {
						$track_datas[] = $array_data_to_track;
					}
					$trk_count = 1;
					$tot_trk_count = count($track_datas);
					
// echo '<pre>';print_r($array_data_to_track);echo '<br/>'; print_r($track_datas);die();

					if ($track_datas) {

						echo '<div style = "box-shadow: 1px 1px 10px 1px #d2d2d2;">
							<div style= "font-size: 1.5rem; padding: 20px;"><img src="https://hitshipo.com/assets/img/others/dhl.png" style="width: 70px; height: 70px; border-radius: 50%; vertical-align: middle; margin-right: 20px">
							DHL Express Tracking</div>';

						foreach ($track_datas as $value) {
							if (isset($general_settings['a2z_dhlexpress_site_id']) && isset($general_settings['a2z_dhlexpress_site_pwd'])) {
								$trk_no = $value['tracking_num'];
								$user_id = $value['user_id'];		//Test track No : 2192079993	'.$trk_no.'

								$xml = '<?xml version="1.0" encoding="UTF-8"?>
										<req:KnownTrackingRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com TrackingRequestKnown.xsd" schemaVersion="1.0">
											<Request>
												<ServiceHeader>
													<MessageTime>2002-06-25T11:28:56-08:00</MessageTime>
													<MessageReference>1234567890123456789012345678</MessageReference>
													<SiteID>'.$general_settings['a2z_dhlexpress_site_id'].'</SiteID>
													<Password>'.$general_settings['a2z_dhlexpress_site_pwd'].'</Password>
												</ServiceHeader>
											</Request>
											<LanguageCode>en</LanguageCode>
											<AWBNumber>'.$trk_no.'</AWBNumber>
											<LevelOfDetails>ALL_CHECK_POINTS</LevelOfDetails>
											<PiecesEnabled>B</PiecesEnabled>
										</req:KnownTrackingRequest>';

								$ch=curl_init();
									curl_setopt($ch, CURLOPT_URL,"https://xmlpi-ea.dhl.com/XMLShippingServlet"); 
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_HEADER, 0);
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									$result = curl_exec($ch);

									if (!empty($result)) {
										$xml = simplexml_load_string($result);
										$xml = json_decode(json_encode($xml), true);
// echo '<pre>';print_r($xml);die();
										if(isset($xml['AWBInfo']['ShipmentInfo']['ShipmentEvent']) && $xml['AWBInfo']['ShipmentInfo']['ShipmentEvent']){

											$events = $xml['AWBInfo']['ShipmentInfo']['ShipmentEvent'];
											$last_event_status = '';
											
											$event_count = count($events);
        									if (isset($events[$event_count -1])) {
        										$last_event_status = $events[$event_count -1]['ServiceEvent']['Description'];
        									}

											$to_disp = '<div style= "background-color:#4CBB87; width: 100%; height: 80px; display: flex; flex-direction: row;">
															<div style= "color: #ecf0f1; display: flex; flex-direction: column; align-items: center; padding: 23px; width: 50%;">Package Status: '.$last_event_status.'</div>
															<span style= "border-left: 4px solid #fdfdfd; margin-top: 20px; height: 40px;"></span>
															<div style= "color: #ecf0f1; display: flex; flex-direction: column; align-items: center; padding: 12px; width: 50%;">Package '.$trk_count.' of '.$tot_trk_count.'
																<span>Tracking No: '.$trk_no.'</span>
															</div>
														</div>
														<div style= "padding-bottom: 5px;">
															<ul style= "list-style: none; padding-bottom: 5px;">';
											
        									foreach ($events as $key => $value) {
        										$event_status = $value['ServiceEvent']['Description'];
        										$event_loc = $value['ServiceArea']['Description'];
        										$event_time = date('h:i - A', strtotime($value['Time']));
        										$event_date = date('M d Y', strtotime($value['Date']));
        										
// echo '<pre>';echo '<h4>XML</h4>';print_r($value);print_r($events);die();
        										$to_disp .= '<li style= "display: flex; flex-direction: row;">
																<div style= "display: flex;margin-top: 0px; margin-bottom: 0px; ">
																	<div style="border-left:1px #ecf0f1 solid; position: relative; left:161px; height:150%; margin-top: -28px; z-index: -1;"></div>
																	<div style= "display: flex; flex-direction: column; width: 120px; align-items: end;">
																		<p style= "font-weight: bold; margin: 0;">'.$event_date.'</p>
																		<p style= "margin: 0; color: #4a5568;">'.$event_time.'</p>
																	</div>
																	<div style= "display: flex; flex-direction: column; width: 80px; align-items: center;">';

														if ($value['ServiceEvent']['EventCode'] == "OK") {
															$to_disp .= '<img style="width: 34px; height: 34px;" src="data:image/svg+xml;charset=utf-8;base64,PHN2ZyB4bWxuczpza2V0Y2g9Imh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaC9ucyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTI4IDEyOCI+PHN0eWxlIHR5cGU9InRleHQvY3NzIj4uc3Qwe2ZpbGw6IzRDQkI4Nzt9IC5zdDF7ZmlsbDojRkZGRkZGO308L3N0eWxlPjxnIGlkPSJEZWxpdmVkIiBza2V0Y2g6dHlwZT0iTVNMYXllckdyb3VwIj48cGF0aCBpZD0iT3ZhbC03LUNvcHktMiIgc2tldGNoOnR5cGU9Ik1TU2hhcGVHcm91cCIgY2xhc3M9InN0MCIgZD0iTTY0IDEyOGMzNS4zIDAgNjQtMjguNyA2NC02NHMtMjguNy02NC02NC02NC02NCAyOC43LTY0IDY0IDI4LjcgNjQgNjQgNjR6Ii8+PHBhdGggaWQ9IlNoYXBlIiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIiBjbGFzcz0ic3QxIiBkPSJNODIuNSA1My4ybC0zLjQtMy40Yy0uNS0uNS0xLS43LTEuNy0uN3MtMS4yLjItMS43LjdsLTE2LjIgMTYuNS03LjMtNy40Yy0uNS0uNS0xLS43LTEuNy0uN3MtMS4yLjItMS43LjdsLTMuNCAzLjRjLS41LjUtLjcgMS0uNyAxLjdzLjIgMS4yLjcgMS43bDkgOS4xIDMuNCAzLjRjLjUuNSAxIC43IDEuNy43czEuMi0uMiAxLjctLjdsMy40LTMuNCAxNy45LTE4LjJjLjUtLjUuNy0xIC43LTEuN3MtLjItMS4yLS43LTEuN3oiLz48L2c+PC9zdmc+">';
														}else {
															$to_disp .= '<div style="width: 36px; height: 36px; border-radius: 50%; border-width: 1px; border-style: solid; border-color: #ecf0f1; margin-top: 10px; background-color: #ffffff;">
																		<div style="width: 12px; height: 12px; transform: translate(-50%,-50%); background-color: #ddd; border-radius: 100%; margin-top: 17px; margin-left: 17px;"></div>
																	</div>';
														}
														
														$to_disp .= '</div>
																	<div style= "display: flex; flex-direction: column; width: 250px;">
																		<p style= "font-weight: bold; margin: 0;">'.$event_status.'</p>
																		<p style= "margin: 0; color: #4a5568;">'.$event_loc.'</p>
																	</div>
																</div>
															</li>';
        									}
        									$to_disp .= '</ul></div>';
        								}else {
        									$to_disp = '<h4 style= "text-align: center;">Sorry! No data found for this package...<h4/></div>';
        									echo $to_disp;
        									return;
        								}
									}else {
										$to_disp = '<h4 style= "text-align: center;>Sorry! No data found for this package...<h4/></div>';
										echo $to_disp;
										return;
									}
							}
							$trk_count ++;
						}

						$to_disp = '</div>';
						echo $to_disp;
					}
				}
			}
			public function save_user_fields($user_id){
				if(isset($_POST['a2z_dhlexpress_country'])){
					$general_settings['a2z_dhlexpress_site_id'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_site_id']) ? $_POST['a2z_dhlexpress_site_id'] : '');
					$general_settings['a2z_dhlexpress_site_pwd'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_site_pwd']) ? $_POST['a2z_dhlexpress_site_pwd'] : '');
					$general_settings['a2z_dhlexpress_acc_no'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_acc_no']) ? $_POST['a2z_dhlexpress_acc_no'] : '');
					$general_settings['a2z_dhlexpress_import_no'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_import_no']) ? $_POST['a2z_dhlexpress_import_no'] : '');
					$general_settings['a2z_dhlexpress_shipper_name'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_shipper_name']) ? $_POST['a2z_dhlexpress_shipper_name'] : '');
					$general_settings['a2z_dhlexpress_company'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_company']) ? $_POST['a2z_dhlexpress_company'] : '');
					$general_settings['a2z_dhlexpress_mob_num'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_mob_num']) ? $_POST['a2z_dhlexpress_mob_num'] : '');
					$general_settings['a2z_dhlexpress_email'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_email']) ? $_POST['a2z_dhlexpress_email'] : '');
					$general_settings['a2z_dhlexpress_address1'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_address1']) ? $_POST['a2z_dhlexpress_address1'] : '');
					$general_settings['a2z_dhlexpress_address2'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_address2']) ? $_POST['a2z_dhlexpress_address2'] : '');
					$general_settings['a2z_dhlexpress_city'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_city']) ? $_POST['a2z_dhlexpress_city'] : '');
					$general_settings['a2z_dhlexpress_state'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_state']) ? $_POST['a2z_dhlexpress_state'] : '');
					$general_settings['a2z_dhlexpress_zip'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_zip']) ? $_POST['a2z_dhlexpress_zip'] : '');
					$general_settings['a2z_dhlexpress_country'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_country']) ? $_POST['a2z_dhlexpress_country'] : '');
					$general_settings['a2z_dhlexpress_gstin'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_gstin']) ? $_POST['a2z_dhlexpress_gstin'] : '');
					$general_settings['a2z_dhlexpress_con_rate'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_con_rate']) ? $_POST['a2z_dhlexpress_con_rate'] : '');
					$general_settings['a2z_dhlexpress_def_dom'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_def_dom']) ? $_POST['a2z_dhlexpress_def_dom'] : '');

					$general_settings['a2z_dhlexpress_def_inter'] = sanitize_text_field(isset($_POST['a2z_dhlexpress_def_inter']) ? $_POST['a2z_dhlexpress_def_inter'] : '');

					update_post_meta($user_id,'a2z_dhl_vendor_settings',$general_settings);
				}

			}

			public function hit_define_dhl_credentails( $user ){

				$main_settings = get_option('a2z_dhl_main_settings');
				$main_settings = empty($main_settings) ? array() : $main_settings;
				$allow = false;
				
				if(!isset($main_settings['a2z_dhlexpress_v_roles'])){
					return;
				}else{
					foreach ($user->roles as $value) {
						if(in_array($value, $main_settings['a2z_dhlexpress_v_roles'])){
							$allow = true;
						}
					}
				}
				
				if(!$allow){
					return;
				}

				$general_settings = get_post_meta($user->ID,'a2z_dhl_vendor_settings',true);
				$general_settings = empty($general_settings) ? array() : $general_settings;
				$countires =  array(
									'AF' => 'Afghanistan',
									'AL' => 'Albania',
									'DZ' => 'Algeria',
									'AS' => 'American Samoa',
									'AD' => 'Andorra',
									'AO' => 'Angola',
									'AI' => 'Anguilla',
									'AG' => 'Antigua and Barbuda',
									'AR' => 'Argentina',
									'AM' => 'Armenia',
									'AW' => 'Aruba',
									'AU' => 'Australia',
									'AT' => 'Austria',
									'AZ' => 'Azerbaijan',
									'BS' => 'Bahamas',
									'BH' => 'Bahrain',
									'BD' => 'Bangladesh',
									'BB' => 'Barbados',
									'BY' => 'Belarus',
									'BE' => 'Belgium',
									'BZ' => 'Belize',
									'BJ' => 'Benin',
									'BM' => 'Bermuda',
									'BT' => 'Bhutan',
									'BO' => 'Bolivia',
									'BA' => 'Bosnia and Herzegovina',
									'BW' => 'Botswana',
									'BR' => 'Brazil',
									'VG' => 'British Virgin Islands',
									'BN' => 'Brunei',
									'BG' => 'Bulgaria',
									'BF' => 'Burkina Faso',
									'BI' => 'Burundi',
									'KH' => 'Cambodia',
									'CM' => 'Cameroon',
									'CA' => 'Canada',
									'CV' => 'Cape Verde',
									'KY' => 'Cayman Islands',
									'CF' => 'Central African Republic',
									'TD' => 'Chad',
									'CL' => 'Chile',
									'CN' => 'China',
									'CO' => 'Colombia',
									'KM' => 'Comoros',
									'CK' => 'Cook Islands',
									'CR' => 'Costa Rica',
									'HR' => 'Croatia',
									'CU' => 'Cuba',
									'CY' => 'Cyprus',
									'CZ' => 'Czech Republic',
									'DK' => 'Denmark',
									'DJ' => 'Djibouti',
									'DM' => 'Dominica',
									'DO' => 'Dominican Republic',
									'TL' => 'East Timor',
									'EC' => 'Ecuador',
									'EG' => 'Egypt',
									'SV' => 'El Salvador',
									'GQ' => 'Equatorial Guinea',
									'ER' => 'Eritrea',
									'EE' => 'Estonia',
									'ET' => 'Ethiopia',
									'FK' => 'Falkland Islands',
									'FO' => 'Faroe Islands',
									'FJ' => 'Fiji',
									'FI' => 'Finland',
									'FR' => 'France',
									'GF' => 'French Guiana',
									'PF' => 'French Polynesia',
									'GA' => 'Gabon',
									'GM' => 'Gambia',
									'GE' => 'Georgia',
									'DE' => 'Germany',
									'GH' => 'Ghana',
									'GI' => 'Gibraltar',
									'GR' => 'Greece',
									'GL' => 'Greenland',
									'GD' => 'Grenada',
									'GP' => 'Guadeloupe',
									'GU' => 'Guam',
									'GT' => 'Guatemala',
									'GG' => 'Guernsey',
									'GN' => 'Guinea',
									'GW' => 'Guinea-Bissau',
									'GY' => 'Guyana',
									'HT' => 'Haiti',
									'HN' => 'Honduras',
									'HK' => 'Hong Kong',
									'HU' => 'Hungary',
									'IS' => 'Iceland',
									'IN' => 'India',
									'ID' => 'Indonesia',
									'IR' => 'Iran',
									'IQ' => 'Iraq',
									'IE' => 'Ireland',
									'IL' => 'Israel',
									'IT' => 'Italy',
									'CI' => 'Ivory Coast',
									'JM' => 'Jamaica',
									'JP' => 'Japan',
									'JE' => 'Jersey',
									'JO' => 'Jordan',
									'KZ' => 'Kazakhstan',
									'KE' => 'Kenya',
									'KI' => 'Kiribati',
									'KW' => 'Kuwait',
									'KG' => 'Kyrgyzstan',
									'LA' => 'Laos',
									'LV' => 'Latvia',
									'LB' => 'Lebanon',
									'LS' => 'Lesotho',
									'LR' => 'Liberia',
									'LY' => 'Libya',
									'LI' => 'Liechtenstein',
									'LT' => 'Lithuania',
									'LU' => 'Luxembourg',
									'MO' => 'Macao',
									'MK' => 'Macedonia',
									'MG' => 'Madagascar',
									'MW' => 'Malawi',
									'MY' => 'Malaysia',
									'MV' => 'Maldives',
									'ML' => 'Mali',
									'MT' => 'Malta',
									'MH' => 'Marshall Islands',
									'MQ' => 'Martinique',
									'MR' => 'Mauritania',
									'MU' => 'Mauritius',
									'YT' => 'Mayotte',
									'MX' => 'Mexico',
									'FM' => 'Micronesia',
									'MD' => 'Moldova',
									'MC' => 'Monaco',
									'MN' => 'Mongolia',
									'ME' => 'Montenegro',
									'MS' => 'Montserrat',
									'MA' => 'Morocco',
									'MZ' => 'Mozambique',
									'MM' => 'Myanmar',
									'NA' => 'Namibia',
									'NR' => 'Nauru',
									'NP' => 'Nepal',
									'NL' => 'Netherlands',
									'NC' => 'New Caledonia',
									'NZ' => 'New Zealand',
									'NI' => 'Nicaragua',
									'NE' => 'Niger',
									'NG' => 'Nigeria',
									'NU' => 'Niue',
									'KP' => 'North Korea',
									'MP' => 'Northern Mariana Islands',
									'NO' => 'Norway',
									'OM' => 'Oman',
									'PK' => 'Pakistan',
									'PW' => 'Palau',
									'PA' => 'Panama',
									'PG' => 'Papua New Guinea',
									'PY' => 'Paraguay',
									'PE' => 'Peru',
									'PH' => 'Philippines',
									'PL' => 'Poland',
									'PT' => 'Portugal',
									'PR' => 'Puerto Rico',
									'QA' => 'Qatar',
									'CG' => 'Republic of the Congo',
									'RE' => 'Reunion',
									'RO' => 'Romania',
									'RU' => 'Russia',
									'RW' => 'Rwanda',
									'SH' => 'Saint Helena',
									'KN' => 'Saint Kitts and Nevis',
									'LC' => 'Saint Lucia',
									'VC' => 'Saint Vincent and the Grenadines',
									'WS' => 'Samoa',
									'SM' => 'San Marino',
									'ST' => 'Sao Tome and Principe',
									'SA' => 'Saudi Arabia',
									'SN' => 'Senegal',
									'RS' => 'Serbia',
									'SC' => 'Seychelles',
									'SL' => 'Sierra Leone',
									'SG' => 'Singapore',
									'SK' => 'Slovakia',
									'SI' => 'Slovenia',
									'SB' => 'Solomon Islands',
									'SO' => 'Somalia',
									'ZA' => 'South Africa',
									'KR' => 'South Korea',
									'SS' => 'South Sudan',
									'ES' => 'Spain',
									'LK' => 'Sri Lanka',
									'SD' => 'Sudan',
									'SR' => 'Suriname',
									'SZ' => 'Swaziland',
									'SE' => 'Sweden',
									'CH' => 'Switzerland',
									'SY' => 'Syria',
									'TW' => 'Taiwan',
									'TJ' => 'Tajikistan',
									'TZ' => 'Tanzania',
									'TH' => 'Thailand',
									'TG' => 'Togo',
									'TO' => 'Tonga',
									'TT' => 'Trinidad and Tobago',
									'TN' => 'Tunisia',
									'TR' => 'Turkey',
									'TC' => 'Turks and Caicos Islands',
									'TV' => 'Tuvalu',
									'VI' => 'U.S. Virgin Islands',
									'UG' => 'Uganda',
									'UA' => 'Ukraine',
									'AE' => 'United Arab Emirates',
									'GB' => 'United Kingdom',
									'US' => 'United States',
									'UY' => 'Uruguay',
									'UZ' => 'Uzbekistan',
									'VU' => 'Vanuatu',
									'VE' => 'Venezuela',
									'VN' => 'Vietnam',
									'YE' => 'Yemen',
									'ZM' => 'Zambia',
									'ZW' => 'Zimbabwe',
								);
				 $_dhl_carriers = array(
					//"Public carrier name" => "technical name",
					'1'                    => 'DOMESTIC EXPRESS 12:00',
					'2'                    => 'B2C',
					'3'                    => 'B2C',
					'4'                    => 'JETLINE',
					'5'                    => 'SPRINTLINE',
					'7'                    => 'EXPRESS EASY',
					'8'                    => 'EXPRESS EASY',
					'9'                    => 'EUROPACK',
					'B'                    => 'BREAKBULK EXPRESS',
					'C'                    => 'MEDICAL EXPRESS',
					'D'                    => 'EXPRESS WORLDWIDE',
					'E'                    => 'EXPRESS 9:00',
					'F'                    => 'FREIGHT WORLDWIDE',
					'G'                    => 'DOMESTIC ECONOMY SELECT',
					'H'                    => 'ECONOMY SELECT',
					'I'                    => 'DOMESTIC EXPRESS 9:00',
					'J'                    => 'JUMBO BOX',
					'K'                    => 'EXPRESS 9:00',
					'L'                    => 'EXPRESS 10:30',
					'M'                    => 'EXPRESS 10:30',
					'N'                    => 'DOMESTIC EXPRESS',
					'O'                    => 'DOMESTIC EXPRESS 10:30',
					'P'                    => 'EXPRESS WORLDWIDE',
					'Q'                    => 'MEDICAL EXPRESS',
					'R'                    => 'GLOBALMAIL BUSINESS',
					'S'                    => 'SAME DAY',
					'T'                    => 'EXPRESS 12:00',
					'U'                    => 'EXPRESS WORLDWIDE',
					'V'                    => 'EUROPACK',
					'W'                    => 'ECONOMY SELECT',
					'X'                    => 'EXPRESS ENVELOPE',
					'Y'                    => 'EXPRESS 12:00'	
				);

				 $dhl_core = array();
			$dhl_core['AD'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['AE'] = array('region' => 'AP', 'currency' =>'AED', 'weight' => 'KG_CM');
			$dhl_core['AF'] = array('region' => 'AP', 'currency' =>'AFN', 'weight' => 'KG_CM');
			$dhl_core['AG'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['AI'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['AL'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['AM'] = array('region' => 'AP', 'currency' =>'AMD', 'weight' => 'KG_CM');
			$dhl_core['AN'] = array('region' => 'AM', 'currency' =>'ANG', 'weight' => 'KG_CM');
			$dhl_core['AO'] = array('region' => 'AP', 'currency' =>'AOA', 'weight' => 'KG_CM');
			$dhl_core['AR'] = array('region' => 'AM', 'currency' =>'ARS', 'weight' => 'KG_CM');
			$dhl_core['AS'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['AT'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['AU'] = array('region' => 'AP', 'currency' =>'AUD', 'weight' => 'KG_CM');
			$dhl_core['AW'] = array('region' => 'AM', 'currency' =>'AWG', 'weight' => 'LB_IN');
			$dhl_core['AZ'] = array('region' => 'AM', 'currency' =>'AZN', 'weight' => 'KG_CM');
			$dhl_core['AZ'] = array('region' => 'AM', 'currency' =>'AZN', 'weight' => 'KG_CM');
			$dhl_core['GB'] = array('region' => 'EU', 'currency' =>'GBP', 'weight' => 'KG_CM');
			$dhl_core['BA'] = array('region' => 'AP', 'currency' =>'BAM', 'weight' => 'KG_CM');
			$dhl_core['BB'] = array('region' => 'AM', 'currency' =>'BBD', 'weight' => 'LB_IN');
			$dhl_core['BD'] = array('region' => 'AP', 'currency' =>'BDT', 'weight' => 'KG_CM');
			$dhl_core['BE'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['BF'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['BG'] = array('region' => 'EU', 'currency' =>'BGN', 'weight' => 'KG_CM');
			$dhl_core['BH'] = array('region' => 'AP', 'currency' =>'BHD', 'weight' => 'KG_CM');
			$dhl_core['BI'] = array('region' => 'AP', 'currency' =>'BIF', 'weight' => 'KG_CM');
			$dhl_core['BJ'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['BM'] = array('region' => 'AM', 'currency' =>'BMD', 'weight' => 'LB_IN');
			$dhl_core['BN'] = array('region' => 'AP', 'currency' =>'BND', 'weight' => 'KG_CM');
			$dhl_core['BO'] = array('region' => 'AM', 'currency' =>'BOB', 'weight' => 'KG_CM');
			$dhl_core['BR'] = array('region' => 'AM', 'currency' =>'BRL', 'weight' => 'KG_CM');
			$dhl_core['BS'] = array('region' => 'AM', 'currency' =>'BSD', 'weight' => 'LB_IN');
			$dhl_core['BT'] = array('region' => 'AP', 'currency' =>'BTN', 'weight' => 'KG_CM');
			$dhl_core['BW'] = array('region' => 'AP', 'currency' =>'BWP', 'weight' => 'KG_CM');
			$dhl_core['BY'] = array('region' => 'AP', 'currency' =>'BYR', 'weight' => 'KG_CM');
			$dhl_core['BZ'] = array('region' => 'AM', 'currency' =>'BZD', 'weight' => 'KG_CM');
			$dhl_core['CA'] = array('region' => 'AM', 'currency' =>'CAD', 'weight' => 'LB_IN');
			$dhl_core['CF'] = array('region' => 'AP', 'currency' =>'XAF', 'weight' => 'KG_CM');
			$dhl_core['CG'] = array('region' => 'AP', 'currency' =>'XAF', 'weight' => 'KG_CM');
			$dhl_core['CH'] = array('region' => 'EU', 'currency' =>'CHF', 'weight' => 'KG_CM');
			$dhl_core['CI'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['CK'] = array('region' => 'AP', 'currency' =>'NZD', 'weight' => 'KG_CM');
			$dhl_core['CL'] = array('region' => 'AM', 'currency' =>'CLP', 'weight' => 'KG_CM');
			$dhl_core['CM'] = array('region' => 'AP', 'currency' =>'XAF', 'weight' => 'KG_CM');
			$dhl_core['CN'] = array('region' => 'AP', 'currency' =>'CNY', 'weight' => 'KG_CM');
			$dhl_core['CO'] = array('region' => 'AM', 'currency' =>'COP', 'weight' => 'KG_CM');
			$dhl_core['CR'] = array('region' => 'AM', 'currency' =>'CRC', 'weight' => 'KG_CM');
			$dhl_core['CU'] = array('region' => 'AM', 'currency' =>'CUC', 'weight' => 'KG_CM');
			$dhl_core['CV'] = array('region' => 'AP', 'currency' =>'CVE', 'weight' => 'KG_CM');
			$dhl_core['CY'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['CZ'] = array('region' => 'EU', 'currency' =>'CZK', 'weight' => 'KG_CM');
			$dhl_core['DE'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['DJ'] = array('region' => 'EU', 'currency' =>'DJF', 'weight' => 'KG_CM');
			$dhl_core['DK'] = array('region' => 'AM', 'currency' =>'DKK', 'weight' => 'KG_CM');
			$dhl_core['DM'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['DO'] = array('region' => 'AP', 'currency' =>'DOP', 'weight' => 'LB_IN');
			$dhl_core['DZ'] = array('region' => 'AM', 'currency' =>'DZD', 'weight' => 'KG_CM');
			$dhl_core['EC'] = array('region' => 'EU', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['EE'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['EG'] = array('region' => 'AP', 'currency' =>'EGP', 'weight' => 'KG_CM');
			$dhl_core['ER'] = array('region' => 'EU', 'currency' =>'ERN', 'weight' => 'KG_CM');
			$dhl_core['ES'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['ET'] = array('region' => 'AU', 'currency' =>'ETB', 'weight' => 'KG_CM');
			$dhl_core['FI'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['FJ'] = array('region' => 'AP', 'currency' =>'FJD', 'weight' => 'KG_CM');
			$dhl_core['FK'] = array('region' => 'AM', 'currency' =>'GBP', 'weight' => 'KG_CM');
			$dhl_core['FM'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['FO'] = array('region' => 'AM', 'currency' =>'DKK', 'weight' => 'KG_CM');
			$dhl_core['FR'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['GA'] = array('region' => 'AP', 'currency' =>'XAF', 'weight' => 'KG_CM');
			$dhl_core['GB'] = array('region' => 'EU', 'currency' =>'GBP', 'weight' => 'KG_CM');
			$dhl_core['GD'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['GE'] = array('region' => 'AM', 'currency' =>'GEL', 'weight' => 'KG_CM');
			$dhl_core['GF'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['GG'] = array('region' => 'AM', 'currency' =>'GBP', 'weight' => 'KG_CM');
			$dhl_core['GH'] = array('region' => 'AP', 'currency' =>'GHS', 'weight' => 'KG_CM');
			$dhl_core['GI'] = array('region' => 'AM', 'currency' =>'GBP', 'weight' => 'KG_CM');
			$dhl_core['GL'] = array('region' => 'AM', 'currency' =>'DKK', 'weight' => 'KG_CM');
			$dhl_core['GM'] = array('region' => 'AP', 'currency' =>'GMD', 'weight' => 'KG_CM');
			$dhl_core['GN'] = array('region' => 'AP', 'currency' =>'GNF', 'weight' => 'KG_CM');
			$dhl_core['GP'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['GQ'] = array('region' => 'AP', 'currency' =>'XAF', 'weight' => 'KG_CM');
			$dhl_core['GR'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['GT'] = array('region' => 'AM', 'currency' =>'GTQ', 'weight' => 'KG_CM');
			$dhl_core['GU'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['GW'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['GY'] = array('region' => 'AP', 'currency' =>'GYD', 'weight' => 'LB_IN');
			$dhl_core['HK'] = array('region' => 'AM', 'currency' =>'HKD', 'weight' => 'KG_CM');
			$dhl_core['HN'] = array('region' => 'AM', 'currency' =>'HNL', 'weight' => 'KG_CM');
			$dhl_core['HR'] = array('region' => 'AP', 'currency' =>'HRK', 'weight' => 'KG_CM');
			$dhl_core['HT'] = array('region' => 'AM', 'currency' =>'HTG', 'weight' => 'LB_IN');
			$dhl_core['HU'] = array('region' => 'EU', 'currency' =>'HUF', 'weight' => 'KG_CM');
			$dhl_core['IC'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['ID'] = array('region' => 'AP', 'currency' =>'IDR', 'weight' => 'KG_CM');
			$dhl_core['IE'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['IL'] = array('region' => 'AP', 'currency' =>'ILS', 'weight' => 'KG_CM');
			$dhl_core['IN'] = array('region' => 'AP', 'currency' =>'INR', 'weight' => 'KG_CM');
			$dhl_core['IQ'] = array('region' => 'AP', 'currency' =>'IQD', 'weight' => 'KG_CM');
			$dhl_core['IR'] = array('region' => 'AP', 'currency' =>'IRR', 'weight' => 'KG_CM');
			$dhl_core['IS'] = array('region' => 'EU', 'currency' =>'ISK', 'weight' => 'KG_CM');
			$dhl_core['IT'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['JE'] = array('region' => 'AM', 'currency' =>'GBP', 'weight' => 'KG_CM');
			$dhl_core['JM'] = array('region' => 'AM', 'currency' =>'JMD', 'weight' => 'KG_CM');
			$dhl_core['JO'] = array('region' => 'AP', 'currency' =>'JOD', 'weight' => 'KG_CM');
			$dhl_core['JP'] = array('region' => 'AP', 'currency' =>'JPY', 'weight' => 'KG_CM');
			$dhl_core['KE'] = array('region' => 'AP', 'currency' =>'KES', 'weight' => 'KG_CM');
			$dhl_core['KG'] = array('region' => 'AP', 'currency' =>'KGS', 'weight' => 'KG_CM');
			$dhl_core['KH'] = array('region' => 'AP', 'currency' =>'KHR', 'weight' => 'KG_CM');
			$dhl_core['KI'] = array('region' => 'AP', 'currency' =>'AUD', 'weight' => 'KG_CM');
			$dhl_core['KM'] = array('region' => 'AP', 'currency' =>'KMF', 'weight' => 'KG_CM');
			$dhl_core['KN'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['KP'] = array('region' => 'AP', 'currency' =>'KPW', 'weight' => 'LB_IN');
			$dhl_core['KR'] = array('region' => 'AP', 'currency' =>'KRW', 'weight' => 'KG_CM');
			$dhl_core['KV'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['KW'] = array('region' => 'AP', 'currency' =>'KWD', 'weight' => 'KG_CM');
			$dhl_core['KY'] = array('region' => 'AM', 'currency' =>'KYD', 'weight' => 'KG_CM');
			$dhl_core['KZ'] = array('region' => 'AP', 'currency' =>'KZF', 'weight' => 'LB_IN');
			$dhl_core['LA'] = array('region' => 'AP', 'currency' =>'LAK', 'weight' => 'KG_CM');
			$dhl_core['LB'] = array('region' => 'AP', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['LC'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'KG_CM');
			$dhl_core['LI'] = array('region' => 'AM', 'currency' =>'CHF', 'weight' => 'LB_IN');
			$dhl_core['LK'] = array('region' => 'AP', 'currency' =>'LKR', 'weight' => 'KG_CM');
			$dhl_core['LR'] = array('region' => 'AP', 'currency' =>'LRD', 'weight' => 'KG_CM');
			$dhl_core['LS'] = array('region' => 'AP', 'currency' =>'LSL', 'weight' => 'KG_CM');
			$dhl_core['LT'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['LU'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['LV'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['LY'] = array('region' => 'AP', 'currency' =>'LYD', 'weight' => 'KG_CM');
			$dhl_core['MA'] = array('region' => 'AP', 'currency' =>'MAD', 'weight' => 'KG_CM');
			$dhl_core['MC'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['MD'] = array('region' => 'AP', 'currency' =>'MDL', 'weight' => 'KG_CM');
			$dhl_core['ME'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['MG'] = array('region' => 'AP', 'currency' =>'MGA', 'weight' => 'KG_CM');
			$dhl_core['MH'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['MK'] = array('region' => 'AP', 'currency' =>'MKD', 'weight' => 'KG_CM');
			$dhl_core['ML'] = array('region' => 'AP', 'currency' =>'COF', 'weight' => 'KG_CM');
			$dhl_core['MM'] = array('region' => 'AP', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['MN'] = array('region' => 'AP', 'currency' =>'MNT', 'weight' => 'KG_CM');
			$dhl_core['MO'] = array('region' => 'AP', 'currency' =>'MOP', 'weight' => 'KG_CM');
			$dhl_core['MP'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['MQ'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['MR'] = array('region' => 'AP', 'currency' =>'MRO', 'weight' => 'KG_CM');
			$dhl_core['MS'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['MT'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['MU'] = array('region' => 'AP', 'currency' =>'MUR', 'weight' => 'KG_CM');
			$dhl_core['MV'] = array('region' => 'AP', 'currency' =>'MVR', 'weight' => 'KG_CM');
			$dhl_core['MW'] = array('region' => 'AP', 'currency' =>'MWK', 'weight' => 'KG_CM');
			$dhl_core['MX'] = array('region' => 'AM', 'currency' =>'MXN', 'weight' => 'KG_CM');
			$dhl_core['MY'] = array('region' => 'AP', 'currency' =>'MYR', 'weight' => 'KG_CM');
			$dhl_core['MZ'] = array('region' => 'AP', 'currency' =>'MZN', 'weight' => 'KG_CM');
			$dhl_core['NA'] = array('region' => 'AP', 'currency' =>'NAD', 'weight' => 'KG_CM');
			$dhl_core['NC'] = array('region' => 'AP', 'currency' =>'XPF', 'weight' => 'KG_CM');
			$dhl_core['NE'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['NG'] = array('region' => 'AP', 'currency' =>'NGN', 'weight' => 'KG_CM');
			$dhl_core['NI'] = array('region' => 'AM', 'currency' =>'NIO', 'weight' => 'KG_CM');
			$dhl_core['NL'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['NO'] = array('region' => 'EU', 'currency' =>'NOK', 'weight' => 'KG_CM');
			$dhl_core['NP'] = array('region' => 'AP', 'currency' =>'NPR', 'weight' => 'KG_CM');
			$dhl_core['NR'] = array('region' => 'AP', 'currency' =>'AUD', 'weight' => 'KG_CM');
			$dhl_core['NU'] = array('region' => 'AP', 'currency' =>'NZD', 'weight' => 'KG_CM');
			$dhl_core['NZ'] = array('region' => 'AP', 'currency' =>'NZD', 'weight' => 'KG_CM');
			$dhl_core['OM'] = array('region' => 'AP', 'currency' =>'OMR', 'weight' => 'KG_CM');
			$dhl_core['PA'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['PE'] = array('region' => 'AM', 'currency' =>'PEN', 'weight' => 'KG_CM');
			$dhl_core['PF'] = array('region' => 'AP', 'currency' =>'XPF', 'weight' => 'KG_CM');
			$dhl_core['PG'] = array('region' => 'AP', 'currency' =>'PGK', 'weight' => 'KG_CM');
			$dhl_core['PH'] = array('region' => 'AP', 'currency' =>'PHP', 'weight' => 'KG_CM');
			$dhl_core['PK'] = array('region' => 'AP', 'currency' =>'PKR', 'weight' => 'KG_CM');
			$dhl_core['PL'] = array('region' => 'EU', 'currency' =>'PLN', 'weight' => 'KG_CM');
			$dhl_core['PR'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['PT'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['PW'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['PY'] = array('region' => 'AM', 'currency' =>'PYG', 'weight' => 'KG_CM');
			$dhl_core['QA'] = array('region' => 'AP', 'currency' =>'QAR', 'weight' => 'KG_CM');
			$dhl_core['RE'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['RO'] = array('region' => 'EU', 'currency' =>'RON', 'weight' => 'KG_CM');
			$dhl_core['RS'] = array('region' => 'AP', 'currency' =>'RSD', 'weight' => 'KG_CM');
			$dhl_core['RU'] = array('region' => 'AP', 'currency' =>'RUB', 'weight' => 'KG_CM');
			$dhl_core['RW'] = array('region' => 'AP', 'currency' =>'RWF', 'weight' => 'KG_CM');
			$dhl_core['SA'] = array('region' => 'AP', 'currency' =>'SAR', 'weight' => 'KG_CM');
			$dhl_core['SB'] = array('region' => 'AP', 'currency' =>'SBD', 'weight' => 'KG_CM');
			$dhl_core['SC'] = array('region' => 'AP', 'currency' =>'SCR', 'weight' => 'KG_CM');
			$dhl_core['SD'] = array('region' => 'AP', 'currency' =>'SDG', 'weight' => 'KG_CM');
			$dhl_core['SE'] = array('region' => 'EU', 'currency' =>'SEK', 'weight' => 'KG_CM');
			$dhl_core['SG'] = array('region' => 'AP', 'currency' =>'SGD', 'weight' => 'KG_CM');
			$dhl_core['SH'] = array('region' => 'AP', 'currency' =>'SHP', 'weight' => 'KG_CM');
			$dhl_core['SI'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['SK'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['SL'] = array('region' => 'AP', 'currency' =>'SLL', 'weight' => 'KG_CM');
			$dhl_core['SM'] = array('region' => 'EU', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['SN'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['SO'] = array('region' => 'AM', 'currency' =>'SOS', 'weight' => 'KG_CM');
			$dhl_core['SR'] = array('region' => 'AM', 'currency' =>'SRD', 'weight' => 'KG_CM');
			$dhl_core['SS'] = array('region' => 'AP', 'currency' =>'SSP', 'weight' => 'KG_CM');
			$dhl_core['ST'] = array('region' => 'AP', 'currency' =>'STD', 'weight' => 'KG_CM');
			$dhl_core['SV'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['SY'] = array('region' => 'AP', 'currency' =>'SYP', 'weight' => 'KG_CM');
			$dhl_core['SZ'] = array('region' => 'AP', 'currency' =>'SZL', 'weight' => 'KG_CM');
			$dhl_core['TC'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['TD'] = array('region' => 'AP', 'currency' =>'XAF', 'weight' => 'KG_CM');
			$dhl_core['TG'] = array('region' => 'AP', 'currency' =>'XOF', 'weight' => 'KG_CM');
			$dhl_core['TH'] = array('region' => 'AP', 'currency' =>'THB', 'weight' => 'KG_CM');
			$dhl_core['TJ'] = array('region' => 'AP', 'currency' =>'TJS', 'weight' => 'KG_CM');
			$dhl_core['TL'] = array('region' => 'AP', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['TN'] = array('region' => 'AP', 'currency' =>'TND', 'weight' => 'KG_CM');
			$dhl_core['TO'] = array('region' => 'AP', 'currency' =>'TOP', 'weight' => 'KG_CM');
			$dhl_core['TR'] = array('region' => 'AP', 'currency' =>'TRY', 'weight' => 'KG_CM');
			$dhl_core['TT'] = array('region' => 'AM', 'currency' =>'TTD', 'weight' => 'LB_IN');
			$dhl_core['TV'] = array('region' => 'AP', 'currency' =>'AUD', 'weight' => 'KG_CM');
			$dhl_core['TW'] = array('region' => 'AP', 'currency' =>'TWD', 'weight' => 'KG_CM');
			$dhl_core['TZ'] = array('region' => 'AP', 'currency' =>'TZS', 'weight' => 'KG_CM');
			$dhl_core['UA'] = array('region' => 'AP', 'currency' =>'UAH', 'weight' => 'KG_CM');
			$dhl_core['UG'] = array('region' => 'AP', 'currency' =>'USD', 'weight' => 'KG_CM');
			$dhl_core['US'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['UY'] = array('region' => 'AM', 'currency' =>'UYU', 'weight' => 'KG_CM');
			$dhl_core['UZ'] = array('region' => 'AP', 'currency' =>'UZS', 'weight' => 'KG_CM');
			$dhl_core['VC'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['VE'] = array('region' => 'AM', 'currency' =>'VEF', 'weight' => 'KG_CM');
			$dhl_core['VG'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['VI'] = array('region' => 'AM', 'currency' =>'USD', 'weight' => 'LB_IN');
			$dhl_core['VN'] = array('region' => 'AP', 'currency' =>'VND', 'weight' => 'KG_CM');
			$dhl_core['VU'] = array('region' => 'AP', 'currency' =>'VUV', 'weight' => 'KG_CM');
			$dhl_core['WS'] = array('region' => 'AP', 'currency' =>'WST', 'weight' => 'KG_CM');
			$dhl_core['XB'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'LB_IN');
			$dhl_core['XC'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'LB_IN');
			$dhl_core['XE'] = array('region' => 'AM', 'currency' =>'ANG', 'weight' => 'LB_IN');
			$dhl_core['XM'] = array('region' => 'AM', 'currency' =>'EUR', 'weight' => 'LB_IN');
			$dhl_core['XN'] = array('region' => 'AM', 'currency' =>'XCD', 'weight' => 'LB_IN');
			$dhl_core['XS'] = array('region' => 'AP', 'currency' =>'SIS', 'weight' => 'KG_CM');
			$dhl_core['XY'] = array('region' => 'AM', 'currency' =>'ANG', 'weight' => 'LB_IN');
			$dhl_core['YE'] = array('region' => 'AP', 'currency' =>'YER', 'weight' => 'KG_CM');
			$dhl_core['YT'] = array('region' => 'AP', 'currency' =>'EUR', 'weight' => 'KG_CM');
			$dhl_core['ZA'] = array('region' => 'AP', 'currency' =>'ZAR', 'weight' => 'KG_CM');
			$dhl_core['ZM'] = array('region' => 'AP', 'currency' =>'ZMW', 'weight' => 'KG_CM');
			$dhl_core['ZW'] = array('region' => 'AP', 'currency' =>'USD', 'weight' => 'KG_CM');

				 echo '<hr><h3 class="heading">DHL Express - <a href="https://hitshipo.com/" target="_blank">HITShipo</a></h3>';
				    ?>
				    
				    <table class="form-table">
						<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('DHL Integration Team will give this details to you.','a2z_dhlexpress') ?>"></span>	<?php _e('DHL XML API Site ID','a2z_dhlexpress') ?></h4>
							<p> <?php _e('Leave this field as empty to use default account.','a2z_dhlexpress') ?> </p>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_site_id" value="<?php echo (isset($general_settings['a2z_dhlexpress_site_id'])) ? $general_settings['a2z_dhlexpress_site_id'] : ''; ?>">
						</td>

					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('DHL Integration Team will give this details to you.','a2z_dhlexpress') ?>"></span>	<?php _e('DHL XML API Password','a2z_dhlexpress') ?></h4>
							<p> <?php _e('Leave this field as empty to use default account.','a2z_dhlexpress') ?> </p>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_site_pwd" value="<?php echo (isset($general_settings['a2z_dhlexpress_site_pwd'])) ? $general_settings['a2z_dhlexpress_site_pwd'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('DHL Integration Team will give this details to you.','a2z_dhlexpress') ?>"></span>	<?php _e('DHL Account Number','a2z_dhlexpress') ?></h4>
							<p> <?php _e('Leave this field as empty to use default account.','a2z_dhlexpress') ?> </p>
						</td>
						<td>
							
							<input type="text" name="a2z_dhlexpress_acc_no" value="<?php echo (isset($general_settings['a2z_dhlexpress_acc_no'])) ? $general_settings['a2z_dhlexpress_acc_no'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('This is for proceed with return labels.','a2z_dhlexpress') ?>"></span>	<?php _e('DHL Import Account Number','a2z_dhlexpress') ?></h4>
							<p> <?php _e('Leave this field as empty to use default account.','a2z_dhlexpress') ?> </p>
						</td>
						<td>
							
							<input type="text" name="a2z_dhlexpress_import_no" value="<?php echo (isset($general_settings['a2z_dhlexpress_import_no'])) ? $general_settings['a2z_dhlexpress_import_no'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Shipping Person Name','a2z_dhlexpress') ?>"></span>	<?php _e('Shipper Name','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_shipper_name" value="<?php echo (isset($general_settings['a2z_dhlexpress_shipper_name'])) ? $general_settings['a2z_dhlexpress_shipper_name'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Shipper Company Name.','a2z_dhlexpress') ?>"></span>	<?php _e('Company Name','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_company" value="<?php echo (isset($general_settings['a2z_dhlexpress_company'])) ? $general_settings['a2z_dhlexpress_company'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Shipper Mobile / Contact Number.','a2z_dhlexpress') ?>"></span>	<?php _e('Contact Number','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_mob_num" value="<?php echo (isset($general_settings['a2z_dhlexpress_mob_num'])) ? $general_settings['a2z_dhlexpress_mob_num'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Email Address of the Shipper.','a2z_dhlexpress') ?>"></span>	<?php _e('Email Address','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_email" value="<?php echo (isset($general_settings['a2z_dhlexpress_email'])) ? $general_settings['a2z_dhlexpress_email'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Address Line 1 of the Shipper from Address.','a2z_dhlexpress') ?>"></span>	<?php _e('Address Line 1','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_address1" value="<?php echo (isset($general_settings['a2z_dhlexpress_address1'])) ? $general_settings['a2z_dhlexpress_address1'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Address Line 2 of the Shipper from Address.','a2z_dhlexpress') ?>"></span>	<?php _e('Address Line 2','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_address2" value="<?php echo (isset($general_settings['a2z_dhlexpress_address2'])) ? $general_settings['a2z_dhlexpress_address2'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%;padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('City of the Shipper from address.','a2z_dhlexpress') ?>"></span>	<?php _e('City','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_city" value="<?php echo (isset($general_settings['a2z_dhlexpress_city'])) ? $general_settings['a2z_dhlexpress_city'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('State of the Shipper from address.','a2z_dhlexpress') ?>"></span>	<?php _e('State (Two Digit String)','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_state" value="<?php echo (isset($general_settings['a2z_dhlexpress_state'])) ? $general_settings['a2z_dhlexpress_state'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Postal/Zip Code.','a2z_dhlexpress') ?>"></span>	<?php _e('Postal/Zip Code','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_zip" value="<?php echo (isset($general_settings['a2z_dhlexpress_zip'])) ? $general_settings['a2z_dhlexpress_zip'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Country of the Shipper from Address.','a2z_dhlexpress') ?>"></span>	<?php _e('Country','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<select name="a2z_dhlexpress_country" class="wc-enhanced-select" style="width:210px;">
								<?php foreach($countires as $key => $value)
								{

									if(isset($general_settings['a2z_dhlexpress_country']) && ($general_settings['a2z_dhlexpress_country'] == $key))
									{
										echo "<option value=".$key." selected='true'>".$value." [". $dhl_core[$key]['currency'] ."]</option>";
									}
									else
									{
										echo "<option value=".$key.">".$value." [". $dhl_core[$key]['currency'] ."]</option>";
									}
								} ?>
							</select>
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('GSTIN/VAT No.','a2z_dhlexpress') ?>"></span>	<?php _e('GSTIN/VAT No','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_gstin" value="<?php echo (isset($general_settings['a2z_dhlexpress_gstin'])) ? $general_settings['a2z_dhlexpress_gstin'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Conversion Rate from Site Currency to DHL Currency.','a2z_dhlexpress') ?>"></span>	<?php _e('Conversion Rate from Site Currency to DHL Currency ( Ignore if auto conversion is Enabled )','a2z_dhlexpress') ?></h4>
						</td>
						<td>
							<input type="text" name="a2z_dhlexpress_con_rate" value="<?php echo (isset($general_settings['a2z_dhlexpress_con_rate'])) ? $general_settings['a2z_dhlexpress_con_rate'] : ''; ?>">
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Default Domestic Express Shipping.','a2z_dhlexpress') ?>"></span>	<?php _e('Default Domestic Service','a2z_dhlexpress') ?></h4>
							<p><?php _e('This will be used while shipping label generation.','a2z_dhlexpress') ?></p>
						</td>
						<td>
							<select name="a2z_dhlexpress_def_dom" class="wc-enhanced-select" style="width:210px;">
								<?php foreach($_dhl_carriers as $key => $value)
								{
									if(isset($general_settings['a2z_dhlexpress_def_dom']) && ($general_settings['a2z_dhlexpress_def_dom'] == $key))
									{
										echo "<option value=".$key." selected='true'>[".$key."] ".$value."</option>";
									}
									else
									{
										echo "<option value=".$key.">[".$key."] ".$value."</option>";
									}
								} ?>
							</select>
						</td>
					</tr>
					<tr>
						<td style=" width: 50%; padding: 5px; ">
							<h4> <span class="woocommerce-help-tip" data-tip="<?php _e('Default International Shipping.','a2z_dhlexpress') ?>"></span>	<?php _e('Default International Service','a2z_dhlexpress') ?></h4>
							<p><?php _e('This will be used while shipping label generation.','a2z_dhlexpress') ?></p>
						</td>
						<td>
							<select name="a2z_dhlexpress_def_inter" class="wc-enhanced-select" style="width:210px;">
								<?php foreach($_dhl_carriers as $key => $value)
								{
									if(isset($general_settings['a2z_dhlexpress_def_inter']) && ($general_settings['a2z_dhlexpress_def_inter'] == $key))
									{
										echo "<option value=".$key." selected='true'>[".$key."] ".$value."</option>";
									}
									else
									{
										echo "<option value=".$key.">[".$key."] ".$value."</option>";
									}
								} ?>
							</select>
						</td>
					</tr>
				    </table>
				    <hr>
				    <?php
			}
			public function hit_save_product_meta( $post_id ){
				if(isset( $_POST['dhl_express_shipment'])){
					$dhl_express_shipment = $_POST['dhl_express_shipment'];
					if( !empty( $dhl_express_shipment ) )
					update_post_meta( $post_id, 'dhl_express_address', (string) esc_html( $dhl_express_shipment ) );	
				}
							
			}
			public function hit_choose_vendor_address(){
				global $woocommerce, $post;
				$hit_multi_vendor = get_option('hit_multi_vendor');
				$hit_multi_vendor = empty($hit_multi_vendor) ? array() : $hit_multi_vendor;
				$selected_addr = get_post_meta( $post->ID, 'dhl_express_address', true);

				$main_settings = get_option('a2z_dhl_main_settings');
				$main_settings = empty($main_settings) ? array() : $main_settings;
				if(!isset($main_settings['a2z_dhlexpress_v_roles']) || empty($main_settings['a2z_dhlexpress_v_roles'])){
					return;
				}
				$v_users = get_users( [ 'role__in' => $main_settings['a2z_dhlexpress_v_roles'] ] );
				
				?>
				<div class="options_group">
				<p class="form-field dhl_express_shipment">
					<label for="dhl_express_shipment"><?php _e( 'DHL Express Account', 'woocommerce' ); ?></label>
					<select id="dhl_express_shipment" style="width:240px;" name="dhl_express_shipment" class="wc-enhanced-select" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>">
						<option value="default" >Default Account</option>
						<?php
							if ( $v_users ) {
								foreach ( $v_users as $value ) {
									echo '<option value="' .  $value->data->ID  . '" '.($selected_addr == $value->data->ID ? 'selected="true"' : '').'>' . $value->data->display_name . '</option>';
								}
							}
						?>
					</select>
					</p>
				</div>
				<?php
			}

			public function a2z_dhlexpress_init()
			{
				include_once("controllors/a2z_dhlexpress_init.php");
			}
			public function hit_order_status_update(){
				global $woocommerce;
				if(isset($_GET['h1t_updat3_0rd3r']) && isset($_GET['key']) && isset($_GET['action'])){
					$order_id = $_GET['h1t_updat3_0rd3r'];
					$key = $_GET['key'];
					$action = $_GET['action'];
					$order_ids = explode(",",$order_id);
					$general_settings = get_option('a2z_dhl_main_settings',array());
					
					if(isset($general_settings['a2z_dhlexpress_integration_key']) && $general_settings['a2z_dhlexpress_integration_key'] == $key){
						if($action == 'processing'){
							foreach ($order_ids as $order_id) {
								$order = wc_get_order( $order_id );
								$order->update_status( 'processing' );
							}
						}else if($action == 'completed'){
							foreach ($order_ids as $order_id) {
								  $order = wc_get_order( $order_id );
								  $order->update_status( 'completed' );
								  	
							}
						}
					}
					die();
				}

				if(isset($_GET['h1t_updat3_sh1pp1ng']) && isset($_GET['key']) && isset($_GET['user_id']) && isset($_GET['carrier']) && isset($_GET['track']) && isset($_GET['pic_status'])){
					$order_id = $_GET['h1t_updat3_sh1pp1ng'];
					$key = $_GET['key'];
					$general_settings = get_option('a2z_dhl_main_settings',array());
					$user_id = $_GET['user_id'];
					$carrier = $_GET['carrier'];
					$track = $_GET['track'];
					$pic_status = $_GET['pic_status'];
					$output['status'] = 'success';
					$output['tracking_num'] = $track;
					$output['label'] = "https://app.hitshipo.com/api/shipping_labels/".$user_id."/".$carrier."/order_".$order_id."_track_".$track."_label.pdf";
					$output['invoice'] = "https://app.hitshipo.com/api/shipping_labels/".$user_id."/".$carrier."/order_".$order_id."_track_".$track."_invoice.pdf";
					$result_arr = array();
					if(isset($general_settings['a2z_dhlexpress_integration_key']) && $general_settings['a2z_dhlexpress_integration_key'] == $key){
						
						if(isset($_GET['label'])){
							$output['user_id'] = $_GET['label'];
							if(isset($general_settings['a2z_dhlexpress_v_enable']) && $general_settings['a2z_dhlexpress_v_enable'] == 'yes'){
								$result_arr = json_decode(get_option('hit_dhl_values_'.$order_id, array()));
							}
							
							$result_arr[] = $output;

						}else{
							$result_arr[] = $output;							
						}

						update_option('hit_dhl_values_'.$order_id, json_encode($result_arr));
						
						if (isset($pic_status) && $pic_status == "success") {

							$pic_res['confirm_no'] = $_GET['confirm_no'];
							$pic_res['ready_time'] = $_GET['ready_time'];
							$pic_res['pickup_date'] = $_GET['pickup_date'];
							$pic_res['status'] = "success";

							update_option('hit_dhl_pickup_values_'.$order_id, json_encode($pic_res));

						}elseif (isset($pic_status) && $pic_status == "failed"){
							$pic_res['status'] = "failed";
							update_option('hit_dhl_pickup_values_'.$order_id, json_encode($pic_res));

						}
					}

					die();
				}
			}
			public function a2z_dhlexpress_method( $methods )
			{
				if (is_admin() && !is_ajax() || apply_filters('a2z_shipping_method_enabled', true)) {
					$methods['a2z_dhlexpress'] = 'a2z_dhlexpress'; 
				}

				return $methods;
			}
			
			public function a2z_dhlexpress_plugin_action_links($links)
			{
				$setting_value = version_compare(WC()->version, '2.1', '>=') ? "wc-settings" : "woocommerce_settings";
				$plugin_links = array(
					'<a href="' . admin_url( 'admin.php?page=' . $setting_value  . '&tab=shipping&section=az_dhlexpress' ) . '" style="color:green;">' . __( 'Configure', 'a2z_dhlexpress' ) . '</a>',
					'<a href="#" target="_blank" >' . __('Support', 'a2z_dhlexpress') . '</a>'
					);
				return array_merge( $plugin_links, $links );
			}
			public function create_dhl_shipping_meta_box() {
	       		add_meta_box( 'hit_create_dhl_shipping', __('DHL Shipping Label','a2z_dhlexpress'), array($this, 'create_dhl_shipping_label_genetation'), 'shop_order', 'side', 'core' );
	       		add_meta_box( 'hit_create_dhl_return_shipping', __('DHL Return Label','a2z_dhlexpress'), array($this, 'create_dhl_return_label_genetation'), 'shop_order', 'side', 'core' );
		    }
		    public function create_dhl_shipping_label_genetation($post){
		    	// print_r('expression');
		    	// die();		    	
		        if($post->post_type !='shop_order' ){
		    		return;
		    	}
		    	$order = wc_get_order( $post->ID );
		    	$order_id = $order->get_id();
		        $_dhl_carriers = array(
								//"Public carrier name" => "technical name",
								'1'                    => 'DOMESTIC EXPRESS 12:00',
								'2'                    => 'B2C',
								'3'                    => 'B2C',
								'4'                    => 'JETLINE',
								'5'                    => 'SPRINTLINE',
								'7'                    => 'EXPRESS EASY',
								'8'                    => 'EXPRESS EASY',
								'9'                    => 'EUROPACK',
								'B'                    => 'BREAKBULK EXPRESS',
								'C'                    => 'MEDICAL EXPRESS',
								'D'                    => 'EXPRESS WORLDWIDE',
								'E'                    => 'EXPRESS 9:00',
								'F'                    => 'FREIGHT WORLDWIDE',
								'G'                    => 'DOMESTIC ECONOMY SELECT',
								'H'                    => 'ECONOMY SELECT',
								'I'                    => 'DOMESTIC EXPRESS 9:00',
								'J'                    => 'JUMBO BOX',
								'K'                    => 'EXPRESS 9:00',
								'L'                    => 'EXPRESS 10:30',
								'M'                    => 'EXPRESS 10:30',
								'N'                    => 'DOMESTIC EXPRESS',
								'O'                    => 'DOMESTIC EXPRESS 10:30',
								'P'                    => 'EXPRESS WORLDWIDE',
								'Q'                    => 'MEDICAL EXPRESS',
								'R'                    => 'GLOBALMAIL BUSINESS',
								'S'                    => 'SAME DAY',
								'T'                    => 'EXPRESS 12:00',
								'U'                    => 'EXPRESS WORLDWIDE',
								'V'                    => 'EUROPACK',
								'W'                    => 'ECONOMY SELECT',
								'X'                    => 'EXPRESS ENVELOPE',
								'Y'                    => 'EXPRESS 12:00'	
							);

		        $general_settings = get_option('a2z_dhl_main_settings',array());
		       	
		        $items = $order->get_items();

    		    $custom_settings = array();
		    	$custom_settings['default'] =  array();
		    	$vendor_settings = array();

		    	$pack_products = array();
				
				foreach ( $items as $item ) {
					$product_data = $item->get_data();
				    $product = array();
				    $product['product_name'] = $product_data['name'];
				    $product['product_quantity'] = $product_data['quantity'];
				    $product['product_id'] = $product_data['product_id'];
				    
				    $pack_products[] = $product;
				    
				}

				if(isset($general_settings['a2z_dhlexpress_v_enable']) && $general_settings['a2z_dhlexpress_v_enable'] == 'yes' && isset($general_settings['a2z_dhlexpress_v_labels']) && $general_settings['a2z_dhlexpress_v_labels'] == 'yes'){
					// Multi Vendor Enabled
					foreach ($pack_products as $key => $value) {

						$product_id = $value['product_id'];
						$dhl_account = get_post_meta($product_id,'dhl_express_address', true);
						if(empty($dhl_account) || $dhl_account == 'default'){
							$dhl_account = 'default';
							if (!isset($vendor_settings[$dhl_account])) {
								$vendor_settings[$dhl_account] = $custom_settings['default'];
							}
							
							$vendor_settings[$dhl_account]['products'][] = $value;
						}

						if($dhl_account != 'default'){
							$user_account = get_post_meta($dhl_account,'a2z_dhl_vendor_settings', true);
							$user_account = empty($user_account) ? array() : $user_account;
							if(!empty($user_account)){
								if(!isset($vendor_settings[$dhl_account])){

									$vendor_settings[$dhl_account] = $custom_settings['default'];
									unset($value['product_id']);
									$vendor_settings[$dhl_account]['products'][] = $value;
								}
							}else{
								$dhl_account = 'default';
								$vendor_settings[$dhl_account] = $custom_settings['default'];
								$vendor_settings[$dhl_account]['products'][] = $value;
							}
						}

					}

				}

				if(empty($vendor_settings)){
					$custom_settings['default']['products'] = $pack_products;
				}else{
					$custom_settings = $vendor_settings;
				}

		       	$json_data = get_option('hit_dhl_values_'.$order_id);
		       	$pickup_json_data = get_option('hit_dhl_pickup_values_'.$order_id);
		       	// echo $pickup_json_data;
		       	$notice = get_option('hit_dhl_status_'.$order_id, null);
		        if($notice && $notice != 'success'){
		        	echo "<p style='color:red'>".$notice."</p>";
		        	delete_option('hit_dhl_status_'.$order_id);
		        }
		        if(!empty($json_data)){
   					$array_data = json_decode( $json_data, true );
   					// echo '<pre>';print_r($array_data);die();
		       		if(isset($array_data[0])){
		       			foreach ($array_data as $key => $value) {
		       				if(isset($value['user_id'])){
		       					unset($custom_settings[$value['user_id']]);
		       				}
		       				if(isset($value['user_id']) && $value['user_id'] == 'default'){
		       					echo '<br/><b>Default Account</b><br/>';
		       				}else{
		       					$user = get_user_by( 'id', $value['user_id'] );
		       					echo '<br/><b>Account:</b> <small>'.$user->display_name.'</small><br/>';
		       				}
			       			echo '<a href="'.$value['label'].'" target="_blank" style="background:#FFCC00; color: #D40511;border-color: #FFCC00;box-shadow: 0px 1px 0px #FFCC00;text-shadow: 0px 1px 0px #D40511;margin-top:3px;" class="button button-primary"> Shipping Label</a> ';
			       			echo ' <a href="'.$value['invoice'].'" target="_blank" class="button button-primary" style="margin-top:3px;"> Invoice </a><br/>';
		       			}	
		       		}else{
		       			$custom_settings = array();
		       			echo '<a href="'.$array_data['label'].'" target="_blank" style="background:#FFCC00; color: #D40511;border-color: #FFCC00;box-shadow: 0px 1px 0px #FFCC00;text-shadow: 0px 1px 0px #D40511;" class="button button-primary"> Shipping Label</a> ';
			       			echo ' <a href="'.$array_data['invoice'].'" target="_blank" class="button button-primary"> Invoice </a>';
		       		}
   				}
	       		foreach ($custom_settings as $ukey => $value) {
	       			if($ukey == 'default'){
	       				echo '<br/><b>Default Account</b>';
				        echo '<br/><select name="hit_dhl_express_service_code_default">';
				        if(!empty($general_settings['a2z_dhlexpress_carrier'])){
				        	foreach ($general_settings['a2z_dhlexpress_carrier'] as $key => $value) {
				        		echo "<option value='".$key."'>".$key .' - ' .$_dhl_carriers[$key]."</option>";
				        	}
				        }
				        echo '</select>';
				        echo '<br/><b>Shipment Content</b>';
		        
				        echo '<br/><input type="text" style="width:250px;margin-bottom:10px;"  name="hit_dhl_shipment_content_default" placeholder="Shipment content" value="' . (($general_settings['a2z_dhlexpress_ship_content']) ? $general_settings['a2z_dhlexpress_ship_content'] : "") . '" >';
				        
				        echo '<input type="checkbox" name="hit_dhl_add_pickup_default" checked> <b>Create Pickup along with shipment.</b><br/><br/>';
				        echo '<button name="hit_dhl_create_label" value="default" style="background:#FFCC00; color: #D40511;border-color: #FFCC00;box-shadow: 0px 1px 0px #FFCC00;text-shadow: 0px 1px 0px #D40511;" class="button button-primary">Create Shipment</button>';
				        
	       			}else{

	       				$user = get_user_by( 'id', $ukey );
		       			echo '<br/><b>Account:</b> <small>'.$user->display_name.'</small>';
				        echo '<br/><select name="hit_dhl_express_service_code_'.$ukey.'">';
				        if(!empty($general_settings['a2z_dhlexpress_carrier'])){
				        	foreach ($general_settings['a2z_dhlexpress_carrier'] as $key => $value) {
				        		echo "<option value='".$key."'>".$key .' - ' .$_dhl_carriers[$key]."</option>";
				        	}
				        }
				        echo '</select>';
				        echo '<br/><b>Shipment Content</b>';
		        
				        echo '<br/><input type="text" style="width:250px;margin-bottom:10px;"  name="hit_dhl_shipment_content_'.$ukey.'" placeholder="Shipment content" value="' . (($general_settings['a2z_dhlexpress_ship_content']) ? $general_settings['a2z_dhlexpress_ship_content'] : "") . '" >';
				       
				        echo '<input type="checkbox" name="hit_dhl_add_pickup_'.$ukey.'" checked> <b>Create Pickup along with shipment.</b><br/><br/>';
				        echo '<button name="hit_dhl_create_label" value="'.$ukey.'" style="background:#FFCC00; color: #D40511;border-color: #FFCC00;box-shadow: 0px 1px 0px #FFCC00;text-shadow: 0px 1px 0px #D40511;" class="button button-primary">Create Shipment</button><br/>';
				        
	       			}
	       			
	       		}
		        
		        if (!empty($pickup_json_data) && empty($json_data)) {
		        	$pickup_array_data = json_decode( $pickup_json_data, true );
		        	if (isset($pickup_array_data['status']) && $pickup_array_data['status'] == "failed") {
		        		echo "<br/>Pickup creation failed<br/>";
		        	}else{
		        	echo '<h4>DHL pickup details:</h4>';
		        	echo '<b>Confirmation No : </b>'.$pickup_array_data['confirm_no'].'<br/>';
		        	echo '<b>Ready By Time : </b>'.$pickup_array_data['ready_time'].'<br/>';
		        	echo '<b>Pickup Date : </b>'.$pickup_array_data['pickup_date'].'<br/>';
		        	}
		        }else {
		        	echo '<h4>Pickup request can only be created with shipment request</h4>';
		        }

		       	if(!empty($json_data)){
		       		
		       		echo '<br/><button name="hit_dhl_reset" class="button button-secondary" style="margin-top:3px;"> Reset Shipments</button>';
		       		if (!empty($pickup_json_data)) {
			        	$pickup_array_data = json_decode( $pickup_json_data, true );
			        	if (isset($pickup_array_data['status']) && $pickup_array_data['status'] == "failed") {
			        		echo "<br/><p style='color:red'>(Note: Manual pickup scheduling is currently not available. Recreating the shipment will reduce HITShippo balance.)</p>";
			        	}else{
			        	echo '<h4>DHL pickup details:</h4>';
			        	echo '<b>Confirmation No :</b>'.$pickup_array_data['confirm_no'].'<br/>';
			        	echo '<b>Ready By Time :</b>'.$pickup_array_data['ready_time'].'<br/>';
			        	echo '<b>Pickup Date :</b>'.$pickup_array_data['pickup_date'].'<br/>';
			        	}
			        }else {
			        	echo '<br/><br/>Pickup Not Created';
			        }
		       	}

		    }

		    public function create_dhl_return_label_genetation($post){
		    	// print_r('expression');
		    	// die();		    	
		        if($post->post_type !='shop_order' ){
		    		return;
		    	}
		    	$order = wc_get_order( $post->ID );
		    	$order_id = $order->get_id();
		        $_dhl_carriers = array(
								//"Public carrier name" => "technical name",
								'1'                    => 'DOMESTIC EXPRESS 12:00',
								'2'                    => 'B2C',
								'3'                    => 'B2C',
								'4'                    => 'JETLINE',
								'5'                    => 'SPRINTLINE',
								'7'                    => 'EXPRESS EASY',
								'8'                    => 'EXPRESS EASY',
								'9'                    => 'EUROPACK',
								'B'                    => 'BREAKBULK EXPRESS',
								'C'                    => 'MEDICAL EXPRESS',
								'D'                    => 'EXPRESS WORLDWIDE',
								'E'                    => 'EXPRESS 9:00',
								'F'                    => 'FREIGHT WORLDWIDE',
								'G'                    => 'DOMESTIC ECONOMY SELECT',
								'H'                    => 'ECONOMY SELECT',
								'I'                    => 'DOMESTIC EXPRESS 9:00',
								'J'                    => 'JUMBO BOX',
								'K'                    => 'EXPRESS 9:00',
								'L'                    => 'EXPRESS 10:30',
								'M'                    => 'EXPRESS 10:30',
								'N'                    => 'DOMESTIC EXPRESS',
								'O'                    => 'DOMESTIC EXPRESS 10:30',
								'P'                    => 'EXPRESS WORLDWIDE',
								'Q'                    => 'MEDICAL EXPRESS',
								'R'                    => 'GLOBALMAIL BUSINESS',
								'S'                    => 'SAME DAY',
								'T'                    => 'EXPRESS 12:00',
								'U'                    => 'EXPRESS WORLDWIDE',
								'V'                    => 'EUROPACK',
								'W'                    => 'ECONOMY SELECT',
								'X'                    => 'EXPRESS ENVELOPE',
								'Y'                    => 'EXPRESS 12:00'	
							);

		        $general_settings = get_option('a2z_dhl_main_settings',array());
		       	
		       	$json_data = get_option('hit_dhl_return_values_'.$order_id);
		       	if(empty($json_data)){

			        echo '<b>Choose Service to Return</b>';
			        echo '<br/><select name="hit_dhl_express_return_service_code">';
			        if(!empty($general_settings['a2z_dhlexpress_carrier'])){
			        	foreach ($general_settings['a2z_dhlexpress_carrier'] as $key => $value) {
			        		echo "<option value='".$key."'>".$key .' - ' .$_dhl_carriers[$key]."</option>";
			        	}
			        }
			        echo '</select>';
			        

			        echo '<br/><b>Products to return</b>';
			        echo '<br/>';
			        echo '<table>';
			        $items = $order->get_items();
					foreach ( $items as $item ) {
						$product_data = $item->get_data();
					    
					    $product_variation_id = $item->get_variation_id();
					    $product_id = $product_data['product_id'];
					    if(!empty($product_variation_id) && $product_variation_id != 0){
					    	$product_id = $product_variation_id;
					    }

					    echo "<tr><td><input type='checkbox' name='return_products[]' checked value='".$product_id."'>
					    	</td>";
					    echo "<td style='width:150px;'><small title='".$product_data['name']."'>". substr($product_data['name'],0,7)."</small></td>";
					    echo "<td><input type='number' name='qty_products[".$product_id."]' style='width:50px;' value='".$product_data['quantity']."'></td>";
					    echo "</tr>";
					    
					    
					}
			        echo '</table><br/>';

			        $notice = get_option('hit_dhl_return_status_'.$order_id, null);
			        if($notice && $notice != 'success'){
			        	echo "<p style='color:red'>".$notice."</p>";
			        	delete_option('hit_dhl_return_status_'.$order_id);
			        }

			        echo '<button name="hit_dhl_create_return_label" style="background:#FFCC00; color: #D40511;border-color: #FFCC00;box-shadow: 0px 1px 0px #FFCC00;text-shadow: 0px 1px 0px #D40511;" class="button button-primary">Create Return Shipment</button>';
			        
		       	} else{
		       		$array_data = json_decode( $json_data, true );
		       		echo '<a href="'.$array_data['label'].'" target="_blank" style="background:#FFCC00; color: #D40511;border-color: #FFCC00;box-shadow: 0px 1px 0px #FFCC00;text-shadow: 0px 1px 0px #D40511;" class="button button-primary"> Return Label </a> ';
		       		echo ' <a href="'.$array_data['invoice'].'" target="_blank" class="button button-primary"> Invoice </a>';
		       		
		       	}

		    }

		    public function hit_wc_checkout_order_processed($order_id){
		    	// die();
				$post = get_post($order_id);
				
		    	if($post->post_type !='shop_order' ){
		    		return;
		    	}

		    	$ship_content = !empty($_POST['hit_dhl_shipment_content']) ? $_POST['hit_dhl_shipment_content'] : 'Shipment Content';
		        $order = wc_get_order( $order_id );

		        $service_code = $multi_ven = '';
		        foreach( $order->get_shipping_methods() as $item_id => $item ){
					$service_code = $item->get_meta('a2z_dhl_service');
					$multi_ven = $item->get_meta('a2z_multi_ven');

				}
				if(empty($service_code)){
					return;
				}
				$general_settings = get_option('a2z_dhl_main_settings',array());
		    	$order_data = $order->get_data();
		    	$items = $order->get_items();
		    	
		    	if(!isset($general_settings['a2z_dhlexpress_label_automation']) || $general_settings['a2z_dhlexpress_label_automation'] != 'yes'){
		    		return;
		    	}

		    	$custom_settings = array();
				$custom_settings['default'] = array(
									'a2z_dhlexpress_site_id' => $general_settings['a2z_dhlexpress_site_id'],
									'a2z_dhlexpress_site_pwd' => $general_settings['a2z_dhlexpress_site_pwd'],
									'a2z_dhlexpress_acc_no' => $general_settings['a2z_dhlexpress_acc_no'],
									'a2z_dhlexpress_import_no' => $general_settings['a2z_dhlexpress_import_no'],
									'a2z_dhlexpress_shipper_name' => $general_settings['a2z_dhlexpress_shipper_name'],
									'a2z_dhlexpress_company' => $general_settings['a2z_dhlexpress_company'],
									'a2z_dhlexpress_mob_num' => $general_settings['a2z_dhlexpress_mob_num'],
									'a2z_dhlexpress_email' => $general_settings['a2z_dhlexpress_email'],
									'a2z_dhlexpress_address1' => $general_settings['a2z_dhlexpress_address1'],
									'a2z_dhlexpress_address2' => $general_settings['a2z_dhlexpress_address2'],
									'a2z_dhlexpress_city' => $general_settings['a2z_dhlexpress_city'],
									'a2z_dhlexpress_state' => $general_settings['a2z_dhlexpress_state'],
									'a2z_dhlexpress_zip' => $general_settings['a2z_dhlexpress_zip'],
									'a2z_dhlexpress_country' => $general_settings['a2z_dhlexpress_country'],
									'a2z_dhlexpress_gstin' => $general_settings['a2z_dhlexpress_gstin'],
									'a2z_dhlexpress_con_rate' => $general_settings['a2z_dhlexpress_con_rate'],
									'service_code' => $service_code,
									'a2z_dhlexpress_label_email' => $general_settings['a2z_dhlexpress_label_email'],
								);
				$vendor_settings = array();



				if(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'KG_CM')
				{
					$dhl_mod_weight_unit = 'kg';
					$dhl_mod_dim_unit = 'cm';
				}elseif(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'LB_IN')
				{
					$dhl_mod_weight_unit = 'lbs';
					$dhl_mod_dim_unit = 'in';
				}
				else
				{
					$dhl_mod_weight_unit = 'kg';
					$dhl_mod_dim_unit = 'cm';
				}
			    

				$pack_products = array();
				
				foreach ( $items as $item ) {
					$product_data = $item->get_data();

				    $product = array();
				    $product['product_name'] = str_replace('"', '', $product_data['name']);
				    $product['product_quantity'] = $product_data['quantity'];
				    $product['product_id'] = $product_data['product_id'];
				    
				    $product_variation_id = $item->get_variation_id();
				    if(empty($product_variation_id) || $product_variation_id == 0){
				    	$getproduct = wc_get_product( $product_data['product_id'] );
				    }else{
				    	$getproduct = wc_get_product( $product_variation_id );
				    }
				    $woo_weight_unit = get_option('woocommerce_weight_unit');
					$woo_dimension_unit = get_option('woocommerce_dimension_unit');

					$dhl_mod_weight_unit = $dhl_mod_dim_unit = '';

				    $product['price'] = $getproduct->get_price();

				    if(!$product['price']){
						$product['price'] = (isset($product_data['total']) && isset($product_data['quantity'])) ? number_format(($product_data['total'] / $product_data['quantity']), 2) : 0;
					}

				    if ($woo_dimension_unit != $dhl_mod_dim_unit) {
				    	$prod_width = round($getproduct->get_width(), 3);
				    	$prod_height = round($getproduct->get_height(), 3);
				    	$prod_depth = round($getproduct->get_length(), 3);

				    	//wc_get_dimension( $dimension, $to_unit, $from_unit );
				    	$product['width'] = round(wc_get_dimension( $prod_width, $dhl_mod_dim_unit, $woo_dimension_unit ), 3);
				    	$product['height'] = round(wc_get_dimension( $prod_height, $dhl_mod_dim_unit, $woo_dimension_unit ), 3);
						$product['depth'] = round(wc_get_dimension( $prod_depth, $dhl_mod_dim_unit, $woo_dimension_unit ), 3);

				    }else {
				    	$product['width'] = round($getproduct->get_width(),3);
				    	$product['height'] = round($getproduct->get_height(),3);
				    	$product['depth'] = round($getproduct->get_length(),3);
				    }
				    
				    if ($woo_weight_unit != $dhl_mod_weight_unit) {
				    	$prod_weight = $getproduct->get_weight();
				    	$product['weight'] = round(wc_get_weight( $prod_weight, $dhl_mod_weight_unit, $woo_weight_unit ), 3);
				    }else{
				    	$product['weight'] = round($getproduct->get_weight(),3);
					}
				    $pack_products[] = $product;
				    
				}

				if(isset($general_settings['a2z_dhlexpress_v_enable']) && $general_settings['a2z_dhlexpress_v_enable'] == 'yes' && isset($general_settings['a2z_dhlexpress_v_labels']) && $general_settings['a2z_dhlexpress_v_labels'] == 'yes'){
					// Multi Vendor Enabled
					foreach ($pack_products as $key => $value) {

						$product_id = $value['product_id'];
						$dhl_account = get_post_meta($product_id,'dhl_express_address', true);
						if(empty($dhl_account) || $dhl_account == 'default'){
							$dhl_account = 'default';
							if (!isset($vendor_settings[$dhl_account])) {
								$vendor_settings[$dhl_account] = $custom_settings['default'];
							}
							
							$vendor_settings[$dhl_account]['products'][] = $value;
						}

						if($dhl_account != 'default'){
							$user_account = get_post_meta($dhl_account,'a2z_dhl_vendor_settings', true);
							$user_account = empty($user_account) ? array() : $user_account;
							if(!empty($user_account)){
								if(!isset($vendor_settings[$dhl_account])){

									$vendor_settings[$dhl_account] = $custom_settings['default'];
									
									if($user_account['a2z_dhlexpress_site_id'] != '' && $user_account['a2z_dhlexpress_site_pwd'] != '' && $user_account['a2z_dhlexpress_acc_no'] != ''){
										
										$vendor_settings[$dhl_account]['a2z_dhlexpress_site_id'] = $user_account['a2z_dhlexpress_site_id'];

										if($user_account['a2z_dhlexpress_site_pwd'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_site_pwd'] = $user_account['a2z_dhlexpress_site_pwd'];
										}

										if($user_account['a2z_dhlexpress_acc_no'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_acc_no'] = $user_account['a2z_dhlexpress_acc_no'];
										}

										$vendor_settings[$dhl_account]['a2z_dhlexpress_import_no'] = !empty($user_account['a2z_dhlexpress_import_no']) ? $user_account['a2z_dhlexpress_import_no'] : '';
										
									}

									if ($user_account['a2z_dhlexpress_address1'] != '' && $user_account['a2z_dhlexpress_city'] != '' && $user_account['a2z_dhlexpress_state'] != '' && $user_account['a2z_dhlexpress_zip'] != '' && $user_account['a2z_dhlexpress_country'] != '' && $user_account['a2z_dhlexpress_shipper_name'] != '') {
										
										if($user_account['a2z_dhlexpress_shipper_name'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_shipper_name'] = $user_account['a2z_dhlexpress_shipper_name'];
										}

										if($user_account['a2z_dhlexpress_company'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_company'] = $user_account['a2z_dhlexpress_company'];
										}

										if($user_account['a2z_dhlexpress_mob_num'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_mob_num'] = $user_account['a2z_dhlexpress_mob_num'];
										}

										if($user_account['a2z_dhlexpress_email'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_email'] = $user_account['a2z_dhlexpress_email'];
										}

										if ($user_account['a2z_dhlexpress_address1'] != '') {
											$vendor_settings[$dhl_account]['a2z_dhlexpress_address1'] = $user_account['a2z_dhlexpress_address1'];
										}

										$vendor_settings[$dhl_account]['a2z_dhlexpress_address2'] = $user_account['a2z_dhlexpress_address2'];
										
										if($user_account['a2z_dhlexpress_city'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_city'] = $user_account['a2z_dhlexpress_city'];
										}

										if($user_account['a2z_dhlexpress_state'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_state'] = $user_account['a2z_dhlexpress_state'];
										}

										if($user_account['a2z_dhlexpress_zip'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_zip'] = $user_account['a2z_dhlexpress_zip'];
										}

										if($user_account['a2z_dhlexpress_country'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_country'] = $user_account['a2z_dhlexpress_country'];
										}

										$vendor_settings[$dhl_account]['a2z_dhlexpress_gstin'] = $user_account['a2z_dhlexpress_gstin'];
										$vendor_settings[$dhl_account]['a2z_dhlexpress_con_rate'] = $user_account['a2z_dhlexpress_con_rate'];
									}

									if(isset($general_settings['a2z_dhlexpress_v_email']) && $general_settings['a2z_dhlexpress_v_email'] == 'yes'){
										$user_dat = get_userdata($dhl_account);
										$vendor_settings[$dhl_account]['a2z_dhlexpress_label_email'] = $user_dat->data->user_email;
									}
									
									if($multi_ven !=''){
										$array_ven = explode('|',$multi_ven);
										$scode = '';
										foreach ($array_ven as $key => $svalue) {
											$ex_service = explode("_", $svalue);
											if($ex_service[0] == $dhl_account){
												$vendor_settings[$dhl_account]['service_code'] = $ex_service[1];
											}
										}
										
										if($scode == ''){
											if($order_data['shipping']['country'] != $vendor_settings[$dhl_account]['a2z_dhlexpress_country']){
												$vendor_settings[$dhl_account]['service_code'] = $user_account['a2z_dhlexpress_def_inter'];
											}else{
												$vendor_settings[$dhl_account]['service_code'] = $user_account['a2z_dhlexpress_def_dom'];
											}
										}

									}else{
										if($order_data['shipping']['country'] != $vendor_settings[$dhl_account]['a2z_dhlexpress_country']){
											$vendor_settings[$dhl_account]['service_code'] = $user_account['a2z_dhlexpress_def_inter'];
										}else{
											$vendor_settings[$dhl_account]['service_code'] = $user_account['a2z_dhlexpress_def_dom'];
										}

									}
								}
								unset($value['product_id']);
								$vendor_settings[$dhl_account]['products'][] = $value;
							}
						}

					}

				}

				if(empty($vendor_settings)){
					$custom_settings['default']['products'] = $pack_products;
				}else{
					$custom_settings = $vendor_settings;
				}

				$order_id = $order_data['id'];
	       		$order_currency = $order_data['currency'];

	       		$order_shipping_first_name = $order_data['shipping']['first_name'];
				$order_shipping_last_name = $order_data['shipping']['last_name'];
				$order_shipping_company = empty($order_data['shipping']['company']) ? $order_data['shipping']['first_name'] :  $order_data['shipping']['company'];
				$order_shipping_address_1 = $order_data['shipping']['address_1'];
				$order_shipping_address_2 = $order_data['shipping']['address_2'];
				$order_shipping_city = $order_data['shipping']['city'];
				$order_shipping_state = $order_data['shipping']['state'];
				$order_shipping_postcode = $order_data['shipping']['postcode'];
				$order_shipping_country = $order_data['shipping']['country'];
				$order_shipping_phone = $order_data['billing']['phone'];
				$order_shipping_email = $order_data['billing']['email'];
				if(!empty($general_settings) && isset($general_settings['a2z_dhlexpress_integration_key'])){
					$mode = 'live';
					if(isset($general_settings['a2z_dhlexpress_test']) && $general_settings['a2z_dhlexpress_test']== 'yes'){
						$mode = 'test';
					}
					$execution = 'manual';
					if(isset($general_settings['a2z_dhlexpress_label_automation']) && $general_settings['a2z_dhlexpress_label_automation']== 'yes'){
						$execution = 'auto';
					}

					$boxes_to_shipo = array();
					if (isset($general_settings['a2z_dhlexpress_packing_type']) && $general_settings['a2z_dhlexpress_packing_type'] == "box") {
						if (isset($general_settings['a2z_dhlexpress_boxes']) && !empty($general_settings['a2z_dhlexpress_boxes'])) {
							foreach ($general_settings['a2z_dhlexpress_boxes'] as $box) {
								if ($box['enabled'] != 1) {
									continue;
								}else {
									$boxes_to_shipo[] = $box;
								}
							}
						}
					}


					foreach ($custom_settings as $key => $cvalue) {

						$c_codes = [];

						foreach($cvalue['products'] as $prod_to_shipo){
							$saved_cc = get_post_meta( $prod_to_shipo['product_id'], 'hits_dhl_cc', true);
							if(!empty($saved_cc)){
								$c_codes[] = $saved_cc;
							}
						}
						//For Automatic Label Generation						
						
						$data = array();
						$data['integrated_key'] = $general_settings['a2z_dhlexpress_integration_key'];
						$data['order_id'] = $order_id;
						$data['exec_type'] = $execution;
						$data['mode'] = $mode;
						$data['ship_price'] = $order_data['shipping_total'];
						$data['meta'] = array(
							"site_id" => $cvalue['a2z_dhlexpress_site_id'],
							"password"  => $cvalue['a2z_dhlexpress_site_pwd'],
							"accountnum" => $cvalue['a2z_dhlexpress_acc_no'],
							"t_company" => $order_shipping_company,
							"t_address1" => str_replace('"', '', $order_shipping_address_1),
							"t_address2" => str_replace('"', '', $order_shipping_address_2),
							"t_city" => $order_shipping_city,
							"t_state" => $order_shipping_state,
							"t_postal" => $order_shipping_postcode,
							"t_country" => $order_shipping_country,
							"t_name" => $order_shipping_first_name . ' '. $order_shipping_last_name,
							"t_phone" => $order_shipping_phone,
							"t_email" => $order_shipping_email,
							"dutiable" => $general_settings['a2z_dhlexpress_duty_payment'],
							"insurance" => $general_settings['a2z_dhlexpress_insure'],
							"pack_this" => "Y",
							"products" => $cvalue['products'],
							"pack_algorithm" => $general_settings['a2z_dhlexpress_packing_type'],
							"boxes" => $boxes_to_shipo,
							"max_weight" => $general_settings['a2z_dhlexpress_max_weight'],
							"plt" => ($general_settings['a2z_dhlexpress_ppt'] == 'yes') ? "Y" : "N",
							"airway_bill" => ($general_settings['a2z_dhlexpress_aabill'] == 'yes') ? "Y" : "N",
							"sd" => ($general_settings['a2z_dhlexpress_sat'] == 'yes') ? "Y" : "N",
							"cod" => ($general_settings['a2z_dhlexpress_cod'] == 'yes') ? "Y" : "N",
							"service_code" => $service_code,
							"shipment_content" => $ship_content,
							"email_alert" => ( isset($general_settings['a2z_dhlexpress_email_alert']) && ($general_settings['a2z_dhlexpress_email_alert'] == 'yes') ) ? "Y" : "N",
							"s_company" => $cvalue['a2z_dhlexpress_company'],
							"s_address1" => $cvalue['a2z_dhlexpress_address1'],
							"s_address2" => $cvalue['a2z_dhlexpress_address2'],
							"s_city" => $cvalue['a2z_dhlexpress_city'],
							"s_state" => $cvalue['a2z_dhlexpress_state'],
							"s_postal" => $cvalue['a2z_dhlexpress_zip'],
							"s_country" => $cvalue['a2z_dhlexpress_country'],
							"gstin" => $cvalue['a2z_dhlexpress_gstin'],
							"s_name" => $cvalue['a2z_dhlexpress_shipper_name'],
							"s_phone" => $cvalue['a2z_dhlexpress_mob_num'],
							"s_email" => $cvalue['a2z_dhlexpress_email'],
							"label_size" => $general_settings['a2z_dhlexpress_print_size'],
							"sent_email_to" => $cvalue['a2z_dhlexpress_label_email'],
							"pic_exec_type" => (isset($general_settings['a2z_dhlexpress_pickup_automation']) && $general_settings['a2z_dhlexpress_pickup_automation'] == 'yes') ? "auto" : "manual",
				            "pic_loc_type" => (isset($general_settings['a2z_dhlexpress_pickup_loc_type']) ? $general_settings['a2z_dhlexpress_pickup_loc_type'] : ''),
				            "pic_pac_loc" => (isset($general_settings['a2z_dhlexpress_pickup_pac_loc']) ? $general_settings['a2z_dhlexpress_pickup_pac_loc'] : ''),
				            "pic_contact_per" => (isset($general_settings['a2z_dhlexpress_pickup_per_name']) ? $general_settings['a2z_dhlexpress_pickup_per_name'] : ''),
				            "pic_contact_no" => (isset($general_settings['a2z_dhlexpress_pickup_per_contact_no']) ? $general_settings['a2z_dhlexpress_pickup_per_contact_no'] : ''),
				            "pic_door_to" => (isset($general_settings['a2z_dhlexpress_pickup_door_to']) ? $general_settings['a2z_dhlexpress_pickup_door_to'] : ''),
				            "pic_type" => (isset($general_settings['a2z_dhlexpress_pickup_type']) ? $general_settings['a2z_dhlexpress_pickup_type'] : ''),
				            "pic_days_after" => (isset($general_settings['a2z_dhlexpress_pickup_date']) ? $general_settings['a2z_dhlexpress_pickup_date'] : ''),
				            "pic_open_time" => (isset($general_settings['a2z_dhlexpress_pickup_open_time']) ? $general_settings['a2z_dhlexpress_pickup_open_time'] : ''),
				            "pic_close_time" => (isset($general_settings['a2z_dhlexpress_pickup_close_time']) ? $general_settings['a2z_dhlexpress_pickup_close_time'] : ''),
				            "pic_mail_date" => date('c'),
				    		"pic_date" => date("Y-m-d"),
							"label" => $key,
							"payment_con" => (isset($general_settings['a2z_dhlexpress_pay_con']) ? $general_settings['a2z_dhlexpress_pay_con'] : 'S'),
							"cus_payment_con" => (isset($general_settings['a2z_dhlexpress_cus_pay_con']) ? $general_settings['a2z_dhlexpress_cus_pay_con'] : ''),
							"translation" => ( (isset($general_settings['a2z_dhlexpress_translation']) && $general_settings['a2z_dhlexpress_translation'] == "yes" ) ? 'Y' : 'N'),
							"translation_key" => (isset($general_settings['a2z_dhlexpress_translation_key']) ? $general_settings['a2z_dhlexpress_translation_key'] : ''),
							"commodity_code" => $c_codes,
							
						);
						//echo"<pre>";print_r($data);die();
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt_array($curl, array(
							CURLOPT_URL            => "https://app.hitshipo.com/api/dhl_order.php",
							// CURLOPT_URL            => "http://localhost/hitshipo/api/dhl_order.php",      //For Automatic label
				            CURLOPT_RETURNTRANSFER => false,
				            CURLOPT_ENCODING       => "",
				            CURLOPT_MAXREDIRS      => 10,
				            CURLOPT_HEADER         => false,
				            CURLOPT_TIMEOUT        => 60,
				            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				            CURLOPT_CUSTOMREQUEST  => 'POST',
						));
				    	 curl_exec($curl);

				  //   	echo "<pre>";
				  //   	echo "1";
						// print_r(json_decode( curl_exec($curl), true ));
						// die();
					}
	       		
				}	
		    }

		    // Save the data of the Meta field
			public function hit_create_dhl_shipping( $order_id ) {
				
		    	$post = get_post($order_id);
		    	if($post->post_type !='shop_order' ){
		    		return;
		    	}
		    	
		    	if (  isset( $_POST[ 'hit_dhl_reset' ] ) ) {
		    		delete_option('hit_dhl_values_'.$order_id);
		    	}

		    	if (  isset( $_POST['hit_dhl_create_label']) ) {
		    		$create_shipment_for = $_POST['hit_dhl_create_label'];
		           $service_code = $_POST['hit_dhl_express_service_code_'.$create_shipment_for];
		           $ship_content = !empty($_POST['hit_dhl_shipment_content_'.$create_shipment_for]) ? $_POST['hit_dhl_shipment_content_'.$create_shipment_for] : 'Shipment Content';
		           $pickup_mode = (isset($_POST['hit_dhl_add_pickup_'.$create_shipment_for]) && $_POST['hit_dhl_add_pickup_'.$create_shipment_for]) ? 'auto' : 'manual';
		           $order = wc_get_order( $order_id );
			       if($order){
		       		$order_data = $order->get_data();
			       		$order_id = $order_data['id'];
			       		$order_currency = $order_data['currency'];

			       		$order_shipping_first_name = $order_data['shipping']['first_name'];
						$order_shipping_last_name = $order_data['shipping']['last_name'];
						$order_shipping_company = empty($order_data['shipping']['company']) ? $order_data['shipping']['first_name'] :  $order_data['shipping']['company'];
						$order_shipping_address_1 = $order_data['shipping']['address_1'];
						$order_shipping_address_2 = $order_data['shipping']['address_2'];
						$order_shipping_city = $order_data['shipping']['city'];
						$order_shipping_state = $order_data['shipping']['state'];
						$order_shipping_postcode = $order_data['shipping']['postcode'];
						$order_shipping_country = $order_data['shipping']['country'];
						$order_shipping_phone = $order_data['billing']['phone'];
						$order_shipping_email = $order_data['billing']['email'];

						$items = $order->get_items();
						$pack_products = array();
						$general_settings = get_option('a2z_dhl_main_settings',array());

						foreach ( $items as $item ) {
							$product_data = $item->get_data();
						    $product = array();
						    $product['product_name'] = str_replace('"', '', $product_data['name']);
						    $product['product_quantity'] = $product_data['quantity'];
						   	$product['product_id'] = $product_data['product_id'];

						    $product_variation_id = $item->get_variation_id();
						    if(empty($product_variation_id)){
						    	$getproduct = wc_get_product( $product_data['product_id'] );
						    }else{
						    	$getproduct = wc_get_product( $product_variation_id );
						    }
						    
						    $woo_weight_unit = get_option('woocommerce_weight_unit');
							$woo_dimension_unit = get_option('woocommerce_dimension_unit');

							$dhl_mod_weight_unit = $dhl_mod_dim_unit = '';

							if(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'KG_CM')
							{
								$dhl_mod_weight_unit = 'kg';
								$dhl_mod_dim_unit = 'cm';
							}elseif(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'LB_IN')
							{
								$dhl_mod_weight_unit = 'lbs';
								$dhl_mod_dim_unit = 'in';
							}
							else
							{
								$dhl_mod_weight_unit = 'kg';
								$dhl_mod_dim_unit = 'cm';
							}

						    $product['price'] = $getproduct->get_price();

						    if(!$product['price']){
								$product['price'] = (isset($product_data['total']) && isset($product_data['quantity'])) ? number_format(($product_data['total'] / $product_data['quantity']), 2) : 0;
							}

						    if ($woo_dimension_unit != $dhl_mod_dim_unit) {
					    	$prod_width = $getproduct->get_width();
					    	$prod_height = $getproduct->get_height();
					    	$prod_depth = $getproduct->get_length();

					    	//wc_get_dimension( $dimension, $to_unit, $from_unit );
					    	$product['width'] = round(wc_get_dimension( $prod_width, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);
					    	$product['height'] = round(wc_get_dimension( $prod_height, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);
							$product['depth'] = round(wc_get_dimension( $prod_depth, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);

						    }else {
						    	$product['width'] = $getproduct->get_width();
						    	$product['height'] = $getproduct->get_height();
						    	$product['depth'] = $getproduct->get_length();
						    }
						    
						    if ($woo_weight_unit != $dhl_mod_weight_unit) {
						    	$prod_weight = $getproduct->get_weight();
						    	$product['weight'] = round(wc_get_weight( $prod_weight, $dhl_mod_weight_unit, $woo_weight_unit ), 2);
						    }else{
						    	$product['weight'] = $getproduct->get_weight();
							}

						    $pack_products[] = $product;
						    
						}
						
						$custom_settings = array();
						$custom_settings['default'] = array(
											'a2z_dhlexpress_site_id' => $general_settings['a2z_dhlexpress_site_id'],
											'a2z_dhlexpress_site_pwd' => $general_settings['a2z_dhlexpress_site_pwd'],
											'a2z_dhlexpress_acc_no' => $general_settings['a2z_dhlexpress_acc_no'],
											'a2z_dhlexpress_import_no' => $general_settings['a2z_dhlexpress_import_no'],
											'a2z_dhlexpress_shipper_name' => $general_settings['a2z_dhlexpress_shipper_name'],
											'a2z_dhlexpress_company' => $general_settings['a2z_dhlexpress_company'],
											'a2z_dhlexpress_mob_num' => $general_settings['a2z_dhlexpress_mob_num'],
											'a2z_dhlexpress_email' => $general_settings['a2z_dhlexpress_email'],
											'a2z_dhlexpress_address1' => $general_settings['a2z_dhlexpress_address1'],
											'a2z_dhlexpress_address2' => $general_settings['a2z_dhlexpress_address2'],
											'a2z_dhlexpress_city' => $general_settings['a2z_dhlexpress_city'],
											'a2z_dhlexpress_state' => $general_settings['a2z_dhlexpress_state'],
											'a2z_dhlexpress_zip' => $general_settings['a2z_dhlexpress_zip'],
											'a2z_dhlexpress_country' => $general_settings['a2z_dhlexpress_country'],
											'a2z_dhlexpress_gstin' => $general_settings['a2z_dhlexpress_gstin'],
											'a2z_dhlexpress_con_rate' => $general_settings['a2z_dhlexpress_con_rate'],
											'service_code' => $service_code,
											'a2z_dhlexpress_label_email' => $general_settings['a2z_dhlexpress_label_email'],
										);
						$vendor_settings = array();
						if(isset($general_settings['a2z_dhlexpress_v_enable']) && $general_settings['a2z_dhlexpress_v_enable'] == 'yes' && isset($general_settings['a2z_dhlexpress_v_labels']) && $general_settings['a2z_dhlexpress_v_labels'] == 'yes'){
						// Multi Vendor Enabled
						foreach ($pack_products as $key => $value) {
							$product_id = $value['product_id'];
							$dhl_account = get_post_meta($product_id,'dhl_express_address', true);
							if(empty($dhl_account) || $dhl_account == 'default'){
								$dhl_account = 'default';
								if (!isset($vendor_settings[$dhl_account])) {
									$vendor_settings[$dhl_account] = $custom_settings['default'];
								}
								
								$vendor_settings[$dhl_account]['products'][] = $value;
							}

							if($dhl_account != 'default'){
								$user_account = get_post_meta($dhl_account,'a2z_dhl_vendor_settings', true);
								$user_account = empty($user_account) ? array() : $user_account;
								if(!empty($user_account)){
									if(!isset($vendor_settings[$dhl_account])){

										$vendor_settings[$dhl_account] = $custom_settings['default'];
										
									if($user_account['a2z_dhlexpress_site_id'] != '' && $user_account['a2z_dhlexpress_site_pwd'] != '' && $user_account['a2z_dhlexpress_acc_no'] != ''){
										
										$vendor_settings[$dhl_account]['a2z_dhlexpress_site_id'] = $user_account['a2z_dhlexpress_site_id'];

										if($user_account['a2z_dhlexpress_site_pwd'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_site_pwd'] = $user_account['a2z_dhlexpress_site_pwd'];
										}

										if($user_account['a2z_dhlexpress_acc_no'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_acc_no'] = $user_account['a2z_dhlexpress_acc_no'];
										}

										$vendor_settings[$dhl_account]['a2z_dhlexpress_import_no'] = !empty($user_account['a2z_dhlexpress_import_no']) ? $user_account['a2z_dhlexpress_import_no'] : '';
										
									}

									if ($user_account['a2z_dhlexpress_address1'] != '' && $user_account['a2z_dhlexpress_city'] != '' && $user_account['a2z_dhlexpress_state'] != '' && $user_account['a2z_dhlexpress_zip'] != '' && $user_account['a2z_dhlexpress_country'] != '' && $user_account['a2z_dhlexpress_shipper_name'] != '') {
										
										if($user_account['a2z_dhlexpress_shipper_name'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_shipper_name'] = $user_account['a2z_dhlexpress_shipper_name'];
										}

										if($user_account['a2z_dhlexpress_company'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_company'] = $user_account['a2z_dhlexpress_company'];
										}

										if($user_account['a2z_dhlexpress_mob_num'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_mob_num'] = $user_account['a2z_dhlexpress_mob_num'];
										}

										if($user_account['a2z_dhlexpress_email'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_email'] = $user_account['a2z_dhlexpress_email'];
										}

										if ($user_account['a2z_dhlexpress_address1'] != '') {
											$vendor_settings[$dhl_account]['a2z_dhlexpress_address1'] = $user_account['a2z_dhlexpress_address1'];
										}

										$vendor_settings[$dhl_account]['a2z_dhlexpress_address2'] = $user_account['a2z_dhlexpress_address2'];
										
										if($user_account['a2z_dhlexpress_city'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_city'] = $user_account['a2z_dhlexpress_city'];
										}

										if($user_account['a2z_dhlexpress_state'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_state'] = $user_account['a2z_dhlexpress_state'];
										}

										if($user_account['a2z_dhlexpress_zip'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_zip'] = $user_account['a2z_dhlexpress_zip'];
										}

										if($user_account['a2z_dhlexpress_country'] != ''){
											$vendor_settings[$dhl_account]['a2z_dhlexpress_country'] = $user_account['a2z_dhlexpress_country'];
										}

										$vendor_settings[$dhl_account]['a2z_dhlexpress_gstin'] = $user_account['a2z_dhlexpress_gstin'];
										$vendor_settings[$dhl_account]['a2z_dhlexpress_con_rate'] = $user_account['a2z_dhlexpress_con_rate'];

									}
										
										if(isset($general_settings['a2z_dhlexpress_v_email']) && $general_settings['a2z_dhlexpress_v_email'] == 'yes'){
											$user_dat = get_userdata($dhl_account);
											$vendor_settings[$dhl_account]['a2z_dhlexpress_label_email'] = $user_dat->data->user_email;
										}
										

										if($order_data['shipping']['country'] != $vendor_settings[$dhl_account]['a2z_dhlexpress_country']){
											$vendor_settings[$dhl_account]['service_code'] = empty($service_code) ? $user_account['a2z_dhlexpress_def_inter'] : $service_code;
										}else{
											$vendor_settings[$dhl_account]['service_code'] = empty($service_code) ? $user_account['a2z_dhlexpress_def_dom'] : $service_code;
										}
									}
									unset($value['product_id']);
									$vendor_settings[$dhl_account]['products'][] = $value;
								}
							}

						}

					}

					if(empty($vendor_settings)){
						$custom_settings['default']['products'] = $pack_products;
					}else{
						$custom_settings = $vendor_settings;
					}

					if(!empty($general_settings) && isset($general_settings['a2z_dhlexpress_integration_key']) && isset($custom_settings[$create_shipment_for])){
						$mode = 'live';
						if(isset($general_settings['a2z_dhlexpress_test']) && $general_settings['a2z_dhlexpress_test']== 'yes'){
							$mode = 'test';
						}

						$execution = 'manual';
						
						$boxes_to_shipo = array();
						if (isset($general_settings['a2z_dhlexpress_packing_type']) && $general_settings['a2z_dhlexpress_packing_type'] == "box") {
							if (isset($general_settings['a2z_dhlexpress_boxes']) && !empty($general_settings['a2z_dhlexpress_boxes'])) {
								foreach ($general_settings['a2z_dhlexpress_boxes'] as $box) {
									if ($box['enabled'] != 1) {
										continue;
									}else {
										$boxes_to_shipo[] = $box;
									}
								}
							}
						}

						$c_codes = [];

						foreach($custom_settings[$create_shipment_for]['products'] as $prod_to_shipo){
							$saved_cc = get_post_meta( $prod_to_shipo['product_id'], 'hits_dhl_cc', true);
							if(!empty($saved_cc)){
								$c_codes[] = $saved_cc;
							}
						}
						
						$data = array();
						$data['integrated_key'] = $general_settings['a2z_dhlexpress_integration_key'];
						$data['order_id'] = $order_id;
						$data['exec_type'] = $execution;
						$data['mode'] = $mode;
						$data['meta'] = array(
							"site_id" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_site_id'],
							"password"  => $custom_settings[$create_shipment_for]['a2z_dhlexpress_site_pwd'],
							"accountnum" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_acc_no'],
							"t_company" => $order_shipping_company,
							"t_address1" => str_replace('"', '', $order_shipping_address_1),
							"t_address2" => str_replace('"', '', $order_shipping_address_2),
							"t_city" => $order_shipping_city,
							"t_state" => $order_shipping_state,
							"t_postal" => $order_shipping_postcode,
							"t_country" => $order_shipping_country,
							"t_name" => $order_shipping_first_name . ' '. $order_shipping_last_name,
							"t_phone" => $order_shipping_phone,
							"t_email" => $order_shipping_email,
							"dutiable" => $general_settings['a2z_dhlexpress_duty_payment'],
							"insurance" => $general_settings['a2z_dhlexpress_insure'],
							"pack_this" => "Y",
							"products" => $custom_settings[$create_shipment_for]['products'],
							"pack_algorithm" => $general_settings['a2z_dhlexpress_packing_type'],
							"boxes" => $boxes_to_shipo,
							"max_weight" => $general_settings['a2z_dhlexpress_max_weight'],
							"plt" => ($general_settings['a2z_dhlexpress_ppt'] == 'yes') ? "Y" : "N",
							"airway_bill" => ($general_settings['a2z_dhlexpress_aabill'] == 'yes') ? "Y" : "N",
							"sd" => ($general_settings['a2z_dhlexpress_sat'] == 'yes') ? "Y" : "N",
							"cod" => ($general_settings['a2z_dhlexpress_cod'] == 'yes') ? "Y" : "N",
							"service_code" => $custom_settings[$create_shipment_for]['service_code'],
							"shipment_content" => $ship_content,
							"email_alert" => ( isset($general_settings['a2z_dhlexpress_email_alert']) && ($general_settings['a2z_dhlexpress_email_alert'] == 'yes') ) ? "Y" : "N",
							"s_company" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_company'],
							"s_address1" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_address1'],
							"s_address2" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_address2'],
							"s_city" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_city'],
							"s_state" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_state'],
							"s_postal" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_zip'],
							"s_country" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_country'],
							"gstin" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_gstin'],
							"s_name" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_shipper_name'],
							"s_phone" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_mob_num'],
							"s_email" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_email'],
							"label_size" => $general_settings['a2z_dhlexpress_print_size'],
							"sent_email_to" => $custom_settings[$create_shipment_for]['a2z_dhlexpress_label_email'],
							"pic_exec_type" => $pickup_mode,
			                "pic_loc_type" => (isset($general_settings['a2z_dhlexpress_pickup_loc_type']) ? $general_settings['a2z_dhlexpress_pickup_loc_type'] : ''),
				            "pic_pac_loc" => (isset($general_settings['a2z_dhlexpress_pickup_pac_loc']) ? $general_settings['a2z_dhlexpress_pickup_pac_loc'] : ''),
				            "pic_contact_per" => (isset($general_settings['a2z_dhlexpress_pickup_per_name']) ? $general_settings['a2z_dhlexpress_pickup_per_name'] : ''),
				            "pic_contact_no" => (isset($general_settings['a2z_dhlexpress_pickup_per_contact_no']) ? $general_settings['a2z_dhlexpress_pickup_per_contact_no'] : ''),
				            "pic_door_to" => (isset($general_settings['a2z_dhlexpress_pickup_door_to']) ? $general_settings['a2z_dhlexpress_pickup_door_to'] : ''),
				            "pic_type" => (isset($general_settings['a2z_dhlexpress_pickup_type']) ? $general_settings['a2z_dhlexpress_pickup_type'] : ''),
				            "pic_days_after" => (isset($general_settings['a2z_dhlexpress_pickup_date']) ? $general_settings['a2z_dhlexpress_pickup_date'] : ''),
				            "pic_open_time" => (isset($general_settings['a2z_dhlexpress_pickup_open_time']) ? $general_settings['a2z_dhlexpress_pickup_open_time'] : ''),
				            "pic_close_time" => (isset($general_settings['a2z_dhlexpress_pickup_close_time']) ? $general_settings['a2z_dhlexpress_pickup_close_time'] : ''),
				            "pic_mail_date" => date('c'),
		    				"pic_date" => date("Y-m-d"),
		    				"payment_con" => (isset($general_settings['a2z_dhlexpress_pay_con']) ? $general_settings['a2z_dhlexpress_pay_con'] : 'S'),
							"cus_payment_con" => (isset($general_settings['a2z_dhlexpress_cus_pay_con']) ? $general_settings['a2z_dhlexpress_cus_pay_con'] : ''),
							"translation" => ( (isset($general_settings['a2z_dhlexpress_translation']) && $general_settings['a2z_dhlexpress_translation'] == "yes" ) ? 'Y' : 'N'),
							"translation_key" => (isset($general_settings['a2z_dhlexpress_translation_key']) ? $general_settings['a2z_dhlexpress_translation_key'] : ''),
							"commodity_code" => $c_codes,
						);
							// echo '<pre>';print_r(json_encode($data));die();
							$curl = curl_init();
							curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode( $data));
							curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
							curl_setopt_array($curl, array(
								CURLOPT_URL            => "https://app.hitshipo.com/api/dhl_manual.php",
								// CURLOPT_URL            => "localhost/hitshipo/api/dhl_manual.php",		//For Manual Label
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING       => "",
								CURLOPT_MAXREDIRS      => 10,
								CURLOPT_HEADER         => false,
								CURLOPT_TIMEOUT        => 60,
								CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST  => 'POST',
							));

							$output = json_decode(curl_exec($curl),true);
							// echo '<pre>';print_r($output);die();
							if($output){
								if(isset($output['status']) || isset($output['pickup_status'])){

									if(isset($output['status']) && is_array($output['status']) && $output['status'][0] != 'success'){
										   update_option('hit_dhl_status_'.$order_id, $output['status'][0]);

									}else if(isset($output['status']) && $output['status'] == 'success'){
										$output['user_id'] = $create_shipment_for;
										$result_arr = json_decode(get_option('hit_dhl_values_'.$order_id, array()));
										$result_arr[] = $output;

										update_option('hit_dhl_values_'.$order_id, json_encode($result_arr));
										
									}
									if (isset($output['pickup_status']) && $output['pickup_status'] != 'Success') {
										$pic_res['status'] = "failed";
										update_option('hit_dhl_pickup_values_'.$order_id, json_encode($pic_res));
									}elseif (isset($output['pickup_status']) && $output['pickup_status'] == 'Success') {
										$pic_res['confirm_no'] = $output['pickup_confirm_no'];
										$pic_res['ready_time'] = $output['pickup_ready_time'];
										$pic_res['pickup_date'] = $output['pickup_date'];
										$pic_res['status'] = "success";

										update_option('hit_dhl_pickup_values_'.$order_id, json_encode($pic_res));
									}
								}else{
									update_option('hit_dhl_status_'.$order_id, 'Site not Connected with HITShipo. Contact HITShipo Team.');
																		}
							}else{
								update_option('hit_dhl_status_'.$order_id, 'Site not Connected with HITShipo. Contact HITShipo Team.');
							}
						}	
			       }
		        }
		    }

		    // Save the data of the Meta field
			public function hit_create_dhl_return_shipping( $order_id ) {
				
		    	$post = get_post($order_id);
		    	if($post->post_type !='shop_order' ){
		    		return;
		    	}
		    	
		    	if (  isset( $_POST[ 'hit_dhl_reset' ] ) ) {
		    		delete_option('hit_dhl_return_values_'.$order_id);
		    	}

		    	if (  isset( $_POST['hit_dhl_create_return_label']) && isset( $_POST[ 'hit_dhl_express_return_service_code' ] ) ) {
		           $service_code = $_POST['hit_dhl_express_return_service_code'];
		           $ship_content = 'Return Shipment';
		           $enabled_products = isset($_POST['return_products']) ? $_POST['return_products'] : array();
		           $qty_products = isset($_POST['qty_products']) ? $_POST['qty_products'] : array();
		           $order = wc_get_order( $order_id );
			       if($order && !empty($enabled_products)){

			       		$order_data = $order->get_data();
			       		$order_id = $order_data['id'];
			       		$order_currency = $order_data['currency'];

			       		$order_shipping_first_name = $order_data['shipping']['first_name'];
						$order_shipping_last_name = $order_data['shipping']['last_name'];
						$order_shipping_company = empty($order_data['shipping']['company']) ? $order_data['shipping']['first_name'] :  $order_data['shipping']['company'];
						$order_shipping_address_1 = $order_data['shipping']['address_1'];
						$order_shipping_address_2 = $order_data['shipping']['address_2'];
						$order_shipping_city = $order_data['shipping']['city'];
						$order_shipping_state = $order_data['shipping']['state'];
						$order_shipping_postcode = $order_data['shipping']['postcode'];
						$order_shipping_country = $order_data['shipping']['country'];
						$order_shipping_phone = $order_data['billing']['phone'];
						$order_shipping_email = $order_data['billing']['email'];

						$items = $order->get_items();
						$pack_products = array();
						
						foreach ( $items as $item ) {
							$product_data = $item->get_data();
						    $product = array();
						    $product['product_name'] = str_replace('"', '', $product_data['name']);
						    $product['product_quantity'] = $product_data['quantity'];
						    $product['product_id'] = $product_data['product_id'];

						    $product_variation_id = $item->get_variation_id();
						    $product_id = $product_data['product_id'];
						    if(empty($product_variation_id) || $product_variation_id == 0){
						    	$getproduct = wc_get_product( $product_data['product_id'] );
						    }else{
						    	$getproduct = wc_get_product( $product_variation_id );
						    	$product_id = $product_variation_id;
						    }
						    
						    if(!in_array($product_id, $enabled_products)){
						    	continue;
						    }else{
						    	if($qty_products[$product_id] == 0){
						    		continue;
						    	}else{
						    		$product['product_quantity'] = $qty_products[$product_id];

						    	}
						    }
						    $woo_weight_unit = get_option('woocommerce_weight_unit');
							$woo_dimension_unit = get_option('woocommerce_dimension_unit');

							$dhl_mod_weight_unit = $dhl_mod_dim_unit = '';

							if(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'KG_CM')
							{
								$dhl_mod_weight_unit = 'kg';
								$dhl_mod_dim_unit = 'cm';
							}elseif(!empty($general_settings['a2z_dhlexpress_weight_unit']) && $general_settings['a2z_dhlexpress_weight_unit'] == 'LB_IN')
							{
								$dhl_mod_weight_unit = 'lbs';
								$dhl_mod_dim_unit = 'in';
							}
							else
							{
								$dhl_mod_weight_unit = 'kg';
								$dhl_mod_dim_unit = 'cm';
							}

						    $product['price'] = $getproduct->get_price();

						    if(!$product['price']){
								$product['price'] = (isset($product_data['total']) && isset($product_data['quantity'])) ? number_format(($product_data['total'] / $product_data['quantity']), 2) : 0;
							}

						    if ($woo_dimension_unit != $dhl_mod_dim_unit) {
					    	$prod_width = $getproduct->get_width();
					    	$prod_height = $getproduct->get_height();
					    	$prod_depth = $getproduct->get_length();

					    	//wc_get_dimension( $dimension, $to_unit, $from_unit );
					    	$product['width'] = round(wc_get_dimension( $prod_width, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);
					    	$product['height'] = round(wc_get_dimension( $prod_height, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);
							$product['depth'] = round(wc_get_dimension( $prod_depth, $dhl_mod_dim_unit, $woo_dimension_unit ), 2);

						    }else {
						    	$product['width'] = $getproduct->get_width();
						    	$product['height'] = $getproduct->get_height();
						    	$product['depth'] = $getproduct->get_length();
						    }
						    
						    if ($woo_weight_unit != $dhl_mod_weight_unit) {
						    	$prod_weight = $getproduct->get_weight();
						    	$product['weight'] = round(wc_get_weight( $prod_weight, $dhl_mod_weight_unit, $woo_weight_unit ), 2);
						    }else{
						    	$product['weight'] = $getproduct->get_weight();
							}

						    $pack_products[] = $product;
						    
						}

						$general_settings = get_option('a2z_dhl_main_settings',array());
						
						if(!empty($general_settings) && isset($general_settings['a2z_dhlexpress_integration_key']) && isset($general_settings['a2z_dhlexpress_import_no'])){
							$mode = 'live';
							if(isset($general_settings['a2z_dhlexpress_test']) && $general_settings['a2z_dhlexpress_test']== 'yes'){
								$mode = 'test';
							}

							$execution = 'manual';
							
							$boxes_to_shipo = array();
							if (isset($general_settings['a2z_dhlexpress_packing_type']) && $general_settings['a2z_dhlexpress_packing_type'] == "box") {
								if (isset($general_settings['a2z_dhlexpress_boxes']) && !empty($general_settings['a2z_dhlexpress_boxes'])) {
									foreach ($general_settings['a2z_dhlexpress_boxes'] as $box) {
										if ($box['enabled'] != 1) {
											continue;
										}else {
											$boxes_to_shipo[] = $box;
										}
									}
								}
							}

							$c_codes = [];
							foreach($pack_products as $prod_to_shipo){
								$saved_cc = get_post_meta( $prod_to_shipo['product_id'], 'hits_dhl_cc', true);
								if(!empty($saved_cc)){
									$c_codes[] = $saved_cc;
								}
							}
							
							$data = array();
							$data['integrated_key'] = $general_settings['a2z_dhlexpress_integration_key'];
							$data['order_id'] = 'R_'.$order_id;
							$data['exec_type'] = $execution;
							$data['mode'] = $mode;
							$data['meta'] = array(
								"site_id" => $general_settings['a2z_dhlexpress_site_id'],
								"password"  => $general_settings['a2z_dhlexpress_site_pwd'],
								"accountnum" => $general_settings['a2z_dhlexpress_import_no'],
								"s_company" => $order_shipping_company,
								"s_address1" => $order_shipping_address_1,
								"s_address2" => $order_shipping_address_2,
								"s_city" => $order_shipping_city,
								"s_state" => $order_shipping_state,
								"s_postal" => $order_shipping_postcode,
								"s_country" => $order_shipping_country,
								"s_name" => $order_shipping_first_name . ' '. $order_shipping_last_name,
								"s_phone" => $order_shipping_phone,
								"s_email" => $order_shipping_email,
								"dutiable" => $general_settings['a2z_dhlexpress_duty_payment'],
								"insurance" => "no",
								"pack_this" => "Y",
								"products" => $pack_products,
								"pack_algorithm" => $general_settings['a2z_dhlexpress_packing_type'],
								"boxes" => $boxes_to_shipo,
								"max_weight" => $general_settings['a2z_dhlexpress_max_weight'],
								"plt" => (isset($general_settings['a2z_dhlexpress_ppt']) && $general_settings['a2z_dhlexpress_ppt'] == 'yes') ? "Y" : "N",
								"airway_bill" => ($general_settings['a2z_dhlexpress_aabill'] == 'yes') ? "Y" : "N",
								"sd" => "N",
								"cod" => "N",
								"service_code" => $service_code,
								"shipment_content" => $ship_content,
								"email_alert" => ( isset($general_settings['a2z_dhlexpress_email_alert']) && ($general_settings['a2z_dhlexpress_email_alert'] == 'yes') ) ? "Y" : "N",
								"t_company" => $general_settings['a2z_dhlexpress_company'],
								"t_address1" => str_replace('"', '', $general_settings['a2z_dhlexpress_address1']),
								"t_address2" => str_replace('"', '', $general_settings['a2z_dhlexpress_address2']),
								"t_city" => $general_settings['a2z_dhlexpress_city'],
								"t_state" => $general_settings['a2z_dhlexpress_state'],
								"t_postal" => $general_settings['a2z_dhlexpress_zip'],
								"t_country" => $general_settings['a2z_dhlexpress_country'],
								"gstin" => $general_settings['a2z_dhlexpress_gstin'],
								"t_name" => $general_settings['a2z_dhlexpress_shipper_name'],
								"t_phone" => $general_settings['a2z_dhlexpress_mob_num'],
								"t_email" => $general_settings['a2z_dhlexpress_email'],
								"label_size" => $general_settings['a2z_dhlexpress_print_size'],
								"sent_email_to" => $general_settings['a2z_dhlexpress_label_email'],
								"pic_exec_type" => 'manual',
				                "pic_loc_type" => (isset($general_settings['a2z_dhlexpress_pickup_loc_type']) ? $general_settings['a2z_dhlexpress_pickup_loc_type'] : ''),
					            "pic_pac_loc" => (isset($general_settings['a2z_dhlexpress_pickup_pac_loc']) ? $general_settings['a2z_dhlexpress_pickup_pac_loc'] : ''),
					            "pic_contact_per" => (isset($general_settings['a2z_dhlexpress_pickup_per_name']) ? $general_settings['a2z_dhlexpress_pickup_per_name'] : ''),
					            "pic_contact_no" => (isset($general_settings['a2z_dhlexpress_pickup_per_contact_no']) ? $general_settings['a2z_dhlexpress_pickup_per_contact_no'] : ''),
					            "pic_door_to" => (isset($general_settings['a2z_dhlexpress_pickup_door_to']) ? $general_settings['a2z_dhlexpress_pickup_door_to'] : ''),
					            "pic_type" => (isset($general_settings['a2z_dhlexpress_pickup_type']) ? $general_settings['a2z_dhlexpress_pickup_type'] : ''),
					            "pic_days_after" => (isset($general_settings['a2z_dhlexpress_pickup_date']) ? $general_settings['a2z_dhlexpress_pickup_date'] : ''),
					            "pic_open_time" => (isset($general_settings['a2z_dhlexpress_pickup_open_time']) ? $general_settings['a2z_dhlexpress_pickup_open_time'] : ''),
					            "pic_close_time" => (isset($general_settings['a2z_dhlexpress_pickup_close_time']) ? $general_settings['a2z_dhlexpress_pickup_close_time'] : ''),
					            "pic_mail_date" => date('c'),
			    				"pic_date" => date("Y-m-d"),
			    				"payment_con" => (isset($general_settings['a2z_dhlexpress_pay_con']) ? $general_settings['a2z_dhlexpress_pay_con'] : 'S'),
								"cus_payment_con" => (isset($general_settings['a2z_dhlexpress_cus_pay_con']) ? $general_settings['a2z_dhlexpress_cus_pay_con'] : ''),
								"translation" => ( (isset($general_settings['a2z_dhlexpress_translation']) && $general_settings['a2z_dhlexpress_translation'] == "yes" ) ? 'Y' : 'N'),
								"translation_key" => (isset($general_settings['a2z_dhlexpress_translation_key']) ? $general_settings['a2z_dhlexpress_translation_key'] : ''),
								"commodity_code" => $c_codes,
							);
								//echo "<pre>";print_r($data);die();
								$curl = curl_init();
								curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode( $data));
								curl_setopt_array($curl, array(
									CURLOPT_URL            => "https://app.hitshipo.com/api/dhl_manual.php",
									// CURLOPT_URL            => "localhost/hitshipo/api/dhl_manual.php",		// For Return Label
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING       => "",
									CURLOPT_MAXREDIRS      => 10,
									CURLOPT_HEADER         => false,
									CURLOPT_TIMEOUT        => 60,
									CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST  => 'POST',
								));

								$output = json_decode(curl_exec($curl),true);
								
								if($output){
									if(isset($output['status']) || isset($output['pickup_status'])){

										if(isset($output['status']) && is_array($output['status']) && $output['status'][0] != 'success'){
											   update_option('hit_dhl_return_status_'.$order_id, $output['status'][0]);

										}else if(isset($output['status']) && $output['status'] == 'success'){

											update_option('hit_dhl_return_values_'.$order_id, json_encode($output));
											if (!empty($output['tracking_num'])) {
												$track = $output['tracking_num'];
												update_post_meta($order_id, apply_filters('a2z_rtracking_id_meta_name', 'a2z_rtracking_num'), $track);
											}
										}
									}else{
										update_option('hit_dhl_return_status_'.$order_id, 'Site not Connected with HITShipo. Contact HITShipo Team.');
																			}
								}else{
									update_option('hit_dhl_return_status_'.$order_id, 'Site not Connected with HITShipo. Contact HITShipo Team.');
								}
						}	
			       }else{
			       		update_option('hit_dhl_return_status_'.$order_id, 'Enable atleast one product to create Return label.');
			       }
		        }
		    }
		}
		
	}
	$a2z_dhlexpress = new a2z_dhlexpress_parent();
}
