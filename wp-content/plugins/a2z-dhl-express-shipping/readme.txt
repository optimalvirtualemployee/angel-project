=== Automated DHL Express Shipping ===
Contributors: a2zplugins
Tags: DHL, DHL Express, automated,DHL Express shipping, DHL Woocommerce
Requires at least: 4.0.1
Tested up to: 5.5.3
Requires PHP: 5.6
Stable tag: 2.10.7
License: GPLv3 or later License
URI: http://www.gnu.org/licenses/gpl-3.0.html

(Fully automated) Real-time rates, shipping label, return label, pickup, invoice, multi vendor,etc. supports all countries. 

== Description ==

[DHL Express shipping](https://wordpress.org/plugins/a2z-dhl-express-shipping/) plugin, integrate the [DHL Express](https://www.dhl.com/en.html) for express delivery in Domestic and Internationally. According to the destination, We are providing all kind of DHL Services. It supports all Countries.

Annoyed of clicking button to create shipping label and generating it here is a hassle free solution, [HITStacks](https://hitstacks.com/hitshipo.php) is the tool with fully automated will reduce your cost and will save your time. 

Further, We will track your shipments and will update the order status automatically.

= FRONT OFFICE (CHECKOUT PAGE): =

To fetch real-time rates on the checkout page, we will send product information and location to DHL.

We are providing the following domestic & international shipping carriers of DHL:
 * Domestic Express
 * Worldwide Express
 * DHL Express
 * DHL Economy

and more 14 Services.

You can get the DHL provided discounts.
By using hooks and filters you can make currency conversions, product skipping package, definition customization and supports the insurance.

= BACK OFFICE (SHIPPING ): =

[DHL Express shipping](https://wordpress.org/plugins/a2z-dhl-express-shipping/) plugin is deeply integrated with [HITStacks](https://hitstacks.com/hitshipo.php). So the shipping labels will be generated automatically. You can get the shipping label through email or from the order page.

 This plugin also supported the manual shipments option. By using this you can create the shipments directly from the order page. [HITShipo](https://hitstacks.com/hitshipo.php) will keep track of the orders and update the order state to complete.

= Useful filters =

1) Filter to restric the country

> add_filter("a2z_dhlexpress_rate_packages","a2z_dhlexpress_rate_packages_func",10,1);
> function a2z_dhlexpress_rate_packages_func($package){
> 	$return_package = array();
> 	if($package['destination']['country'] == 'RU'){
>		return $return_package;
>	}
>	return $package;
> }

2) Flat Rate based on order total for services

> function hitstacks_dhlexpress_rate_cost_fnc($rate_cost, $rate_code, $order_total){
>	if($order_total > 250){
>		return 0;
>	}
>	return 20; // Return currency is must to be a DHL confiured currency.
> }
> add_filter("hitstacks_dhlexpress_rate_cost", "hitstacks_dhlexpress_rate_cost_fnc", 10,3);

3) Change estimated delivery date format or text.

> add_filter('hitstacks_dhlexpres_delivery_date', 'hit_dhl_est_delivery_format',10,3);
> function hit_dhl_est_delivery_format($string, $est_date, $est_time){
> 	return $string;
> }

= Your customer will appreciate : =

* The Product is delivered very quickly. The reason is, there this no delay between the order and shipping label action.
* Access to the many services of DHL for domestic & international shipping.
* Good impression of the shop.


= HITStacks Action Sample =
[youtube https://www.youtube.com/watch?v=TZei_H5NkyU]


= Informations for Configure plugin =

> If you have already a DHL Express Account, please contact your DHL account manager to get your credentials.
> If you are not registered yet, please contact our customer service.
> Functions of the module are available only after receiving your API’s credentials.
> Please note also that phone number registration for the customer on the address webform should be mandatory.
> Create account in hitstacks.

Plugin Tags: <blockquote>DHL, DHL Express, dhlexpress,DHL Express shipping, DHL Woocommerce, dhl express for woocommerce, official dhl express, dhl express plugin, dhl plugin, create shipment, dhl shipping, dhl shipping rates</blockquote>


= About DHL =

DHL Express is a division of the German logistics company Deutsche Post DHL providing international courier, parcel, and express mail services. Deutsche Post DHL is the world's largest logistics company operating around the world, particularly in sea and air mail

= About HITStacks =

We are Web Development Company. We are planning for make everything automated. 

= What HITStacks Tell to Customers? =

> "Configure & take rest"

== Screenshots ==
1. Configuration - DHL Details.
2. Configuration - DHL Shipper Address.
3. Configuration - DHL Rate Section.
4. Configuration - DHL Available Services.
5. Output - DHL Shipping Rates in Shop.
6. Output - My Account Page Shipping Section.
7. Output - Edit Order Page Shipping Section.
8. Configuration - Packing algorithm Settings.
9. Configuration - Shipping label Settings.
10. Tracking - Customer side auto tracking UI.
11. Integration - HITShipo integration plans.
12. Why HIT Shipo?.


== Changelog ==

= 2.10.7 =
*Release Date - 15 December 2020*
	> Minor fixes.

= 2.10.6 =
*Release Date - 26 November 2020*
	> Added option to choose dhl email alert.

= 2.10.5 =
*Release Date - 24 November 2020*
	> Minor Fixes.

= 2.10.4 =
*Release Date - 11 November 2020*
	> Fixed product price of grouped products not sending to shipo for some customers.

= 2.10.3 =
*Release Date - 11 November 2020*
	> Fixed sending weights wrongly.

= 2.10.2 =
*Release Date - 10 November 2020*
	> Minor Fixes.

= 2.10.1 =
*Release Date - 07 November 2020*
	> Added option to add commodity code in products.

= 2.10.0 =
*Release Date - 07 November 2020*
	> Introduced Bulk Shipment label generation.

= 2.9.4 =
*Release Date - 05 November 2020*
	> Added field to save commodity code.

= 2.9.3 =
*Release Date - 28 October 2020*
	> Minor Bug Fixes.

= 2.9.2 =
*Release Date - 22 October 2020*
	> Minor Bug Fixes.

= 2.9.1 =
*Release Date - 17 October 2020*
	> Minor Bug Fixes.

= 2.9.0 =
*Release Date - 17 October 2020*
	> Country Exclude Rate Section.

= 2.8.6 =
*Release Date - 25 September 2020*
	> Added option address translation .

= 2.8.5 =
*Release Date - 16 September 2020*
	> Added option to choose payment country.

= 2.8.4 =
*Release Date - 27 August 2020*
	> Duitable value N for SE to NL .

= 2.8.3 =
*Release Date - 26 August 2020*
	> Duitable value N for CZ to AT .

= 2.8.2 =
*Release Date - 22 August 2020*
	> Fixed declared value not sending for some users.

= 2.8.1 =
*Release Date - 21 August 2020*
	> Some minor bugs fixed.

= 2.8.0 =
*Release Date - 05 August 2020*
	> Auto currency conversion introduced.
	> Only enabled boxes will be send to Shipo.
	> Some minor bugs fixed.

= 2.7.3 =
*Release Date - 1 Aug 2020*
	> Some minor bugs fixed.
	
= 2.7.2 =
*Release Date - 28 July 2020*
	> Some minor bugs fixed.

= 2.7.1 =
*Release Date - 28 July 2020*
	> Some minor bugs fixed.

= 2.7.0 =
*Release Date - 23 July 2020*
	> Box packing introduced.

= 2.6.6 =
*Release Date - 17 July 2020*
	> Auto label hook changed and some other minor bugs fixed.

= 2.6.5 =
*Release Date - 08 July 2020*
	> Fixed insured amount not sending.

= 2.6.4 =
*Release Date - 24 June 2020*
	> Fixed some minor bugs.

= 2.6.3 =
*Release Date - 09 June 2020*
	> Fixed Checkout issues with displaying estimated delivery dates.

= 2.6.2 =
*Release Date - 02 June 2020*
	> added setting for displaying estimated delivery dates in checkout.
	
= 2.6.0 =
*Release Date - 02 June 2020*
	> Changed currency to GH country.

= 2.6.0 =
*Release Date - 02 June 2020*
	> Added tracking in front office.

= 2.5.5 =
*Release Date - 29 May 2020*
	> Some minor bugs fixed.

= 2.5.4 =
*Release Date - 28 May 2020*
	> Product with default user account fixed.

= 2.5.3 =
*Release Date - 28 May 2020*
	> Some minor bugs fixed.

= 2.5.1, 2.5.2 =
*Release Date - 26 May 2020*
	> Default shipper address will be used while not having address for vendors.
	> Some minor bugs fixed.

= 2.5.0 =
*Release Date - 1 May 2020*
	> Multi Vendor Shipments generated.

= 2.4.1, 2.4.2, 2.4.3 =
*Release Date - 28 April 2020*
	> Missing language domain added.

= 2.4.0 =
*Release Date - 28 April 2020*
	> Return Shipment Label option implemented.
	> Missing language domain added.

= 2.3.7 =
*Release Date - 18 April 2020*
	> Shipping Price get option implemented for audit.

= 2.3.6 =
*Release Date - 16 April 2020*
	> Minor bugs fixed.

= 2.3.5 =
*Release Date - 26 March 2020*
	> Restric country hook added.

= 2.3.4 =
*Release Date - 25 March 2020*
	> Minor bugs fixed with currency code.


= 2.3.3 =
*Release Date - 20 March 2020*
	> New filter added for shipping price.

= 2.3.2 =
*Release Date - 19 March 2020*
	> Minor bugs fixed.

= 2.3.1 =
*Release Date - 11 March 2020*
	> Minor bugs fixed.

= 2.3.0 =
*Release Date - 22 Feb 2020*
	> Pickup Option Introduced.

= 2.2.3 =
*Release Date - 05 Feb 2020*
	> Minor Bug fix.

= 2.2.2 =
*Release Date - 05 Feb 2020*
	> New server compatible check.

= 2.2.1 =
*Release Date - 30 Jan 2020*
	> Invoice wrong character fix.

= 2.2.0 =
*Release Date - 28 Jan 2020*
	> GSTN option included.

= 2.1.0 =
*Release Date - 26 Jan 2020*
	> Included price adjustment.
	> Included Conversion rate.
	> Included KG_LBS choose option.

= 2.0.0 =
*Release Date - 11 November 2019*
	> Releasing Shipping Lable option.

= 1.1.0 =
*Release Date - 09 November 2018*
	> Bug Fix on Configuration.
	
= 1.0.0 =
*Release Date - 06 November 2018*
	> Initial Version
