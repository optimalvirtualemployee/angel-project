(function ($) {
	'use strict';

	function cancelRequestForm() {
		var parent = this;
		this.init = function () {
			this.clickDetect();
		}

		this.clickDetect = function () {
			jQuery(document).on('click', '.pi_cancel_request_form', parent.getCancelRequestForm);
		}

		this.getCancelRequestForm = function (event) {
			event.preventDefault();
			var caller_button = jQuery(this);
			var url = caller_button.attr('href');
			$.magnificPopup.open({
				items: {
					src: url,
					type: "ajax",
					showCloseBtn: true,
					closeOnContentClick: false,
					closeOnBgClick: false,
					mainClass: 'mfp-fade',
					removalDelay: 300
				}
			});
		}



	}

	var cancelRequestFormObj = new cancelRequestForm();
	cancelRequestFormObj.init();

})(jQuery);
