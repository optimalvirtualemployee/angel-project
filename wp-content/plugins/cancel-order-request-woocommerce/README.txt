=== Cancel order request for WooCommerce ===
Contributors: rajeshsingh520
Donate link: piwebsolution.com
Tags: order cancellation, cancel request, cancel order, woocommerce cancel order
Requires at least: 3.0.1
Tested up to: 5.6
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Order cancellation request option to customer for WooCommerce

== Description ==

Replace order cancel button with order cancellation request button, here is what this plugin can do for you

&#9989; You can enable order cancellation request button based on the **order status**
&#9989; If you want to **replace the order cancellation button** with this order cancellation request button then activate this button on order with status pending and failed
&#9989; User can **add a reason** why they want to cancel the order
&#9989; **Admin will receive a email** with the order cancellation request and the reason for cancellation
&#9989; **Display custom note to your customers** when they try to send cancellation request.
&#9989; Admin can either decide to **cancel the order** or move it **back to processing state**
&#9989; If Admin mark the order as cancel the **user will be send a email** stating there order cancellation request was accepted 
&#9989; If Admin moves the order status as processing or complete then the user will get an **email stating there cancellation request was denied**
&#9989; **Hide cancellation request button** after a certain period of time.
&#9989; Give **list of cancellation reason** for user to select from
&#9989; View order **detail link added in the email send to the customer**, you have the option to add this link for Registered customer or Guest customer or both
&#9989; **Guest customer can request order cancellation**, from the link given in the order detail page (Thank your page)

== Frequently Asked Questions ==

= Can I still have order cancellation button =
Yes, you can have order cancellation button, Order cancellation button is shown for the order with status Failed, On-hold, so you make the request button to show for other order status

= Can I show Cancel request button for Processing order =
Yes, just select the processing status in the plugin setting and it will show for the order with status processing

= How to allow customer to send reason for the order cancellation along with the cancellation request =
Yes this option is available in the plugin by default, when customer will click on the cancel order button he will be asked to enter a reason for order cancellation.

= Where can I see the order cancellation reason =
Admin will receive the order cancellation reason in the email, and admin can see the reason in the order page as well

= How admin will know above the order cancellation request =
admin will receive a email and he can see the same in his dashboard

= How to decline the order cancellation request =
By change the status of the order from cancel request to processing or complete 

= How to accept the order cancellation request =
By changing the order status to Cancel state we can mark the order as cancel

= How customer will know his cancellation request was decline =
when you will change the order status to complete or processing, then user will receive a email stating his order cancellation request was decline

= How customer will know if his order cancellation request was accepted =
He will receive a email stating his cancellation request for the order xyz was accepted

= I want to change the wordings of the email =
You can do that using the translation file, all the text are translation supported, we have added the language pot file 

= I want to show custom note to the customer when they want to cancel order =
Yes you can specify special message, that will be displayed to customer when they click cancel order button 

= I want to hide the order cancellation request button after certain period of time =
Yes you can do that, you can set the time in terms of minutes after that period of time after placing the order the order cancellation option will go away

= I want to give a list of cancellation reasons to customer to select from =
Yes you can do that, just add multiple reason with each reason in single line and it will be shown in the cancellation request form to the customer

= There is Click to view order details link in the email send to the customer =
this link is added by the plugin so customer can easily view there order detail outside the email 

= Can Guest customer send a order cancellation request =
Yes, they can send cancellation request too.

= From where does the guest customer send cancellation request =
They have the cancellation request link on the thank you page, the link for the order detail page is also added in the email send to guest customer so they can go to this thank you page and access this cancellation request button