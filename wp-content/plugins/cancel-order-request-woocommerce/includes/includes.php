<?php

require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/pisol.class.form.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-emails.php';

require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/menu.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/basic.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/order-status.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/order.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/plugins.php';


require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/cancel-request.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/detect-order-status-change.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/order-detail-link-email.php';