<?php

/**
 * Fired during plugin activation
 *
 * @link       piwebsolution.com
 * @since      1.0.0
 *
 * @package    Cancel_Order_Request_Woocommerce
 * @subpackage Cancel_Order_Request_Woocommerce/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cancel_Order_Request_Woocommerce
 * @subpackage Cancel_Order_Request_Woocommerce/includes
 * @author     PI Websolution <sales@piwebsolution.com>
 */
class Cancel_Order_Request_Woocommerce_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
