#, fuzzy
msgid ""
msgstr ""
"Language: \n"
"POT-Creation-Date: 2020-11-21 06:23+0000\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: Loco https://localise.biz/"

#: cancel-order-request-woocommerce.php:40
msgid ""
"Please Install and Activate WooCommerce plugin, without that this plugin "
"can't work"
msgstr ""

#: cancel-order-request-woocommerce.php:94
msgid "Settings"
msgstr ""

#: cancel-order-request-woocommerce.php:95
msgid "Send suggestions to improve"
msgstr ""

#: admin/basic.php:27
msgid "Show cancel order request button on order with this status"
msgstr ""

#: admin/basic.php:27
msgid "Cancel order button will be allowed for order with this status"
msgstr ""

#: admin/basic.php:29
msgid "Admin message to show above the cancel order request box"
msgstr ""

#: admin/basic.php:29
msgid "This message will be shown above the cancel box"
msgstr ""

#: admin/basic.php:31
msgid "Predefined reason for cancellation"
msgstr ""

#: admin/basic.php:31
msgid ""
"Add one reason in one line without html e.g: <br>Product not as described<br>"
"\n"
"            Not interested any more<br>\n"
"            Other"
msgstr ""

#: admin/basic.php:35
msgid "Remove cancel button after this time (minutes)"
msgstr ""

#: admin/basic.php:35
msgid ""
"Order cancel request will be only allowed up till this much of time after "
"the order placement time, if this is left blank or set to 0 then button will "
"not be hidden based on time"
msgstr ""

#: admin/basic.php:37
msgid "Add order detail page link in customer email"
msgstr ""

#: admin/basic.php:37
msgid "Registered customer"
msgstr ""

#: admin/basic.php:37
msgid "Guest customer"
msgstr ""

#: admin/basic.php:37
msgid ""
"Order detail page link is added in the customer email so customer can view "
"there order detail on website even guest customer can use this link to view "
"there order details"
msgstr ""

#: admin/menu.php:21 admin/menu.php:22
msgid "Cancel order request"
msgstr ""

#: admin/order-status.php:16 admin/order-status.php:27
#: public/cancel-request.php:27
msgid "Cancel Request"
msgstr ""

#: admin/order-status.php:21
#, php-format
msgid "Cancel Request <span class=\"count\">(%s)</span>"
msgid_plural "Cancel Requests <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: admin/order.php:20
msgid "Selected reason for cancellation: "
msgstr ""

#: admin/order.php:24
msgid "Cancellation reason: "
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:18
msgid "Admin Order Cancellation Request"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:19
msgid ""
"Order cancellation request email sent to the admin when customer send the "
"order cancellation request."
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:20
msgid "Order cancellation request received for order #{order_number_link}"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:21
msgid "Order cancellation request received for order #{order_number}"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:112
#: includes/class-customer-order-cancellation-request-email.php:111
#: includes/class-customer-order-cancelled-rejected.php:113
#: includes/class-customer-order-cancelled.php:113
msgid "Enable/Disable"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:114
#: includes/class-customer-order-cancellation-request-email.php:113
#: includes/class-customer-order-cancelled-rejected.php:115
#: includes/class-customer-order-cancelled.php:115
msgid "Enable this email notification"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:118
msgid "Recipient(s)"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:120
#, php-format
msgid ""
"Enter recipients (comma separated) for this email. Defaults to <code>"
"%s</code>."
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:127
#: includes/class-customer-order-cancellation-request-email.php:117
#: includes/class-customer-order-cancelled-rejected.php:119
#: includes/class-customer-order-cancelled.php:119
msgid "Email type"
msgstr ""

#: includes/class-admin-order-cancellation-request-email.php:129
#: includes/class-customer-order-cancellation-request-email.php:119
#: includes/class-customer-order-cancelled-rejected.php:121
#: includes/class-customer-order-cancelled.php:121
msgid "Choose which format of email to send."
msgstr ""

#: includes/class-customer-order-cancellation-request-email.php:18
msgid "Customer Order Cancellation Request"
msgstr ""

#: includes/class-customer-order-cancellation-request-email.php:19
msgid "Order cancellation request received confirmation send to the customer."
msgstr ""

#: includes/class-customer-order-cancellation-request-email.php:20
msgid ""
"Order cancellation request for order no. #{order_number_link} is submitted "
"successfully"
msgstr ""

#: includes/class-customer-order-cancellation-request-email.php:21
msgid ""
"Order cancellation request for order no. #{order_number} is submitted "
"successfully"
msgstr ""

#: includes/class-customer-order-cancelled-rejected.php:18
msgid "Cancellation request rejected"
msgstr ""

#: includes/class-customer-order-cancelled-rejected.php:19
msgid "Order cancellation request rejected."
msgstr ""

#: includes/class-customer-order-cancelled-rejected.php:20
msgid ""
"Your cancellation request for order no. #{order_number_link} is rejected"
msgstr ""

#: includes/class-customer-order-cancelled-rejected.php:21
msgid "Your cancellation request for order no. #{order_number} is rejected"
msgstr ""

#: includes/class-customer-order-cancelled.php:18
msgid "Cancellation request accepted"
msgstr ""

#: includes/class-customer-order-cancelled.php:19
msgid "Order cancellation request accepted."
msgstr ""

#: includes/class-customer-order-cancelled.php:20
msgid "Your order no. #{order_number_link} cancelled as per your request"
msgstr ""

#: includes/class-customer-order-cancelled.php:21
msgid "Your order no. #{order_number} cancelled as per your request"
msgstr ""

#: public/cancel-request.php:126
msgid "You do not have permissions to request cancellation for this order."
msgstr ""

#: public/cancel-request.php:173
msgid "Want to cancel this order? "
msgstr ""

#: public/cancel-request.php:175
msgid "Click here"
msgstr ""

#: public/cancel-request.php:180
msgid "Your order cancellation request is submitted"
msgstr ""

#: public/order-detail-link-email.php:16 public/order-detail-link-email.php:28
msgid "Click to view order details"
msgstr ""

#: public/partials/cancel-order-request-form.php:3
#, php-format
msgid "Request to cancel order #%d"
msgstr ""

#: public/partials/cancel-order-request-form.php:10
msgid "Reason for order canceling"
msgstr ""

#: public/partials/cancel-order-request-form.php:15
msgid "Send Cancellation Request"
msgstr ""

#. %s: Customer billing full name
#: templates/emails/admin-order-cancel-request.php:27
msgid "Selected reason:"
msgstr ""

#: templates/emails/admin-order-cancel-request.php:30
msgid "Order cancellation reason:"
msgstr ""

#. %s: Customer billing full name
#: templates/emails/customer-order-cancel-request.php:26
#: templates/emails/plain/customer-order-cancel-request.php:25
msgid ""
"Order cancellation request is received and order has been put on hold. Will "
"inform if your cancellation is approved or not"
msgstr ""

#. %s: Customer billing full name
#: templates/emails/order-cancellation-rejected.php:26
#: templates/emails/plain/order-cancellation-rejected.php:25
#, php-format
msgid "Your cancellation request for order no. #%s is rejected"
msgstr ""

#. %s: Customer billing full name
#: templates/emails/order-cancelled.php:26
#: templates/emails/plain/order-cancelled.php:25
#, php-format
msgid "Your order no. #%s cancelled as per your request "
msgstr ""

#. %s: Customer billing full name
#: templates/emails/plain/admin-order-cancel-request.php:27
#, php-format
msgid "Selected reason: %s"
msgstr ""

#: templates/emails/plain/admin-order-cancel-request.php:30
#, php-format
msgid "Order cancellation reason: %s"
msgstr ""

#. Name of the plugin
msgid "Cancel order request for WooCommerce"
msgstr ""

#. Description of the plugin
msgid ""
"Gives option to replace Cancel order button with Cancel order request button,"
" so user can send cancellation request"
msgstr ""

#. URI of the plugin
msgid "piwebsolution.com/cancel-order-request-woocommerce"
msgstr ""

#. Author of the plugin
msgid "PI Websolution"
msgstr ""

#. Author URI of the plugin
msgid "piwebsolution.com"
msgstr ""
