<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://coderockz.com
 * @since      1.0.0
 *
 * @package    Coderockz_Woo_Delivery
 * @subpackage Coderockz_Woo_Delivery/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Coderockz_Woo_Delivery
 * @subpackage Coderockz_Woo_Delivery/public
 * @author     CodeRockz <admin@coderockz.com>
 */
class Coderockz_Woo_Delivery_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	public $helper;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->helper = new Coderockz_Woo_Delivery_Helper();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Coderockz_Woo_Delivery_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Coderockz_Woo_Delivery_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if( is_checkout() && ! ( is_wc_endpoint_url( 'order-pay' ) || is_wc_endpoint_url( 'order-received' )) ) {
			wp_enqueue_style( "flatpickr_css", plugin_dir_url( __FILE__ ) . 'css/flatpickr.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/coderockz-woo-delivery-public.css', array(), $this->version, 'all' );
		}

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Coderockz_Woo_Delivery_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Coderockz_Woo_Delivery_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if( is_checkout() && ! ( is_wc_endpoint_url( 'order-pay' ) || is_wc_endpoint_url( 'order-received' )) ) {
			
			wp_enqueue_script( "flatpickr_js", plugin_dir_url( __FILE__ ) . 'js/flatpickr.min.js', [], $this->version, true );


			$theme_name = esc_html( wp_get_theme()->get( 'Name' ) );

			if(strpos($theme_name,"Flatsome") !== false) {
				wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/coderockz-woo-delivery-public-flatsome.js', array( 'jquery','select2', 'flatpickr_js' ), $this->version, true );
			} else {
				wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/coderockz-woo-delivery-public.js', array( 'jquery','selectWoo', 'flatpickr_js' ), $this->version, true );
			}

		}
		$coderockz_woo_delivery_nonce = wp_create_nonce('coderockz_woo_delivery_nonce');
	        wp_localize_script($this->plugin_name, 'coderockz_woo_delivery_ajax_obj', array(
	            'coderockz_woo_delivery_ajax_url' => admin_url('admin-ajax.php'),
	            'nonce' => $coderockz_woo_delivery_nonce,
	        ));

	}


		// This function adds the delivery time and delivery date fields and it's functionalities
	public function coderockz_woo_delivery_add_custom_field() {
		
		if(get_option('coderockz_woo_delivery_add_delivery_type') == false) {


			$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');
			$enable_delivery_date = (isset($delivery_date_settings['enable_delivery_date']) && !empty($delivery_date_settings['enable_delivery_date'])) ? $delivery_date_settings['enable_delivery_date'] : false;

	    	$timezone = $this->helper->get_the_timezone();
			date_default_timezone_set($timezone);
			$today = date('Y-m-d', time());
			$formated = date('Y-m-d H:i:s', strtotime($today));
			$formated_obj = new DateTime($formated);
			$last_day = $formated_obj->modify("+30 day")->format("Y-m-d");
			$filtered_date = $today .' - '. $last_day;
			$filtered_dates = explode(' - ', $filtered_date);
			$period = new DatePeriod(new DateTime($filtered_dates[0]), new DateInterval('P1D'), new DateTime($filtered_dates[1].' +1 day'));
			$dates = [];
		    foreach ($period as $date) {
		        $dates[] = $date->format("Y-m-d");
		    }
		    foreach ($dates as $date) {

		    	if($enable_delivery_date) {
		    		$args = array(
				        'limit' => -1,
				        'delivery_date' => strtotime(date('Y-m-d', strtotime($date))),
				        'return' => 'ids'
				    );
		    	} else {

		    		$args = array(
				        'limit' => -1,
				        'date_created' => date('Y-m-d', strtotime($date)),
				        'return' => 'ids'
				    );
		    		
		    	}
	    		
		    	
			    $orders_array = wc_get_orders( $args );
			    foreach ($orders_array as $order_id) {
			    	update_post_meta($order_id, 'delivery_type', 'delivery');
			    }

		    }

		    $today = date('Y-m-d', time());
			$formated = date('Y-m-d H:i:s', strtotime($today));
			$formated_obj = new DateTime($formated);
		    $previous_last_day = $formated_obj->modify("-10 day")->format("Y-m-d");
			$previous_filtered_date = $previous_last_day .' - '. $today;
			$previous_filtered_dates = explode(' - ', $previous_filtered_date);
			$previous_period = new DatePeriod(new DateTime($previous_filtered_dates[0]), new DateInterval('P1D'), new DateTime($previous_filtered_dates[1].' +1 day'));

		    $previous_dates = [];
		    foreach ($previous_period as $previous_date) {
		        $previous_dates[] = $previous_date->format("Y-m-d");
		    }

		    foreach ($previous_dates as $previous_date) {
	    		if($enable_delivery_date) {
		    		$previous_args = array(
				        'limit' => -1,
				        'delivery_date' => strtotime(date('Y-m-d', strtotime($previous_date))),
				        'return' => 'ids'
				    );
		    	} else {
	    		$previous_args = array(
			        'limit' => -1,
			        'date_created' => date('Y-m-d', strtotime($previous_date)),
			        'return' => 'ids'
			    );
	    		}
		    	
			    $previous_orders_array = wc_get_orders( $previous_args );


			    foreach ($previous_orders_array as $previous_order_id) {
			    	
			    	update_post_meta($previous_order_id, 'delivery_type', 'delivery');
			    }

		    }

		    update_option('coderockz_woo_delivery_add_delivery_type','completed');

		}


		if(get_option('coderockz_woo_delivery_date_time_change') == false) {
		

		$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');
		$enable_delivery_date = (isset($delivery_date_settings['enable_delivery_date']) && !empty($delivery_date_settings['enable_delivery_date'])) ? $delivery_date_settings['enable_delivery_date'] : false;

    	$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);
		$today = date('Y-m-d', time());
		$formated = date('Y-m-d H:i:s', strtotime($today));
		$formated_obj = new DateTime($formated);
		$last_day = $formated_obj->modify("+30 day")->format("Y-m-d");
		$filtered_date = $today .' - '. $last_day;
		$filtered_dates = explode(' - ', $filtered_date);
		$period = new DatePeriod(new DateTime($filtered_dates[0]), new DateInterval('P1D'), new DateTime($filtered_dates[1].' +1 day'));
		$dates = [];
	    foreach ($period as $date) {
	        $dates[] = $date->format("Y-m-d");
	    }
	    foreach ($dates as $date) {

	    	if($enable_delivery_date) {
	    		$args = array(
			        'limit' => -1,
			        'delivery_date' => strtotime(date('Y-m-d', strtotime($date))),
			        'delivery_type' => "delivery",
			        'return' => 'ids'
			    );
	    	} else {

	    		$args = array(
			        'limit' => -1,
			        'date_created' => date('Y-m-d', strtotime($date)),
			        'delivery_type' => "delivery",
			        'return' => 'ids'
			    );
	    		
	    	}
    		
	    	
		    $orders_array = wc_get_orders( $args );
		    foreach ($orders_array as $order_id) {
		    	if(metadata_exists('post', $order_id, 'delivery_date') && get_post_meta($order_id, 'delivery_date', true) !="") {
		    		if(strpos(get_post_meta($order_id, 'delivery_date', true),'-') !== false) {

		    		} else {
		    			update_post_meta($order_id, 'delivery_date', date('Y-m-d', get_post_meta($order_id, 'delivery_date', true)));
		    		}

		    	}

		    	if(metadata_exists('post', $order_id, 'delivery_time') && get_post_meta($order_id, 'delivery_time', true) !="") {

		    		if(strpos(get_post_meta($order_id, 'delivery_time', true),',') !== false) {
		    			$minutes = explode(',', get_post_meta($order_id, 'delivery_time', true));
		    			$new_time = date("H:i", mktime(0, (int)$minutes[0])) . ' - ' . date("H:i", mktime(0, (int)$minutes[1]));

		    			update_post_meta($order_id, 'delivery_time', $new_time);
		    		}

		    		

		    	}
		    }
	    }

	    $today = date('Y-m-d', time());
		$formated = date('Y-m-d H:i:s', strtotime($today));
		$formated_obj = new DateTime($formated);
	    $previous_last_day = $formated_obj->modify("-10 day")->format("Y-m-d");
		$previous_filtered_date = $previous_last_day .' - '. $today;
		$previous_filtered_dates = explode(' - ', $previous_filtered_date);
		$previous_period = new DatePeriod(new DateTime($previous_filtered_dates[0]), new DateInterval('P1D'), new DateTime($previous_filtered_dates[1].' +1 day'));

	    $previous_dates = [];
	    foreach ($previous_period as $previous_date) {
	        $previous_dates[] = $previous_date->format("Y-m-d");
	    }

	    foreach ($previous_dates as $previous_date) {
    		if($enable_delivery_date) {
	    		$previous_args = array(
			        'limit' => -1,
			        'delivery_date' => strtotime(date('Y-m-d', strtotime($previous_date))),
			        'delivery_type' => "delivery",
			        'return' => 'ids'
			    );
	    	} else {
    		$previous_args = array(
		        'limit' => -1,
		        'date_created' => date('Y-m-d', strtotime($previous_date)),
		        'delivery_type' => "delivery",
		        'return' => 'ids'
		    );
    		}
	    	
		    $previous_orders_array = wc_get_orders( $previous_args );


		    foreach ($previous_orders_array as $previous_order_id) {
		    	if(metadata_exists('post', $previous_order_id, 'delivery_date') && get_post_meta($previous_order_id, 'delivery_date', true) !="") {
		    		if(strpos(get_post_meta($previous_order_id, 'delivery_date', true),'-') !== false) {

		    		} else {
		    			update_post_meta($previous_order_id, 'delivery_date', date('Y-m-d', get_post_meta($previous_order_id, 'delivery_date', true)));
		    		}
		    		

		    	}

		    	if(metadata_exists('post', $previous_order_id, 'delivery_time') && get_post_meta($previous_order_id, 'delivery_time', true) !="") {

		    		if(strpos(get_post_meta($previous_order_id, 'delivery_time', true),',') !== false) {
		    			$pre_minutes = explode(',', get_post_meta($previous_order_id, 'delivery_time', true));
		    			$pre_new_time = date("H:i", mktime(0, (int)$pre_minutes[0])) . ' - ' . date("H:i", mktime(0, (int)$pre_minutes[1]));

		    			update_post_meta($previous_order_id, 'delivery_time', $pre_new_time);
		    		}

		    		

		    	}
		    }
	    }

	    update_option('coderockz_woo_delivery_date_time_change','completed');

		}

		//unset the plugin session & cookie first

		if(isset($_COOKIE['coderockz_woo_delivery_option_time_pickup'])) {
		    unset($_COOKIE['coderockz_woo_delivery_option_time_pickup']);
			setcookie("coderockz_woo_delivery_option_time_pickup", null, -1, '/');
		} elseif(!is_null(WC()->session)) {		  
			WC()->session->__unset( 'coderockz_woo_delivery_option_time_pickup' );  
		}


		// retrieving the data for delivery time
		$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');
		$pickup_date_settings = get_option('coderockz_woo_delivery_pickup_date_settings');
		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');
		$delivery_option_settings = get_option('coderockz_woo_delivery_option_delivery_settings');

		// if any timezone data is saved, set default timezone with the data
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);

		
		// starting the creating of view of delivery date and delivery time
		
		echo "<div data-plugin-url='".CODEROCKZ_WOO_DELIVERY_URL."' id='coderockz_woo_delivery_setting_wrapper'>";

		$disable_fields_for_downloadable_products = (isset(get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products']) && !empty(get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products'])) ? get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products'] : false;

		$has_virtual_downloadable_products = $this->helper->check_virtual_downloadable_products();

		$enable_delivery_option = (isset($delivery_option_settings['enable_option_time_pickup']) && !empty($delivery_option_settings['enable_option_time_pickup'])) ? $delivery_option_settings['enable_option_time_pickup'] : false;
		$delivery_option_field_label = (isset($delivery_option_settings['delivery_option_label']) && !empty($delivery_option_settings['delivery_option_label'])) ? stripslashes($delivery_option_settings['delivery_option_label']) : "Order Type";
		$delivery_field_label = (isset($delivery_option_settings['delivery_label']) && !empty($delivery_option_settings['delivery_label'])) ? stripslashes($delivery_option_settings['delivery_label']) : "Delivery";
		$pickup_field_label = (isset($delivery_option_settings['pickup_label']) && !empty($delivery_option_settings['pickup_label'])) ? stripslashes($delivery_option_settings['pickup_label']) : "Pickup";

		if($enable_delivery_option && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			echo '<div id="coderockz_woo_delivery_delivery_selection_field" style="display:none;">';
				woocommerce_form_field('coderockz_woo_delivery_delivery_selection_box',
				[
					'type' => 'select',
					'class' => [
						'coderockz_woo_delivery_delivery_selection_box form-row-wide'
					],
					'label' => __($delivery_option_field_label, 'coderockz-woo-delivery'),
					'placeholder' => __($delivery_option_field_label, 'coderockz-woo-delivery'),
				    'options' => Coderockz_Woo_Delivery_Delivery_Option::delivery_option($delivery_option_settings),
					'required' => true,
				], WC()->checkout->get_value('coderockz_woo_delivery_delivery_selection_box'));
			echo '</div>';
		}


		$today = date('Y-m-d', time());

		$disable_dates = [];
		$pickup_disable_dates = [];

		$selectable_start_date = date('Y-m-d H:i:s', time());
		$start_date = new DateTime($selectable_start_date);

		$off_days = (isset($delivery_date_settings['off_days']) && !empty($delivery_date_settings['off_days'])) ? $delivery_date_settings['off_days'] : array();

		if(count($off_days)) {
				$date = $start_date;
				foreach ($off_days as $year => $months) {
					foreach($months as $month =>$days){
						$month_num = date_parse($month)['month'];
						if(strlen($month_num) == 1) {
							$month_num_final = "0".$month_num;
						} else {
							$month_num_final = $month_num;
						}
						$days = explode(',', $days);
						foreach($days as $day){
							$disable_dates[] = $year . "-" . $month_num_final . "-" .$day;
							$pickup_disable_dates[] = $year . "-" . $month_num_final . "-" .$day;
						}
					}
				}
			}


		// Delivery Date --------------------------------------------------------------

		$enable_delivery_date = (isset($delivery_date_settings['enable_delivery_date']) && !empty($delivery_date_settings['enable_delivery_date'])) ? $delivery_date_settings['enable_delivery_date'] : false;

		$auto_select_first_date = (isset($delivery_date_settings['auto_select_first_date']) && !empty($delivery_date_settings['auto_select_first_date'])) ? $delivery_date_settings['auto_select_first_date'] : false;


		if( $enable_delivery_date && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {

			$delivery_days = isset($delivery_date_settings['delivery_days']) && $delivery_date_settings['delivery_days'] != "" ? $delivery_date_settings['delivery_days'] : "6,0,1,2,3,4,5";			

			$delivery_date_field_label = (isset($delivery_date_settings['field_label']) && !empty($delivery_date_settings['field_label'])) ? $delivery_date_settings['field_label'] : "Delivery Date";
			$delivery_date_mandatory = (isset($delivery_date_settings['delivery_date_mandatory']) && !empty($delivery_date_settings['delivery_date_mandatory'])) ? $delivery_date_settings['delivery_date_mandatory'] : false;
			$delivery_date_format = (isset($delivery_date_settings['date_format']) && !empty($delivery_date_settings['date_format'])) ? $delivery_date_settings['date_format'] : "F j, Y";
			$week_starts_from = (isset($delivery_date_settings['week_starts_from']) && !empty($delivery_date_settings['week_starts_from'])) ? $delivery_date_settings['week_starts_from']:"0";
			
			$selectable_date = (isset($delivery_date_settings['selectable_date']) && !empty($delivery_date_settings['selectable_date']))?$delivery_date_settings['selectable_date']:"365";

			$delivery_days = explode(',', $delivery_days);

			if(count($delivery_days)) {
				$week_days = ['0', '1', '2', '3', '4', '5', '6'];
				$ignore_days = array_diff($week_days, $delivery_days);

				$ignore_week_days = [];

				foreach ($week_days as $key => $week_day)
				{
					if(in_array($week_day, $ignore_days))
					{
						$ignore_week_days[] = $week_day;
					}
				}

				$disable_week_days = $ignore_week_days;
			}

			$disable_dates = array_unique($disable_dates, false);
			$disable_dates = array_values($disable_dates);

			echo '<div id="coderockz_woo_delivery_delivery_date_section" style="display:none;">';
			woocommerce_form_field('coderockz_woo_delivery_date_field',
			[
				'type' => 'text',
				'class' => array(
				  'coderockz_woo_delivery_date_field form-row-wide'
				) ,
				'id' => "coderockz_woo_delivery_date_datepicker",
				'label' => $delivery_date_field_label,
				'placeholder' => $delivery_date_field_label,
				'required' => $delivery_date_mandatory,
				'custom_attributes' => [
					'data-selectable_dates' => $selectable_date,
					'data-disable_week_days' => json_encode($disable_week_days),
					'data-date_format' => $delivery_date_format,
					'data-disable_dates' => json_encode($disable_dates),
					'data-week_starts_from' => $week_starts_from,
					'data-default_date' => $auto_select_first_date,
				],
			] , WC()->checkout->get_value('coderockz_woo_delivery_date_field'));
			echo '</div>';
		}

		// End Delivery Date


		// Delivery Time --------------------------------------------------------------
		$enable_delivery_time = (isset($delivery_time_settings['enable_delivery_time']) && !empty($delivery_time_settings['enable_delivery_time'])) ? $delivery_time_settings['enable_delivery_time'] : false;

		$delivery_time_field_label = (isset($delivery_time_settings['field_label']) && !empty($delivery_time_settings['field_label'])) ? $delivery_time_settings['field_label'] : "Delivery Time";

		$delivery_time_mandatory = (isset($delivery_time_settings['delivery_time_mandatory']) && !empty($delivery_time_settings['delivery_time_mandatory'])) ? $delivery_time_settings['delivery_time_mandatory'] : false;

		$auto_select_first_time = (isset($delivery_time_settings['auto_select_first_time']) && !empty($delivery_time_settings['auto_select_first_time'])) ? $delivery_time_settings['auto_select_first_time'] : false;

		$order_limit_notice = (isset(get_option('coderockz_woo_delivery_localization_settings')['order_limit_notice']) && !empty(get_option('coderockz_woo_delivery_localization_settings')['order_limit_notice'])) ? "(".get_option('coderockz_woo_delivery_localization_settings')['order_limit_notice'].")" : "(Maximum Order Limit Exceed)";

		if( $enable_delivery_time && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {

			echo '<div id="coderockz_woo_delivery_delivery_time_section" style="display:none;">';
			
			woocommerce_form_field('coderockz_woo_delivery_time_field',
			[
				'type' => 'select',
				'class' => [
					'coderockz_woo_delivery_time_field form-row-wide'
				],
				'label' => $delivery_time_field_label,
				'placeholder' => $delivery_time_field_label,
				'options' => Coderockz_Woo_Delivery_Time_Option::delivery_time_option($delivery_time_settings),
				'required' => $delivery_time_mandatory,
				'custom_attributes' => [
					'data-default_time' => $auto_select_first_time,
					'data-order_limit_notice' => $order_limit_notice
				],
			], WC()->checkout->get_value('coderockz_woo_delivery_time_field'));
			echo '</div>';
		}
		
		// End Delivery Time

		// Pickup Date --------------------------------------------------------------

		$enable_pickup_date = (isset($pickup_date_settings['enable_pickup_date']) && !empty($pickup_date_settings['enable_pickup_date'])) ? $pickup_date_settings['enable_pickup_date'] : false;

		if( $enable_pickup_date  && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {

			$auto_select_first_pickup_date = (isset($pickup_date_settings['auto_select_first_pickup_date']) && !empty($pickup_date_settings['auto_select_first_pickup_date'])) ? $pickup_date_settings['auto_select_first_pickup_date'] : false;			

			$pickup_days = isset($pickup_date_settings['pickup_days']) && $pickup_date_settings['pickup_days'] != "" ? $pickup_date_settings['pickup_days'] : "6,0,1,2,3,4,5";			

			$pickup_date_mandatory = (isset($pickup_date_settings['pickup_date_mandatory']) && !empty($pickup_date_settings['pickup_date_mandatory'])) ? $pickup_date_settings['pickup_date_mandatory'] : false;
			$pickup_date_format = (isset($pickup_date_settings['date_format']) && !empty($pickup_date_settings['date_format'])) ? $pickup_date_settings['date_format'] : "F j, Y";	

			$pickup_week_starts_from = (isset($pickup_date_settings['week_starts_from']) && !empty($pickup_date_settings['week_starts_from'])) ? $pickup_date_settings['week_starts_from']:"0";
			
			$pickup_selectable_date = (isset($pickup_date_settings['selectable_date']) && !empty($pickup_date_settings['selectable_date']))?$pickup_date_settings['selectable_date']:"365";

			$pickup_days = explode(',', $pickup_days);

			$week_days = ['0', '1', '2', '3', '4', '5', '6'];
			$pickup_disable_week_days = array_values(array_diff($week_days, $pickup_days));


			$pickup_disable_dates = array_unique($pickup_disable_dates, false);
			$pickup_disable_dates = array_values($pickup_disable_dates);

			$pickup_date_field_heading = (isset($pickup_date_settings['pickup_field_label']) && !empty($pickup_date_settings['pickup_field_label'])) ? stripslashes($pickup_date_settings['pickup_field_label']) : "Pickup Date";

			echo '<div id="coderockz_woo_delivery_pickup_date_section" style="display:none;">';

			woocommerce_form_field('coderockz_woo_delivery_pickup_date_field',
			[
				'type' => 'text',
				'class' => array(
				  'coderockz_woo_delivery_pickup_date_field form-row-wide'
				) ,
				'id' => "coderockz_woo_delivery_pickup_date_datepicker",
				'label' => __($pickup_date_field_heading, 'coderockz-woo-delivery'),
				'placeholder' => __($pickup_date_field_heading, 'coderockz-woo-delivery'),
				'required' => $pickup_date_mandatory, 
				'custom_attributes' => [
					'data-pickup_selectable_dates' => $pickup_selectable_date,
					'data-pickup_disable_week_days' => json_encode($pickup_disable_week_days),
					'data-pickup_date_format' => $pickup_date_format,
					'data-pickup_disable_dates' => json_encode($pickup_disable_dates),
					'data-pickup_week_starts_from' => $pickup_week_starts_from,
					'data-pickup_default_date' => $auto_select_first_pickup_date,
				],
			] , WC()->checkout->get_value('coderockz_woo_delivery_pickup_date_field'));
			echo '</div>';

		}

		// End Pickup Date


		// Pickup Time --------------------------------------------------------------
		
		$enable_pickup_time = (isset($pickup_time_settings['enable_pickup_time']) && !empty($pickup_time_settings['enable_pickup_time'])) ? $pickup_time_settings['enable_pickup_time'] : false;
		$pickup_time_field_label = (isset($pickup_time_settings['field_label']) && !empty($pickup_time_settings['field_label'])) ? stripslashes($pickup_time_settings['field_label']) : "Pickup Time";

		$pickup_time_mandatory = (isset($pickup_time_settings['pickup_time_mandatory']) && !empty($pickup_time_settings['pickup_time_mandatory'])) ? $pickup_time_settings['pickup_time_mandatory'] : false;
		$pickup_auto_select_first_time = (isset($pickup_time_settings['auto_select_first_time']) && !empty($pickup_time_settings['auto_select_first_time'])) ? $pickup_time_settings['auto_select_first_time'] : false;

		$pickup_limit_notice = (isset(get_option('coderockz_woo_delivery_localization_settings')['pickup_limit_notice']) && !empty(get_option('coderockz_woo_delivery_localization_settings')['pickup_limit_notice'])) ? "(".stripslashes(get_option('coderockz_woo_delivery_localization_settings')['pickup_limit_notice']).")" : "(Maximum Pickup Limit Exceed)";

		if($enable_pickup_time && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {


			echo '<div id="coderockz_woo_delivery_pickup_time_section" style="display:none;">';

			woocommerce_form_field('coderockz_woo_delivery_pickup_time_field',
			[
				'type' => 'select',
				'class' => [
					'coderockz_woo_delivery_pickup_time_field form-row-wide'
				],
				'label' => __($pickup_time_field_label, 'coderockz-woo-delivery'),
				'placeholder' => __($pickup_time_field_label, 'coderockz-woo-delivery'),
				'options' => Coderockz_Woo_Delivery_Pickup_Option::pickup_time_option($pickup_time_settings),
				'required' => $pickup_time_mandatory,
				'custom_attributes' => [
					'data-default_time' => $pickup_auto_select_first_time,
					'data-pickup_limit_notice' => $pickup_limit_notice,
				],
			], WC()->checkout->get_value('coderockz_woo_delivery_pickup_time_field'));
			echo '</div>';

		}
		// End Pickup Time

		echo "</div>";
	}

	/**
	 * Checkout Process
	*/	
	public function coderockz_woo_delivery_customise_checkout_field_process() {
		
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);
		$today = date('Y-m-d', time());


		$delivery_option_settings = get_option('coderockz_woo_delivery_option_delivery_settings');
		$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');
		$pickup_date_settings = get_option('coderockz_woo_delivery_pickup_date_settings');
		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');
		$enable_delivery_option = (isset($delivery_option_settings['enable_option_time_pickup']) && !empty($delivery_option_settings['enable_option_time_pickup'])) ? $delivery_option_settings['enable_option_time_pickup'] : false;

		$enable_delivery_date = (isset($delivery_date_settings['enable_delivery_date']) && !empty($delivery_date_settings['enable_delivery_date'])) ? $delivery_date_settings['enable_delivery_date'] : false;
		$delivery_date_mandatory = (isset($delivery_date_settings['delivery_date_mandatory']) && !empty($delivery_date_settings['delivery_date_mandatory'])) ? $delivery_date_settings['delivery_date_mandatory'] : false;

		$enable_pickup_date = (isset($pickup_date_settings['enable_pickup_date']) && !empty($pickup_date_settings['enable_pickup_date'])) ? $pickup_date_settings['enable_pickup_date'] : false;
		$pickup_date_mandatory = (isset($pickup_date_settings['pickup_date_mandatory']) && !empty($pickup_date_settings['pickup_date_mandatory'])) ? $pickup_date_settings['pickup_date_mandatory'] : false;


		$enable_delivery_time = (isset($delivery_time_settings['enable_delivery_time']) && !empty($delivery_time_settings['enable_delivery_time'])) ? $delivery_time_settings['enable_delivery_time'] : false;
		$delivery_time_mandatory = (isset($delivery_time_settings['delivery_time_mandatory']) && !empty($delivery_time_settings['delivery_time_mandatory'])) ? $delivery_time_settings['delivery_time_mandatory'] : false;


		$enable_pickup_time = (isset($pickup_time_settings['enable_pickup_time']) && !empty($pickup_time_settings['enable_pickup_time'])) ? $pickup_time_settings['enable_pickup_time'] : false;
		$pickup_time_mandatory = (isset($pickup_time_settings['pickup_time_mandatory']) && !empty($pickup_time_settings['pickup_time_mandatory'])) ? $pickup_time_settings['pickup_time_mandatory'] : false;

		$disable_fields_for_downloadable_products = (isset(get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products']) && !empty(get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products'])) ? get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products'] : false;

		$checkout_notice = get_option('coderockz_woo_delivery_localization_settings');
		$checkout_delivery_option_notice = (isset($checkout_notice['checkout_delivery_option_notice']) && !empty($checkout_notice['checkout_delivery_option_notice'])) ? stripslashes($checkout_notice['checkout_delivery_option_notice']) : "Please Select Your Order Type.";
		$checkout_date_notice = (isset($checkout_notice['checkout_date_notice']) && !empty($checkout_notice['checkout_date_notice'])) ? stripslashes($checkout_notice['checkout_date_notice']) : "Please Enter Delivery Date.";
		$checkout_pickup_date_notice = (isset($checkout_notice['checkout_pickup_date_notice']) && !empty($checkout_notice['checkout_pickup_date_notice'])) ? stripslashes($checkout_notice['checkout_pickup_date_notice']) : "Please Enter Pickup Date.";
		$checkout_time_notice = (isset($checkout_notice['checkout_time_notice']) && !empty($checkout_notice['checkout_time_notice'])) ? stripslashes($checkout_notice['checkout_time_notice']) : "Please Enter Delivery Time.";	
		$checkout_pickup_time_notice = (isset($checkout_notice['checkout_pickup_time_notice']) && !empty($checkout_notice['checkout_pickup_time_notice'])) ? stripslashes($checkout_notice['checkout_pickup_time_notice']) : "Please Enter Pickup Time.";	

		

		$has_virtual_downloadable_products = $this->helper->check_virtual_downloadable_products();

		if(isset($_COOKIE['coderockz_woo_delivery_option_time_pickup'])) {
		  $delivery_option_session = $_COOKIE['coderockz_woo_delivery_option_time_pickup'];
		} elseif(!is_null(WC()->session)) {
		  $delivery_option_session = WC()->session->get( 'coderockz_woo_delivery_option_time_pickup' );
		}

		if ($enable_delivery_option && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {
			if (!isset($_POST['coderockz_woo_delivery_delivery_selection_box'])) wc_add_notice(__($checkout_delivery_option_notice) , 'error');
		}

		// if the field is set, if not then show an error message.

		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "delivery") && $enable_delivery_date && $delivery_date_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) && isset($_POST['coderockz_woo_delivery_date_field'])) {
			if ($_POST['coderockz_woo_delivery_date_field'] == "") wc_add_notice(__($checkout_date_notice) , 'error');
		} elseif (!$enable_delivery_option && $enable_delivery_date && $delivery_date_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) && isset($_POST['coderockz_woo_delivery_date_field'])) {
			if ($_POST['coderockz_woo_delivery_date_field'] == "") wc_add_notice(__($checkout_date_notice) , 'error');
		}


		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "pickup") && $enable_pickup_date && $pickup_date_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) && isset($_POST['coderockz_woo_delivery_pickup_date_field'])) {
			if ($_POST['coderockz_woo_delivery_pickup_date_field'] == "") wc_add_notice(__($checkout_pickup_date_notice) , 'error');
		} elseif (!$enable_delivery_option && $enable_pickup_date && $pickup_date_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) && isset($_POST['coderockz_woo_delivery_pickup_date_field'])) {
			if ($_POST['coderockz_woo_delivery_pickup_date_field'] == "") wc_add_notice(__($checkout_pickup_date_notice) , 'error');
		}

		// if the field is set, if not then show an error message.
		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "delivery") && $enable_delivery_time && $delivery_time_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {

			if (!$_POST['coderockz_woo_delivery_time_field']) wc_add_notice(__($checkout_time_notice) , 'error');


			if(($enable_delivery_date && $_POST['coderockz_woo_delivery_date_field'] && !empty($_POST['coderockz_woo_delivery_date_field'])) && ($enable_delivery_time && $_POST['coderockz_woo_delivery_time_field'] && $_POST['coderockz_woo_delivery_time_field'] != "")) {
				$this->check_delivery_quantity_before_placed($_POST['coderockz_woo_delivery_date_field'],$_POST['coderockz_woo_delivery_time_field']);
			} elseif((!$enable_delivery_date) && ($enable_delivery_time && $_POST['coderockz_woo_delivery_time_field'] && $_POST['coderockz_woo_delivery_time_field'] != "")) {

				$this->check_delivery_quantity_before_placed($today,$_POST['coderockz_woo_delivery_time_field'],true);

			}
			
		} elseif (!$enable_delivery_option && $enable_delivery_time && $delivery_time_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {
			if (!$_POST['coderockz_woo_delivery_time_field']) wc_add_notice(__($checkout_time_notice) , 'error');
			if(($enable_delivery_date && $_POST['coderockz_woo_delivery_date_field'] && !empty($_POST['coderockz_woo_delivery_date_field'])) && ($enable_delivery_time && $_POST['coderockz_woo_delivery_time_field'] && !empty($_POST['coderockz_woo_delivery_time_field']) )) {
				$this->check_delivery_quantity_before_placed($_POST['coderockz_woo_delivery_date_field'],$_POST['coderockz_woo_delivery_time_field']);
			} elseif((!$enable_delivery_date) && ($enable_delivery_time && $_POST['coderockz_woo_delivery_time_field'] && !empty($_POST['coderockz_woo_delivery_time_field']) )) {

				$this->check_delivery_quantity_before_placed($today,$_POST['coderockz_woo_delivery_time_field'],true);

			}
		}
		
		// if the field is set, if not then show an error message.
		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "pickup") && $enable_pickup_time && $pickup_time_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {
			if (!$_POST['coderockz_woo_delivery_pickup_time_field']) wc_add_notice(__($checkout_pickup_time_notice) , 'error');

			if(($enable_pickup_date && $_POST['coderockz_woo_delivery_pickup_date_field'] && !empty($_POST['coderockz_woo_delivery_pickup_date_field'])) && ($enable_pickup_time && $_POST['coderockz_woo_delivery_pickup_time_field'] && !empty($_POST['coderockz_woo_delivery_pickup_time_field']))) {
				$this->check_pickup_quantity_before_placed($_POST['coderockz_woo_delivery_pickup_date_field'],$_POST['coderockz_woo_delivery_pickup_time_field']);
			} elseif((!$enable_pickup_date) && ($enable_pickup_time && $_POST['coderockz_woo_delivery_pickup_time_field'] && !empty($_POST['coderockz_woo_delivery_pickup_time_field']))) {

				$this->check_pickup_quantity_before_placed($today,$_POST['coderockz_woo_delivery_pickup_time_field'],true);

			}



		} elseif(!$enable_delivery_option && $enable_pickup_time && $pickup_time_mandatory && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products)) {
			if (!$_POST['coderockz_woo_delivery_pickup_time_field']) wc_add_notice(__($checkout_pickup_time_notice) , 'error');
			if(($enable_pickup_date && $_POST['coderockz_woo_delivery_pickup_date_field'] && !empty($_POST['coderockz_woo_delivery_pickup_date_field'])) && ($enable_pickup_time && $_POST['coderockz_woo_delivery_pickup_time_field'] && !empty($_POST['coderockz_woo_delivery_pickup_time_field']))) {
				$this->check_pickup_quantity_before_placed($_POST['coderockz_woo_delivery_pickup_date_field'],$_POST['coderockz_woo_delivery_pickup_time_field']);
			} elseif((!$enable_pickup_date) && ($enable_pickup_time && $_POST['coderockz_woo_delivery_pickup_time_field'] && !empty($_POST['coderockz_woo_delivery_pickup_time_field']))) {

				$this->check_pickup_quantity_before_placed($today,$_POST['coderockz_woo_delivery_pickup_time_field'],true);

			}
		}
		
	}

	public function check_delivery_quantity_before_placed($delivery_date,$delivery_time,$no_delivery_date = false) {
		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);
		$delivery_time = sanitize_text_field($delivery_time);
	    if($no_delivery_date) {
			$order_date = date("Y-m-d", (int)sanitize_text_field(strtotime($delivery_date)));
			$selected_date = $order_date; 
			$args = array(
		        'limit' => -1,
		        'date_created' => $order_date,
		        'delivery_time' => $delivery_time,
		        'delivery_type' => "delivery",
		        'return' => 'ids'
		    );



		} else {
			$selected_date = date("Y-m-d", strtotime(sanitize_text_field($delivery_date)));
			$args = array(
		        'limit' => -1,
		        'delivery_date' => date("Y-m-d", strtotime(sanitize_text_field($delivery_date))),
		        'delivery_time' => $delivery_time,
		        'delivery_type' => "delivery",
		        'return' => 'ids'
		    );		    
		}

	    $order_ids = wc_get_orders( $args );

	    if($delivery_time != "") {
        	$delivery_times = explode(' - ', $delivery_time);
			$slot_key_one = explode(':', $delivery_times[0]);
			$slot_key_two = explode(':', $delivery_times[1]);
			$delivery_time = ((int)$slot_key_one[0]*60+(int)$slot_key_one[1]).' - '.((int)$slot_key_two[0]*60+(int)$slot_key_two[1]);
			$delivery_times = explode(" - ",$delivery_time);
			$delivery_time_last_time = ((int)$slot_key_two[0]*60+(int)$slot_key_two[1]);   		
		}

		$today = date('Y-m-d', time());
		$current_time = (date("G")*60)+date("i");

		if($today == $selected_date && $current_time > $delivery_time_last_time) wc_add_notice(__('Selected delivery time already passed. Please Reload The Page') , 'error');


	    $time_settings = get_option('coderockz_woo_delivery_time_settings');
  		$x = (int)$time_settings['delivery_time_starts'];
  		$each_time_slot = (isset($time_settings['each_time_slot']) && !empty($time_settings['each_time_slot'])) ? (int)$time_settings['each_time_slot'] : (int)$time_settings['delivery_time_ends']-(int)$time_settings['delivery_time_starts'];
  		$max_order = (isset($time_settings['max_order_per_slot']) && $time_settings['max_order_per_slot'] != "") ? $time_settings['max_order_per_slot'] : 10000000000000;

		while((int)$time_settings['delivery_time_ends']>$x) {
			$second_time = $x+$each_time_slot;
			if($second_time > (int)$time_settings['delivery_time_ends']) {
				$second_time = (int)$time_settings['delivery_time_ends'];
			}
			$key = $x . ' - ' . $second_time; 
			if(!empty($delivery_time) && ($delivery_time == $key) ) {	
				$time_max_order = (int)$max_order;
				if (count($order_ids)>=$time_max_order) {
					wc_add_notice(__('Maximum Order Limit Exceed For This Time Slot. Please Reload The Page') , 'error');
				}

				break; 
		    }
			$x = $second_time;
		}

	}


	public function check_pickup_quantity_before_placed($pickup_date,$pickup_time,$no_pickup_date = false) {
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);
		$pickup_time = sanitize_text_field($pickup_time);
	    if($no_pickup_date) {
			$order_date = date("Y-m-d", (int)sanitize_text_field(strtotime($pickup_date)));
			$selected_date = $order_date;
			$args = array(
		        'limit' => -1,
		        'date_created' => $order_date,
		        'pickup_time' => $pickup_time,
		        'delivery_type' => "pickup",
		        'return' => 'ids'
		    );

		} else {
			$selected_date = date("Y-m-d", strtotime(sanitize_text_field($pickup_date)));
			$args = array(
		        'limit' => -1,
		        'pickup_date' => date("Y-m-d", strtotime(sanitize_text_field($pickup_date))),
		        'pickup_time' => $pickup_time,
		        'delivery_type' => "pickup",
		        'return' => 'ids'
		    );		    
		}

	    $order_ids = wc_get_orders( $args );

	    if($pickup_time != "") {
        	if(strpos($pickup_time, ' - ') !== false) {
        		$pickup_times = explode(' - ', $pickup_time);
				$slot_key_one = explode(':', $pickup_times[0]);
				$slot_key_two = explode(':', $pickup_times[1]);
				$pickup_time = ((int)$slot_key_one[0]*60+(int)$slot_key_one[1]).' - '.((int)$slot_key_two[0]*60+(int)$slot_key_two[1]);
				$pickup_times = explode(" - ",$pickup_time);
				$pickup_time_last_time = ((int)$slot_key_two[0]*60+(int)$slot_key_two[1]);
        	} else {
        		$pickup_times = [];
        		$slot_key_one = explode(':', $pickup_time);
        		$pickup_time = ((int)$slot_key_one[0]*60+(int)$slot_key_one[1]);
        		$pickup_times[] = $pickup_time;
        	}
    		
		}

		$today = date('Y-m-d', time());
		$current_time = (date("G")*60)+date("i");

		if($today == $selected_date && $current_time > $pickup_time_last_time) wc_add_notice(__('Selected pickup time already passed. Please Reload The Page') , 'error');


	    $pickup_settings = get_option('coderockz_woo_delivery_pickup_settings');
  		$x = (int)$pickup_settings['pickup_time_starts'];
  		$each_time_slot = (isset($pickup_settings['each_time_slot']) && !empty($pickup_settings['each_time_slot'])) ? (int)$pickup_settings['each_time_slot'] : (int)$pickup_settings['pickup_time_ends']-(int)$pickup_settings['pickup_time_starts'];
  		$max_order = (isset($pickup_settings['max_pickup_per_slot']) && $pickup_settings['max_pickup_per_slot'] != "") ? $pickup_settings['max_pickup_per_slot'] : 10000000000000;

		while((int)$pickup_settings['pickup_time_ends']>$x) {
			$second_time = $x+$each_time_slot;
			if($second_time > (int)$pickup_settings['pickup_time_ends']) {
				$second_time = (int)$pickup_settings['pickup_time_ends'];
			}
			$key = $x . ' - ' . $second_time; 
			if(!empty($pickup_time) && ($pickup_time == $key) ) {	
				$pickup_max_order = (int)$max_order;
				if (count($order_ids)>=$pickup_max_order) {
					wc_add_notice(__('Maximum Order Limit Exceed For This Pickup Slot. Please Reload The Page') , 'error');
				}

				break; 
		    }
			$x = $second_time;
		}

	}

	/**
	 * Update value of field
	*/
	public function coderockz_woo_delivery_customise_checkout_field_update_order_meta($order_id) {
		
		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);

		if(isset($_POST['coderockz_woo_delivery_date_field'])) {
			$en_delivery_date = sanitize_text_field($_POST['coderockz_woo_delivery_date_field']);
		}
		
		if(isset($_POST['coderockz_woo_delivery_pickup_date_field'])) {
			$en_pickup_date = sanitize_text_field($_POST['coderockz_woo_delivery_pickup_date_field']);
		}
		
		$delivery_option_settings = get_option('coderockz_woo_delivery_option_delivery_settings');
		$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');
		$pickup_date_settings = get_option('coderockz_woo_delivery_pickup_date_settings');
		$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');
		$enable_delivery_option = (isset($delivery_option_settings['enable_option_time_pickup']) && !empty($delivery_option_settings['enable_option_time_pickup'])) ? $delivery_option_settings['enable_option_time_pickup'] : false;

		$enable_delivery_date = (isset($delivery_date_settings['enable_delivery_date']) && !empty($delivery_date_settings['enable_delivery_date'])) ? $delivery_date_settings['enable_delivery_date'] : false;

		$enable_pickup_date = (isset($pickup_date_settings['enable_pickup_date']) && !empty($pickup_date_settings['enable_pickup_date'])) ? $pickup_date_settings['enable_pickup_date'] : false;

		$enable_delivery_time = (isset($delivery_time_settings['enable_delivery_time']) && !empty($delivery_time_settings['enable_delivery_time'])) ? $delivery_time_settings['enable_delivery_time'] : false;
	  	
		$enable_pickup_time = (isset($pickup_time_settings['enable_pickup_time']) && !empty($pickup_time_settings['enable_pickup_time'])) ? $pickup_time_settings['enable_pickup_time'] : false;

		$disable_fields_for_downloadable_products = (isset(get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products']) && !empty(get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products'])) ? get_option('coderockz_woo_delivery_other_settings')['disable_fields_for_downloadable_products'] : false;

		$has_virtual_downloadable_products = $this->helper->check_virtual_downloadable_products();

	  	
		if ($enable_delivery_option && $_POST['coderockz_woo_delivery_delivery_selection_box'] != "" ) {
			update_post_meta($order_id, 'delivery_type', $_POST['coderockz_woo_delivery_delivery_selection_box']);
		} elseif(!$enable_delivery_option && (($enable_delivery_time && !$enable_pickup_time) || ($enable_delivery_date && !$enable_pickup_date)) && $_POST['coderockz_woo_delivery_time_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'delivery_type', 'delivery');
		} elseif(!$enable_delivery_option && ((!$enable_delivery_time && $enable_pickup_time) || (!$enable_delivery_date && $enable_pickup_date)) && $_POST['coderockz_woo_delivery_pickup_time_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'delivery_type', 'pickup');
		}


		if(isset($_COOKIE['coderockz_woo_delivery_option_time_pickup'])) {
		  $delivery_option_session = $_COOKIE['coderockz_woo_delivery_option_time_pickup'];
		} elseif(!is_null(WC()->session)) {
		  $delivery_option_session = WC()->session->get( 'coderockz_woo_delivery_option_time_pickup' );
		}

	  	if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "delivery") && $enable_delivery_date && $_POST['coderockz_woo_delivery_date_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'delivery_date', date("Y-m-d", strtotime($en_delivery_date)));
		} elseif (!$enable_delivery_option && $enable_delivery_date && $_POST['coderockz_woo_delivery_date_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'delivery_date', date("Y-m-d", strtotime($en_delivery_date)));
		}

		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "pickup") && $enable_pickup_date && $_POST['coderockz_woo_delivery_pickup_date_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'pickup_date', date("Y-m-d", strtotime($en_pickup_date)));
		} elseif (!$enable_delivery_option && $enable_pickup_date && $_POST['coderockz_woo_delivery_pickup_date_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'pickup_date', date("Y-m-d", strtotime($en_pickup_date)));
		}


		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "delivery") && $enable_delivery_time && $_POST['coderockz_woo_delivery_time_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'delivery_time', sanitize_text_field($_POST['coderockz_woo_delivery_time_field']));
		} elseif (!$enable_delivery_option && $enable_delivery_time && $_POST['coderockz_woo_delivery_time_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'delivery_time', sanitize_text_field($_POST['coderockz_woo_delivery_time_field']));
		}

		if(($enable_delivery_option && isset($delivery_option_session) && $delivery_option_session == "pickup") && $enable_pickup_time && $_POST['coderockz_woo_delivery_pickup_time_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'pickup_time', sanitize_text_field($_POST['coderockz_woo_delivery_pickup_time_field']));
		} elseif(!$enable_delivery_option && $enable_pickup_time && $_POST['coderockz_woo_delivery_pickup_time_field'] != "" && (!$has_virtual_downloadable_products || $disable_fields_for_downloadable_products) ) {
			update_post_meta($order_id, 'pickup_time', sanitize_text_field($_POST['coderockz_woo_delivery_pickup_time_field']));
		}

	}

	public function coderockz_woo_delivery_option_delivery_time_pickup() {
		check_ajax_referer('coderockz_woo_delivery_nonce');

		$delivery_option = (isset($_POST['deliveryOption']) && $_POST['deliveryOption'] !="") ? sanitize_text_field($_POST['deliveryOption']) : "";
		setcookie('coderockz_woo_delivery_option_time_pickup', $delivery_option, time() + 60 * 60, '/');
		WC()->session->set( 'coderockz_woo_delivery_option_time_pickup', $delivery_option );


		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);


		$disable_delivery_date_passed_time = [];
		$disable_pickup_date_passed_time = [];

		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');

		$enable_delivery_time = (isset($delivery_time_settings['enable_delivery_time']) && !empty($delivery_time_settings['enable_delivery_time'])) ? $delivery_time_settings['enable_delivery_time'] : false;
	  	
		$enable_pickup_time = (isset($pickup_time_settings['enable_pickup_time']) && !empty($pickup_time_settings['enable_pickup_time'])) ? $pickup_time_settings['enable_pickup_time'] : false;
		
		
		if($enable_delivery_time) {
			$time_slot_end = [0];
			$time_settings = get_option('coderockz_woo_delivery_time_settings');
			$time_slot_end[] = (int)$time_settings['delivery_time_ends'];												
			$highest_timeslot_end = max($time_slot_end);

			$current_time = (date("G")*60)+date("i");

			if($current_time>$highest_timeslot_end) {
				$disable_delivery_date_passed_time[] = date('Y-m-d', time());
			}

		}

		if($enable_pickup_time) {

			$pickup_slot_end = [0];

		    $pickup_settings = get_option('coderockz_woo_delivery_pickup_settings');
			$pickup_slot_end[] = (int)$pickup_settings['pickup_time_ends'];

			$highest_pickupslot_end = max($pickup_slot_end);

			$current_time = (date("G")*60)+date("i");
			if($current_time>$highest_pickupslot_end) {
				$disable_pickup_date_passed_time[] = date('Y-m-d', time());
			}
		}

		$response=[
			"disable_delivery_date_passed_time" => $disable_delivery_date_passed_time,
			"disable_pickup_date_passed_time" => $disable_pickup_date_passed_time,
		];
		$response = json_encode($response);
		wp_send_json_success($response);
	}

	//Without this function of filter "woocommerce_order_data_store_cpt_get_orders_query" query with post_meta "delivery_date" is not possible
	public function coderockz_woo_delivery_handle_custom_query_var( $query, $query_vars ) {
		if ( ! empty( $query_vars['delivery_date'] ) ) {
			$query['meta_query'][] = array(
				'key' => 'delivery_date',
				'value' => esc_attr( $query_vars['delivery_date'] ),
			);
		}

		if ( ! empty( $query_vars['pickup_date'] ) ) {
			$query['meta_query'][] = array(
				'key' => 'pickup_date',
				'value' => esc_attr( $query_vars['pickup_date'] ),
			);
		}

		if ( ! empty( $query_vars['delivery_type'] ) ) {
			$query['meta_query'][] = array(
				'key' => 'delivery_type',
				'value' => esc_attr( $query_vars['delivery_type'] ),
			);
		}

		if ( ! empty( $query_vars['delivery_time'] ) ) {
			$query['meta_query'][] = array(
				'key' => 'delivery_time',
				'value' => esc_attr( $query_vars['delivery_time'] ),
			);
		}

		if ( ! empty( $query_vars['pickup_time'] ) ) {
			$query['meta_query'][] = array(
				'key' => 'pickup_time',
				'value' => esc_attr( $query_vars['pickup_time'] ),
			);
		}

		return $query;
	}

	public function coderockz_woo_delivery_get_orders() {

		check_ajax_referer('coderockz_woo_delivery_nonce');
		
		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		// if any timezone data is saved, set default timezone with the data
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);

		$max_order_per_slot = (isset($delivery_time_settings['max_order_per_slot']) && !empty($delivery_time_settings['max_order_per_slot'])) ? $delivery_time_settings['max_order_per_slot'] : 0;
		
		$disabled_current_time_slot = (isset($delivery_time_settings['disabled_current_time_slot']) && !empty($delivery_time_settings['disabled_current_time_slot'])) ? $delivery_time_settings['disabled_current_time_slot'] : false;

		if(isset($_POST['onlyDeliveryTime']) && $_POST['onlyDeliveryTime']) {
			$order_date = date("Y-m-d", sanitize_text_field(strtotime($_POST['date']))); 
			$args = array(
		        'limit' => -1,
		        'date_created' => $order_date,
		        'delivery_type' => 'delivery',
		        'return' => 'ids'
		    );

		} else {
			$args = array(
		        'limit' => -1,
		        'delivery_date' => date("Y-m-d", strtotime(sanitize_text_field($_POST['date']))),
		        'return' => 'ids'
		    );
		}

	    $order_ids = wc_get_orders( $args );

		$delivery_times = [];

		foreach ($order_ids as $order) {
			$date = get_post_meta($order,"delivery_date",true);
			$time = get_post_meta($order,"delivery_time",true);

			if((isset($date) && isset($time)) || isset($time)) {
				$delivery_times[] = $time;
			}
		}

		$current_time = (date("G")*60)+date("i");

		$response = [
			"delivery_times" => $delivery_times,
			"max_order_per_slot" => $max_order_per_slot,
			'disabled_current_time_slot' => $disabled_current_time_slot,
			"current_time" => $current_time,
		];
		$response = json_encode($response);
		wp_send_json_success($response);
	}


	public function coderockz_woo_delivery_get_orders_pickup() {

		check_ajax_referer('coderockz_woo_delivery_nonce');
		
		$delivery_pickup_settings = get_option('coderockz_woo_delivery_pickup_settings');
		// if any timezone data is saved, set default timezone with the data
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);

		$pickup_max_order_per_slot = (isset($delivery_pickup_settings['max_pickup_per_slot']) && !empty($delivery_pickup_settings['max_pickup_per_slot'])) ? $delivery_pickup_settings['max_pickup_per_slot'] : 0;

		
		$pickup_disabled_current_time_slot = (isset($delivery_pickup_settings['disabled_current_pickup_time_slot']) && !empty($delivery_pickup_settings['disabled_current_pickup_time_slot'])) ? $delivery_pickup_settings['disabled_current_pickup_time_slot'] : false;

		
		if(isset($_POST['onlyPickupTime']) && $_POST['onlyPickupTime']) {
			$order_date = date("Y-m-d", strtotime(sanitize_text_field($_POST['date']))); 
			$args = array(
		        'limit' => -1,
		        'date_created' => $order_date,
		        'delivery_type' => 'pickup',
		        'return' => 'ids'
		    );

		} else {
			$pickup_date = date("Y-m-d", strtotime(sanitize_text_field($_POST['date'])));
			$args = array(
		        'limit' => -1,
		        'pickup_date' => $pickup_date,
		        'return' => 'ids'
		    );		    
		}

		$order_ids = wc_get_orders( $args );

		$pickup_delivery_times = [];

	  	foreach ($order_ids as $order) {
			$date = get_post_meta($order,"pickup_date",true);
			$time = get_post_meta($order,"pickup_time",true);
			
			if((isset($date) && isset($time)) || isset($time)) {
				$pickup_delivery_times[] = $time;
			}

		}

		$current_time = (date("G")*60)+date("i");

		$response = [
			"pickup_delivery_times" => $pickup_delivery_times,
			"pickup_max_order_per_slot" => $pickup_max_order_per_slot,
			'pickup_disabled_current_time_slot' => $pickup_disabled_current_time_slot,
			"current_time" => $current_time,
		];
		$response = json_encode($response);
		wp_send_json_success($response);

	}


	public function coderockz_woo_delivery_disable_max_delivery_pickup_date() {
		check_ajax_referer('coderockz_woo_delivery_nonce');
		// if any timezone data is saved, set default timezone with the data
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);

		$disable_delivery_date_passed_time = [];
		$disable_pickup_date_passed_time = [];

		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');

		$enable_delivery_time = (isset($delivery_time_settings['enable_delivery_time']) && !empty($delivery_time_settings['enable_delivery_time'])) ? $delivery_time_settings['enable_delivery_time'] : false;
	  	
		$enable_pickup_time = (isset($pickup_time_settings['enable_pickup_time']) && !empty($pickup_time_settings['enable_pickup_time'])) ? $pickup_time_settings['enable_pickup_time'] : false;
		
		
		if($enable_delivery_time) {

			$time_slot_end = [0];

			$time_settings = get_option('coderockz_woo_delivery_time_settings');
			$time_slot_end[] = (int)$time_settings['delivery_time_ends'];												
			$highest_timeslot_end = max($time_slot_end);
			$current_time = (date("G")*60)+date("i");

			if($current_time>$highest_timeslot_end) {
				$disable_delivery_date_passed_time[] = date('Y-m-d', time());
			}
		}

		if($enable_pickup_time) {

			$pickup_slot_end = [0];

	    	$pickup_settings = get_option('coderockz_woo_delivery_pickup_settings');
			$pickup_slot_end[] = (int)$pickup_settings['pickup_time_ends'];

			$highest_pickupslot_end = max($pickup_slot_end);

			$current_time = (date("G")*60)+date("i");

			if($current_time>$highest_pickupslot_end) {
				$disable_pickup_date_passed_time[] = date('Y-m-d', time());
			}

		}

		$response=[
			"disable_delivery_date_passed_time" => $disable_delivery_date_passed_time,
			"disable_pickup_date_passed_time" => $disable_pickup_date_passed_time,
		];
		$response = json_encode($response);
		wp_send_json_success($response);
		
	}

	
	public function coderockz_woo_delivery_add_account_orders_column( $columns ) {
		if(class_exists('CodeRockz_Woocommerce_Delivery_Date_Time')) {
			$columns  = array_splice($columns, 0, 3, true) +
				['order_delivery_details' => "Delivery Details"] +
				array_splice($columns, 1, count($columns) - 1, true);
		}
		
	    return $columns;
	}

	public function coderockz_woo_delivery_show_delivery_details_my_account_tab($order) {
		if(class_exists('CodeRockz_Woocommerce_Delivery_Date_Time')) {

			$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');			
			$pickup_date_settings = get_option('coderockz_woo_delivery_pickup_date_settings');			
			$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
			$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');
			$delivery_pickup_settings = get_option('coderockz_woo_delivery_pickup_location_settings');
			$additional_field_settings = get_option('coderockz_woo_delivery_additional_field_settings');

			$delivery_date_field_label = (isset($delivery_date_settings['field_label']) && !empty($delivery_date_settings['field_label'])) ? stripslashes($delivery_date_settings['field_label']) : "Delivery Date";
			$pickup_date_field_label = (isset($pickup_date_settings['pickup_field_label']) && !empty($pickup_date_settings['pickup_field_label'])) ? stripslashes($pickup_date_settings['pickup_field_label']) : "Pickup Date";
			$delivery_time_field_label = (isset($delivery_time_settings['field_label']) && !empty($delivery_time_settings['field_label'])) ? stripslashes($delivery_time_settings['field_label']) : "Delivery Time";
			$pickup_time_field_label = (isset($pickup_time_settings['field_label']) && !empty($pickup_time_settings['field_label'])) ? stripslashes($pickup_time_settings['field_label']) : "Pickup Time";


			// if any timezone data is saved, set default timezone with the data
			$timezone = $this->helper->get_the_timezone();
			date_default_timezone_set($timezone);

			$delivery_date_format = (isset($delivery_date_settings['date_format']) && !empty($delivery_date_settings['date_format'])) ? $delivery_date_settings['date_format'] : "F j, Y";


			$pickup_date_format = (isset($pickup_date_settings['date_format']) && !empty($pickup_date_settings['date_format'])) ? $pickup_date_settings['date_format'] : "F j, Y";

			
			
			$my_account_column = "";
			if(metadata_exists('post', $order->get_id(), 'delivery_date') && get_post_meta($order->get_id(), 'delivery_date', true) !="") {

				$delivery_date = date($delivery_date_format, strtotime(get_post_meta( $order->get_id(), 'delivery_date', true )));

				$my_account_column .= $delivery_date_field_label.": " . $delivery_date;
				$my_account_column .= "<br>";
			}

			if(metadata_exists('post', $order->get_id(), 'delivery_time') && get_post_meta($order->get_id(), 'delivery_time', true) !="") {

				$time_format = (isset($delivery_time_settings['time_format']) && !empty($delivery_time_settings['time_format']))?$delivery_time_settings['time_format']:"12";
				if($time_format == 12) {
					$time_format = "h:i A";
				} elseif ($time_format == 24) {
					$time_format = "H:i";
				}

				$minutes = get_post_meta($order->get_id(),"delivery_time",true);

				$minutes = explode(' - ', $minutes);

	    		$time_value = date($time_format, strtotime($minutes[0])) . ' - ' . date($time_format, strtotime($minutes[1]));


				$my_account_column .= $delivery_time_field_label.": " . $time_value;
				$my_account_column .= "<br>";
			}

			if(metadata_exists('post', $order->get_id(), 'pickup_date') && get_post_meta($order->get_id(), 'pickup_date', true) !="") {
				$pickup_date = date($pickup_date_format, strtotime(get_post_meta( $order->get_id(), 'pickup_date', true )));
				$my_account_column .= $pickup_date_field_label.": " . $pickup_date;
				$my_account_column .= "<br>";
			}

			if(metadata_exists('post', $order->get_id(), 'pickup_time') && get_post_meta($order->get_id(), 'pickup_time', true) !="") {
				$pickup_minutes = get_post_meta($order->get_id(),"pickup_time",true);
				$pickup_time_format = (isset($pickup_time_settings['time_format']) && !empty($pickup_time_settings['time_format']))?$pickup_time_settings['time_format']:"12";
				if($pickup_time_format == 12) {
					$pickup_time_format = "h:i A";
				} elseif ($pickup_time_format == 24) {
					$pickup_time_format = "H:i";
				}
				$pickup_minutes = explode(' - ', $pickup_minutes);

	    		$pickup_time_value = date($pickup_time_format, strtotime($pickup_minutes[0])) . ' - ' . date($pickup_time_format, strtotime($pickup_minutes[1]));


				$my_account_column .= $pickup_time_field_label.": " . $pickup_time_value;
				$my_account_column .= "<br>";

			}

			echo $my_account_column;
		}
	}

	public function coderockz_woo_delivery_add_delivery_information_row( $total_rows, $order ) {
 
		$delivery_date_settings = get_option('coderockz_woo_delivery_date_settings');			
		$pickup_date_settings = get_option('coderockz_woo_delivery_pickup_date_settings');			
		$delivery_time_settings = get_option('coderockz_woo_delivery_time_settings');
		$pickup_time_settings = get_option('coderockz_woo_delivery_pickup_settings');

		$delivery_date_field_label = (isset($delivery_date_settings['field_label']) && !empty($delivery_date_settings['field_label'])) ? stripslashes($delivery_date_settings['field_label']) : "Delivery Date";
		$pickup_date_field_label = (isset($pickup_date_settings['pickup_field_label']) && !empty($pickup_date_settings['pickup_field_label'])) ? stripslashes($pickup_date_settings['pickup_field_label']) : "Pickup Date";
		$delivery_time_field_label = (isset($delivery_time_settings['field_label']) && !empty($delivery_time_settings['field_label'])) ? stripslashes($delivery_time_settings['field_label']) : "Delivery Time";
		$pickup_time_field_label = (isset($pickup_time_settings['field_label']) && !empty($pickup_time_settings['field_label'])) ? stripslashes($pickup_time_settings['field_label']) : "Pickup Time";


		// if any timezone data is saved, set default timezone with the data
		$timezone = $this->helper->get_the_timezone();
		date_default_timezone_set($timezone);

		$delivery_date_format = (isset($delivery_date_settings['date_format']) && !empty($delivery_date_settings['date_format'])) ? $delivery_date_settings['date_format'] : "F j, Y";

		$pickup_date_format = (isset($pickup_date_settings['date_format']) && !empty($pickup_date_settings['date_format'])) ? $pickup_date_settings['date_format'] : "F j, Y";


		$time_format = (isset($delivery_time_settings['time_format']) && !empty($delivery_time_settings['time_format']))?$delivery_time_settings['time_format']:"12";
		if($time_format == 12) {
			$time_format = "h:i A";
		} elseif ($time_format == 24) {
			$time_format = "H:i";
		}

		$pickup_time_format = (isset($pickup_time_settings['time_format']) && !empty($pickup_time_settings['time_format']))?$pickup_time_settings['time_format']:"12";
		if($pickup_time_format == 12) {
			$pickup_time_format = "h:i A";
		} elseif ($pickup_time_format == 24) {
			$pickup_time_format = "H:i";
		}

		if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
	        $order_id = $order->get_id();
	    } else {
	        $order_id = $order->id;
	    }

	    $delivery_option_settings = get_option('coderockz_woo_delivery_option_delivery_settings');
	    $enable_delivery_option = (isset($delivery_option_settings['enable_option_time_pickup']) && !empty($delivery_option_settings['enable_option_time_pickup'])) ? $delivery_option_settings['enable_option_time_pickup'] : false;
	    $order_type_field_label = (isset($delivery_option_settings['delivery_option_label']) && !empty($delivery_option_settings['delivery_option_label'])) ? stripslashes($delivery_option_settings['delivery_option_label']) : "Order Type";
	    $delivery_field_label = (isset($delivery_option_settings['delivery_label']) && !empty($delivery_option_settings['delivery_label'])) ? stripslashes($delivery_option_settings['delivery_label']) : "Delivery";
		$pickup_field_label = (isset($delivery_option_settings['pickup_label']) && !empty($delivery_option_settings['pickup_label'])) ? stripslashes($delivery_option_settings['pickup_label']) : "Pickup";
	    if($enable_delivery_option && metadata_exists('post', $order_id, 'delivery_type') && get_post_meta($order_id, 'delivery_type', true) !="") {

			if(get_post_meta($order_id, 'delivery_type', true) == "delivery") {
				$total_rows['delivery_type'] = array(
				   'label' => $order_type_field_label,
				   'value'   => $delivery_field_label
				);
			} elseif(get_post_meta($order_id, 'delivery_type', true) == "pickup") {
				$total_rows['delivery_type'] = array(
				   'label' => $order_type_field_label,
				   'value'   => $pickup_field_label
				);
			}

	    }
	    
	    if(metadata_exists('post', $order_id, 'delivery_date') && get_post_meta( $order_id, 'delivery_date', true ) != "") {

	    	$delivery_date = date($delivery_date_format, strtotime(get_post_meta( $order_id, 'delivery_date', true )));

	    	$total_rows['delivery_date'] = array(
			   'label' => $delivery_date_field_label,
			   'value'   => $delivery_date
			);
	    }
		
	    if(metadata_exists('post', $order_id, 'delivery_time') && get_post_meta($order_id,"delivery_time",true) != "") {

			$minutes = get_post_meta($order_id,"delivery_time",true);
			$minutes = explode(' - ', $minutes);

    		$time_value = date($time_format, strtotime($minutes[0])) . ' - ' . date($time_format, strtotime($minutes[1]));

			$total_rows['delivery_time'] = array(
			   'label' => $delivery_time_field_label,
			   'value'   => $time_value
			);
		}

		if(metadata_exists('post', $order_id, 'pickup_date') && get_post_meta( $order_id, 'pickup_date', true ) != "") {

			$pickup_date = date($pickup_date_format, strtotime(get_post_meta( $order_id, 'pickup_date', true )));

	    	$total_rows['pickup_date'] = array(
			   'label' => $pickup_date_field_label,
			   'value'   => $pickup_date
			);
	    }

		if(metadata_exists('post', $order_id, 'pickup_time') && get_post_meta($order_id,"pickup_time",true) != "") {
			$pickup_minutes = get_post_meta($order_id,"pickup_time",true);
			$pickup_minutes = explode(' - ', $pickup_minutes);

    		$pickup_time_value = date($pickup_time_format, strtotime($pickup_minutes[0])) . ' - ' . date($pickup_time_format, strtotime($pickup_minutes[1]));

			$total_rows['pickup_time'] = array(
			   'label' => $pickup_time_field_label,
			   'value'   => $pickup_time_value
			);
		}
		 
		return $total_rows;
	}

	public function delete_woo_delivery_plugin_session_cookie() {
		//unset the plugin session & cookie first
		if(is_cart()){
			if(isset($_COOKIE['coderockz_woo_delivery_available_shipping_methods'])) {
			    unset($_COOKIE["coderockz_woo_delivery_available_shipping_methods"]);
				setcookie("coderockz_woo_delivery_available_shipping_methods", null, -1, '/');
			} 

			if(!is_null(WC()->session)) {		  
				WC()->session->__unset( 'coderockz_woo_delivery_available_shipping_methods' );  
			}
		}

	}

}
