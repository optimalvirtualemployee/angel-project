<?php
/* 
Author - Vipin Singh
Title - User library 
Version - 1.1
Date -  04-12-2018
*/
class UserDAO extends Database {

    private $tblUser = 'sn_users';   
    private $tblUserToken = 'sn_users_token';
    //private $tblUserMeta = 'hl_usermeta';
    //private $tblOtp = 'hl_user_otp';
    //private $tblWooWallet = 'hl_woo_wallet_transactions';
    //private $tblState = 'hl_states_cities'; 
    //private $tblFirebase = 'hl_firebase_token';


    function updateToken($user_id, $token, $device_id) {
      $sql = "INSERT INTO $this->tblUserToken (user_id, token, device_id) VALUES(:user_id, :token, :device_id) ";
      $sql .= "ON DUPLICATE KEY UPDATE token = :token";
      $this->query($sql);
      $this->bind(':user_id', $user_id);
      $this->bind(':token', $token);
      $this->bind(':device_id', $device_id);
      $this->execute();
      return $this->lastInsertId();
    }

    function updateTokenFirebase($device_id, $firebase_toke) {
      $sql = "INSERT INTO $this->tblFirebase (device_id, firebase_toke) VALUES( :device_id, :firebase_toke) ";
      $sql .= "ON DUPLICATE KEY UPDATE firebase_toke = :firebase_toke";
      $this->query($sql);
      $this->bind(':device_id', $device_id);
      $this->bind(':firebase_toke', $firebase_toke);
      $this->execute();
      return $this->lastInsertId();
    }

    function getLoginByCreds($user_login) {
      $sql = "SELECT * FROM $this->tblUser WHERE user_login=:user_login "; 
      $this->query($sql);
      $this->bind(':user_login', $user_login);
      $result = $this->single();
      return $result; 
    }

    function getOtpInsertTbl($user_mobile, $user_otp) {
      $sql = "INSERT INTO $this->tblOtp (user_mobile, user_otp) VALUES(:user_mobile, :user_otp) ";
      $this->query($sql);
      $this->bind(':user_mobile', $user_mobile);
      $this->bind(':user_otp', $user_otp);
      $this->execute();
      return $this->lastInsertId();
    }

    function getOtp($user_mobile, $user_otp) {
      $sql = "SELECT * FROM $this->tblOtp WHERE user_mobile=:user_mobile AND user_otp = :user_otp";
      $this->query($sql);
      $this->bind(':user_mobile', $user_mobile);
      $this->bind(':user_otp', $user_otp);
      $result = $this->single();
      return $result;
    }
    
    function deleteOtpByUserMobile($user_mobile) {
      $sql = "DELETE FROM $this->tblOtp WHERE user_mobile=:user_mobile";
      $this->query($sql);
      $this->bind(':user_mobile', $user_mobile);
      $result = $this->execute();
      return $result; 
    }

    function getUserBalance($user_id) {
      $sql = "SELECT ID,user_login,user_email,display_name,transaction_id,type,balance FROM $this->tblUser INNER JOIN $this->tblWooWallet ON $this->tblWooWallet.user_id = $this->tblUser.ID WHERE $this->tblUser.ID = '".$user_id."' ORDER BY $this->tblWooWallet.date DESC LIMIT 0,1";
      $this->query($sql);
      $this->bind(':ID', $user_id);
      $result = $this->single();
      return $result; 
    }

    /*function insertUserBalance($user_id,$points) {
      $sql = "INSERT INTO $this->tblWooWallet (blog_id,user_id,type,amount,balance,currency,deleted) VALUES('1','".$user_id."','credit','".$points."','".$points."','INR','0')";
      $this->query($sql);
      $result = $this->execute();
      return $result; 
    }*/
    function insertUserBalance($user_id,$amount,$points,$details) {
      $sql = "INSERT INTO $this->tblWooWallet (blog_id,user_id,type,amount,balance,currency,details,deleted) VALUES('1','".$user_id."','credit','".$amount."','".$points."','INR','".$details."','0')";
      $this->query($sql);
      $result = $this->execute();
      return $result; 
    }

    function registerMobile($user_login,$user_email,$display_name) {
      $sql = "INSERT INTO $this->tblUser (user_login,user_email,display_name) VALUES('".$user_login."','".$user_email."','".$display_name."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }
    
    function userRegisterMetaTbl($req,$lastId) {
      foreach ($req as $key => $val) {        
        $sql = "INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$lastId."','".$key."','".$val."') ";
        $this->query($sql);
        $result = $this->execute();
      }
      return $result;
    }

    function userinfoforuploadinvoice($user_id){
      $sql = "SELECT ID,user_login,display_name FROM $this->tblUser WHERE ID = '".(int)$user_id."' LIMIT 0,1";
      $this->query($sql);
      $this->bind(':ID', $user_id);
      $result = $this->single();
      return $result;
    }

    function loggedUserUploadInvoice($uid,$filename,$fileval) {        
      $sql = "INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$lastId."','".$filename."','".$fileval."') ";
      $this->query($sql);
      $result = $this->execute();
      return $result;
    }

    function getallInvoice($uid, $type){      
      $sql = "SELECT * FROM $this->tblWooWallet WHERE user_id = '".$uid."' ";      
      if(!empty($type)){
        $sql .= " AND type = '".$type."' ";
      }
      $this->query($sql);
      $result = $this->resultset();
      return $result;
    }
    
    function uploadPic($uid,$profileImg){
      $sql = "UPDATE $this->tblUserMeta SET user_id = '".$uid."', meta_key = 'profileImg', meta_value = '".$profileImg."' WHERE user_id = '".$uid."' ";
      echo $sql;
      $this->query($sql);
      $result = $this->execute();
      return $result;
    }

    function registerInvoiceUpload($req,$lastId) {
      foreach ($req as $key => $val) {        
        $sql = "INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$lastId."','".$key."','".$val."') ";
        $this->query($sql);        
      }
      return $this->execute();
    }

    function allState() {
      $sql = "SELECT city_id, city_name, state FROM $this->tblState ";
      $this->query($sql);
      $result = $this->resultset();
      return $result;
    }

    function userRegisterMetaTblForProfile($req,$user_id) {      
        $i=0;
          foreach($req['childData'] as $vkey => $vVal){
            if(count($vVal)>0){
              foreach($vVal as $nkey => $nval){                 
               $this->query("INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$user_id."','".$nkey.'_'.$i."','".$nval."') ");  
               $result=$this->execute();   
            }
          }
          $i++;
        }
     
      foreach ($req as $key => $val) {         
       //if(!isset($req['childData'])){
        if($key != 'childData'){
         $this->query("INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$user_id."','".$key."','".$val."') ");
         $result=$this->execute();
       }
      } 
      return $result;
    }

    function userDeleteUserMetaTblByID($req,$user_id) {
      $i=0;
        foreach($req['childData'] as $vkey => $vVal){
          if(count($vVal)>0){
              foreach($vVal as $nkey => $nval){ 
              $offset = $nkey.'_'.$i;                
               $sql = "DELETE FROM $this->tblUserMeta WHERE user_id='".$user_id."' AND meta_key='".$offset."' ";
                $this->query($sql);
                $result = $this->execute(); 
          }
        }
        $i++;
      }
      foreach ($req as $key => $val) {  
      //if(!isset($req['childData'])){
        if($key != 'childData'){      
          $sql = "DELETE FROM $this->tblUserMeta WHERE user_id='".$user_id."' AND meta_key LIKE '%Child%' ";
          $this->query($sql);
          $result = $this->execute();
        }
      }
      return $result;
    }

    /*function userRegisterMetaTblForProfile($req,$user_id) {
      foreach ($req as $key => $val) {        
        $sql = "INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$user_id."','".$key."','".$val."') ";
        $this->query($sql);
        $result = $this->execute();
      }
      return $result;
    }

    function userDeleteUserMetaTblByID($req,$user_id) {
      foreach ($req as $key => $val) {        
        $sql = "DELETE FROM $this->tblUserMeta WHERE user_id='".$user_id."' AND meta_key='".$key."' ";
        $this->query($sql);
        $result = $this->execute();
      }
      return $result;
    }*/

    function uploadImg($uid,$data){
      foreach ($data as $key => $val) { 
        $sql = "INSERT INTO $this->tblUserMeta (user_id,meta_key,meta_value) VALUES ('".$uid."', '".$key."', '".$val."')";
        $this->query($sql);
        $result = $this->execute();
      }
      return $result;
    }

    function uploadImgDelete($user_id) {       
      $sql = "DELETE FROM $this->tblUserMeta WHERE user_id='".$user_id."' AND meta_key='profileImg' ";
      $this->query($sql);
      $result = $this->execute();
      return $result;
    }

function getWalletBalance($user_id){
      $sql = "SELECT sum(amount) as tot FROM $this->tblUser INNER JOIN $this->tblWooWallet ON $this->tblWooWallet.user_id = $this->tblUser.ID WHERE $this->tblUser.ID = '".$user_id."' AND type='credit' ";
      $this->query($sql);
      $this->bind(':ID', $user_id);
      $result = $this->single();
      
      $credit = $result['tot'];

      $sql = "SELECT sum(amount) as tot FROM $this->tblUser INNER JOIN $this->tblWooWallet ON $this->tblWooWallet.user_id = $this->tblUser.ID WHERE $this->tblUser.ID = '".$user_id."' AND type='debit' ";
      $this->query($sql);
      $this->bind(':ID', $user_id);
      $result = $this->single();

      $debit = $result['tot'];

      $obj = new stdClass();
      $obj->credit = $credit;
      $obj->debit = $debit;
      $obj->balance = number_format(($credit - $debit),2);
      return $obj;
    }

    function updateUserBalance($user_id,$amount,$balance,$type,$details) {
      $sql = "INSERT INTO $this->tblWooWallet (blog_id,user_id,type,amount,balance,currency,deleted,details) VALUES('1','".$user_id."','".$type."','".$amount."','".$balance."','INR','0','".$details."')";
      $this->query($sql);
      $result = $this->execute();
      return $result; 
    }

    function getUserMeta($user_id){
      $sql = "SELECT meta_key,meta_value FROM hl_usermeta WHERE user_id = '".$user_id."' ORDER BY user_id ASC ";
      $this->query($sql);
      $result = $this->resultset();
      return $result; 
    }

    public function getDeviceId($user_id){
      $sql = "SELECT user_id,device_id FROM hl_users_token WHERE user_id = '".$user_id."' AND device_id IN (SELECT device_id from hl_firebase_token) ";
      $this->query($sql);
      $result = $this->single();
      return $result;
    }

    public function getfirebaseId($device_id){
      $sql = "SELECT firebase_toke FROM hl_firebase_token WHERE device_id = '".$device_id."' ";
      $this->query($sql);
      $result = $this->single();
      return $result;
    }

    public function insertNotification($user_id,$msg_type,$message){
      $sql = "INSERT INTO hl_notification_msg (user_id,msg_type,message) VALUES('".$user_id."','".$msg_type."','".$message."')";
      $this->query($sql);
      $result = $this->execute();
      return $result;
    }
    
    public function getNotificationById($user_id) {
      $sql = "SELECT * FROM hl_notification_msg WHERE user_id = '".$user_id."' ORDER BY id DESC LIMIT 0, 20";
      $this->query($sql);
      $result = $this->resultset();
      return $result;
    }

    public function getNotificationCountByID($user_id) {
      $sql = "SELECT count(*) as tot FROM hl_notification_msg WHERE user_id = '".$user_id."' AND status= '0' ORDER BY id DESC";
      $this->query($sql);
      $result = $this->single();
      return $result;
    }

  //Update Status Notification By Id
  public function UpdateStatusNotificationbyId($mid){
    $sql = "UPDATE hl_notification_msg SET status = '1' WHERE id = '".$mid."' ";
    $this->query($sql);
    $result = $this->execute();
    return $result;
  }

}

//end of class
