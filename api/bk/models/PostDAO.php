<?php
/* 
Author - Vipin Singh
Title - User library 
Version - 1.1
Date -  04-12-2018
*/
class PostDAO extends Database {

  private $tblUser = 'hl_users';
  private $tblUserMeta = 'hl_usermeta';
  private $tblOtp = 'hl_user_otp';
  private $tblWooWallet = 'hl_woo_wallet_transactions';
  private $tblState = 'hl_states_cities';    
  private $tblUserToken = 'hl_users_token';
  private $tblPosts = 'hl_posts';
  private $tblTerms = 'hl_term_relationships';
  private $tblTaxonomy = 'hl_term_taxonomy';
  private $tblPostMeta = 'hl_postmeta';
  private $tblSubscribe = 'hl_subs_user_list';
  private $tbldadclub = 'hl_dadclubreview';
  private $tblmotherConf = 'hl_motherconfession';
  private $tblGetyourSample = 'hl_get_your_sample';
  private $tblAskDoctor = 'hl_ask_doctor';

  // Get Content Engage Web Service
    function getEngagePostData($post_id){
      $sql = "SELECT $this->tblPosts.* FROM $this->tblPosts LEFT JOIN $this->tblTerms ON ($this->tblPosts.ID = $this->tblTerms.object_id) LEFT JOIN $this->tblTaxonomy ON ($this->tblTerms.term_taxonomy_id = $this->tblTaxonomy.term_taxonomy_id) WHERE $this->tblTaxonomy.term_id IN ($post_id) ";
      $this->query($sql);      
      $result = $this->resultset();
      return $result;     
    }
    // Get all Data For New API for Get All Id
    function getEngagePostAllData($post_id){
      $sql = "SELECT SQL_CALC_FOUND_ROWS $this->tblPosts.ID FROM $this->tblPosts LEFT JOIN $this->tblTerms ON ($this->tblPosts.ID = $this->tblTerms.object_id) WHERE 1=1  AND ( $this->tblTerms.term_taxonomy_id IN ($post_id)) AND $this->tblPosts.post_type = 'educate' AND (($this->tblPosts.post_status = 'publish')) GROUP BY $this->tblPosts.ID ORDER BY $this->tblPosts.post_date ";      
      $this->query($sql);      
      $result = $this->resultset();
      return $result;      
    }

    // get All Educate
    function getEducatePostAllData($post_id){
      $sql = "SELECT SQL_CALC_FOUND_ROWS $this->tblPosts.ID FROM $this->tblPosts LEFT JOIN $this->tblTerms ON ($this->tblPosts.ID = $this->tblTerms.object_id) WHERE 1=1  AND ( $this->tblTerms.term_taxonomy_id IN ($post_id)) AND $this->tblPosts.post_type = 'engage' AND (($this->tblPosts.post_status = 'publish')) GROUP BY $this->tblPosts.ID ORDER BY $this->tblPosts.post_date ";
      $this->query($sql);      
      $result = $this->resultset();
      return $result;      
    }

    // get All Educate
    function getOnlineProPostAllData($post_id){
      $sql = "SELECT SQL_CALC_FOUND_ROWS $this->tblPosts.ID as cid FROM $this->tblPosts LEFT JOIN $this->tblTerms ON ($this->tblPosts.ID = $this->tblTerms.object_id) WHERE 1=1  AND ( $this->tblTerms.term_taxonomy_id IN ($post_id)) AND $this->tblPosts.post_type = 'onlineproduct' AND (($this->tblPosts.post_status = 'publish')) GROUP BY $this->tblPosts.ID ORDER BY $this->tblPosts.post_date ";      
      $this->query($sql);      
      $result = $this->resultset();
      return $result;      
    }

    function getAllOnlinePro(){
      $sql = "SELECT SQL_CALC_FOUND_ROWS $this->tblPosts.ID as cid FROM $this->tblPosts LEFT JOIN $this->tblTerms ON ($this->tblPosts.ID = $this->tblTerms.object_id) WHERE 1=1 AND $this->tblPosts.post_type = 'onlineproduct' AND (($this->tblPosts.post_status = 'publish')) GROUP BY $this->tblPosts.ID ORDER BY $this->tblPosts.post_date";
      $this->query($sql);
      $result = $this->resultset();
      return $result;
    }

    function getPostDetailsByID($post_id){
      $sql = "SELECT * FROM $this->tblPosts WHERE $this->tblPosts.post_status = 'publish' AND  $this->tblPosts.ID IN ($post_id) ";
      $this->query($sql);      
      $result = $this->resultset();
      return $result; 
    }

    function getEngagePostImgURL($post_id){
      $sql = "SELECT guid from $this->tblPosts WHERE ID in ( SELECT meta_value FROM hl_postmeta WHERE meta_key = '_thumbnail_id' AND post_id = '".$post_id."' )";
      $this->query($sql);
      $result = $this->resultset();
      return $result; 
    }
    // Get Price Online Products
    function getPriceforOnlinePro($post_id){
      $sql = "SELECT meta_value FROM hl_postmeta WHERE meta_key = 'product_price' AND post_id = '".$post_id."' ";      
      $this->query($sql);
      $result = $this->resultset();
      return $result; 
    }
    // Get URL Online Products
    function getURLforOnlinePro($post_id){
      $sql = "SELECT meta_value FROM hl_postmeta WHERE meta_key = 'product_links' AND post_id = '".$post_id."' ";      
      $this->query($sql);
      $result = $this->resultset();
      return $result; 
    }

    // Get all Data For New API for Image URL
    function getEngagePostImgAllURL($post_id){
      $sql = "SELECT guid from $this->tblPosts WHERE ID IN ( SELECT meta_value FROM hl_postmeta WHERE meta_key = '_thumbnail_id' AND post_id IN ($post_id) )";
      $this->query($sql);
      $result = $this->resultset();
      return $result; 
    }
    // Get All Educate Data
    function getAllPostById($cid,$url){
      $cAllList = $this->getEngagePostAllData($cid);
      $idArr = array();
      $imArr = array();
      for ($i=0; $i < count($cAllList); $i++) { 
        $idArr[] = $cAllList[$i]['ID'];
      }      
      $contId = implode(',', $idArr); 
      $cList1 = $this->getPostDetailsByID(rtrim($contId,','));

      for ($i=0; $i < count($cList1); $i++) { 
        $img = $this->getEngagePostImgAllURL((int)$cList1[$i]['ID']);
        $cList[$i] = $cList1[$i];
        $cList[$i]['postURL'] = $url.$cList1[$i]['post_name'];
        $cList[$i]['imageURL'] = $img[0]['guid'];
      }
      return $cList;
    }
    // Educate Page Details
    function getAllEducatePostById($cid,$url){
      $cAllList = $this->getEducatePostAllData($cid);      
      $idArr = array();
      $imArr = array();
      for ($i=0; $i < count($cAllList); $i++) { 
        $idArr[] = $cAllList[$i]['ID'];
      }      
      $contId = implode(',', $idArr); 
      $cList1 = $this->getPostDetailsByID(rtrim($contId,','));
      for ($i=0; $i < count($cList1); $i++) { 
        $img = $this->getEngagePostImgAllURL((int)$cList1[$i]['ID']);
        $cList[$i] = $cList1[$i];
        $cList[$i]['postURL'] = $url.$cList1[$i]['post_name'];
        $cList[$i]['imageURL'] = $img[0]['guid'];
      }
      return $cList;
    }

    // Get All Online Products
    function getAllOnlineProductsById($cid,$url){
      $cAllList = $this->getOnlineProPostAllData($cid);
      $idArr = array();
      $imArr = array();
      for ($i=0; $i < count($cAllList); $i++) { 
        $idArr[] = $cAllList[$i]['cid'];
      }      
      $contId = implode(',', $idArr); 
      $cList1 = $this->getPostDetailsByID(rtrim($contId,','));
      for ($i=0; $i < count($cList1); $i++) { 
        $img = $this->getEngagePostImgAllURL((int)$cList1[$i]['ID']);
        $price = $this->getPriceforOnlinePro((int)$cList1[$i]['ID']);
        $proUrl = $this->getURLforOnlinePro((int)$cList1[$i]['ID']);
        $cList[$i] = $cList1[$i];
        $cList[$i]['postURL'] = $url.$cList1[$i]['post_name'];
        $cList[$i]['imageURL'] = $img[0]['guid'];
        $cList[$i]['price'] = $price[0]['meta_value'];
        $cList[$i]['ProURL'] = $proUrl[0]['meta_value'];
      }
      return $cList;
    }

    // Get show All Online Products
    function getShowAllOnlineProductById($url){
      $cAllList = $this->getAllOnlinePro();    
      $idArr = array();
      $imArr = array();
      for ($i=0; $i < count($cAllList); $i++) { 
        $idArr[] = $cAllList[$i]['cid'];
      }      
      $contId = implode(',', $idArr); 
      $cList1 = $this->getPostDetailsByID(rtrim($contId,','));
      for ($i=0; $i < count($cList1); $i++) { 
        $img = $this->getEngagePostImgAllURL((int)$cList1[$i]['ID']);
        $price = $this->getPriceforOnlinePro((int)$cList1[$i]['ID']);
        $proUrl = $this->getURLforOnlinePro((int)$cList1[$i]['ID']);
        $cList[$i] = $cList1[$i];
        $cList[$i]['postURL'] = $url.$cList1[$i]['post_name'];
        $cList[$i]['imageURL'] = $img[0]['guid'];
        $cList[$i]['price'] = $price[0]['meta_value'];
        $cList[$i]['ProURL'] = $proUrl[0]['meta_value'];
      }
      return $cList;
    }

    // Subscribe Now Insert
    function subscribeNowInsert($req,$user_id) {        
      $sql = "INSERT INTO $this->tblSubscribe (pid,uid,uname,uemail,address) VALUES ('".(int)$req['pid']."','".$user_id."','".$req['uname']."','".$req['uemail']."','".$req['address']."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }

    // Get Your Sample Insert
    function getYourSampleInsert($req,$user_id) {        
      $sql = "INSERT INTO $this->tblGetyourSample (pid,uid,uname,uemail,address) VALUES ('".(int)$req['pid']."','".$user_id."','".$req['uname']."','".$req['uemail']."','".$req['address']."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }

    // Dad's Club  Insert
    function dadClubInsert($req) {        
      $sql = "INSERT INTO $this->tbldadclub (dadtitle,dadchildname,dadchildage,dadchildgender,dadmessage,dadanonnonymous) VALUES ('".$req['dadtitle']."','".$req['dadchildname']."','".$req['dadchildage']."','".$req['dadchildgender']."','".$req['dadmessage']."','".$req['dadanonnonymous']."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }
    // Get Data ALL FAQ Engage
    function getAllFaqDetailsEngageById($url){
      $cAllList = $this->getAllFaqPostEnageId();
      $idArr = array();
      $imArr = array();
      for ($i=0; $i < count($cAllList); $i++) { 
        $idArr[] = $cAllList[$i];
      }      
      $contId = implode(',', $idArr);
      $cList1 = $this->getPostDetailsByID(rtrim($contId,','));
      for ($i=0; $i < count($cList1); $i++) { 
        $img = $this->getEngagePostImgAllURL((int)$cList1[$i]['ID']);
        $cList[$i] = $cList1[$i];
        $cList[$i]['postURL'] = $url.$cList1[$i]['post_name'];
        $cList[$i]['imageURL'] = $img[0]['guid'];
      }
      return $cList;
    }
    // Get Frequently Read Article for Engage 
    function getAllFaqPostEnageId(){
      $sql = "SELECT SQL_CALC_FOUND_ROWS $this->tblPosts.ID FROM $this->tblPosts INNER JOIN $this->tblPostMeta ON ( $this->tblPosts.ID = $this->tblPostMeta.post_id ) WHERE 1=1 AND ( ( $this->tblPostMeta.meta_key = 'suggested_article' AND $this->tblPostMeta.meta_value = '1' ) ) AND $this->tblPosts.post_type = 'engage' AND ($this->tblPosts.post_status = 'publish' OR $this->tblPosts.post_status = 'acf-disabled' AND $this->tblPosts.post_status = 'publish') GROUP BY $this->tblPosts.ID ORDER BY $this->tblPostMeta.meta_value+0 DESC LIMIT 0, 6 ";
      $this->query($sql);
      $result = $this->resultset();      
      $resultId = array();
        foreach ($result as $key => $val) {
          array_push($resultId, $val['ID']);
        }
      return $resultId; 
    }

    // Get Frequently Read Article for Educate 
    function getAllFaqPostEducateId(){
      $sql = "SELECT SQL_CALC_FOUND_ROWS  $this->tblPosts.ID FROM $this->tblPosts  INNER JOIN $this->tblPostMeta ON ( $this->tblPosts.ID = $this->tblPostMeta.post_id ) WHERE 1=1  AND ( ( $this->tblPostMeta.meta_key = 'suggested_article' AND $this->tblPostMeta.meta_value = '1' )) AND $this->tblPosts.post_type = 'educate' AND ($this->tblPosts.post_status = 'publish' OR $this->tblPosts.post_status = 'acf-disabled' AND $this->tblPosts.post_status = 'private') GROUP BY $this->tblPosts.ID ORDER BY $this->tblPostMeta.meta_value+0 DESC LIMIT 0, 6 ";
      $this->query($sql);
      $result = $this->resultset();      
      $resultId = array();
        foreach ($result as $key => $val) {
          array_push($resultId, $val['ID']);
        }
      return $resultId; 
    }
    // Get Data ALL FAQ Educate
    function getAllFaqDetailsEducateById($url){
      $cAllList = $this->getAllFaqPostEducateId();
      /*print "<pre>";
      print_r($cAllList);
      die;*/
      $idArr = array();
      $imArr = array();
      for ($i=0; $i < count($cAllList); $i++) { 
        $idArr[] = $cAllList[$i];
      }      
      $contId = implode(',', $idArr);
      $cList1 = $this->getPostDetailsByID(rtrim($contId,','));
      for ($i=0; $i < count($cList1); $i++) { 
        $img = $this->getEngagePostImgAllURL((int)$cList1[$i]['ID']);
        $cList[$i] = $cList1[$i];
        $cList[$i]['postURL'] = $url.$cList1[$i]['post_name'];
        $cList[$i]['imageURL'] = $img[0]['guid'];
      }
      return $cList;
    }

    // Mother's Confession  Insert
    function motherConfessionInsert($req) {        
      $sql = "INSERT INTO $this->tblmotherConf (childTitle,childname,childage,childgender,message,motherremainanonmous) VALUES ('".$req['childTitle']."','".$req['childname']."','".$req['childage']."','".$req['childgender']."','".$req['message']."','".$req['motherremainanonmous']."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }

    function getCombineData($cid,$url){
      $cList = $this->getEngagePostData($cid);
      $cList[0]['postURL'] = $url.$cList[0]['post_name'];
      $imgList = $this->getEngagePostImgURL($cList[0]['ID']);
      $cListdata[0]['actualimageURL'] = $imgList[0]['guid'];
      $actuallink = explode('wp-content', $cListdata[0]['actualimageURL']);
      $cList[0]['imageURL'] = IMG_URL.$actuallink[1];
      return $cList;
    }

    function getSliderCombineData($post_id,$url){
      $cList = $this->getEngagePostData($post_id);
      $cList[0]['postURL'] = $url.$cList[0]['post_name'];
      $cList[1]['postURL'] = $url.$cList[1]['post_name'];
      $cList[2]['postURL'] = $url.$cList[2]['post_name'];
      $imgList1 = $this->getEngagePostImgURL($cList[0]['ID']);
      $imgList2 = $this->getEngagePostImgURL($cList[1]['ID']);
      $imgList3 = $this->getEngagePostImgURL($cList[2]['ID']);
      
      $cList[0]['guid'] = $imgList1[0]['guid'];
      $actuallink1 = explode('wp-content', $cList[0]['guid']);
      $cList[0]['imageURL'] = IMG_URL.$actuallink1[1];

      $cList[1]['guid'] = $imgList2[0]['guid'];
      $actuallink2 = explode('wp-content', $cList[1]['guid']);
      $cList[1]['imageURL'] = IMG_URL.$actuallink2[1];

      $cList[2]['guid'] = $imgList3[0]['guid'];
      $actuallink3 = explode('wp-content', $cList[2]['guid']);
      $cList[2]['imageURL'] = IMG_URL.$actuallink1[1];
      return $cList;
    }

    function getTwoSliderCombineData($post_id,$url){
      $cList = $this->getEngagePostData($post_id);
      $cList[0]['postURL'] = $url.$cList[0]['post_name'];
      $cList[1]['postURL'] = $url.$cList[1]['post_name'];
      $imgList1 = $this->getEngagePostImgURL($cList[0]['ID']);
      $imgList2 = $this->getEngagePostImgURL($cList[1]['ID']);
      
      $cList[0]['guid'] = $imgList1[0]['guid'];
      $actuallink1 = explode('wp-content', $cList[0]['guid']);
      $cList[0]['imageURL'] = IMG_URL.$actuallink1[1];

      $cList[1]['guid'] = $imgList2[0]['guid'];
      $actuallink2 = explode('wp-content', $cList[1]['guid']);
      $cList[1]['imageURL'] = IMG_URL.$actuallink2[1];
      return $cList;
    }

    function getAllOurProduct(){
      $sql = "SELECT post_id,meta_key,meta_value FROM $this->tblPostMeta WHERE post_id = 724 AND meta_value NOT LIKE '%field\_%' ";
      $this->query($sql);
      $result = $this->resultset();
      return $result;
    }

    function getContentPage($post_id){
      $sql = "SELECT post_title,post_content,post_status FROM $this->tblPosts WHERE `ID` = '".$post_id."'";
      $this->query($sql);
      $result = $this->resultset();
      return $result;
    }

    //Insert data Ask A Doctor 
    function askadoctorInsert($req) {        
      $sql = "INSERT INTO $this->tblAskDoctor (title,fname,lname,askemail,askphone,city,street,country,state,zipcode,askage,areaofconcern,message) VALUES ('".$req['title']."','".$req['fname']."','".$req['lname']."','".$req['askemail']."','".$req['askphone']."','".$req['city']."','".$req['street']."','".$req['country']."','".$req['state']."','".$req['zipcode']."','".$req['askage']."','".$req['areaofconcern']."','".$req['message']."') ";
      $this->query($sql);
      $this->execute();
      return $this->lastInsertId();
    }

}

