<?php

require_once "models/UserDAO.php";
/* 
Author - Vipin Singh
Title - woocommerce library 
Version - 1.1
Date -  02-11-2020
*/

function getProducts($request, $response, $args){ 

    
//woocommerce library
require_once( 'lib/woocommerce-api.php' );
require_once('../wp-load.php');

$base_url = get_home_url();
$up_token = "u9Sz6Ystfh?!8UnJrzZ?";

$options = array(
    'debug'           => true,
    'return_as_array' => false,
    'validate_url'    => false,
    'timeout'         => 30,
    'ssl_verify'      => false,
);
try {
    $client = new WC_API_Client( $base_url, WOO_CONSUMER_KEY, WOO_CONSUMER_SECRET, $options );
//woocommerce library

$req = $request->getParams();


switch ($req['action']){
    
    case "product_list":
       
        $result = $client->products->get('',array( 'filter[limit]' => 1000 ));
        $int = 1;
        foreach($result->products as $data ){
           
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                if($int >= $req['i'] && $int <= $req['j']){

                    $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'images'=> $data->images,'brand'=> $brands_data); 
                    
                }
                $int =  $int + 1;
            }
        break;

    case "p_by_cat":
        $cat_slug = $req['cat_slug'];
       
        $result = $client->products->get('',array( 'filter[category]' => $cat_slug, 'filter[limit]' => 1000 ));
        $int = 1;
        foreach($result->products as $data ){
           
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                if($int >= $req['i'] && $int <= $req['j']){

                    $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'images'=> $data->images,'brand'=> $brands_data); 

                }
                $int =  $int + 1;
            }
        break;

    case "p_details":
        $product_details = $client->products->get( $req['p_id'] );
        $final_data = $product_details->product;
        break;

    case "top_selling":
        $result = $client->products->get('',array('filter[limit]' => 1000 ));
        $int = 1;
        foreach($result->products as $data ){
           
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                $top_selling = get_post_meta( $data->id, 'top_selling_product', true );
                if($top_selling == 1){
                    if($int >= $req['i'] && $int <= $req['j']){
                        $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'images'=> $data->images,'brand'=> $brands_data);  
                        }             
                }

                $int =  $int + 1;
            }
        break;

    case "top_rewarded":
        $result = $client->products->get('',array('filter[limit]' => 1000 ));
        $int = 1;
        foreach($result->products as $data ){
           
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                $top_rewarded = get_post_meta( $data->id, 'top_rewarded_product', true );
                if($top_rewarded == 1){
                    if($int >= $req['i'] && $int <= $req['j']){
                        $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'images'=> $data->images,'brand'=> $brands_data);  
                        }             
                }
                $int =  $int + 1;
            }
        break;


    case "recently_viewed":
        $result = $client->products->get('',array('filter[limit]' => 1000 ));
        $int = 1;
        foreach($result->products as $data ){
           
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                $top_selling = get_post_meta( $data->id, 'recently_viewed_product', true );
                if($top_selling == 1){
                    if($int >= $req['i'] && $int <= $req['j']){
                        $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'images'=> $data->images,'brand'=> $brands_data);  
                        }             
                }

                $int =  $int + 1;
            }
        break;


    case "most_viewed":
        $result = $client->products->get('',array('filter[limit]' => 1000 ));
        $int = 1;
        foreach($result->products as $data ){
           
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                $top_selling = get_post_meta( $data->id, 'most_viewed_product', true );
                if($top_selling == 1){
                    if($int >= $req['i'] && $int <= $req['j']){
                        $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'images'=> $data->images,'brand'=> $brands_data);  
                        }             
                }

                $int =  $int + 1;
            }
        break;


    case "filter":
        $brand_array = explode(',',$req['brand']);
        $category_array = explode(',',$req['category']);

        if(count($brand_array) > 0){
            $filter_con .= "in_array($brands_data, $brand_array) ";
        }
        if(count($category_array) > 0){
            $filter_con .= "|| in_array($cats_data_slug, $category_array) ";
        }
        if($req['discount'] == 1){
            $filter_con .= "|| floatval($data->sale_price) > 0";
        }

        $result = $client->products->get('',array( 'filter[limit]' => 1000 ));
        $int = 1;
            foreach($result->products as $data ){
           
                $cats_data = array();
                $cats_data_slug = array();
                $terms_cat = get_the_terms( $data->id, 'product_cat' );

                
                foreach($terms_cat as $terms_cat_data) {
                    $cats_data[] =  $terms_cat_data->term_id;
                    $cats_data_slug[] =  $terms_cat_data->slug;
                }

                
                $brands = get_the_terms($data->id,'pwb-brand');
                
                if(count($brands) > 1){
                    foreach( $brands as $brand ) {
                        $brands_data = $brand->name; 
                    }
                }else{
                        $brands_data = '';
                }
                 echo $filter_con;
        die;
                if($filter_con){

                    if($int >= $req['i'] && $int <= $req['j']){
                        $final_data[] = array('id'=> $data->id,'title'=> $data->title,'description'=> $data->description,'size'=>$data->attributes[0]->options,'short_description'=> $data->short_description,'price'=> floatval($data->price),'regular_price'=> floatval($data->regular_price),'sale_price'=> floatval($data->sale_price),'managing_stock'=>$data->managing_stock,'in_stock'=> $data->in_stock,'stock_quantity'=> $data->stock_quantity,'categories'=> $cats_data,'categories_slug'=> $cats_data_slug,'images'=> $data->images,'brand'=> $brands_data);
                    }
                }
                $int =  $int + 1;
            }
        break;

    default:
        $final_data = "Bad Request";
}
    sendResponse($final_data);

    //catch
    } catch ( WC_API_Client_Exception $e ) {

        echo $e->getMessage() . PHP_EOL;
        echo $e->getCode() . PHP_EOL;

        if ( $e instanceof WC_API_Client_HTTP_Exception ) {

            print_r( $e->get_request() );
            print_r( $e->get_response() );
        }
    }

}

function getOurProducts($request, $response, $args) { 

  //woocommerce library
  require_once( 'lib/woocommerce-api.php' );
  require_once('../wp-load.php');

  $base_url = get_home_url();
  $up_token = "u9Sz6Ystfh?!8UnJrzZ?";

  $options = array(
      'debug'           => true,
      'return_as_array' => false,
      'validate_url'    => false,
      'timeout'         => 30,
      'ssl_verify'      => false,
  );
  try {
     $client = new WC_API_Client( $base_url, WOO_CONSUMER_KEY, WOO_CONSUMER_SECRET, $options );
  //woocommerce library
     pre($client);

  $product_data = get_field('our_products','724');

  sendResponse($product_data);

      //catch
      } catch ( WC_API_Client_Exception $e ) {

          echo $e->getMessage() . PHP_EOL;
          echo $e->getCode() . PHP_EOL;

        if ( $e instanceof WC_API_Client_HTTP_Exception ) {

            print_r( $e->get_request() );
            print_r( $e->get_response() );
        }
    }

}


function getBrands($request, $response, $args){     
  //woocommerce library
  require_once( 'lib/woocommerce-api.php' );
  require_once('../wp-load.php');

  $base_url = get_home_url();

  $options = array(
      'debug'           => true,
      'return_as_array' => false,
      'validate_url'    => false,
      'timeout'         => 30,
      'ssl_verify'      => false,
  );
  try {
    $client = new WC_API_Client( $base_url, WOO_CONSUMER_KEY, WOO_CONSUMER_SECRET, $options );
    //woocommerce library

    $args = array(
      'taxonomy' => 'pwb-brand',
      'orderby' => 'name',
      'order'   => 'ASC'
    );

    $brands_data =  get_categories($args);

      foreach($brands_data as $brands ){           
        $final_data[] = array('id'=> $brands->cat_ID,'name'=> $brands->name,'slug'=> $brands->slug,'count'=>$brands->category_count);  
    }             

    sendResponse($final_data);

    //catch
    } catch ( WC_API_Client_Exception $e ) {

      echo $e->getMessage() . PHP_EOL;
      echo $e->getCode() . PHP_EOL;

      if ( $e instanceof WC_API_Client_HTTP_Exception ) {
        print_r( $e->get_request() );
        print_r( $e->get_response() );
      }
    }

}


  function getCategories($request, $response, $args){     
    //woocommerce library 
    require_once( 'lib/woocommerce-api.php' );
    require_once('../wp-load.php');

    $base_url = get_home_url();

    $options = array(
      'debug'           => true,
      'return_as_array' => false,
      'validate_url'    => false,
      'timeout'         => 30,
      'ssl_verify'      => false,
    );
    
    try {
      $client = new WC_API_Client( $base_url, WOO_CONSUMER_KEY, WOO_CONSUMER_SECRET, $options );
      
      $getCat =  $client->products->get_categories();
      $productCatLists = $getCat->product_categories;
      //pre($productCatLists);
      $finalData = array_shift($productCatLists);
      
      $code       = STATUS_OK;
      $message    = 'Success';
      sendResponse($productCatLists, $code, $message);

      //catch
    } catch ( WC_API_Client_Exception $e ) {
      echo $e->getMessage() . PHP_EOL;
      echo $e->getCode() . PHP_EOL;
      if ( $e instanceof WC_API_Client_HTTP_Exception ) {
        print_r( $e->get_request() );
        print_r( $e->get_response() );
      }
    }
  }

  function getProductByCategory($request, $response, $args){
    $req      = $request->getParams();
    $httpObj  = getWoocommerceObject();
    $catRow   = $httpObj->products->get_categories($args['catid'])->product_category;

    $order    = (isset($req['order']))?$req['order']:'DESC';
    $orderby  = (isset($req['orderby']))?$req['orderby']:'id';
    $page     = (isset($req['page']))?$req['page']:'1';
    $perpage  = (isset($req['perpage']))?$req['perpage']:'10';

    $filters  = array( 
      'filter[category]'    =>  $catRow->name,
      'filter[orderby]'     =>  $orderby,            
      'filter[order]'       =>  $order,
      'filter[limit]'       =>  $perpage,
      'filter[offset]'      =>  $page
    );

    //$httpObj = getWoocommerceObject();
    $arrRow = $httpObj->products->get('',$filters)->products;
    
    $code       = STATUS_OK;
    $message    = 'Success';
    sendResponse($arrRow, $code, $message); 
  }


  function getProductsByID($request, $response, $args){   
    $req = $request->getParams();
    if((int)$args['pid']>0){
      $httpObj = getWoocommerceObject();
      $arrRow = $httpObj->products->get($args['pid'])->product;
    }else{
      $order      = (isset($req['order']))?$req['order']:'DESC';
      $orderby    = (isset($req['orderby']))?$req['orderby']:'id';
      $page       = (isset($req['page']))?$req['page']:'1';
      $perpage    = (isset($req['perpage']))?$req['perpage']:'10';

      $filters    = array( 
        'filter[orderby]'     =>  $orderby,
        'filter[order]'     =>  $order,
        'filter[limit]'     =>  $perpage,
        'filter[offset]'      => $page
      );
 
      $httpObj = getWoocommerceObject();
        $json = array();
    if(isset($req['category_ids'])){
      $expx = explode(",",$req['category_ids']);
      if(count($expx) > 0){
          for($i=0;$i<count($expx);$i++){
            $catRow = $httpObj->products->get_categories((int)$expx[$i])->product_category;
            array_push($json, $catRow->name);
          }
        }
        $filters['filter[category]']  =   implode(",",$json);
      }     
      $arrRow = $httpObj->products->get('',$filters)->products;  
      sendResponse($arrRow);        
    } 
    sendResponse($arrRow); 
  }
  
  function getOfferLists($request, $response, $args){
    $req          = $request->getParams();
    $httpObj      = getWoocommerceObject();
    $couponLists  = array();

    if((int)$req['coupon_id'] > 0) {
      $couponLists['coupons'] = $httpObj->coupons->get((int)$req['coupon_id'])->coupon;
    } else {
      $couponLists['coupons'] = $httpObj->coupons->get()->coupons;
    }

    if (!empty($couponLists)) {
      $code       = STATUS_OK;
      $message    = 'Success';
      sendResponse($couponLists, $code, $message);
    } else {
      $code       = STATUS_OK;
      $message    = 'No record found!';
      sendResponse($couponLists, $code, $message);
    }     
  }
  
  
  function getvendorsbycategory($request, $response, $args){
       
       require_once( 'lib/woocommerce-api.php' );
        require_once('../wp-load.php');
        global $wpdb;
       // pre($wpdb); 
         $req= $request->getParams();
        
         $cat=$req['cat'];
         $lat2=$req['lat'];
         $lng2=$req['lng'];
       
		$dbuser=array();
        
        $query="SELECT sn_users.ID, sn_users.user_nicename 
FROM sn_users INNER JOIN sn_usermeta 
ON sn_users.ID = sn_usermeta.user_id 
WHERE sn_usermeta.meta_key = 'sn_capabilities' 
AND sn_usermeta.meta_value LIKE '%wcfm_vendor%' 
ORDER BY sn_users.user_nicename";

$users = $wpdb->get_results($query);
//print_r($users);
   /* print_r($posts);      
		$args = array(
			'role'    => 'wcfm_vendor',
			'orderby' => 'nicename',
			'order'   => 'ASC'
		);
		$users = get_users( $args );
		
		print_r($users); */ 
   $results=array();
		foreach ( $users as $user ) {
			  
            $id=$user->ID;
			$lat1=get_user_meta($id,'latadr',true);
			$lng1=get_user_meta($id,'lngadr',true);
			
           
			
			if($lat1 != "" && $lng1 != "" && $lat2 != "" && $lng2 != ""){
              
                $theta = $lng1 - $lng2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);
                $distance='';
                $distance= ceil($miles * 1.609344);
                // echo "$lat1 !=  && $lng1 !=  && $lat2 !=  && $lng2 != distance = $distance id= $id <br> <br>";
				if($distance <= 50){
					array_push($dbuser,$id);
					$author = get_user_by('id', $id);
					$displayname=$author->data->display_name;
					$store=get_user_meta($id,'store_name',true);
					$get_author_gravatar = get_avatar_url($id, array('size' => 450));
                   // echo "cat= $cat and id = $id";
                   //  $client = new WC_API_Client( $base_url, WOO_CONSUMER_KEY, WOO_CONSUMER_SECRET, $options );
                    // $result = $client->products->get('',array( 'filter[category]' => $cat_slug, 'filter[limit]' => 1000 ));
        
                    
                    $args = array(  
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        
                        'author__in' => array($id)
                    );
                
                    $loop = new WP_Query( $args );
                   $products=count($loop->posts); 
                   // $products=$loop->post_count;
                    if($products > 0){
                        
                        $addr=get_user_meta($id,'wcfmvm_static_infos',true);
                        $rating=get_user_meta($id,'_wcfmmp_avg_review_rating',true);
                        $address=$addr['address']['city'];
                                            
                        if($addr['address']['state']  != ""){
                            $address .=', '.$addr['address']['state'];
                        }
                        
                        $location=$addr['address']['addr_1'];                                         
                        if($addr['address']['addr_2']  != ""){
                            $location .=', '.$addr['address']['addr_2'];
                        }                 
                        if($addr['address']['city']  != ""){
                            $location .=', '.$addr['address']['city'];
                        }                 
                        if($addr['address']['state']  != ""){
                            $location .=', '.$addr['address']['state'];
                        }              
                        if($addr['address']['country']  != ""){
                            $location .=', '.$addr['address']['country'];
                        }              
                        if($addr['address']['zip']  != ""){
                            $location .=', '.$addr['address']['zip'];
                        }
                        
                        
                        $item=array('name'=>$displayname,'id'=>$id,'image'=>$get_author_gravatar,'location'=> $address,'address' =>$location,'distance' =>$distance,'rating'=>$rating);
                        array_push($results,$item);
                        
                    }
                    
					
					
				}
			
            
                 $code       = STATUS_OK;
                $message    = 'Success';
                $arrRow=$results;
            }
		}
        if(count($users) == 0){
            $code       = STATUS_UNAUTHORIZED;
            $message    = 'No Store Found!';
            $arrRow[]   = '';
        }
        
    sendResponse($arrRow, $code, $message);    
    
  }
  
  
function getproductsbyvendors($request, $response, $args){
    $req = $request->getParams();
    global $wpdb;
    
    if(isset($req['cat']))
    {
        $vendor=$req['cat'];
        $results=array();
        $args = array(  
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,                        
                        'author__in' => array($vendor)
                    );
                
                    $loop = new WP_Query( $args );
                   $products=count($loop->posts); 
                   // $products=$loop->post_count;
                  if ( $loop->have_posts() ) :
                  while ( $loop->have_posts() ) : $loop->the_post(); 
        
                        
                       $id=get_the_ID();
                       $rating=get_post_meta($id,'_wc_average_rating',true);
                       $price=get_post_meta($id,'_regular_price',true);
                       $saleprice=get_post_meta($id,'_sale_price',true);
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full' );                        
                        $item=array('name'=>get_the_title(),'id'=>get_the_ID(),'rating'=>$rating,'image' => $image[0]);
                       $gprice=0;
                        if($saleprice){
                            $item['saleprice']=$saleprice;
                        }
                        else{
                            $item['saleprice']="";
                        }
                        if($price){
                            $gprice=$price; 
                        }
                        $item['price']=$gprice;
                        array_push($results,$item);
                        
                  endwhile;
                  endif;
                 $code       = STATUS_OK;
                $message    = 'Success';
                $arrRow=$results;
    }
    else{
         $code       = STATUS_UNAUTHORIZED;
            $message    = 'No Product Found!';
            $arrRow[]   = '';
    }
    sendResponse($arrRow, $code, $message);  
    
  }
  
  
  function getrecommendedproduct($request, $response, $args){
    
        $req = $request->getParams();
        global $wpdb;
        $results=array();
      
        if(isset($req['cat']))
        {
            $cat=$req['cat'];
            $args = array(
                'post_type'      => 'product',
                'posts_per_page' => 10,
                'author__not_in' => array( $cat ),
                'meta_query'     => array(
                    'relation' => 'OR',
                    array( // Simple products type
                        'key'           => '_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    ),
                    array( // Variable products type
                        'key'           => '_min_variation_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    )
                )
            );
            
            $loop = new WP_Query( $args );
            if ( $loop->have_posts() ) :
                  while ( $loop->have_posts() ) : $loop->the_post();         
                        
                       $id=get_the_ID();
                       $rating=get_post_meta($id,'_wc_average_rating',true);
                       $price=get_post_meta($id,'_regular_price',true);
                       $saleprice=get_post_meta($id,'_sale_price',true);
                         $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full' );                                
                        $item=array('name'=>get_the_title(),'id'=>get_the_ID(),'rating'=>$rating,'image' => $image[0]);
                       $gprice=0;
                        if($saleprice){
                            $item['saleprice']=$saleprice;
                        }
                        if($price){
                            $gprice=$price; 
                        }
                        $item['price']=$gprice;
                        array_push($results,$item);
                        
                    endwhile;
             endif;
                 $code       = STATUS_OK;
                $message    = 'Success';
                $arrRow=$results;                           
              
        }
        else{
         $code       = STATUS_UNAUTHORIZED;
            $message    = 'No Product Found!';
            $arrRow[]   = '';
    }
    sendResponse($arrRow, $code, $message);  
    
  }
  
  
  
  function popularproducts(){
    
    
    global $wpdb;
    $results=array();
         $args = array(
                'post_type'      => 'product',
                'posts_per_page' => 10,
                'meta_key' => '_featured',  
                'meta_value' => 'yes'
            );
            
            $loop = new WP_Query( $args );
            if ( $loop->have_posts() ) :
                  while ( $loop->have_posts() ) : $loop->the_post();         
                        
                       $id=get_the_ID();
                       $rating=get_post_meta($id,'_wc_average_rating',true);
                       $price=get_post_meta($id,'_regular_price',true);
                       $saleprice=get_post_meta($id,'_sale_price',true);
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full' );                                
                        $item=array('name'=>get_the_title(),'id'=>get_the_ID(),'rating'=>$rating,'image' => $image[0]);
                       $gprice=0;
                        if($saleprice){
                            $item['saleprice']=$saleprice;
                        }
                        if($price){
                            $gprice=$price; 
                        }
                        $item['price']=$gprice;
                        array_push($results,$item);
                        
                    endwhile;
             endif;
                 $code       = STATUS_OK;
                $message    = 'Success';
                $arrRow=$results;
    sendResponse($arrRow, $code, $message); 
    
  }
  
  function searchresultss($request, $response, $args){
    
    global $wpdb;
    
    $req = $request->getParams();
    $term=$req['term'];
    
    $results=array();
         $args = array(
                'post_type'      => 'product',
                's' => $term
            );
            
            $loop = new WP_Query( $args );
            if ( $loop->have_posts() ) :
                  while ( $loop->have_posts() ) : $loop->the_post();         
                        
                       $id=get_the_ID();
                       $rating=get_post_meta($id,'_wc_average_rating',true);
                       $price=get_post_meta($id,'_regular_price',true);
                       $saleprice=get_post_meta($id,'_sale_price',true);
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full' );                                
                        $item=array('name'=>get_the_title(),'id'=>get_the_ID(),'rating'=>$rating,'image' => $image[0]);
                       $gprice=0;
                        if($saleprice){
                            $item['saleprice']=$saleprice;
                        }
                        if($price){
                            $gprice=$price; 
                        }
                        $item['price']=$gprice;
                        
                        $terms = get_the_terms ( $id, 'product_cat' );
                        //print_r($terms);
                        $item['category']=array('id'=>$terms[0]->term_id,'slug'=>$terms[0]->slug,'name'=>$terms[0]->name);
                        
                        array_push($results,$item);
                        
                    endwhile;
             endif;
                 $code       = STATUS_OK;
                $message    = 'Success';
                $arrRow=$results;
    sendResponse($arrRow, $code, $message); 
    
  }
  