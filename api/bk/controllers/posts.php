<?php
/* 
Author - Vipin Singh
Title - User library 
Version - 1.1
Date -  04-12-2018
*/
require_once "models/PostDAO.php";

	function getContentEngage($request, $response, $args){
	  $code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  $PostDAO = new PostDAO();
	  $arrRow['mother_confesnion'] = $PostDAO->getAllEducatePostById(61,$url);
	  $arrRow['dad_club'] = $PostDAO->getAllEducatePostById(63,$url);
	  $arrRow['get_your_sample'] = $PostDAO->getAllEducatePostById(31,$url);
	  $arrRow['your_parenting_experience'] = $PostDAO->getAllEducatePostById(62,$url);
	  //$arrRow['contests'] = $PostDAO->getAllContestPostById(33,$url);
	  $arrRow['frequently_read_article'] = $PostDAO->getAllFaqDetailsEngageById($url);

	  sendResponse($arrRow, $code, $message);
	}

	function getAllOnlineProducts($request, $response, $args){
	  $code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  $PostDAO = new PostDAO();

	  $url = ONLINE_PRO;
	  $arrRow['showall'] = $PostDAO->getShowAllOnlineProductById($url);
	  $arrRow['pre_bath'] = $PostDAO->getAllOnlineProductsById(69,$url);
	  $arrRow['post_bath'] = $PostDAO->getAllOnlineProductsById(71,$url);
	  $arrRow['giftpacks'] = $PostDAO->getAllOnlineProductsById(73,$url);
	  $arrRow['diapers'] = $PostDAO->getAllOnlineProductsById(72,$url);
	  $arrRow['bath'] = $PostDAO->getAllOnlineProductsById(70,$url);

	  sendResponse($arrRow, $code, $message);
	}

	// Insert Subscribe Now Post
	function subscribeNow($request, $response, $args){
		$code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  $user_id = $args['user_id'];	  
	  $PostDAO = new PostDAO();
	  $arrRow = $PostDAO->subscribeNowInsert($req,$user_id);
	  sendResponse($arrRow, $code, $message);
	}

	// Insert Ask A Doctor Post
	function askadoctor($request, $response, $args){
	  $code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  //$user_id = $args['user_id'];	  
	  $PostDAO = new PostDAO();
	  $arrRow = $PostDAO->askadoctorInsert($req);
	  sendResponse($arrRow, $code, $message);
	}

	// Insert Get Your Sample Post
	function getyoursample($request, $response, $args){
		$code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  $user_id = $args['user_id'];	  
	  $PostDAO = new PostDAO();
	  $arrRow = $PostDAO->getYourSampleInsert($req,$user_id);
	  sendResponse($arrRow, $code, $message);
	}

	// Insert Data Dad's Club
	function dadclub($request, $response, $args){
		$code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();  
	  $PostDAO = new PostDAO();
	  $arrRow = $PostDAO->dadClubInsert($req);
	  sendResponse($arrRow, $code, $message);
	}

	// Insert data for Mother Confession
	function motherconfession($request, $response, $args){
		$code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();  
	  $PostDAO = new PostDAO();
	  $arrRow = $PostDAO->motherConfessionInsert($req);
	  sendResponse($arrRow, $code, $message);
	}

	function getContentEducate($request, $response, $args){
	  $code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  $PostDAO = new PostDAO();

	  $url = EDUCATE_URL;
	  $arrRow['baby_care'] = $PostDAO->getAllPostById(54,$url);
	  $arrRow['baby_health'] = $PostDAO->getAllPostById(55,$url);
	  $arrRow['breast_feeding'] = $PostDAO->getAllPostById(56,$url);
	  $arrRow['doctor_videos'] = $PostDAO->getAllPostById(29,$url);
	  $arrRow['fitness'] = $PostDAO->getAllPostById(57,$url);
	  $arrRow['magazine_subscription'] = $PostDAO->getAllPostById(28,$url);
	  $arrRow['nutrition'] = $PostDAO->getAllPostById(58,$url);
	  $arrRow['parenting'] = $PostDAO->getAllPostById(59,$url);
	  $arrRow['pregnancy'] = $PostDAO->getAllPostById(60,$url);
	  $arrRow['frequently_read_article'] = $PostDAO->getAllFaqDetailsEducateById($url);
	  //$arrRow['banner'] = $PostDAO->getCombineData(46,$url);
	  //$arrRow['trendingToday'] = $PostDAO->getCombineData(47,$url);
	  //$arrRow['magzineSubscription'] = $PostDAO->getCombineData(48,$url);
	  //$arrRow['doctorVideo'] = $PostDAO->getSliderCombineData(49,$url);

	  sendResponse($arrRow, $code, $message);
	}

	function getContentEmpower($request, $response, $args){
	  $code = STATUS_OK;
	  $message = 'Success';
	  $req = $request->getParams();
	  $PostDAO = new PostDAO();

	  $url = EMPOWER_URL;
	  $arrRow['banner'] = $PostDAO->getCombineData(50,$url);
	  $arrRow['trendingToday'] = $PostDAO->getCombineData(51,$url);
	  $arrRow['productConsultation'] = $PostDAO->getTwoSliderCombineData(52,$url);
	  $arrRow['portieaServices'] = $PostDAO->getTwoSliderCombineData(53,$url);

	  sendResponse($arrRow, $code, $message);
	}

	function gettermcondition($request, $response, $args){
  	$code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $PostDAO = new PostDAO();
    $arrRow = $PostDAO->getContentPage(479);
  	sendResponse($arrRow, $code, $message);
  }

  function getprivacypolicy($request, $response, $args){
  	$code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $PostDAO = new PostDAO();
    $arrRow = $PostDAO->getContentPage(3);
  	sendResponse($arrRow, $code, $message);
  }
	
	

  /*function getourproduct($request, $response, $args){
  	$code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $PostDAO = new PostDAO();

    $allproduct = $PostDAO->getAllOurProduct();
    print "<pre>";
    print_r($allproduct);
    die;

  	sendResponse($arrRow, $code, $message);
  }*/