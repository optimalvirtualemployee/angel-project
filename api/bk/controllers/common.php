<?php
require_once "models/CommonDAO.php";
function pre($data) {
    echo '<pre>';
    print_r($data);
    die;
}

function logError($errorMessage) {
    error_log("\r\n" . date('Y-m-d H:i:s') . ': ' . $errorMessage, 3, BASE_PATH . "errors.log");
}

function getUploadsUrl($path = '') {
    return UPLOADS_URL . $path;
}

function getResponse($data, $code, $message, $metadata) {
    $metadata = empty($metadata) ? null : $metadata;
    $data = empty($data) ? null : $data;

    $response['code'] = $code;
    $response['message'] = $message;
    $response['metadata'] = $metadata;
    $response['data'] = $data;
    return $response;
}

function sendResponse($data = array(), $code = STATUS_OK, $message = "Success", $metadata = array()) {
    echo json_encode(getResponse($data, $code, $message, $metadata), true);
    die;
}

function generateToken($userid, $email, $device_id) {
    $str = time() . $userid . $email . $device_id;
    return md5($str);
}

function openRequest($request, $response, $args, $method) {
    try {
        $method($request, $response, $args);
    } catch (Exception $e) {
        logError($e->getMessage());
        sendResponse(array(), STATUS_INTERNAL_SERVER_ERROR, $e->getMessage());
    }
}

function tokenRequest($request, $response, $args, $method) {
    try {
        $headers = $request->getHeaders();
        //pre($headers);
        $token = trim($headers['HTTP_TOKEN'][0]);
        $commonDAO = new CommonDAO();
        $tokenRow = array();
        if ($token != '') {
            $tokenRow = $commonDAO->getUserByToken($token);
        }

        if (!empty($tokenRow)) {
            $args['user_id'] = $tokenRow['user_id'];
            $method($request, $response, $args);
        } else {
            sendResponse(array(), STATUS_UNAUTHORIZED, 'Invalid token!');
        }
    } catch (Exception $e) {
        logError($e->getMessage());
        sendResponse(array(), STATUS_INTERNAL_SERVER_ERROR, $e->getMessage());
    }
}

  function getWoocommerceObject(){
    require_once( 'lib/woocommerce-api.php' );
    require_once('../wp-load.php');
    $options = array(
      'debug'           => true,
      'return_as_array' => false,
      'validate_url'    => false,
      'timeout'         => 30,
      'ssl_verify'      => false,
    );
    $baseURL = get_home_url();
    try {
      return $client = new WC_API_Client( $baseURL, WOO_CONSUMER_KEY, WOO_CONSUMER_SECRET, $options );        
    } catch ( WC_API_Client_Exception $e ) {
      echo $e->getMessage() . PHP_EOL;
      echo $e->getCode() . PHP_EOL;
      if ( $e instanceof WC_API_Client_HTTP_Exception ) {
        print_r( $e->get_request() );
        print_r( $e->get_response() );
        die;
      }
    }
  }
