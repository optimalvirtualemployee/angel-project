<?php
/* 
Author - Vipin Singh
Title - User library 
Version - 1.1
Date -  04-12-2018
*/
require_once "models/UserDAO.php";
require_once "notificationsend.php";

  function loginuser($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $mobileNo = $req['user_login'];
    $userDAO = new UserDAO();
    $arrRowDatas = $userDAO->getLoginByCreds($mobileNo);
    $umobile = $arrRowDatas['user_login'];

    /*$number = mt_rand(1000,9999);
    
    $curl = curl_init();
    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobileNo."&message=Dear%20Customer%20Your%20OTP%20is%20".$number.".&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3";
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",)
    );
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      $arrRow['success'] = STATUS_OK;
      $arrRow['message'] = 'OTP has been sent your mobile number';
      $arrRowInsert = $userDAO->getOtpInsertTbl($mobileNo, $number);
    }*/
    $arrRow['success'] = STATUS_OK;
    $arrRow['message'] = 'OTP has been sent your mobile number';

    sendResponse($arrRow, $code, $message);
  }

  function otpcheck($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();

    $userDAO = new UserDAO();
    //$arrRowdata = $userDAO->getOtp($req['user_mobile'], $req['user_otp']);
    $arrRowdata = array('user_mobile' => $req['user_mobile'], 'user_otp' => '1234');

    if (!empty($arrRowdata)){  
      $cMobileNo = $arrRowdata['user_mobile'];      
      $arrRowInfo = $userDAO->getLoginByCreds($cMobileNo);      
      
      if (!empty($arrRowInfo)) {        
        $message = 'OTP has matched successfully.';        
        $arrRow = $userDAO->getUserBalance($arrRowInfo['ID']);

        if(empty($arrRow)){
          $arrRow = $arrRowInfo;
          $arrRow['balance'] = 0;
        }
       
        $userDAO->deleteOtpByUserMobile($cMobileNo);
        $arrRow['user_token'] = generateToken($arrRow['ID'], $arrRow['user_email'], $req['device_id']);
        $userDAO->updateToken($arrRow['ID'], $arrRow['user_token'], $req['device_id']);
      } else {
        $arrRow = array('User'=>'New user','status'=>'Not found');
        
        $message = 'New user';
        $userDAO->deleteOtpByUserMobile($cMobileNo);
      }      
      
    } else {
      $arrRow = array('User'=>STATUS_UNAUTHORIZED,'status'=>'OTP does not match.');
      $code = STATUS_UNAUTHORIZED;
      $message = 'OTP does not match.';
    }
    sendResponse($arrRow, $code, $message);
  }

  function getuserinfo($request, $response, $args){
    $code = STATUS_OK;
    $message = 'Success';
    //$req = $request->getParams();  
    $user_id = $args['user_id'];


    $userDAO = new UserDAO();
    $userMetaDetails = $userDAO->getUserMeta($user_id);   
   $json = array();
    foreach ($userMetaDetails as $keys => $vals) { 
        
          $kindex = $vals['meta_key'];
          $vindex = $vals['meta_value']; 
          preg_match_all('~\Child~', $kindex, $match);
          if(count($match[0])>0){
             $newExp = explode("_",$kindex);
             $json['childData'][$newExp[1]][$newExp[0]] = $vindex;
          }else{
            $json[$kindex] = $vindex;
          }         
      }
      ksort($json['childData']);
    $json['wallet'] = $userDAO->getWalletBalance($user_id); 
    $httpObj = getWoocommerceObject();
    $json['avatar'] = $httpObj->customers->get($user_id)->customer->avatar_url;
    sendResponse($json,$code, $message);
  }

  function usersignup($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $mobileNo = $req['user_login'];
    $ocrfile  = $req['ocrfile'];
    $userDAO = new UserDAO();

    $number = mt_rand(100000,999999); 
      
    $pdfdata = array('campaign_id'=>1,'invoice'=>$ocrfile,'customer_transact_id'=>$number,'mobile'=>$mobileNo);
    $pdfurl = "http://api.staging3.bigcityexperiences.co.in/v1/user/mobileregistration";

    $handle = curl_init($pdfurl);
    curl_setopt($handle, CURLOPT_POST, true);
    curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE); // new---
    curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($pdfdata));
    $content = curl_exec($handle);
    curl_close($handle);
    $result = json_decode($content,true);

    $updateMsg = 'Hi%20'.$req["firstname"].'%20We%20have%20received%20your%20invoice%20('.$req["ocrfilename"].').%20Your%20earn%20point%20added%20in%20your%20wallet.';
    $curl = curl_init();
    $urlmg = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=bigcty&to=".$mobileNo."&message=".$updateMsg."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3";
    curl_setopt_array($curl, array(
      CURLOPT_URL => $urlmg,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",)
    );
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    
    //$points = $result['data']['points'];
    $points = 600;

    if ($points > 500) {
      $fname      =   $req['firstname'];
      $email      =   $req['email'];
      $lastId = $userDAO->registerMobile($mobileNo,$email,$fname);

      $data = array();
      $data['firstname']  =   $req['firstname'];
      $data['address']    =   $req['address'];
      $data['area']       =   $req['area'];
      $data['state']      =   $req['state'];
      $data['city']       =   $req['city'];
      $data['pincode']    =   $req['pincode'];
      $data['landmark']   =   $req['landmark'];
      $data['ocrfilename']    =   $req['ocrfilename'];

      $userDAO->userRegisterMetaTbl($data, $lastId);
      $details = "Credited invoice in your account.";
      $amount = $points;
      $userDAO->insertUserBalance($lastId,$amount,$points,$details);
      $arrRow = $userDAO->getUserBalance($lastId);
      $arrRow['user_token'] = generateToken($arrRow['ID'], $arrRow['user_email'], $req['device_id']);
      $userDAO->updateToken($arrRow['ID'], $arrRow['user_token'], $req['device_id']);
    } else {
      $code = STATUS_UNAUTHORIZED;
      $message = "Your point should be greater than 500.";
    }
    
    sendResponse($arrRow, $code, $message);
  }

  function uploadinvoice($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $user_id = $args['user_id'];

    $ocrfile  = $req['ocrfilename'];
    $ocrEncodedFile = $req['ocrfile'];
    $filename = "invoice".time();

    $userDAO = new UserDAO(); 
    $arrRowData = $userDAO->userinfoforuploadinvoice($user_id);

    $mobilenum = $arrRowData['user_login'];
    $uName = $arrRowData['display_name'];
    
    $number = mt_rand(100000,999999);

    $pdfdata = array(
      'campaign_id'=>1,
      'customer_id'=>$user_id,
      'invoice'=>$ocrEncodedFile,
      'customer_transact_id'=>$number,
      'mobile'=>$mobilenum
      );

    $pdfurl = "http://api.staging3.bigcityexperiences.co.in/v1/user/uploadinvoice";
    $handle = curl_init($pdfurl);
    curl_setopt($handle, CURLOPT_POST, true);
    curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE); // new---
    curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($pdfdata));
    $content = curl_exec($handle);
    $err = curl_error($handle);
    if ($err) {
      echo "cURL Error #:" . $err;
    }
    curl_close($handle);    
    $result = json_decode($content,true);// new---
    
    /*$updateMsg = 'Hi'.$uName.'%20We%20have%20received%20your%20invoice%20('.$ocrfile.')%20and%20we%20will%20get%20back%20to%20in%20next%2048%20hrs%20with%20the%20further%20details.';*/
    /*$curl = curl_init();
    $urlmg = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=bigcty&to=".$mobilenum."&message=".$updateMsg."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3";
    curl_setopt_array($curl, array(
      CURLOPT_URL => $urlmg,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",)
    );
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);*/

    $updateMsg = $result['data']['message']. "Your invoice is $ocrfile";

    $deviceid = $userDAO->getDeviceId($user_id);
    $firbaseTokenId = $userDAO->getfirebaseId($deviceid['device_id']);

    $objMessage = new Firebase;

    $message = $updateMsg;

    $firebaseid = $firbaseTokenId['firebase_toke'];

    $resultdata = json_decode($objMessage->send($message,$firebaseid));
    $msg_type = 'Upload Invoice';

    if($resultdata->success){
      $userDAO->insertNotification($deviceid['user_id'],$msg_type,$message);
    }
    
    if ($result) {
      $data = array();
      $data[$filename]  = $req['ocrfilename'];
      $arrRow = $userDAO->registerInvoiceUpload($data, $user_id);
      $code = STATUS_OK;
      $message = $result['data']['message'];
    }
     
    sendResponse($arrRow, $code, $message);
  }

  function getmyinvoice($request, $response, $args){
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $user_id = $args['user_id'];
    
    $userDAO = new UserDAO();
    $arrRow = $userDAO->getallInvoice($user_id, $req['type']);
    sendResponse($arrRow, $code, $message);
  }

  function getmyorder($request, $response, $args){
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    
    $userDAO = new UserDAO();
    $arrRow = $userDAO->getallInvoice($req['ID']);

    sendResponse($arrRow, $code, $message);
  }

  function getstate($request, $response, $args){
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    
    $userDAO = new UserDAO();
    $arrCities = $userDAO->allState();
    $arrRow = array();
    
    foreach($arrCities as $val){
      $row =array();
      if(!empty($arrRow[$val['state']])){
        $row = $arrRow[$val['state']]; 
      }else{
        $row['name'] = $val['state'];
      }      
      $row['cities'][] = array("id"=>$val['city_id'], "name"=>$val['city_name']);
      $arrRow[$val['state']]  = $row; 
    }
    sendResponse($arrRow, $code, $message);
  } 

  function profileupdate($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Profile has been updated';
    $req = $request->getParams();
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();    
    
    $userDAO->userDeleteUserMetaTblByID($req, $user_id);
    $arrRow = $userDAO->userRegisterMetaTblForProfile($req, $user_id);
      
    sendResponse($arrRow, $code, $message);
  }

  function uploadprofileimage($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Profile image has been updated';
    $req = $request->getParams();
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();


    $file_name = $_FILES['profileImg']['name'];
    $uploadfile = UPLOADS_PATH . basename($_FILES['profileImg']['name']);

    if (move_uploaded_file($_FILES['profileImg']['tmp_name'], $uploadfile)) {
      $code = STATUS_OK;
      $message = 'Profile image has been updated';
    } else {
      $code = STATUS_BAD_REQUEST;
      $message = 'Upload error';
    }

    $data = array();
    $data['profileImg'] = UPLOADS_URL . basename($_FILES['profileImg']['name']);

    $userDAO->uploadImgDelete($user_id);
    $arrRow = $userDAO->uploadImg($user_id,$data);

    sendResponse($arrRow, $code, $message);
  }

  function tokenfirebase($request, $response, $args) {
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $userDAO = new UserDAO();
    $arrRow = $userDAO->updateTokenFirebase($req['device_id'], $req['firebase_toke']); 
    sendResponse($arrRow, $code, $message);
  }

  function getnotification($request, $response, $args){
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();
    $arrRow = $userDAO->getNotificationById($user_id);
    sendResponse($arrRow, $code, $message);
  }

  function getnotificationCount($request, $response, $args){
    $code = STATUS_OK;
    $message = 'Success';
    $req = $request->getParams();
    $user_id = $args['user_id'];
    $userDAO = new UserDAO();
    $arrRow = $userDAO->getNotificationCountByID($user_id);
    sendResponse($arrRow, $code, $message);
  }

  function updateStatusNotification($request, $response, $args) {
    $code = STATUS_OK;
    $req = $request->getParams();
    $userDAO = new UserDAO();
    $arrRow = $userDAO->UpdateStatusNotificationbyId($req['id']);
    $message = 'Notification Update';
    sendResponse($arrRow, $code, $message);
  }

